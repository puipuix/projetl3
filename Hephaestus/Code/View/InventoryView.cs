﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Control;
using MGCE.Engine;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hephaestus.Code.Character;
using Hephaestus.Code.View;

namespace Hephaestus.Code.View
{
    class InventoryView
    {
        Stockage stockage;
        int sizeX, sizeY;
        Game game;
        MGGridPane view;
        MGImage pbg;
        MGSpriteSheet bg;
        Boolean hide = true;

        public InventoryView(Game g)
        {

            game = g;
            (sizeX, sizeY) = MGEngine.BufferSize;
            sizeX = sizeX / 2;
            sizeY = sizeY / 2;
            view = new MGGridPane(0, 0);
            view.SetPosition(sizeX - 160, sizeY - 200);

        }

        //affiche la fenetre de selection d'un item pour la fabrication en fonction du stockage en parametre
        public void show(Stockage s, bool setcursor = false)
        {
            view = new MGGridPane(0, 0, 8,8);

            view.SetPosition(sizeX - 156, sizeY - 196);
            if (s.GetAll().Count > 0)
                MGDebug.PrintLine(s.GetAll()[0] + " not empty");
            else
                MGDebug.PrintLine("empty");

            game = MyGame.Game;
            hide = false;
            stockage = s;



            bg = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/bg_inventory"));
            pbg = new MGImage(game, bg);

            pbg.DrawOrder = Const.IHM_DRAW_ORDER;
            bg.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            pbg.Position = new Vector2(sizeX - 160, sizeY - 200);
            pbg.Show();
            int x, y;
            x = y = 0;
            foreach (Couple c in stockage.GetAll())
            {
                if (x == 8)
                {
                    x = 0;
                    y++;
                }

                ItemView iv = new ItemView(c);
                view.add(iv, y, x);

                if (setcursor)
                {
                    iv.ClickManager.OnMouseClickEnd += (o, args) => {
                        Player.player.notifySelection(c.GetItemType()); 
                        Hide(); 
                    };
                }
                x++;
                MGDebug.PrintLine(iv.Position.ToString());

            }




            view.Notify();


        }



        public void Hide()
        {
            MGDebug.PrintLine("hide");
            if (pbg != null)
                pbg.Hide();
            hide = true;
            view.Hide();
        }

        public bool hidden()
        {
            return hide;
        }







    }
}
