﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MGCE.Actor;
using MGCE.Control;
using MGCE.Engine;
using MGCE.Util;
using Hephaestus.Code.Blocks.Tuyau_Objet;
using Hephaestus.Code.Character;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Policy;
using Hephaestus.Code.View;
using Hephaestus.Code.Saves;

namespace Hephaestus.Code.View
{
    static class Hud
    {

        public static Game game;
        public static int sizeX, sizeY;
        private static MGGridPane view, iView;


        private static MGImage ifleche;
        private static MGImage pbg, ibg;
        public static MGImage iselector, inventorySelector;

        public static Couple currentSelectedCouple;

        /// <summary>
        /// cree le hud
        /// </summary>
        /// <param name="g"></param>
        public static void Create(Game g)
        {
            game = g;
            (sizeX, sizeY) = MGEngine.BufferSize;

            MGSpriteSheet bg = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/toolbar"));
            bg.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            pbg = new MGImage(game, bg);
            pbg.DrawOrder = Const.IHM_DRAW_ORDER;
            pbg.ClickManager.OnMouseClickEnd += (o, arg) => {
                if (arg.Button == MGCE.Input.MGInputNames.M_Left && currentSelectedCouple != null)
                {
                    //permet d'ajouter un item a la toolbar 
                    if (Player.player.toolBar.GetAll().Count < 8)
                    {
                        if (Player.player.toolBar.getCountOf(currentSelectedCouple.GetItemType()) == 0)
                        {
                            Player.player.toolBar.Add(currentSelectedCouple.GetItemType(), currentSelectedCouple.GetQuantite());
                        }
                        RefreshToolbar(Player.player.toolBar);
                        currentSelectedCouple = null;
                    }
                }
            };
            view = new MGGridPane(0, 0, 0, 2);

            MGSpriteSheet bgI = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/bg_inventory"));
            bgI.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            ibg = new MGImage(game, bgI);
            ibg.DrawOrder = Const.IHM_DRAW_ORDER;

            iView = new MGGridPane(100, 100, 8, 8);

            MGSpriteSheet selector = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/selected"));
            iselector = new MGImage(game, selector);
            iselector.DrawOrder = Const.IHM_DRAW_ORDER;
            selector.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            inventorySelector = new MGImage(game, selector);
            inventorySelector.DrawOrder = Const.IHM_DRAW_ORDER;

            MGSpriteSheet fleche = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/fleche1"));
            fleche.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            ifleche = new MGImage(game, fleche);
            ifleche.DrawOrder = Const.IHM_DRAW_ORDER;

            Replace();
            MGEngine.OnBufferSizeChange += (o, a) => Replace();
        }
        public static void set(Game g)
        {

        }

        public static void RefreshToolbar(Stockage stockage)
        {
            pbg?.Hide();
            view?.Hide();
            show(stockage);
        }

        /// <summary>
        /// cache toute les fenetres
        /// </summary>
        public static void hide()
        {
            pbg?.Hide();
            iselector?.Hide();
            ifleche?.Hide();
            view?.Hide();
            ibattery?.Hide();
            ibot?.Hide();
        }

        private static bool visible = false;

        /// <summary>
        /// affiche l'inventaire
        /// </summary>
        /// <param name="x">decalage par rapport au centre sur x</param>
        /// <param name="y">decalage par rapport au centre sur y</param>
        public static void InventoryView(int x = 0, int y = 0)
        {
            if (visible)
            {
                hideInventoryView();
                Player.player.closeWindow();
                visible = false;
            } else
            {
                visible = true;


                ibg.Position = new Vector2((sizeX / 2) - 160 + x, (sizeY / 2) - 200 + y);
                ibg.Show();
                iView = new MGGridPane(0, 0, 8, 8);
                iView.SetPosition((sizeX / 2 - 160) + x + 4, (sizeY / 2 - 200) + y + 4);
                Stockage inventory = Player.player.inventory;
                int cpt = 0, cpt2 = 0;
                for (int i = 0; i < inventory.GetAll().Count; i++)
                {
                    int itmp = i;
                    ItemView iv = new ItemView(inventory.GetAll()[i]);
                    iView.add(iv, cpt, cpt2);
                    iv.ClickManager.OnMouseClickEnd += (o, args) => {
                        inventorySelector.Show();
                        inventorySelector.Position = iv.Position;
                        if (currentSelectedCouple != null)
                        { } else
                        {
                            currentSelectedCouple = inventory.GetAll()[itmp];
                        }
                    };
                    iv.Show();
                    x++;
                    iView.Notify();
                    cpt2++;
                    if (cpt2 == 8)
                    {
                        cpt2 = 0;
                        cpt++;
                    }
                }
            }

        }
        /// <summary>
        /// cache la fenetre inventaire
        /// </summary>
        public static void hideInventoryView()
        {
            ibg.Hide();
            inventorySelector.Hide();
            if (iView != null)
                iView.Hide();
            currentSelectedCouple = null;


        }

        public static void Replace()
        {
            pbg.Position = new Vector2(sizeX / 2 - 137, sizeY - 52);
            view.SetPosition((sizeX / 2) - 135, sizeY - 50);

            ifleche.Position = new Vector2(sizeX / 2 - 140, sizeY - 15);
        }

        /// <summary>
        /// affiche le hud (toolbar, batterie, vie)
        /// </summary>
        /// <param name="_s"></param>
        public static void show(Stockage _s)
        {

            pbg.Show();
            view = new MGGridPane(0, 0, 0, 2);
            Replace();
            int x = 0;
            foreach (Couple c in _s.GetAll())
            {
                ItemView iv = new ItemView(c);
                view.add(iv, 0, x);
                iv.ClickManager.OnMouseClickEnd += (o, args) => {
                    if (args.Button == MGCE.Input.MGInputNames.M_Right)
                    {
                        iv.Hide();
                        Player.player.toolBar.Remove(c.GetItemType(), c.GetQuantite());
                    } else
                    {
                        iselector.Show();
                        iselector.Position = iv.Position;
                        Player.player.currentSelection = c.GetItemType();
                    }
                };

                iv.Show();
                x++;
                view.Notify();
                MGDebug.PrintLine(iv.Position);
            }
            ifleche.Show();
            ifleche.Rotation = MathHelper.Pi;
        }
        private static Orientation orientation = Orientation.DOWN;

        /// <summary>
        /// augmente la rotation de la fleche de direction 
        /// </summary>
        public static void IncreaseRotation()
        {
            MGSpriteSheet fleche = null;

            switch (orientation)
            {
                case Orientation.DOWN:
                    orientation = Orientation.LEFT;
                    ifleche.Hide();
                    fleche = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/fleche4"));
                    ifleche = new MGImage(game, fleche);
                    break;
                case Orientation.LEFT:
                    orientation = Orientation.UP;
                    ifleche.Hide();
                    fleche = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/fleche1"));
                    ifleche = new MGImage(game, fleche);
                    break;
                case Orientation.UP:
                    orientation = Orientation.RIGHT;
                    ifleche.Hide();
                    fleche = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/fleche2"));
                    ifleche = new MGImage(game, fleche);
                    break;
                default:
                    orientation = Orientation.DOWN;
                    ifleche.Hide();
                    fleche = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/fleche3"));
                    ifleche = new MGImage(game, fleche);
                    break;

            }
            ifleche.Show();
            ifleche.Position = new Vector2(sizeX / 2 - 170, sizeY - 45);
            ifleche.DrawOrder = Const.IHM_DRAW_ORDER;
            fleche.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            Player.player.orientation = orientation;
        }

        private static MGImage ibattery;
        private static MGSpriteSheet battery;

        /// <summary>
        /// met a jour la vue de la batterie
        /// </summary>
        /// <param name="current"></param>
        /// <param name="max"></param>
        public static void updatePowerHUD(int current, int max)
        {
            double puissance;
            if (current != 0 && max != 0)
            {


                puissance = current * 100 / max;
            } else
            {
                puissance = 0;
            }
            if (ibattery != null)
                ibattery.Hide();

            MGDebug.PrintLine("puissance " + puissance);
            if (puissance >= 90)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-100"));
            } else if (puissance >= 80)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-90"));
            } else if (puissance >= 70)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-80"));
            } else if (puissance >= 60)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-70"));
            } else if (puissance >= 50)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-60"));
            } else if (puissance >= 40)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-50"));
            } else if (puissance >= 30)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-40"));
            } else if (puissance >= 20)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-30"));
            } else if (puissance >= 10)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-20"));
            } else if (puissance > 0)
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-10"));
            } else
            {
                battery = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/batterie-0"));
            }

            ibattery = new MGImage(game, battery);
            ibattery.Show();
            ibattery.Position = new Vector2(40, 40);
            ibattery.DrawOrder = Const.IHM_DRAW_ORDER;
            battery.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);

        }

        private static MGImage ibot;
        private static MGSpriteSheet bot;

        /// <summary>
        /// met a jour la vue de la vie
        /// </summary>
        public static void updateLifeHud()
        {
            float puissance;
            if (Player.player != null && Player.player.Character != null)
            {
                puissance = Player.player.Character.HealPercent;
                String currentBot = "";
                if (Player.player.Character is WallECharacter)
                {
                    currentBot = "Autre/walle";
                } else
                {
                    currentBot = "Autre/eve";
                }


                puissance = puissance * 100;
                if (ibot != null)
                    ibot.Hide();

                if (puissance >= 100)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-100"));
                } else if (puissance >= 90)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-90"));
                } else if (puissance >= 80)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-80"));
                } else if (puissance >= 70)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-70"));
                } else if (puissance >= 60)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-60"));
                } else if (puissance >= 50)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-50"));
                } else if (puissance >= 40)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-40"));
                } else if (puissance >= 30)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-30"));
                } else if (puissance >= 20)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-20"));
                } else if (puissance > 10)
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-10"));
                } else
                {
                    bot = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(currentBot + "-0"));
                }

                ibot = new MGImage(game, bot);
                ibot.Show();
                ibot.Position = new Vector2(40, 260);
                ibot.DrawOrder = Const.IHM_DRAW_ORDER;
                bot.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            }


        }



    }
}
