﻿using MGCE.Events;
using MGCE.Util;
using System.Drawing;
using System.Windows.Forms;

namespace Hephaestus.Code.View
{
    public class MyButton : Button, IMGEventLanguageChangedListener
    {
        public string Key { get; }
        public MyButton(string key, Font font = null) : base()
        {
            Key = key;
            Font = font ?? MainMenu.FONT_BIG;
            Size = new Size(800, Font.Height);
            MGEvent<IMGEventLanguageChangedListener>.Get().AddListener(this);
            OnLanguageChanged("");
        }

        public void OnLanguageChanged(string language)
        {
            Text = MGLanguage.GetOrName(Key);
        }
    }
}
