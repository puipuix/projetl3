﻿using MGCE.Actor;
using MGCE.Control;
using MGCE.Engine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hephaestus.Code.View
{
    public class ItemView : MGControl
    {/// <summary>
    /// retourne une vue de l'item demandé
    /// </summary>
    /// <param name="couple"></param>
        public ItemView(Couple couple) : base(MyGame.Game)
        {
            MGSpriteSheet sp = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>(RessourceManager.MatchImage(couple.GetItemType())));
            MGImage img = new MGImage(MyGame.Game, sp);
            MGLabel lab = new MGLabel(RessourceManager.Load<SpriteFont>("Item/item_font"), MyGame.Game);
            lab.Color = Color.WhiteSmoke;
            DrawOrder = Const.IHM_DRAW_ORDER;
            
            sp.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
            img.TargetSize = new Vector2(32, 32);
            img.Position = Vector2.Zero;
            Size = new Point(32);

            lab.SetText(couple.GetQuantite().ToString());
            lab.Position = new Vector2(22);
            SetProtectedChild(this, img);
            SetProtectedChild(this, lab);
            UpdateControls();
        }
    }
}
