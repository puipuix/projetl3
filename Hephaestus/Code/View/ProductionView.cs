﻿using MGCE.Actor;
using MGCE.Control;
using MGCE.Engine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Text;
using Hephaestus.Code.Character;

namespace Hephaestus.Code.View
{
    class ProductionView
    {

        Production prod;
        int sizeX, sizeY;
        Game game;
        MGGridPane view;
        MGImage pbg;
        MGSpriteSheet bg;
        Boolean hide = true;
        ItemView produce;

        public ProductionView(Game g)
        {

            game = g;
            (sizeX, sizeY) = MGEngine.BufferSize;
            sizeX = sizeX / 2;
            sizeY = sizeY / 2;
            view = new MGGridPane(0, 0);
            view.SetPosition(sizeX - 56, sizeY - 20);

        }

        /// <summary>
        /// affiche la production actuelle d'une machine et les ingredient
        /// </summary>
        /// <param name="s"></param>
        public void show(Production s)
        {
            Hud.InventoryView(300);
            view = new MGGridPane(0, 0, 0, 1);

            view.SetPosition(sizeX - 39, sizeY - 40);


            game = MyGame.Game;
            hide = false;
            prod = s;

            produce = new ItemView(new Couple(s.sortie, s.actualProd));
            produce.ClickManager.OnMouseClickEnd += (o, arg) =>
            {
                Player.player.inventory.Add(s.sortie, s.actualProd); s.actualProd = 0; Hud.InventoryView(300); /*vide l'inventaire et le transfer dans celui du joueur*/};
                produce.Show();
                produce.Position = new Vector2(sizeX - 91, sizeY - 36);

                bg = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Autre/production_view"));
                pbg = new MGImage(game, bg);
                pbg.ClickManager.OnMouseClickEnd += (o, arg) =>
                {//ajout au stockage de la machine les element de l'inventaire du joueur souhaité
                    if (Hud.currentSelectedCouple != null && s.getIngredient().CanAdd(Hud.currentSelectedCouple.GetItemType(), Hud.currentSelectedCouple.GetQuantite()))
                    {
                        if (s.Enter(new Items.Item(Hud.currentSelectedCouple.GetItemType()), Hud.currentSelectedCouple.GetQuantite()))
                            Player.player.inventory.Remove(Hud.currentSelectedCouple.GetItemType(), Hud.currentSelectedCouple.GetQuantite());
                        Hud.InventoryView(300);
                    }
                };

                pbg.DrawOrder = Const.IHM_DRAW_ORDER;
                bg.PlayAnimation(MGSpriteAnimation.DEFAULT_ANIMATION);
                pbg.Position = new Vector2(sizeX - 95, sizeY - 40);
                pbg.Show();
                int x, y;
                x = y = 0;
                foreach (Couple c in prod.getIngredient().GetAll())
                {
                    if (x == 10)
                    {
                        x = 0;
                        y++;
                    }

                    MGDebug.PrintLine(c.GetItemType());

                    ItemView itvi;
                    if (prod.stockage.getCountOf(c.GetItemType()) > 0)
                    {
                        itvi = new ItemView(new Couple(c.GetItemType(), prod.stockage.getCountOf(c.GetItemType())));
                    }
                    else
                    {
                        itvi = new ItemView(new Couple(c.GetItemType(), 0));
                    }

                    view.add(itvi, y, x);


                    x++;

                }




                view.Notify();


            }



        public void Hide()
            {

                if (pbg != null)
                    pbg.Hide();
                hide = true;
                view.Hide();
                if (produce != null)
                    produce.Hide();
            }

            public bool hidden()
            {
                return hide;
            }








        }
    }
