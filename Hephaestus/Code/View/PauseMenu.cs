﻿using Hephaestus.Code.Saves;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Util;
using MGCE.Util.Caster;
using Hephaestus.Code.Blocks.Terrain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Hephaestus.Code.View
{
    public static class PauseMenu
    {
        public static bool Visible { get; private set; }
        public static GameSave CurrentGameSave { get; set; }

        public static MyButton Save { get; }
        public static MyButton Exit { get; }
        public static MyButton Back { get; }

        static PauseMenu()
        {
            Exit = new MyButton("Main menu");
            Exit.Click += (o, args) => {
                Hide();
                CurrentGameSave?.Dispose();
                MainMenu.ShowMain();
            };

            Save = new MyButton("Save");
            Save.Click += (o, args) => {
                if (Visible)
                {
                    CurrentGameSave?.Save();
                }
            };

            Back = new MyButton("Return");
            Back.Click += Hide;

            Replace(null, null);
        }

        private static void Replace(object o, EventArgs args)
        {
            MGPoint size = MGEngine.BufferSize.ToMGPoint();
            Exit.Location = size / 2 - Exit.Size.ToMGPoint() / 2 - new MGPoint(0, 100);
            Save.Location = size / 2 - Save.Size.ToMGPoint() / 2;
            Back.Location = size / 2 - Back.Size.ToMGPoint() / 2 + new MGPoint(0, 100);
        }

        public static void SwitchVisible(GameSave gameSave = null)
        {
            if (Visible)
            {
                Hide();
            } else
            {
                Show(gameSave);
            }
        }

        public static void Show(GameSave gameSave = null)
        {
            Visible = true;
            CurrentGameSave = gameSave;
            if (CurrentGameSave?.World is World w)
            {
                w.IsWorldClickable = false;
            }
            MGEngine.WindowForm.Controls.Clear();
            MGEngine.WindowForm.Controls.AddRange(MGTools.MakeArray(Save, Back, Exit));
        }

        public static void Hide(object obj, EventArgs args)
        {
            Visible = false;
            MGEngine.WindowForm.Controls.Clear();
            if (CurrentGameSave?.World is World w)
            {
                w.IsWorldClickable = true;
            }
        }

        public static void Hide()
        {
            Hide(null, null);
        }
    }
}
