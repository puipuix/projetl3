﻿using MGCE.Events;
using MGCE.Util;
using MGCE.Util.Caster;
using System.Drawing;
using System.Windows.Forms;

namespace Hephaestus.Code.View
{
    public class MyLabeledControl : IMGEventLanguageChangedListener
    {
        public string Key { get; }
        public Control Control { get; }
        public Label Label { get; }
        public Size Size { get; }
        public MGPoint ControlTranslation { get; set; }

        public MGPoint Location { get => Label.Location; set { Label.Location = value; Control.Location = value + new MGPoint(425, 0) + ControlTranslation; } }

        public MyLabeledControl(string key, Control control, int line = 1)
        {
            Key = key;

            Label = new Label() { TextAlign = ContentAlignment.MiddleCenter, Font = MainMenu.FONT_BIG };
            Label.Size = new Size(375, Label.Font.Height * line);

            Control = control;
            Control.Location = new MGPoint(425, 0);
            Control.Size = new Size(375, Label.Font.Height * line);
            Size = new Size(800, Label.Font.Height);
            MGEvent<IMGEventLanguageChangedListener>.Get().AddListener(this);
            OnLanguageChanged("");

        }
        public void OnLanguageChanged(string language)
        {
            Label.Text = MGLanguage.GetOrName(Key);
        }
    }
}
