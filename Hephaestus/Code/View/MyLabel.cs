﻿using MGCE.Events;
using MGCE.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Hephaestus.Code.View
{
    public class MyLabel : Label, IMGEventLanguageChangedListener
    {
        public string Key { get; }
        public MyLabel(string key, Font font = null, int line = 1) : base()
        {
            Key = key;
            Font = font ?? MainMenu.FONT_BIG;
            Size = new Size(800, Font.Height * line);
            TextAlign = ContentAlignment.MiddleCenter;
            MGEvent<IMGEventLanguageChangedListener>.Get().AddListener(this);
            OnLanguageChanged("");
        }

        public void OnLanguageChanged(string language)
        {
            Text = MGLanguage.GetOrName(Key);
        }
    }
}
