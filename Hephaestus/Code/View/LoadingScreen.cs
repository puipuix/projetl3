﻿using MGCE.Engine;
using MGCE.Util;
using MGCE.Util.Caster;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Hephaestus.Code.View
{
    public class LoadingScreen : Panel
    {
        public Label Label { get; set; }
        public Label Title { get; set; }
        public ProgressBar ProgressBar { get; set; }

        public string LoadingText { get => Label.Text; set { Label.Text = value; ReplaceLabel(); } }
        
        public LoadingScreen()
        {
            MGEngine.OnBufferSizeChange += (o, args) =>
            {
                ResizeMe();
            };

            Label = new Label()
            {
                Text = "Loading...",
                TextAlign = ContentAlignment.MiddleCenter,
                BackColor = Color.Transparent,
                ForeColor = Color.WhiteSmoke,
                FlatStyle = FlatStyle.Popup,
                Font = new Font(Label.DefaultFont.FontFamily, 60, FontStyle.Bold | FontStyle.Italic),
            };
            MGDebug.PrintLine(FontFamily.Families.ToString(f => f.Name));
            Title = new Label()
            {
                Text = "Loading...",
                TextAlign = ContentAlignment.MiddleCenter,
                BackColor = Color.Snow,
                ForeColor = Color.Black,
                FlatStyle = FlatStyle.Popup,
                Font = new Font(FontFamily.Families[new Random().Next(0, FontFamily.Families.Length)], 60, FontStyle.Italic | FontStyle.Bold)
            };

            ProgressBar = new ProgressBar()
            {
                Maximum = 100,
                Width = 800,
                Height = 50,
                ForeColor = Color.WhiteSmoke,
                BackColor = Color.Black,
                
            };
            BackgroundImage = Image.FromFile("Content/RessourcePacks/Default/Environnement/magma.png");
            Controls.AddRange(MGTools.MakeArray<Control>(Title, Label, ProgressBar));
            ResizeMe();
        }

        private void ResizeMe()
        {
            MGPoint size = MGEngine.BufferSize.ToMGPoint();
            (Width, Height) = size.Deconstruct();
            ReplaceLabel();
            ProgressBar.Location = new Point((size.Point.X - ProgressBar.Width) / 2, size.Point.Y - ProgressBar.Height - 10);
            Title.Size = new Size(size.Point.X, Title.Font.Height);
            Title.Location = new Point((size.Point.X - Title.Width) / 2, (size.Point.Y - Title.Height) / 2);
        }

        private void ReplaceLabel()
        {
            MGPoint size = MGEngine.BufferSize.ToMGPoint();
            Label.Size = new Point(size.Point.X, Label.Font.Height + 5).ToMGPoint();
            Label.Location = new Point((size.Point.X - Label.Width) / 2, size.Point.Y - ProgressBar.Height - Label.Font.Height - 20);
        }
    }
}
