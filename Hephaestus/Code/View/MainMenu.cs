﻿using MGCE.Engine;
using MGCE.Events;
using MGCE.Util;
using MGCE.Util.Caster;
using Microsoft.Xna.Framework;
using Hephaestus.Code;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util.Light;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FColor = System.Drawing.Color;
using System.Linq;
using Hephaestus.Code.Saves;
using System.DirectoryServices.ActiveDirectory;
using System.Windows.Forms.Layout;
using System.Transactions;
using Hephaestus.Code.Util;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Hephaestus.Code.View
{

    public static class MainMenu
    {
        public static readonly Vector2 MAIN_WORLD_POSITION = new Vector2(0, -15) * Block.BLOCK_SIZE_PX;
        public static readonly Vector2 CONFIG_WORLD_POSITION = new Vector2(10, 0) * Block.BLOCK_SIZE_PX;
        public static readonly Vector2 CREDIT_WORLD_POSITION = new Vector2(-15, -80) * Block.BLOCK_SIZE_PX;
        public static readonly Vector2 PLAY_WORLD_POSITION = new Vector2(5, -60) * Block.BLOCK_SIZE_PX;
        public static readonly Font FONT_BIG = new Font(Label.DefaultFont.FontFamily, 30, FontStyle.Bold | FontStyle.Italic);
        public static readonly Font FONT_SMALL = new Font(Label.DefaultFont.FontFamily, 20, FontStyle.Bold | FontStyle.Italic);

        private static bool _main, _config, _play, _credit;

        public static MyButton Play { get; }
        public static MyButton Config { get; }
        public static MyButton Credit { get; }
        public static MyButton Exit { get; }


        public static TableLayoutPanel Saves { get; }
        public static MyLabeledControl Name { get; }
        public static MyLabeledControl Seed { get; }
        public static MyButton Create { get; }
        public static MyButton PlayToMain { get; }


        public static MyButton ConfigToMain { get; }
        public static MyLabeledControl Language { get; }
        public static MyLabeledControl Brightness { get; }
        public static MyLabeledControl Music { get; }
        public static MyLabeledControl Effect { get; }


        public static MyButton CreditToMain { get; }
        public static MyLabeledControl CreditTexture { get; }
        public static MyLabeledControl CreditProgramming { get; }
        public static MyLabeledControl CreditMusic { get; }
        public static MyLabeledControl CreditSoundEffect { get; }


        public static World BackGroundWorld { get; private set; }
        public static SmoothCameraActor SmoothCamera { get; private set; }


        static MainMenu()
        {
            Play = new MyButton("Play");
            Play.Click += ShowPlay;

            Config = new MyButton("Configuration");
            Config.Click += ShowConfig;

            Credit = new MyButton("Credit");
            Credit.Click += ShowCredit;

            Exit = new MyButton("Exit");
            Exit.Click += (o, args) => {
                if (_main)
                {
                    MyGame.Game.Exit();
                }
            };


            Saves = new TableLayoutPanel() { Size = new Size(800, 400), AutoScroll = true };
            Saves.HorizontalScroll.Visible = true;

            TextBox nameTxt = new TextBox { Multiline = false, PlaceholderText = "MyWorld", Font = FONT_BIG };
            Name = new MyLabeledControl("Name", nameTxt) { ControlTranslation = new MGPoint(0, -5) };

            TextBox seedtxt = new TextBox { Multiline = false, PlaceholderText = "0123456789", Font = FONT_BIG };
            seedtxt.KeyPress += (o, args) => {
                args.Handled = !char.IsDigit(args.KeyChar) && !char.IsControl(args.KeyChar);
            };
            seedtxt.TextChanged += (o, args) => {
                if (System.Text.RegularExpressions.Regex.IsMatch(seedtxt.Text, "[A-z]"))
                {
                    seedtxt.Text = "";
                }
            };
            Seed = new MyLabeledControl("Seed", seedtxt) { ControlTranslation = new MGPoint(0, -5) };

            Create = new MyButton("Create new world");
            Create.Click += (o, args) => {
                if (_play)
                {
                    SoundManager.StopMusic();
                    Hide(null, null);
                    GameSave.Create(nameTxt.Text, string.IsNullOrWhiteSpace(seedtxt.Text) ? new Random().Next() : int.Parse(seedtxt.Text));
                }
            };

            PlayToMain = new MyButton("Return");
            PlayToMain.Click += ShowMain;

            ComboBox languageComb = new ComboBox() { Font = FONT_BIG, Text = MGLanguage.CurrentLanguage };
            Language = new MyLabeledControl("Language", languageComb) { ControlTranslation = new MGPoint(0, -5) };
            languageComb.Items.AddRange(MGLanguage.FindLanguages());
            languageComb.SelectedValueChanged += (o, args) => {
                MGLanguage.LoadLanguageDictionary(languageComb.SelectedItem.ToString());
                Settings.Default.Language = MGLanguage.CurrentLanguage;
                Settings.Default.Save();
            };

            TrackBar trackBarBri = new TrackBar() { TickStyle = TickStyle.None, Minimum = 5, Maximum = 20, LargeChange = 3, Value = (int)(ILightSource.GlobalBrightness * 10) };
            trackBarBri.Scroll += (o, args) => {
                if (_config)
                {
                    Settings.Default.Brightness = ILightSource.GlobalBrightness = trackBarBri.Value / 10f;
                    Settings.Default.Save();
                    BackGroundWorld.LoadedTerrain.Values.ForEach(t => t.NotifyLightChange());
                }
            };
            Brightness = new MyLabeledControl("Brightness", trackBarBri);

            TrackBar trackBarMus = new TrackBar() { TickStyle = TickStyle.None, Minimum = 0, Maximum = 100, Value = Settings.Default.Music };
            trackBarMus.Scroll += (o, args) => {
                if (_config)
                {
                    Settings.Default.Music = trackBarMus.Value;
                    Settings.Default.Save();
                    MediaPlayer.Volume = trackBarMus.Value / 100f;
                }
            };
            Music = new MyLabeledControl("Musics", trackBarMus);


            TrackBar trackBarEff = new TrackBar() { TickStyle = TickStyle.None, Minimum = 0, Maximum = 100, Value = Settings.Default.Environment };
            trackBarEff.Scroll += (o, args) => {
                if (_config)
                {
                    Settings.Default.Environment = trackBarEff.Value;
                    Settings.Default.Save();
                    SoundEffect.MasterVolume = trackBarEff.Value / 100f;
                }
            };
            Effect = new MyLabeledControl("Sound Effects", trackBarEff);

            ConfigToMain = new MyButton("Return");
            ConfigToMain.Click += ShowMain;

            CreditMusic = new MyLabeledControl("Musics", new MyLabel("Undertale OST", FONT_SMALL), 2);
            CreditSoundEffect = new MyLabeledControl("Sound Effects", new MyLabel("BBC", FONT_SMALL), 2) { ControlTranslation = new MGPoint(0, -5) };
            CreditTexture = new MyLabeledControl("Textures", new MyLabel("Samuel Baton", FONT_SMALL), 2) { ControlTranslation = new MGPoint(0, -5) };
            CreditProgramming = new MyLabeledControl("Programming", new MyLabel("Alexis Vernes\nSamuel Baton", FONT_SMALL), 2) { ControlTranslation = new MGPoint(0, -5) };
            CreditToMain = new MyButton("Return");
            CreditToMain.Click += ShowMain;

            MGEngine.OnBufferSizeChange += Replace;
            Replace(null, EventArgs.Empty);
        }



        private static void Replace(object o, EventArgs args)
        {
            MGPoint size = MGEngine.BufferSize.ToMGPoint();

            Play.Location = size / 2 - Play.Size.ToMGPoint() / 2 - new MGPoint(0, 100);
            Config.Location = size / 2 - Config.Size.ToMGPoint() / 2;
            Credit.Location = size / 2 - Credit.Size.ToMGPoint() / 2 + new MGPoint(0, 100);
            Exit.Location = size / 2 - Exit.Size.ToMGPoint() / 2 + new MGPoint(0, 200);

            Saves.Location = size / 2 - Saves.Size.ToMGPoint() / 2 - new MGPoint(0, 300);
            Name.Location = size / 2 - Name.Size.ToMGPoint() / 2 + new MGPoint(0, 000);
            Seed.Location = size / 2 - Seed.Size.ToMGPoint() / 2 + new MGPoint(0, 100);
            Create.Location = size / 2 - Create.Size.ToMGPoint() / 2 + new MGPoint(0, 200);
            PlayToMain.Location = size / 2 - PlayToMain.Size.ToMGPoint() / 2 + new MGPoint(0, 300);

            Brightness.Location = size / 2 - Brightness.Size.ToMGPoint() / 2 - new MGPoint(0, 200);
            Language.Location = size / 2 - Language.Size.ToMGPoint() / 2 - new MGPoint(0, 100);
            Music.Location = size / 2 - Music.Size.ToMGPoint() / 2;
            Effect.Location = size / 2 - Effect.Size.ToMGPoint() / 2 + new MGPoint(0, 100);
            ConfigToMain.Location = size / 2 - ConfigToMain.Size.ToMGPoint() / 2 + new MGPoint(0, 200);

            CreditTexture.Location = size / 2 - CreditTexture.Size.ToMGPoint() / 2 - new MGPoint(0, 400);
            CreditProgramming.Location = size / 2 - CreditProgramming.Size.ToMGPoint() / 2 - new MGPoint(0, 200);
            CreditMusic.Location = size / 2 - CreditMusic.Size.ToMGPoint() / 2;
            CreditSoundEffect.Location = size / 2 - CreditSoundEffect.Size.ToMGPoint() / 2 + new MGPoint(0, 200);
            CreditToMain.Location = size / 2 - CreditToMain.Size.ToMGPoint() / 2 + new MGPoint(0, 400);
        }

        public static void ShowMain(object o, EventArgs args)
        {
            _main = true;
            _play = _config = _credit = false;
            SetVisible(true);
            MGEngine.WindowForm.Controls.Clear();
            MGEngine.WindowForm.Controls.AddRange(MGTools.MakeArray(Play, Config, Credit, Exit));
            SmoothCamera.Target = MAIN_WORLD_POSITION;
        }

        public static void ShowMain()
        {
            ShowMain(null, null);
            SoundManager.PlayMenu();
        }

        public static void ShowPlay(object o, EventArgs args)
        {
            _play = true;
            _main = _config = _credit = false;
            SetVisible(true);
            Saves.Controls.Clear();
            GameSave.SavesName().For((name, i) => {
                Label l = new Label() { Font = FONT_BIG, TextAlign = ContentAlignment.MiddleCenter };
                l.Text = name;
                MyButton b = new MyButton("Start", FONT_BIG);
                MyButton r = new MyButton("Delete", FONT_BIG);
                b.Click += (o, args) => {
                    if (_play)
                    {
                        Hide();
                        GameSave.Load(name);
                        b.Dispose();
                        r.Dispose();
                        SoundManager.StopMusic();
                    }
                };
                r.Click += (o, args) => {
                    if (_play)
                    {
                        GameSave.Delete(name);
                        b.Dispose();
                        r.Dispose();
                        ShowPlay();
                    }
                };
                l.Size = b.Size = r.Size = new MGPoint(Saves.Size.Width / 3 - SystemInformation.VerticalScrollBarWidth, l.Font.Height);
                Saves.Controls.Add(l, 0, i);
                Saves.Controls.Add(b, 1, i);
                Saves.Controls.Add(r, 2, i);
            });
            MGEngine.WindowForm.Controls.Clear();
            MGEngine.WindowForm.Controls.AddRange(MGTools.MakeArray<Control>(Saves, Name.Label, Name.Control, Seed.Label, Seed.Control, Create, PlayToMain));
            SmoothCamera.Target = PLAY_WORLD_POSITION;
        }

        public static void ShowPlay()
        {
            ShowPlay(null, null);
        }

        public static void ShowConfig(object o, EventArgs args)
        {
            _config = true;
            _main = _play = _credit = false;
            SetVisible(true);
            MGEngine.WindowForm.Controls.Clear();
            MGEngine.WindowForm.Controls.AddRange(MGTools.MakeArray<Control>(Brightness.Label, Brightness.Control, Language.Label, Language.Control, Music.Label, Music.Control, Effect.Label, Effect.Control, ConfigToMain));
            SmoothCamera.Target = CONFIG_WORLD_POSITION;
        }

        public static void ShowCredit(object sender, EventArgs e)
        {
            if (_credit) //  disable warning _credit not used
            { }
            _credit = true;
            _main = _play = _config = false;
            SetVisible(true);
            MGEngine.WindowForm.Controls.Clear();
            MGEngine.WindowForm.Controls.AddRange(MGTools.MakeArray<Control>(CreditTexture.Label, CreditTexture.Control, CreditProgramming.Label, CreditProgramming.Control, CreditMusic.Label, CreditMusic.Control, CreditSoundEffect.Label, CreditSoundEffect.Control, CreditToMain));
            SmoothCamera.Target = CREDIT_WORLD_POSITION;
        }

        public static void Hide()
        {
            Hide(null, null);
        }

        public static void Hide(object o, EventArgs args)
        {
            SetVisible(false);
        }

        private static void SetVisible(bool value)
        {
            if (value != (BackGroundWorld is World))
            {
                if (value)
                {
                    BackGroundWorld = new World(new WorldGenerator(0, WorldGenerator.DefaultTree));
                    BackGroundWorld.Activate();
                    BackGroundWorld.SunLight.TimeSpeed = 60 * 10;
                    BackGroundWorld.SunLight.PlayMusics = false;
                    BackGroundWorld.Camera.WorldPosition = MAIN_WORLD_POSITION;
                    BackGroundWorld.IsWorldPlayable = false;
                    SmoothCamera = new SmoothCameraActor(BackGroundWorld.Camera) { Speed = 2f * Block.BLOCK_SIZE_PX, MaxDistance = 10000 };
                    SmoothCamera.Enable();
                } else
                {
                    _main = _play = _config = false;
                    MGEngine.WindowForm.Controls.Clear();
                    BackGroundWorld.Dispose();
                    SmoothCamera.Dispose();
                    BackGroundWorld = null;
                }
            }
        }
    }
}
