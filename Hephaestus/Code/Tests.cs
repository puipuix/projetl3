﻿using MGCE.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MGCE.Actor;
using MGCE.Control;
using MGCE.Control.EventArguments;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Input;
using MGCE.Shapes;
using System.Linq;
using MGCE.Util.TimeLine;
using Hephaestus.Code;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Fluids;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Character;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using Hephaestus.Code.Util.Light;
using Hephaestus.Code.View;
using System;
using System.Runtime;
using System.Text;
using static Hephaestus.Code.Util.Const;
using Hephaestus.Code.Blocks.Utilitaire;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Hephaestus;
using MGCE.Util.Caster;
using System.Runtime.CompilerServices;

namespace Hephaestus.Code
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class TestGame : Game
    {

        public static TestGame Game { get; private set; }

        private bool buildDone = false;

        public TestGame()
        {
            Game = this;
            Content.RootDirectory = "Content";

            MGEngine.Create(this, new MGPoint(256, 64), false, true, false, true, true);
            MGEngine.SetTargetedFps(60);
            MGEngine.OnBufferSizeChange += (o, args) => {
                Settings.Default.ScreenSize = MGEngine.BufferSize.ToMGPoint();
                Settings.Default.Save();
            };
            IsMouseVisible = true;

            IsFixedTimeStep = true;

            GCSettings.LatencyMode = GCLatencyMode.LowLatency;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            MGEngine.Initialize(Settings.Default.Language, "Language");

            MGEvent<ILightPreUpdate>.Create().DrawEvent = true;
            MGEvent<ILightPreUpdate>.Get().CallOrder = MGConst.ORDER_1_BEFORE;
            MGEvent<ILightPreUpdate>.Get().TimeOutMs = 5;
            MGEvent<ILightPreUpdate>.Get().MakeEventAlwaysAsked();

            MGEvent<ILightUpdate>.Create().DrawEvent = true;
            MGEvent<ILightUpdate>.Get().CallOrder = MGConst.ORDER_3_DEFAULT;
            MGEvent<ILightUpdate>.Get().MakeEventAlwaysAsked();

            MGEvent<ILightPostUpdate>.Create().DrawEvent = true;
            MGEvent<ILightPostUpdate>.Get().CallOrder = MGConst.ORDER_5_AFTER;
            MGEvent<ILightPostUpdate>.Get().MakeEventAlwaysAsked();

            MGEvent<IRessourceChangeEventListener>.Create();

            MGEvent<IFluidUpdate>.Create<GameTime>().TimeOutMs = 5;
            MGEvent<IFluidUpdate>.Get().CallOrder = MGConst.DRAW_4_DEFAULT;
            MGEvent<IFluidUpdate>.Get().MakeEventAlwaysAsked(() => MGEngine.UpdateGameTime);

            MGEngine.SpriteBatchParameters[IHM_DRAW_ORDER] = new MGSpriteBatchParameter(SpriteSortMode.Deferred);
            MGEngine.SpriteBatchParameters[DEBUG_DRAW_ORDER] = new MGSpriteBatchParameter(SpriteSortMode.Deferred);

            base.Initialize();
        }

        private void Build()
        {
            MGMultipleContentBuilder cb = new MGMultipleContentBuilder(
                new MGContentBuilder(@"Content/RessourcePacks/Default/Content.mgcb", @"../../cache/RessourcePacks", true),
                new MGContentBuilder(@"Content/Content.mgcb", @"cache", false)
                );

            cb.OnAnyOutputDataReceived += (o, args) => {
                MGDebug.PrintLine(args.Text);
            };

            cb.OnAllEnd += (o, args) => {
                buildDone = true;
            };

            cb.AsyncBuildContent(block: false);
        }

        protected override void LoadContent()
        {
            MGEngine.LoadContent();
            Build();
        }

        protected override void UnloadContent()
        {
            MGEngine.UnloadContent();
            // TODO: Unload any non ContentManager content here
        }

        private void TestEquals(object tested, object expected, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            if (!expected.Equals(tested))
            {
                MGDebug.PrintError("TestEquals: " + tested + " != " + expected, memberName, sourceLineNumber);
            } else
            {
                MGDebug.PrintLine("TestEquals: Ok", memberName, sourceLineNumber);
            }
        }

        private void TestNotEquals(object tested, object expected, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            if (expected.Equals(tested))
            {
                MGDebug.PrintError("TestNotEquals: " + tested + " == " + expected, memberName, sourceLineNumber);
            } else
            {
                MGDebug.PrintLine("TestNotEquals: Ok", memberName, sourceLineNumber);
            }
        }

        private void TestIsNull(object tested, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            if (!(tested is null))
            {
                MGDebug.PrintError("TestIsNull: " + tested + " is not null", memberName, sourceLineNumber);
            } else
            {
                MGDebug.PrintLine("TestIsNull: Ok", memberName, sourceLineNumber);
            }
        }

        private void TestIsNotNull(object tested, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            if (tested is null)
            {
                MGDebug.PrintError("TestIsNotNull: " + tested + " is null", memberName, sourceLineNumber);
            } else
            {
                MGDebug.PrintLine("TestIsNotNull: Ok", memberName, sourceLineNumber);
            }
        }

        protected override void Update(GameTime gameTime)
        {
            MGEngine.Update(gameTime);

            if (buildDone)
            {
                // Add tests
                MGLanguage.Texts.Add("test_key_345", "test_result_345");

                TestEquals(MGLanguage.GetText("unknow_345"), "ERR: [unknow_345] UNKNOW");
                TestEquals(MGLanguage.GetText("test_key_345"), "test_result_345");

                TestEquals(MGLanguage.GetOrName("unknow_345"), "unknow_345");
                TestEquals(MGLanguage.GetOrName("test_key_345"), "test_result_345");

                int seed = MGTools.RNG.Next();
                Noise.DebugMinMax(Noise.GetSimplexNoise(seed));
                Noise.DebugMinMax(Noise.GetPerlinNoise(seed));
                Noise.DebugMinMax(Noise.GetPerlinNoise(seed, true));
                Noise.DebugMinMax(Noise.GetRandomNoise(seed));
                Noise.DebugMinMax(Noise.GetPatchNoise(seed));

                Exit();
            }

            base.Update(gameTime);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Pink);
            base.Draw(gameTime);
        }
    }
}
