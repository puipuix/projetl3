﻿using System;
using Hephaestus.Code;

namespace Hephaestus
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {

        public const bool TEST = false;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#pragma warning disable CS0162 // Code inaccessible détecté
            if (TEST)
            {
                using (var game = new TestGame())
                {
                    game.Run();
                }
            } else
            {
                using (var game = new MyGame())
                {
                    game.Run();
                }
            }

#pragma warning restore CS0162 // Code inaccessible détecté
        }
    }
}
