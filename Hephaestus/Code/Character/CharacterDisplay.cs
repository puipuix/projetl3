﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Input;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Events;
using MGCE.Shapes;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Foreground;
using MGCE.Engine;
using MGCE.Control;
using MGCE.Util;
using Microsoft.Xna.Framework.Audio;
using Hephaestus.Code.Character;
using System.Runtime.Serialization;

namespace Hephaestus.Code.Character
{
    [Serializable]
    public abstract class CharacterDisplay : CameraFollowerActor, IMGEventPreUpdateListener
    {
        public const string ANIMATION_MOVE = "Move", ANIMATION_STAND = MGSpriteAnimation.DEFAULT_ANIMATION, ANIMATION_DEAD = "Dead", ANIMATION_JET = "Jet";
        public const float MIN_FALL_SPEED_FOR_DAMAGE = 8;
        public const bool INVINCIBLE = false;

        public World World { get; set; }

        public MGPolygon HitBox { get; protected set; } = MGPolygon.EMPTY;

        public float Fall { get; protected set; }
        public float FallMaxSpeed { get; protected set; }

        public float Speed { get; protected set; }

        public float JetPackAccel { get; protected set; }
        public float JetPackMaxSpeed { get; protected set; }
        public bool JetPackOn { get; protected set; }
        public TimeSpan JetPackActivation { get; protected set; } = TimeSpan.FromSeconds(0.5);

        public bool OnGround { get; protected set; } = true;


        public float FallDamage { get; protected set; }
        public float MoveDamage { get; protected set; }
        public float HealMax { get; protected set; }
        public float Heal { get;  set; }
        public float HealPercent => Heal / HealMax;
        public bool Alive { get; protected set; } = true;

        public Vector2 Movement { get; private set; }
        public Vector2 Lastposition => WorldPosition - Movement;
        public float JumpForce { get; protected set; }

        public AudioEmitter AudioEmitter { get; set; } = new AudioEmitter();
        public string JetPackSound { get; protected set; } = null;
        public string MoveSound { get; protected set; } = null;
        public string DeathSound { get; protected set; } = null;
        public string JumpSound { get; protected set; } = null;

        public float MiningEfficiency = 1f;

        private TimeSpan lastJumpPress;

        public TerrainBlock MyBlock => World?.GetOrNull(BlockRelativePositionActor.WorldPositionToBlockPosition(WorldPosition));

        public CharacterDisplay(World world) : base(world.Camera)
        {
            World = world;
            SpriteSheet = CreateSpriteSheet();
            TargetSize = new Vector2(Block.BLOCK_SIZE_PX);
            DrawOrder = Const.PLAYER_DRAW_ORDER;

            SpriteSheet.PlayAnimation(ANIMATION_STAND);
        }

        protected CharacterDisplay(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            World = info.GetValue("World", World);
            WorldPosition = info.GetValue("WorldPosition", WorldPosition);
            Heal = info.GetSingle("Heal");
            Alive = Heal > 0;
            SpriteSheet = CreateSpriteSheet();
            TargetSize = new Vector2(Block.BLOCK_SIZE_PX);
            DrawOrder = Const.PLAYER_DRAW_ORDER;
            if (Alive)
            {
                SpriteSheet.PlayAnimation(ANIMATION_STAND);
            } else
            {

                SpriteSheet.PlayAnimation(ANIMATION_DEAD);
            }
        }

        /// <summary>
        /// Enable character's update
        /// </summary>
        public void Activate()
        {
            this.StartListeningEvent<IMGEventPreUpdateListener>();
            if (Alive)
            {
                SpriteSheet.PlayAnimation(ANIMATION_STAND);
            }
        }

        /// <summary>
        /// Disable character's update
        /// </summary>
        public void Disactivate()
        {
            this.StopListeningEvent<IMGEventPreUpdateListener>();
            if (Alive)
            {
                SpriteSheet.PlayAnimation(ANIMATION_STAND);
            }
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("World", World);
            info.AddValue("WorldPosition", WorldPosition);
            info.AddValue("Heal", Heal);
        }

        protected abstract MGSpriteSheet CreateSpriteSheet();

        protected float GetMovementCoef()
        {
            return MyBlock?.ForegroundBlock?.Liquid?.MovementSpeedCoef ?? 1f;
        }

        protected void CheckJetPack(float second)
        {
            if (JetPackOn)
            {
                if (JetPackSound is string)
                {
                    SoundManager.PlayEffect(JetPackSound, AudioEmitter);
                }
                if (MGInputs.IsPressed(MGInputNames.Space))
                {
                    Fall -= JetPackAccel * second;
                    SpriteSheet.PlayAnimation(ANIMATION_JET, fromStart: false);
                } else
                {
                    JetPackOn = false;
                }

            }
        }

        protected void CheckJump(GameTime gameTime)
        {
            if (MGInputs.IsPressed(MGInputNames.Space))
            {
                if (OnGround)
                {
                    Fall -= JumpForce;
                    if (JumpSound is string)
                    {
                        SoundManager.PlayEffect(JumpSound, AudioEmitter);
                    }
                }
                if (gameTime.TotalGameTime - lastJumpPress < JetPackActivation)
                {
                    JetPackOn = true;
                }
                lastJumpPress = gameTime.TotalGameTime;
            }
        }

        protected float CheckLateral(float second)
        {
            float lateralMove = (MGInputs.GetState(MGInputNames.D) - MGInputs.GetState(MGInputNames.Q)) * Speed * second;

            if (lateralMove == 0)
            {
                if (!JetPackOn)
                {
                    SpriteSheet.PlayAnimation(ANIMATION_STAND, fromStart: false);
                }
            } else
            {
                if (MoveSound is string)
                {
                    SoundManager.PlayEffect(MoveSound, AudioEmitter);
                }
                if (!JetPackOn)
                {
                    SpriteSheet.PlayAnimation(ANIMATION_MOVE, fromStart: false);
                }

                Effects = lateralMove > 0 ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            }

            return lateralMove * GetMovementCoef();
        }

        protected void CheckAlive()
        {
            if (Alive && Heal <= 0)
            {
                if (DeathSound is string)
                {
                    SoundManager.PlayEffect(DeathSound, AudioEmitter);
                }
                Alive = false;
                SpriteSheet.PlayAnimation(ANIMATION_DEAD);
                JetPackOn = false;
                SoundManager.PlayDeath();
            }
        }


        public static bool ColideBlock(MGPolygon hitBox, World world, Vector2 worldPosition)
        {
            Vector2 hitBoxSize = hitBox.BoundingBoxSize / Block.BLOCK_SIZE_PX + Vector2.One;
            Point topLeftBlock = BlockRelativePositionActor.WorldPositionToBlockPosition(hitBox.PositionedMin(worldPosition));
            bool colide = false;
            for (int x = 0; x <= hitBoxSize.X && !colide; x++)
            {
                for (int y = 0; y <= hitBoxSize.Y && !colide; y++)
                {
                    ForegroundBlock block = world[topLeftBlock + new Point(x, y)].ForegroundBlock;
                    colide = block.HitBox.Intersects(hitBox, block.WorldPosition, worldPosition);
                }
            }
            return colide;
        }

        protected Vector2 CheckValidMovement(float lateralMove)
        {
            Vector2 validMovement = Vector2.Zero;

            if (lateralMove != 0)
            {
                if (!ColideBlock(HitBox, World, WorldPosition + new Vector2(lateralMove, 0)))
                {
                    validMovement.X = lateralMove;
#pragma warning disable CS0162 // Code inaccessible détecté
                    if (!INVINCIBLE)
                    {
                        Heal -= Math.Abs(lateralMove * MoveDamage);
                    }
                }
            }

            if (Fall != 0)
            {
                bool colide = ColideBlock(HitBox, World, WorldPosition + new Vector2(validMovement.X, Fall));
                OnGround = colide && Fall > 0;
                if (!colide)
                {
                    validMovement.Y = Fall;
                } else
                {
                    if (Alive && Fall > MIN_FALL_SPEED_FOR_DAMAGE && !INVINCIBLE)
                    {
                        Heal -= Fall * Fall * FallDamage;
                    }
#pragma warning restore CS0162 // Code inaccessible détecté
                }
            }

            return validMovement;
        }

        public void PreUptate(GameTime gameTime)
        {
            float second = (float)gameTime.ElapsedGameTime.TotalSeconds;

            float lateralMove = 0;

            if (Alive)
            {
                lateralMove = CheckLateral(second);

                CheckJump(gameTime);

                CheckJetPack(second);
            }
            Fall += World.Gravity * second;

            Movement = CheckValidMovement(lateralMove);

            Fall = MGMath.Clamp(-JetPackMaxSpeed, Movement.Y, FallMaxSpeed);
            if (Fall > 0)
            {
                Fall *= GetMovementCoef();
            }
            
            WorldPosition += Movement;
            if (Alive)
            {
                var f = MyBlock?.ForegroundBlock;
                Heal -= f?.Liquid?.PlayerDamage ?? 0f + f?.Gaz?.PlayerDamage ?? 0f;
            }
            CheckAlive();
        }
        public override bool IsDirty()
        {
            return true;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            //new MGPositionedPolygon(HitBox, ScreenPosition).Draw(gameTime, spriteBatch);
        }
    }
}

[Serializable]
public class WallECharacter : CharacterDisplay
{
    public WallECharacter(World world) : base(world)
    {
        Construct();
    }

    protected WallECharacter(SerializationInfo info, StreamingContext context) : base(info, context)
    {
        Construct();
    }

    private void Construct()
    {
        FallMaxSpeed = 100;

        HealMax = Heal = 1000;

        MoveDamage = Heal / (2000 * Block.BLOCK_SIZE_PX);
        FallDamage = 1f;


        JetPackMaxSpeed = 20;
        JetPackAccel = 0;
        JumpForce = 5;

        MiningEfficiency = 1.5f;

        Speed = 10 * Block.BLOCK_SIZE_PX;

        Vector2 boxSize = new Vector2(Block.BLOCK_SIZE_PX - 10, Block.BLOCK_SIZE_PX - 5);
        HitBox = new MGRectangle(boxSize / 2 + new Vector2(0, -2.5f), boxSize);
    }

    protected override MGSpriteSheet CreateSpriteSheet()
    {
        return new MGSpriteSheet(
                MGSpriteAnimation.NewFromTileTexture(ANIMATION_STAND, RessourceManager.Load<Texture2D>("Character/Robot1"), new Point(64), 6, new Vector2(32)),
                MGSpriteAnimation.NewFromTileTexture(ANIMATION_MOVE, RessourceManager.Load<Texture2D>("Character/Robot1_move"), new Point(64), 6, new Vector2(32)),
                MGSpriteAnimation.NewSingleFrameFromTexture(ANIMATION_DEAD, RessourceManager.Load<Texture2D>("Character/Robot1_death"), new Vector2(32)),
                MGSpriteAnimation.NewFromTileTexture(ANIMATION_JET, RessourceManager.Load<Texture2D>("Character/Robot1"), new Point(64), 6, new Vector2(32)));
    }
}

[Serializable]
public class EveCharacter : CharacterDisplay
{
    public EveCharacter(World world) : base(world)
    {
        Construct();
    }

    protected EveCharacter(SerializationInfo info, StreamingContext context) : base(info, context)
    {
        Construct();
    }

    private void Construct()
    {
        FallMaxSpeed = 5;

        HealMax = Heal = 800;

        MoveDamage = Heal / (10000 * Block.BLOCK_SIZE_PX);
        FallDamage = 0.01f;

        JetPackMaxSpeed = 5;
        JetPackAccel = 30;
        JumpForce = 1;

        Speed = 20 * Block.BLOCK_SIZE_PX;

        JetPackActivation = TimeSpan.MaxValue;

        Vector2 boxSize = new Vector2(Block.BLOCK_SIZE_PX * 0.5f, Block.BLOCK_SIZE_PX * 0.7f);
        HitBox = new MGRectangle(boxSize / 2, boxSize);
    }


    protected override MGSpriteSheet CreateSpriteSheet()
    {
        return new MGSpriteSheet(
                MGSpriteAnimation.NewFromTileTexture(ANIMATION_STAND, RessourceManager.Load<Texture2D>("Character/Robot2"), new Point(64), 6, new Vector2(32)),
                MGSpriteAnimation.NewFromTileTexture(ANIMATION_MOVE, RessourceManager.Load<Texture2D>("Character/Robot2_move"), new Point(64), 6, new Vector2(32)),
                MGSpriteAnimation.NewFromTileTexture(ANIMATION_DEAD, RessourceManager.Load<Texture2D>("Character/Robot2_death"), new Point(64), 6, new Vector2(32)),
                MGSpriteAnimation.NewFromTileTexture(ANIMATION_JET, RessourceManager.Load<Texture2D>("Character/Robot2_fly"), new Point(64), 6, new Vector2(32)));
    }
}