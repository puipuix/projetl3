﻿using Microsoft.Xna.Framework;
using MGCE.Engine;
using MGCE.Input;
using Hephaestus.Code.Blocks.Fluids;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Blocks.Tuyau_Objet;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using Hephaestus.Code.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hephaestus.Code.Blocks.Utilitaire;
using Microsoft.Xna.Framework.Audio;

namespace Hephaestus.Code.Character
{
    public class Player
    {
        public static Player player;
        public CharacterDisplay Character { get; set; }
        public Stockage inventory, toolBar;
        public ItemType currentSelection;
        public World world;
        public Orientation orientation = Orientation.DOWN;
        Game game;
        InventoryView opened;
        ProductionView prodView;
        bool autorized = true, devMode = false;
        private Point p;

        public int BlockDestroySpeed { get; set; } = 2;
        public string DamageSound { get; set; } = "Sound/pickaxe";
        public float DamageSoundVolume { get; set; } = 1;

        public bool openedWindow()
        {
            return (!opened.hidden() || !prodView.hidden());
        }
        /// <summary>
        /// demande la fermeture de toute les fenetres interactive
        /// </summary>
        public void closeWindow()
        {
            opened.Hide();
            prodView.Hide();
        }

        public Player(Game g, World w, CharacterDisplay ch, Stockage inv = null)
        {
            opened = new InventoryView(game);
            prodView = new ProductionView(game);
            player = this;
            world = w;
            if (inv == null)
            {
                inv = new Stockage(30);
            }
            game = g;
            Character = ch;
            inventory = inv;
            toolBar = new Stockage(8);
            for (int i = 0; i < 8 && i < inv.GetAll().Count; i++)//initialise la toolbar
            {
                toolBar.Add(inv.GetAll()[i].GetItemType(), inv.GetAll()[i].GetQuantite());
            }
            Hud.set(g);
            Hud.show(toolBar);
            PlacementBlock();
            DamageBlock();
        }

        /// <summary>
        /// permet de placer un  block
        /// </summary>
        private void PlacementBlock()
        {
            world.ClickManager.OnMouseClickEnd += (obj, args) => {
                if (args.Button == MGInputNames.M_Left && opened.hidden() && autorized && prodView.hidden())
                {
                    p = BlockRelativePositionActor.WorldPositionToBlockPosition(world.Camera.MouseWorldPosition);
                    TerrainBlock t = world[p];

                    bool used = false;
                    if (t.ForegroundBlock is Production)
                    {
                        opened.Hide();
                        prodView.Hide();
                        prodView.show(((Production)t.ForegroundBlock));
                    } else if (Player.player.currentSelection == ItemType.HEALWALLE)
                    {
                        player.Character.Heal = player.Character.HealMax;
                        used = true;
                    } else if (toolBar.getCountOf(currentSelection) > 0)
                    {
                        if (t.ForegroundBlock is AirBlock)
                        {
                            if (Player.player.currentSelection == ItemType.USINE)//ouverture des recette de l'usine
                            {
                                autorized = false;
                                MGDebug.PrintLine("call");
                                opened.show(Stockage.usine, true);
                            } else if (Player.player.currentSelection == ItemType.FOUR)//ouverture des recette du four
                                        {
                                autorized = false;
                                MGDebug.PrintLine("call");
                                opened.show(Stockage.four, true);
                            } else if (Player.player.currentSelection == ItemType.POWER_PLANT)
                            {
                                t.ReplaceForegroundBlock(new PowerPlant(p, world, ItemType.ENERGY));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.MINEUR && world[p] is RessourceTerrain ressource)
                            {
                                t.ReplaceForegroundBlock(new Mineur(p, world, ressource));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.PILE)
                            {
                                t.ReplaceForegroundBlock(new Pile(p, world));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.PIERRE)
                            {
                                t.ReplaceForegroundBlock(new StoneForegroundBlock(p, world));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.TUYAUX)
                            {
                                t.ReplaceForegroundBlock(new TuyauObjet(p, world, orientation));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.TUYAUX_COURBET1)
                            {
                                t.ReplaceForegroundBlock(new TuyauObjetCoudeT1(p, world, orientation));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.TUYAUX_COURBET2)
                            {
                                t.ReplaceForegroundBlock(new TuyauObjetCoudeT2(p, world, orientation));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.TUYAUX_CROIX)
                            {
                                t.ReplaceForegroundBlock(new TuyauObjetCroix(p, world, orientation));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.LAMP)
                            {
                                t.ReplaceForegroundBlock(new ElectricLamp(p, world));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.TORCHE)
                            {
                                t.ReplaceForegroundBlock(new TorchBlock(p, world));
                                used = true;
                            } else if (Player.player.currentSelection == ItemType.WALLE || Player.player.currentSelection == ItemType.EVE)
                            {
                                t.ReplaceForegroundBlock(new CharacterBlock(Player.player.currentSelection, p, world));
                                used = true;
                            } else
                            {

                                if (t.ForegroundBlock.AllowFluids)
                                {
                                    if (Player.player.currentSelection == ItemType.LAVE)
                                    {
                                        if (t.ForegroundBlock.Liquid is null)
                                        {
                                            Lava lava = new Lava(t, world, true);
                                            lava.Amount = 100;
                                        } else if (t.ForegroundBlock.Liquid is Lava lava && t.ForegroundBlock.FluidAmout + 100 < t.ForegroundBlock.MaxFluidAmount)
                                        {
                                            lava.Amount += 100;
                                        }
                                        used = true;
                                    } else if (Player.player.currentSelection == ItemType.EAU)
                                    {
                                        if (t.ForegroundBlock.Liquid is null)
                                        {
                                            Water water = new Water(t, world);
                                            water.Amount = 100;
                                        } else if (t.ForegroundBlock.Liquid is Water water && t.ForegroundBlock.FluidAmout + 100 < t.ForegroundBlock.MaxFluidAmount)
                                        {
                                            water.Amount += 100;
                                        }
                                        used = true;
                                    } else if (Player.player.currentSelection == ItemType.PETROL)
                                    {
                                        if (t.ForegroundBlock.Liquid is null)
                                        {
                                            Petrol petrol = new Petrol(t, world);
                                            petrol.Amount = 100;
                                        } else if (t.ForegroundBlock.Liquid is Petrol petrol && t.ForegroundBlock.FluidAmout + 100 < t.ForegroundBlock.MaxFluidAmount)
                                        {
                                            petrol.Amount += 100;
                                        }
                                        used = true;
                                    }
                                }
                            }
                        }
                    }
                    if (!devMode && used)
                    {
                        inventory.Remove(currentSelection);
                        toolBar.Remove(currentSelection);
                        Hud.hide();
                        Hud.show(toolBar);
                    }
                }
            };
        }

        private void DamageBlock()
        {
            world.ClickManager.OnMousePressed += (obj, args) => {
                if (args.Button == MGCE.Input.MGInputNames.M_Right)
                {
                    Point p = BlockRelativePositionActor.WorldPositionToBlockPosition(world.Camera.MouseWorldPosition);
                    var terrain = world.GetOrNull(p);
                    if (terrain?.ForegroundBlock is ForegroundBlock)
                    {
                        if (!(terrain.ForegroundBlock is AirBlock))
                        {
                            if (devMode && MGInputs.IsPressed(MGInputNames.LeftControl))
                            {
                                terrain.DamageForegroundBlock(ForegroundBlock.BLOCK_HEAL);
                            } else if (Character?.Alive ?? true)
                            {
                                if (DamageSound is string)
                                {
                                    SoundManager.PlayUniqueEffect(DamageSound, terrain.ForegroundBlock.AudioEmitter, DamageSoundVolume);
                                }
                                var loot = terrain.DamageForegroundBlock((int)(BlockDestroySpeed * (Character?.MiningEfficiency ?? 1f)));
                                if (loot != ItemType.NOTHING)
                                {
                                    inventory.Add(loot);
                                    toolBar.Add(loot);
                                    Hud.RefreshToolbar(toolBar);
                                }
                            }
                        }
                    }
                }
            };
        }

        /// <summary>
        /// notifie du choix de l'item a fabriquer par l'utilisateur
        /// </summary>
        /// <param name="t"></param>
        public void notifySelection(ItemType t)
        {
            bool used = false;
            if (currentSelection == ItemType.USINE)
            {
                MGDebug.PrintLine("notified");
                world[p].ReplaceForegroundBlock(new Usine(p, world, t, 100));
                used = true;
            } else if (currentSelection == ItemType.FOUR)
            {
                MGDebug.PrintLine("notified");
                world[p].ReplaceForegroundBlock(new Four(p, world, t, 100));
                used = true;
            }
            if (used)
            {
                inventory.Remove(currentSelection);
                toolBar.Remove(currentSelection);
                currentSelection = ItemType.NOTHING;
                Hud.hide();
                Hud.show(toolBar);
            }

            opened.Hide();
            autorized = true;

        }
    }
}
