﻿using MGCE.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MGCE.Actor;
using MGCE.Control;
using MGCE.Control.EventArguments;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Input;
using MGCE.Shapes;
using System.Linq;
using MGCE.Util.TimeLine;
using Hephaestus.Code;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Fluids;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Character;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using Hephaestus.Code.Util.Light;
using Hephaestus.Code.View;
using System;
using System.Runtime;
using System.Text;
using static Hephaestus.Code.Util.Const;
using Hephaestus.Code.Blocks.Utilitaire;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Hephaestus;

namespace Hephaestus.Code
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MyGame : Game
    {

        public static MyGame Game { get; private set; }
        public Color BackGroundColor { get; set; } = new Color(104, 104, 104);
        MGFPSDisplay FPSDisplay { get; set; }
        public MyGame()
        {
            Game = this;
            Content.RootDirectory = "Content";

            MGEngine.Create(this, Settings.Default.ScreenSize.ToMGPoint(), false, false, false, true, true);
            MGEngine.SetTargetedFps(60);
            MGEngine.OnBufferSizeChange += (o, args) => {
                Settings.Default.ScreenSize = MGEngine.BufferSize.ToMGPoint();
                Settings.Default.Save();
            };
            IsMouseVisible = true;

            IsFixedTimeStep = true;

            GCSettings.LatencyMode = GCLatencyMode.LowLatency;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            MGEngine.Initialize(Settings.Default.Language, "Language");

            MGEvent<ILightPreUpdate>.Create().DrawEvent = true;
            MGEvent<ILightPreUpdate>.Get().CallOrder = MGConst.ORDER_1_BEFORE;
            MGEvent<ILightPreUpdate>.Get().TimeOutMs = 10;
            MGEvent<ILightPreUpdate>.Get().MakeEventAlwaysAsked();

            MGEvent<ILightUpdate>.Create().DrawEvent = true;
            MGEvent<ILightUpdate>.Get().CallOrder = MGConst.ORDER_3_DEFAULT;
            MGEvent<ILightUpdate>.Get().MakeEventAlwaysAsked();

            MGEvent<ILightPostUpdate>.Create().DrawEvent = true;
            MGEvent<ILightPostUpdate>.Get().CallOrder = MGConst.ORDER_5_AFTER;
            MGEvent<ILightPostUpdate>.Get().MakeEventAlwaysAsked();

            MGEvent<IRessourceChangeEventListener>.Create();

            MGEvent<IFluidUpdate>.Create<GameTime>().TimeOutMs = 10;
            MGEvent<IFluidUpdate>.Get().CallOrder = MGConst.DRAW_4_DEFAULT;
            MGEvent<IFluidUpdate>.Get().MakeEventAlwaysAsked(() => MGEngine.UpdateGameTime);

            MGEngine.SpriteBatchParameters[IHM_DRAW_ORDER] = new MGSpriteBatchParameter(SpriteSortMode.Deferred);
            MGEngine.SpriteBatchParameters[DEBUG_DRAW_ORDER] = new MGSpriteBatchParameter(SpriteSortMode.Deferred);

            Stockage.init();
            SoundManager.Initialize();

            base.Initialize();
        }

        private void Build()
        {
            LoadingScreen screen = new LoadingScreen();
            MGEngine.WindowForm.Controls.Add(screen);
            MGEngine.WindowForm.Show();

            MGMultipleContentBuilder cb = new MGMultipleContentBuilder(
                new MGContentBuilder(@"Content/RessourcePacks/Default/Content.mgcb", @"../../cache/RessourcePacks", true),
                new MGContentBuilder(@"Content/Content.mgcb", @"cache", false)
                );

            screen.ProgressBar.Maximum = cb.ContentBuilders.SelectMany(c => c.ContentFound).Count() * 2;
            screen.ProgressBar.Step = 1;
            cb.OnAnyOutputDataReceived += (o, args) => {
                if (!string.IsNullOrWhiteSpace(args.Text) && !args.Text.StartsWith(' ') && !args.Text.StartsWith('\t') && !args.Text.Contains("error"))
                {
                    MGDebug.PrintLine(args.Text);
                    string[] strs = args.Text.Split('/');
                    if (strs.Length <= 1)
                    {
                        screen.LoadingText = args.Text;
                    } else
                    {
                        screen.LoadingText = (strs[0].StartsWith("Clean") ? "Cleaning: " : "Building: ") + strs[^1];
                        screen.ProgressBar.PerformStep();
                    }
                }
            };

            cb.OnAllEnd += (o, args) => {
                Hud.Create(this);
                ForegroundBlock.Initialize();
                MainMenu.ShowMain();
                screen.Dispose();                
                MGDebug.PrintLine(RessourceManager.GetFileSystemPath("Autre/hephaestus", Content) + ".ico");
                if (File.Exists(RessourceManager.GetFileSystemPath("Autre/hephaestus", Content) + ".ico"))
                {
                   MGEngine.WindowForm.Icon = new System.Drawing.Icon(RessourceManager.GetFileSystemPath("Autre/hephaestus", Content) + ".ico");
                }
            };

            cb.AsyncBuildContent(block: false);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            MGEngine.LoadContent(); // Créé le SpriteBatch et charge ce qui on demandé de charger
            FPSDisplay = new MGFPSDisplay(MGEngine.Arial, this) {
                Position = new Vector2(10),
                Color = Color.Blue,
                DrawOrder = IHM_DRAW_ORDER
            };
            Build();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            MGEngine.UnloadContent();
            // TODO: Unload any non ContentManager content here
        }
      

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // echape pour fermer

            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            //  Exit();

           
           

            if (MGInputs.IsPressEnd(MGInputNames.F3))
            {
                if (FPSDisplay.Visible)
                {
                    FPSDisplay.Hide();
                } else
                {
                    FPSDisplay.Show();
                }
            }


           


            MGEngine.Update(gameTime); // appel tout les événements demandé (Update est toujours demandé)
                                       //PPGEvent.Get<IPPGEventUpdateListener>().Listeners
            SoundManager.Update(gameTime);
            base.Update(gameTime);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(BackGroundColor);
            MGEngine.DrawAll(gameTime ?? new GameTime()); // dessine tout ce qui on besoins de l'être
                                                          //PPGEngine.ToDraws

            base.Draw(gameTime);
        }
    }
}
