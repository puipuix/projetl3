﻿using Hephaestus.Code.Blocks.Terrain;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using MGCE.Util;
using Hephaestus.Code.Blocks;
using MGCE.Engine;

namespace Hephaestus.Code.Util.Light
{
    public static class QuadirectionalLightPropagation
    {
        public static void LightPropagate(ILightSource source, World world, Point myPos, Vector3 lastTr, float myDist, Point mainDir, Point altDir, bool main, bool skip = true)
        {
            Vector3 myTrans = lastTr * world[myPos].ForegroundBlock.LightTransparancy;
            LightData data = new LightData(myTrans, myDist);
            if (!skip && LightUpdateCache.ShouldStop(source, myPos, data))
            {
                return;
            }
            LightUpdateCache.SetData(myPos, data);
            if (main)
            {
                LightPropagate(source, world, myPos + mainDir, myTrans, myDist + Block.BLOCK_SIZE_PX, mainDir, altDir, true, false);
                LightPropagate(source, world, myPos + mainDir + altDir, myTrans, myDist + Block.BLOCK_DIAGONAL_PX, mainDir + altDir, mainDir + altDir, false, false);
                LightPropagate(source, world, myPos + mainDir - altDir, myTrans, myDist + Block.BLOCK_DIAGONAL_PX, mainDir - altDir, mainDir - altDir, false, false);
            }
            else
            {
                LightPropagate(source, world, myPos + mainDir, myTrans, myDist + Block.BLOCK_DIAGONAL_PX, mainDir, altDir, false, false);
            }
        }

        public static void LightPropagate(ILightSource source, World world, List<(Point, LightData)> affected)
        {
            LightUpdateCache.PrepareLightUpdate(source);
            Point startBlock = BlockRelativePositionActor.WorldPositionToBlockPosition(source.LightWorldPosition);
            Vector3 startTrans = world[startBlock].ForegroundBlock.LightTransparancy;
            float distance = 0;
            LightUpdateCache.SetData(startBlock, new LightData(startTrans, distance));

            Point top = new Point(0, -1), left = new Point(-1, 0), right = new Point(1, 0), bottom = new Point(0, 1);
            LightPropagate(source, world, startBlock, startTrans, distance, top, right, true);
            LightPropagate(source, world, startBlock, startTrans, distance, bottom, left, true);
            LightPropagate(source, world, startBlock, startTrans, distance, right, bottom, true);
            LightPropagate(source, world, startBlock, startTrans, distance, left, top, true);

            LightUpdateCache.GetExistingData(affected);
        }
    }
}
