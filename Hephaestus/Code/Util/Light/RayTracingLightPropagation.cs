﻿using Microsoft.Xna.Framework;
using MGCE.Util;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using System;
using System.Collections.Generic;
using MGCE.Input;

namespace Hephaestus.Code.Util.Light
{
    public static class RayTracingLightPropagation
    {
        private static List<(Point, Vector3)> NextMalus(World world, Point myPos, Point myDir, Vector3 myTrans, List<(Point, Vector3)> malus)
        {
            List<(Point, Vector3)> nextMalus = new List<(Point, Vector3)>();
            foreach (var tuple in malus)
            {
                Point direction = tuple.Item1;
                Vector3 malusDir = (myDir == direction.Minus()) ? Vector3.Zero : tuple.Item2 * myTrans;
                nextMalus.Add((direction, malusDir * world[myPos + direction].ForegroundBlock.LightTransparancy));
            }
            return nextMalus;
        }

        public static void LightPropagate(ILightSource source, World world, List<(Point, LightData)> affected)
        {
            LightUpdateCache.PrepareLightUpdate(source);
            
            Point startBlock = BlockRelativePositionActor.WorldPositionToBlockPosition(source.LightWorldPosition);
            Vector3 startTrans = world[startBlock].ForegroundBlock.LightTransparancy;
            float distance = Vector2.Distance(BlockRelativePositionActor.BlockPositionToWorldPosition(startBlock), source.LightWorldPosition);
           
            List<(Point, Vector3)> startMalus = new List<(Point, Vector3)>()
            {
                (new Point(0, -1), Vector3.One),
                (new Point(0, 1), Vector3.One),
                (new Point(1, 0), Vector3.One),
                (new Point(-1, 0), Vector3.One),

                (new Point(-1, -1), Vector3.One),
                (new Point(1, 1), Vector3.One),
                (new Point(-1, 1), Vector3.One),
                (new Point(1, -1), Vector3.One),
            };
            LightPropagate(startBlock, Point.Zero, distance, startTrans, source, world, startMalus);
           
            LightUpdateCache.GetExistingData(affected);
        }

        private static void LightPropagate(Point myPos, Point myDir, float myDist, Vector3 myTrans, ILightSource source, World world, List<(Point, Vector3)> malus)
        {
            LightData data = new LightData(myTrans, myDist);
            if (LightUpdateCache.ShouldStop(source, myPos, data))
            {
                return;
            }

            LightUpdateCache.SetData(myPos, data);


            List<(Point, Vector3)> nextMalus = NextMalus(world, myPos, myDir, myTrans, malus);
            foreach (var tuple in nextMalus)
            {
                Point direction = tuple.Item1;
                Vector3 malusDir = tuple.Item2;
                LightPropagate(myPos + direction, direction, myDist + direction.ToVector2().Length() * Block.BLOCK_SIZE_PX, malusDir, source, world, nextMalus);
            }
        }
    }
}
