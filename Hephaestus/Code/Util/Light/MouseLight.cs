﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MGCE.Engine;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Interfaces;
using MGCE.Actor;
using Hephaestus.Code.Blocks.Terrain;
using MGCE.Util;

namespace Hephaestus.Code.Util.Light
{
    public class MouseLight : CameraFollowerActor, ILightSource
    {
        public World World { get; }
        public Color LightColor { get; set; }
        public float Attenuation { get; set; }
        public float Range { get; set; }
        public bool NeedLightUpdate { get; set; }
        public Vector2 LastWorldPosition { get; private set; }
        public List<TerrainBlock> AffectedBlocks { get; } = new List<TerrainBlock>();
        public int VisibleAffectedBlock { get; set; }
        public virtual Vector2 LightWorldPosition => WorldPosition;

        public float LightAngle => MathHelper.Pi;

        public Vector2 LightDirection => Vector2.UnitX;
        public bool IsProtectingBlock => true;
        public MouseLight(Color lightColor, float attenuation, float range, World world) : base(world.Camera)
        {
            World = world;
            World.OnGC += World_OnGC;
            LightColor = lightColor;
            Attenuation = attenuation;
            Range = range;
            StartListeningEvent<ILightPreUpdate>();
        }

        private void World_OnGC(object sender, EventArgs e)
        {
            NeedLightUpdate = true;
            MGDebug.PrintLine("ongc");
        }

        public override void Uptate(GameTime gameTime)
        {
            base.Uptate(gameTime);
            WorldPosition = Camera.MouseWorldPosition;
        }

        public void LightPreUpdate()
        {
            if (NeedLightUpdate || Vector2.Distance(WorldPosition, LastWorldPosition) > 10)
            {
                NeedLightUpdate = false;
                this.AffectBlocks();
                LastWorldPosition = WorldPosition;
            }
        }

        public void RequestLightUpdate()
        {
            NeedLightUpdate = true;
        }

        public void AffectBlocks()
        {
            ILightSource.DefaultAffectBlocks(this);
        }
    }
}
