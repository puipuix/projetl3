﻿using Microsoft.Xna.Framework;
using MGCE.Events;
using MGCE.Util;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Actor;
using Hephaestus.Code.Util;
using MGCE.Engine;
using Hephaestus;

namespace Hephaestus.Code.Util.Light
{
    public interface ILightPreUpdate : IMGEventListener
    {
        void LightPreUpdate();
    }

    public interface ILightUpdate : IMGEventListener
    {
        void LightUpdate();
    }

    public interface ILightPostUpdate : IMGEventListener
    {
        void LightPostUpdate();
    }

    public interface ILightSource : ILightPreUpdate, IMGEventsListenerActor
    {
        public static float GlobalBrightness { get; set; } = Settings.Default.Brightness;

        #region Need Implementations
        World World { get; }

        List<TerrainBlock> AffectedBlocks { get; }
        int VisibleAffectedBlock { get; set; }

        float LightAngle { get; }
        Vector2 LightDirection { get; }

        Vector2 LightWorldPosition { get; }
        Color LightColor { get; }
        /// <summary>
        /// 1 : linear, 2 exponential etc...
        /// </summary>
        float Attenuation { get; }
        float Range { get; }
        bool IsProtectingBlock { get; }
        void RequestLightUpdate();

        #endregion

        #region Static Default Implementations
        public static void DefaultUnAffectBlocks(ILightSource source)
        {
            foreach (TerrainBlock block in source.AffectedBlocks)
            {
                block.RemoveLightSource(source);
            }
            source.AffectedBlocks.Clear();
            source.VisibleAffectedBlock = 0;
        }

        public static void DefaultAffectBlocks(ILightSource source)
        {
            source.UnAffectBlocks();

            List<(Point, LightData)> affected = new List<(Point, LightData)>();
            QuadirectionalLightPropagation.LightPropagate(source, source.World, affected);

            foreach ((TerrainBlock block, LightData data) in affected.Select(t => (source.World[t.Item1], t.Item2)))
            {
                block.AddLightSource(source, data);
                source.AffectedBlocks.Add(block);
                if (block.Visible)
                {
                    source.VisibleAffectedBlock++;
                }
                source.CheckNeedPreUpdate();
            }
        }

        public static void DefaultCheckNeedPreUpdate(ILightSource source)
        {
            if (source.VisibleAffectedBlock > 0)
            {
                source.StartListeningEvent<ILightPreUpdate>();
            }
            else
            {
                source.StopListeningEvent<ILightPreUpdate>();
            }
        }

        public static void DefaultIncreaseVisibleAffectedBlock(ILightSource source)
        {
            source.VisibleAffectedBlock++;
            source.CheckNeedPreUpdate();
        }

        public static void DefaultDecreaseVisibleAffectedBlock(ILightSource source)
        {
            source.VisibleAffectedBlock--;
            source.CheckNeedPreUpdate();
        }

        public static float DefaultLightPower(ILightSource source, LightData data)
        {
            return Math.Max(0.0f, 1.0f - MathF.Pow(data.Distance / source.Range, source.Attenuation));
        }

        public static Vector3 DefaultGetLightEmission(ILightSource source, Vector2 position, LightData data)
        {
            return source.LightColor.ToVector3() * data.Transparency * source.LightPower(data);
        }

        #endregion

        #region Use Static Default Implementations

        public void UnAffectBlocks()
        {
            DefaultUnAffectBlocks(this);
        }

        public void AffectBlocks()
        {
            DefaultAffectBlocks(this);
        }

        public void CheckNeedPreUpdate()
        {
            DefaultCheckNeedPreUpdate(this);
        }

        public void IncreaseVisibleAffectedBlock()
        {
            DefaultIncreaseVisibleAffectedBlock(this);
        }

        public void DecreaseVisibleAffectedBlock()
        {
            DefaultDecreaseVisibleAffectedBlock(this);
        }

        public float LightPower(LightData data)
        {
            return DefaultLightPower(this, data);
        }

        public Vector3 GetLightEmission(Vector2 position, LightData data)
        {
            return DefaultGetLightEmission(this, position, data);
        }

        #endregion

        #region Utils

        public static void RequestAllLightsUpdate(IEnumerable<ILightSource> lights)
        {
            lights.ForEach(k => k.RequestLightUpdate());
        }

        #endregion
    }

    public abstract class LightSourceActor : ILightSource
    {
        public virtual World World { get; set; }

        public virtual List<TerrainBlock> AffectedBlocks { get; set; }

        public virtual int VisibleAffectedBlock { get; set; }

        public virtual float LightAngle { get; set; }

        public virtual Vector2 LightDirection { get; set; }

        public virtual Vector2 LightWorldPosition { get; set; }

        public virtual Color LightColor { get; set; }

        public virtual float Attenuation { get; set; }

        public virtual float Range { get; set; }

        public virtual bool IsProtectingBlock { get; set; }

        HashSet<Type> IMGEventsListenerActor.InternalListenedEvents { get; } = new HashSet<Type>();

        public virtual void UnAffectBlocks()
        {
            ILightSource.DefaultUnAffectBlocks(this);
        }

        public virtual void AffectBlocks()
        {
            ILightSource.DefaultAffectBlocks(this);
        }

        public virtual void CheckNeedPreUpdate()
        {
            ILightSource.DefaultCheckNeedPreUpdate(this);
        }

        public virtual void IncreaseVisibleAffectedBlock()
        {
            ILightSource.DefaultIncreaseVisibleAffectedBlock(this);
        }

        public virtual void DecreaseVisibleAffectedBlock()
        {
            ILightSource.DefaultDecreaseVisibleAffectedBlock(this);
        }

        public virtual float LightPower(LightData data)
        {
            return ILightSource.DefaultLightPower(this, data);
        }

        public virtual Vector3 GetLightEmission(Vector2 position, LightData data)
        {
            return ILightSource.DefaultGetLightEmission(this, position, data);
        }

        public abstract void RequestLightUpdate();
        public abstract void LightPreUpdate();
    }

    public static class LightUpdateCache
    {
        public static int MatrixSize { get; private set; } = 1;
        public static LightData?[] LightMatrixCache { get; private set; } = new LightData?[MatrixSize * MatrixSize];
        public static Point MatrixAlignment { get; private set; }
        public static void PrepareLightUpdate(ILightSource source)
        {
            Array.Fill(LightMatrixCache, null);
            int distance = (2 + (int)((source.Range + 1 / source.Attenuation) / Block.BLOCK_SIZE_PX)) * 2;
            if (MatrixSize < distance)
            {
                MatrixSize = distance;
                LightMatrixCache = new LightData?[MatrixSize * MatrixSize];
            }
            MatrixAlignment = new Point(MatrixSize / 2) - BlockRelativePositionActor.WorldPositionToBlockPosition(source.LightWorldPosition);
        }

        private static ArithmeticException GetError(Exception e, Point blockPos)
        {
            return new ArithmeticException("Error reading position: " + blockPos + " with alignment of: " + MatrixAlignment + " Resulting the index " + MGMath.ToSigleDim(blockPos + MatrixAlignment, MatrixSize) + " in a matrix of size: " + MatrixSize, e);
        }

        public static LightData? GetData(Point blockPos)
        {
            try
            {
                return LightMatrixCache[MGMath.ToSigleDim(blockPos + MatrixAlignment, MatrixSize)];
            }
            catch (Exception e)
            {

                throw GetError(e, blockPos);
            }
        }

        public static bool GetData(Point blockPos, out LightData data)
        {
            LightData? d = GetData(blockPos);
            data = d ?? new LightData();
            return d.HasValue;
        }

        public static void SetData(Point blockPos, LightData? data)
        {
            try
            {
                LightMatrixCache[MGMath.ToSigleDim(blockPos + MatrixAlignment, MatrixSize)] = data;
            }
            catch (Exception e)
            {

                throw GetError(e, blockPos);
            }
        }

        public static void GetExistingData(List<(Point, LightData)> affected)
        {
            for (int i = 0; i < LightMatrixCache.Length; i++)
            {
                if (LightMatrixCache[i].HasValue)
                {
                    affected.Add((MGMath.ToDoubleDim(i, MatrixSize) - MatrixAlignment, LightMatrixCache[i].Value));
                }
            }
        }

        public static bool ShouldStop(ILightSource source, Point pos, LightData newData, bool stopIfOlderBetter = true)
        {
            float current = MGMath.Max(newData.Transparency) * source.LightPower(newData);
            if (current < 0.01)
            {
                return true;
            }
            else if (stopIfOlderBetter && GetData(pos, out var oldData))
            {
                float oldBri = MGMath.Max(oldData.Transparency) * source.LightPower(oldData);
                if (oldBri >= current)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
