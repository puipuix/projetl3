﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hephaestus.Code.Util.Light
{
    public struct LightData
    {
        public float Distance { get; }
        public Vector3 Transparency { get; }

        public LightData(float? distance = null, Vector3? transparency = null)
        {
            Distance = distance ?? 0;
            Transparency = transparency ?? Vector3.One;
        }

        public LightData(Vector3? transparency = null, float? distance = null) : this(distance, transparency)
        {
        }

        public override string ToString()
        {
            return "{D: " + Distance + " T: " + Transparency + "}";
        }
    }
}
