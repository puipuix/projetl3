﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Util;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util;
using System.Windows.Forms;
using MGCE.Input;

namespace Hephaestus.Code.Util.Light
{
    public class SunLight : MGEventsListenerActor, ILightSource, IMGEventUpdateListener
    {
        public World World { get; set; }
        public Vector2 LightWorldPosition => new Vector2(0, int.MinValue);


        public List<TerrainBlock> AffectedBlocks { get; } = new List<TerrainBlock>();
        public int VisibleAffectedBlock { get; set; }

        public float LightAngle => 0;
        public Vector2 LightDirection => Vector2.UnitY;
        public Color LightColor { get; set; }
        public float Attenuation => 0;
        public float Range => 0;
        public bool NeedLightUpdate { get; set; }
        public int Depth { get; }
        public int Surface { get; }
        public bool IsProtectingBlock => false;
        public TimeSpan CurrentDuration
        {
            get => _currentDuration;
            set => _currentDuration = new TimeSpan(value.Ticks % CycleDuration.Ticks);
        }
        private TimeSpan _currentDuration;
        public TimeSpan CurrentTime
        {
            get => TimeSpan.FromTicks((CurrentDuration.Ticks + NightDuration.Ticks / 2) % CycleDuration.Ticks);
            set {
                long tmp = value.Ticks - NightDuration.Ticks / 2;
                CurrentDuration = TimeSpan.FromTicks(tmp < 0 ? tmp + CycleDuration.Ticks : tmp);
            }
        }
        public double TimeSpeed { get; set; } = 60;
        public TimeSpan CycleDuration { get; set; } = TimeSpan.FromDays(1);
        public TimeSpan DayDuration { get; set; } = TimeSpan.FromHours(10);
        public TimeSpan NightDuration { get; set; } = TimeSpan.FromHours(8);
        public TimeSpan EveningDawnDuration => TimeSpan.FromSeconds((CycleDuration - DayDuration - NightDuration).TotalSeconds / 2);

        public TimeSpan SunSetSTart => TimeSpan.Zero;
        public TimeSpan DayStart => EveningDawnDuration;
        public TimeSpan DawnStart => DayStart + DayDuration;
        public TimeSpan NightStart => CycleDuration - NightDuration;

        public Color SunSetColor { get; set; } = new Color(253, 96, 81);
        public Color DayColor { get; set; } = Color.White;
        public Color DawnColor { get; set; } = new Color(250, 123, 98);
        public Color NightColor { get; set; } = Color.Black;

        public enum DayTime { SUNSET, DAY, MIDDAY, DAWN, NIGHT, MIDNIGHT, NONE };
        public DayTime CurrentDayTime { get; private set; } = DayTime.NONE;

        public float MinDiffToUpdate { get; set; } = 0.001f;

        public bool PlayMusics { get; set; } = true;

        HashSet<Type> IMGEventsListenerActor.InternalListenedEvents { get; } = new HashSet<Type>();

        public SunLight(World world, int maxDepthCoor = TerrainFactory.UNDERGROUND_LIMIT_Y, int surfaceCoor = TerrainFactory.OCEAN_LEVEL)
        {
            World = world;
            Depth = maxDepthCoor - surfaceCoor;
            Surface = surfaceCoor;
            world.OnTerrainGenerated += (o, args) => {
                CheckLight(args.Block);
            };

        }

        private void CheckLight(TerrainBlock t)
        {
            Point p = t.BlockPosition;
            if (p.Y < Depth + Surface)
            {
                t.AddLightSource(this, new LightData(t.ForegroundBlock.LightTransparancy, 0f));
                AffectedBlocks.Add(t);
            }
        }

        public void Refresh()
        {
            World.LoadedTerrain.ForEach(p => CheckLight(p.Value));
        }

        public void Activate()
        {
            StartListeningEvent<IMGEventUpdateListener>();
            StartListeningEvent<ILightPreUpdate>();
        }

        public void Disactivate()
        {
            StopListeningAll();
        }

        public Vector3 GetLightEmission(Vector2 position, LightData data)
        {
            float depthPercent = MGMath.Clamp(0, (position.Y - Surface * Block.BLOCK_SIZE_PX) / (Depth * Block.BLOCK_SIZE_PX), 1f);
            return Vector3.Lerp(LightColor.ToVector3(), Vector3.Zero, depthPercent);
        }

        private Color LerpSunLight(TimeSpan timeSpan, Color before, Color inter, Color after)
        {
            float current = (float)timeSpan.TotalSeconds;
            float ttl = (float)EveningDawnDuration.TotalSeconds;
            if (current < ttl / 2)
            {
                return new Color(Vector3.Lerp(before.ToVector3(), inter.ToVector3(), current * 2 / ttl));
            } else
            {
                return new Color(Vector3.Lerp(inter.ToVector3(), after.ToVector3(), (current * 2 - ttl) / ttl));
            }
        }
        public void LightPreUpdate()
        {
            Color next;
            if (CurrentDuration < DayStart)
            {
                next = LerpSunLight(CurrentDuration, NightColor, SunSetColor, DayColor);
                if (CurrentDayTime != DayTime.SUNSET)
                {
                    CurrentDayTime = DayTime.SUNSET;
                    if (PlayMusics)
                    {
                        SoundManager.PlayMorning();
                    }
                }
            } else if (CurrentDuration < DawnStart)
            {
                next = DayColor;
                if (CurrentDayTime != DayTime.DAY && CurrentDayTime != DayTime.MIDDAY)
                {
                    NeedLightUpdate = true;
                    CurrentDayTime = DayTime.DAY;
                    if (PlayMusics)
                    {
                        SoundManager.PlayRandom();
                    }
                }
                if (CurrentDayTime == DayTime.DAY && CurrentDuration > DayStart + DayDuration / 2)
                {
                    CurrentDayTime = DayTime.MIDDAY;
                    if (PlayMusics)
                    {
                        SoundManager.PlayRandom();
                    }
                }
            } else if (CurrentDuration < NightStart)
            {
                next = LerpSunLight(CurrentDuration - DawnStart, DayColor, DawnColor, NightColor);
                if (CurrentDayTime != DayTime.DAWN)
                {
                    CurrentDayTime = DayTime.DAWN;
                    if (PlayMusics)
                    {
                        SoundManager.PlayFall();
                    }
                }
            } else
            {
                next = NightColor;
                if (CurrentDayTime != DayTime.NIGHT && CurrentDayTime != DayTime.MIDNIGHT)
                {
                    NeedLightUpdate = true;
                    CurrentDayTime = DayTime.NIGHT;
                    if (PlayMusics)
                    {
                        SoundManager.PlayRandom();
                    }
                }
                if (CurrentDayTime == DayTime.NIGHT && CurrentDuration > NightStart + NightDuration / 2)
                {
                    CurrentDayTime = DayTime.MIDNIGHT;
                    if (PlayMusics)
                    {
                        SoundManager.PlayRandom();
                    }
                }
            }
            if (NeedLightUpdate || LightColor.ToVector3().DistanceSquare(next.ToVector3()) > MinDiffToUpdate)
            {
                NeedLightUpdate = false;
                LightColor = next;
                AffectedBlocks.ForEach(b => b.NotifyLightChange());
            }
        }

        public void Uptate(GameTime gameTime)
        {
            CurrentDuration += TimeSpan.FromSeconds(gameTime.ElapsedGameTime.TotalSeconds * TimeSpeed);
        }

        public void RequestLightUpdate() { }

    }
}
