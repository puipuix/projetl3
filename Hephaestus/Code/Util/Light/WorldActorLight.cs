﻿using Microsoft.Xna.Framework;
using MGCE.Util;
using MGCE.Actor;
using Hephaestus.Code.Blocks.Terrain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Input;

namespace Hephaestus.Code.Util.Light
{
    public class WorldActorLight : MGEventsListenerActor, ILightSource, IMGDisposable
    {
        public World World { get; private set; }
        public Color LightColor { get; set; }
        public float Attenuation { get; set; }
        public float Range { get; set; }
        public bool NeedLightUpdate { get; set; }
        public Vector2 LastWorldPosition { get; private set; }
        public List<TerrainBlock> AffectedBlocks { get; } = new List<TerrainBlock>();
        public int VisibleAffectedBlock { get; set; }
        public float LightAngle => MathHelper.Pi;
        public Vector2 LightDirection => Vector2.UnitX;
        public virtual Vector2 LightWorldPosition => WorldPosition;
        public CameraFollowerActor Target { get; set; }
        public Vector2 WorldPosition => Target.WorldPosition;
        public float DistanceToMove { get; set; } = 10f;
        public bool IsProtectingBlock => true;
        public bool IsEnable { get; private set; }
        public bool IsDisposed { get; private set; }

        public WorldActorLight(CameraFollowerActor target, Color lightColor, float attenuation, float range, World world)
        {
            World = world;
            World.OnGC += World_OnGC;
            Target = target;
            LightColor = lightColor;
            Attenuation = attenuation;
            Range = range;
        }

        private void World_OnGC(object sender, EventArgs e)
        {
            RequestLightUpdate();
        }

        public void LightPreUpdate()
        {
            if (NeedLightUpdate || Vector2.Distance(WorldPosition, LastWorldPosition) > DistanceToMove)
            {
                NeedLightUpdate = false;
                AffectBlocks();
                LastWorldPosition = WorldPosition;
            }
        }

        public void RequestLightUpdate()
        {
            NeedLightUpdate = true;
            StartListeningEvent<ILightPreUpdate>();
        }

        public void AffectBlocks()
        {
            ILightSource.DefaultAffectBlocks(this);
        }

        public void Enable()
        {
            if (!IsEnable)
            {
                IsEnable = true;
                StartListeningEvent<ILightPreUpdate>();
            }
        }

        public void Disable()
        {
            if (IsEnable)
            {
                IsEnable = false;
                ILightSource.DefaultUnAffectBlocks(this);
                StopListeningEvent<ILightPreUpdate>();
            }
        }

        public void Dispose()
        {
            Disable();
            World.OnGC -= World_OnGC;
            World = null;
            Target = null;
        }
    }
}
