﻿using MGCE.Util;
using Hephaestus.Code.Items;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Hephaestus.Code.util
{
    [Serializable]
    public class Filtre : IMGSerializable
    {
        private List<ItemType> listeFiltre;

        public Filtre(List<ItemType> _l)
        {
            listeFiltre = _l;
        }

        public Filtre(SerializationInfo info, StreamingContext context) : this (info.GetValue<List<ItemType>>("listeFiltre"))
        {
        }

        public void Serialize(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("listeFiltre", listeFiltre);
        }

        public void OnDeserialization(object sender) { }

        /// <summary>
        /// ajoute un element au filtre
        /// </summary>
        /// <param name="_t"></param>
        /// <returns></returns>
        public bool AddElement(ItemType _t)
        {
            if (!listeFiltre.Contains(_t))
            {
                listeFiltre.Add(_t);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// retire un element du filtre
        /// </summary>
        /// <param name="_t"></param>
        /// <returns></returns>
        public bool RemoveElement(ItemType _t)
        {
            return listeFiltre.Remove(_t);
        }

        /// <summary>
        /// retourne si l'objet passe le filtre
        /// </summary>
        /// <param name="_t"></param>
        /// <returns></returns>
        public bool CanAdd(ItemType _t)
        {
            return listeFiltre == null ? true : listeFiltre.Contains(_t);
        }
    }
}