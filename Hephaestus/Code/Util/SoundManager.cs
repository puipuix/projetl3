﻿using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Text;
using MGCE.Util;
using Hephaestus.Code.Util;
using Microsoft.Xna.Framework;
using System.Linq;
using MGCE.Engine;
using Microsoft.Xna.Framework.Media;

namespace Hephaestus.Code.Util
{
    public class UniqueSoundEffectInstance
    {
        public SoundEffect SoundEffect { get; }
        public SoundEffectInstance Instance { get; }
        public AudioEmitter Emitter { get; } = new AudioEmitter();
        public Dictionary<AudioEmitter, TimeSpan> EmittersPosition { get; } = new Dictionary<AudioEmitter, TimeSpan>();

        public UniqueSoundEffectInstance(SoundEffect soundEffect, float volume)
        {
            SoundEffect = soundEffect;
            Instance = SoundEffect.CreateInstance();
            Instance.Volume = volume;
        }

        private void CalculatePositions(bool pauseIfNoEmitters = true)
        {
            Emitter.Position = EmittersPosition.Average(t => (t.Key.Position, 1 / (1 + Vector3.DistanceSquared(t.Key.Position, SoundManager.AudioListener.Position))));
            if (EmittersPosition.Count > 0)
            {
                if (Instance.State != SoundState.Playing)
                {
                    Instance.Play();
                }
                SoundManager.Apply3D(Instance, SoundManager.AudioListener, Emitter);
            } else if (Instance.State == SoundState.Playing)
            {
                if (pauseIfNoEmitters)
                {
                    Instance.Pause();
                } else
                {
                    Instance.Stop();
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            EmittersPosition.RemoveIf(kp => kp.Value < gameTime.TotalGameTime);
            CalculatePositions();
        }

        public void AddEmitter(AudioEmitter emitter)
        {

            EmittersPosition[emitter] = MGEngine.UpdateGameTime.TotalGameTime + SoundEffect.Duration;
            CalculatePositions();
        }

        public void RemoveEmitter(AudioEmitter emitter, bool pauseIfNoEmitters = true)
        {
            if (EmittersPosition.ContainsKey(emitter))
            {
                EmittersPosition.Remove(emitter);
                CalculatePositions(pauseIfNoEmitters);
            }
        }
    }

    public static class SoundManager
    {
        public static Dictionary<string, DateTime> CoolDown { get; } = new Dictionary<string, DateTime>();
        public static HashSet<(SoundEffectInstance, AudioEmitter)> Instances { get; } = new HashSet<(SoundEffectInstance, AudioEmitter)>();
        public static Dictionary<string, UniqueSoundEffectInstance> UniqueGlobalInstances { get; } = new Dictionary<string, UniqueSoundEffectInstance>();
        public static Dictionary<(string, AudioEmitter), SoundEffectInstance> UniqueLocalInstances { get; } = new Dictionary<(string, AudioEmitter), SoundEffectInstance>();
        public static Dictionary<string, SoundEffect> SoundEffects { get; } = new Dictionary<string, SoundEffect>();

        public static AudioListener AudioListener { get; } = new AudioListener();
        public static Vector2 ListenerWorldPosition
        {
            get => new Vector2(AudioListener.Position.X, AudioListener.Position.Y);
            set => AudioListener.Position = new Vector3(value, 2 * DistanceDivisor);
        }

        public static float MusicMasterVolume
        {
            get => _musicMasterVolume;
            set {
                MediaPlayer.Volume /= _musicMasterVolume;
                _musicMasterVolume = value;
                MediaPlayer.Volume *= _musicMasterVolume;
            }
        }
        private static float _musicMasterVolume = 1f;

        public static float DistanceDivisor { get; set; } = 1;
        private static AudioListener _fakeListener = new AudioListener();
        private static AudioEmitter _fakeEmitter = new AudioEmitter();

        public static void Initialize()
        {
            MusicMasterVolume = Settings.Default.Music / 100f;
            SoundEffect.MasterVolume = Settings.Default.Environment / 100f;
        }

        public static SoundEffect GetEffect(string name)
        {
            return SoundEffects.GetOrAdd(name, () => RessourceManager.Load<SoundEffect>(name));
        }

        public static void Apply3D(SoundEffectInstance instance, AudioListener listener, AudioEmitter emitter)
        {
            _fakeListener.Position = listener.Position / DistanceDivisor;
            _fakeEmitter.Position = emitter.Position / DistanceDivisor;
            instance.Apply3D(_fakeListener, _fakeEmitter);
        }

        /// <summary>
        /// Play a sound effect. If allow multiple, the sound effect can have multiple instance for the same emitter
        /// else will only accepts one instance per emitter
        /// </summary>
        /// <param name="name"></param>
        /// <param name="emitter"></param>
        /// <param name="allowMultiple"></param>
        /// <param name="volume"></param>
        public static void PlayEffect(string name, AudioEmitter emitter, bool allowMultiple = false, float volume = 1)
        {
            if (allowMultiple)
            {
                SoundEffectInstance instance = GetEffect(name).CreateInstance();
                instance.Volume = volume;
                instance.Play();
                Instances.Add((instance, emitter));
                Apply3D(instance, AudioListener, emitter);
            } else
            {
                SoundEffectInstance instance = UniqueLocalInstances.GetOrAdd((name, emitter), () => GetEffect(name).CreateInstance());
                instance.Volume = volume;
                if (instance.State != SoundState.Playing)
                {
                    instance.Play();
                    Apply3D(instance, AudioListener, emitter);
                }
            }
        }

        /// <summary>
        /// Play a sound effect that can have only one instance at once
        /// </summary>
        /// <param name="name"></param>
        /// <param name="emitter"></param>
        /// <param name="volume"></param>
        public static void PlayUniqueEffect(string name, AudioEmitter emitter, float volume = 1)
        {
            UniqueGlobalInstances.GetOrAdd(name, () => new UniqueSoundEffectInstance(GetEffect(name), volume))
                .AddEmitter(emitter);
        }

        public static void RemoveUniqueEffectEmitter(string name, AudioEmitter emitter, bool pauseIfNoEmitters = true)
        {
            if (UniqueGlobalInstances.TryGetValue(name, out UniqueSoundEffectInstance effect))
            {
                effect.RemoveEmitter(emitter, pauseIfNoEmitters);
            }
        }

        public static void Update(GameTime gameTime)
        {
            UniqueGlobalInstances.ForEach(i => i.Value.Update(gameTime));

            UniqueLocalInstances.RemoveIf(kp => kp.Value.State != SoundState.Playing);
            UniqueLocalInstances.ForEach(kp => Apply3D(kp.Value, AudioListener, kp.Key.Item2));

            Instances.RemoveIf(kp => kp.Item1.State != SoundState.Playing);
            Instances.ForEach(kp => Apply3D(kp.Item1, AudioListener, kp.Item2));
        }

        public static void PlayMusic(string name, bool force = false, float volume = 1, TimeSpan? cooldown = null, string cooldownKey = null)
        {
            Song song = RessourceManager.Load<Song>(name);
            bool ok = force;
            cooldownKey ??= name;
            if (!song.Equals(MediaPlayer.Queue.ActiveSong))
            {
                if (force)
                {
                    StopMusic();
                } else if (MediaPlayer.State != MediaState.Playing)
                {
                    if (CoolDown.TryGetValue(cooldownKey, out DateTime endCoolDown))
                    {
                        if (endCoolDown < DateTime.Now)
                        {

                            ok = true;
                        }
                    } else
                    {
                        ok = true;
                    }
                }

                if (ok)
                {
                    MediaPlayer.Play(song);
                    MediaPlayer.Volume = volume * MusicMasterVolume;
                    CoolDown[cooldownKey] = DateTime.Now + (cooldown ?? TimeSpan.Zero);
                }
            }
        }

        public static void StopMusic()
        {
            if (MediaPlayer.State == MediaState.Playing)
            {
                MediaPlayer.Stop();
            }
        }

        public static void PlayMenu()
        {
            PlayMusic("Sound/music - menu", true, 0.5f);
        }

        public static void PlayMorning()
        {
            PlayMusic("Sound/music - morning", volume: 0.5f);
        }

        public static void PlayFall()
        {
            PlayMusic("Sound/music - fall", volume: 0.5f);
        }

        public static void PlayPetrol()
        {
            PlayMusic("Sound/music - petrol", volume: 0.5f, cooldown: TimeSpan.FromMinutes(5));
        }
        public static void PlayLava()
        {
            PlayMusic("Sound/music - lava", cooldown: TimeSpan.FromMinutes(1));
        }

        public static void PlayDeath()
        {
            PlayMusic("Sound/music - death", true, 0.5f);
        }

        private static readonly string[] RANDOM_MUSICS = { "Sound/music - random 1", "Sound/music - random 2", "Sound/music - random 3" };
        public static void PlayRandom()
        {
            PlayMusic(RANDOM_MUSICS[MGTools.RNG.Next(RANDOM_MUSICS.Length)], volume: 0.5f);
        }
    }
}
