﻿using MGCE.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Util
{
    public static class Const
    {
        public const int TERRAIN_DRAW_ORDER = MGConst.DRAW_2_BACKGROUND_2;
        public const int FOREGROUND_DRAW_ORDER = MGConst.DRAW_2_BACKGROUND_2 + 1000;
        public const int GAZ_DRAW_ORDER = MGConst.DRAW_2_BACKGROUND_2 + 2000;
        public const int LIQUID_DRAW_ORDER = MGConst.DRAW_2_BACKGROUND_2 + 3000;
        
        public const int WORLD_RENDER_DRAW_ORDER = MGConst.DRAW_3_BACKGROUND_1;

        public const int ENTITY_DRAW_ORDER = MGConst.DRAW_4_DEFAULT - 1000;
        public const int PLAYER_DRAW_ORDER = MGConst.DRAW_4_DEFAULT;

        public const int IHM_DRAW_ORDER = MGConst.DRAW_6_FOREGROUND_2;
        public const int DEBUG_DRAW_ORDER = MGConst.DRAW_9_MAX_FOREGROUND;
    }
}
