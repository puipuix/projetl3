﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using MGCE.Engine;
using MGCE.Events;
using Hephaestus.Code.Items;

namespace Hephaestus.Code.Util
{
    public interface IRessourceChangeEventListener : IMGEventListener
    {
        void OnRessourceChange();
    }



    public static class RessourceManager
    {
        public const string RESSOURCE_FOLDER = "RessourcePacks";
        public const string DEFAULT_FOLDER = "Default";
        public const string BIN = "Bin";

        public static Dictionary<string, string> AssetsPackName { get; } = new Dictionary<string, string>();

        public static List<string> RessourceNames { get; set; } = new List<string>();
        
        /// <summary>
        /// Load and return an asset
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetName"></param>
        public static T Load<T>(string assetName)
        {
            return Load<T>(assetName, MGEngine.Game.Content);
        }
        /// <summary>
        /// retourne l'image demandé
        /// </summary>
        /// <param name="_t">type de l'item</param>
        /// <param name="file">dossier de l'image</param>
        /// <param name="option">option a ajouter a la fin du nom de l'item</param>
        /// <returns></returns>
        public static string MatchImage(ItemType _t,String file="Item",String option="")
        {     
            return file+"/"+_t.ToString().ToLower()+option;
        }

        /// <summary>
        /// Load and return an asset
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetName"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static T Load<T>(string assetName, ContentManager content)
        {
            try
            {
                return content.Load<T>(GetContentPath(assetName));
            } catch (Exception e)
            {
                throw new ContentLoadException("Error with asset: " + assetName, e);
            }
        }

        /// <summary>
        /// Return the ContentManager path
        /// </summary>
        /// <param name="assetName"></param>
        /// <returns></returns>
        public static string GetContentPath(string assetName)
        {
            return string.Concat(string.Concat(RESSOURCE_FOLDER, "/", AssetsPackName.GetValueOrDefault(assetName, DEFAULT_FOLDER), "/", BIN, "/", assetName));
        }

        /// <summary>
        /// Return the file system path the the asser without the extension
        /// </summary>
        /// <param name="assetName"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string GetFileSystemPath(string assetName, ContentManager content)
        {
            return ContentPathToFileSytemPath(string.Concat(content.RootDirectory, "/", GetContentPath(assetName)));
        }

        /// <summary>
        /// replace / with Path.DirectorySeparatorChar
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ContentPathToFileSytemPath(string path)
        {
            return path.Replace('/', Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// replace Path.DirectorySeparatorChar with /
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string FileSytemPathToContentPath(string path)
        {
            return path.Replace(Path.DirectorySeparatorChar, '/');
        }

        public static void UpdateRessources(ContentManager content)
        {
            AssetsPackName.Clear();
            foreach (string packName in RessourceNames)
            {
                string path = ContentPathToFileSytemPath(string.Concat(content.RootDirectory, "/", RESSOURCE_FOLDER, "/", packName, "/", BIN));
                if (Directory.Exists(path))
                {
                    IEnumerable<string> enume = Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories);
                    foreach (string assetPath in enume)
                    {
                        string assetFileName = assetPath.Remove(0, content.RootDirectory.Length + 1 + RESSOURCE_FOLDER.Length + 1 + packName.Length + 1 + BIN.Length + 1); // on enlève "RootDirectory/RESSOURCE_FOLDER/packName/BIN/"
                        int lastIndex = assetFileName.LastIndexOf('.'); // on cherche l'extension

                        // on supprime l'extension puis on change "\" en "/"
                        string assetName = FileSytemPathToContentPath(assetFileName.Remove(lastIndex, assetFileName.Length - lastIndex));

                        AssetsPackName.Add(assetName, packName);
                    }
                }
            }
            MGEvent<IRessourceChangeEventListener>.Get().AskEvent();
        }
    }
}
