﻿using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.Blocks.Tuyau_Objet;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Events;
using Microsoft.Xna.Framework;
using MGCE.Actor;
using MGCE.Util;
using System.Runtime.Serialization;
using MGCE.Engine;

namespace Hephaestus.Code.util
{
    [Serializable]
    public class MachineManager : MGEventsListenerActor, IMGEventPreUpdateListener, IMGSerializable, IMGDisposable
    {
        private static MachineManager machineManager;
        private int tic = 0;
        private int maxPower=0, currentPower = 0;

        private List<IPath> element;
        private List<IPath> elementToSup;

        public bool IsDisposed { get; private set; }

        public MachineManager()
        {
            element = new List<IPath>();
            elementToSup = new List<IPath>();
        }

        protected MachineManager(SerializationInfo info, StreamingContext context)
        {
            elementToSup = new List<IPath>();
            element = info.GetValue("Elements", element);
            maxPower = info.GetInt32("MaxPower");
            currentPower = info.GetInt32("CurrentPower");
        }

        public void Serialize(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Elements", element);
            info.AddValue("MaxPower", maxPower);
            info.AddValue("CurrentPower", currentPower);
        }

        public void OnDeserialization(object sender) { }

        /// <summary>
        /// retourne l'energie disponible actuellement
        /// </summary>
        /// <returns></returns>
        public static int GetCurrentPower()
        {
            return machineManager?.currentPower ?? 0;
        }

        /// <summary>
        /// retourne l'energie max
        /// </summary>
        /// <returns></returns>
        public static int GetMaxPower()
        {
            return machineManager?.maxPower ?? 0;
        }

        /// <summary>
        /// retourne le machineManager
        /// </summary>
        /// <returns></returns>
        public static MachineManager GetMachineManager()
        {
            return machineManager;
        }

        /// <summary>
        /// actualise le machine manager
        /// </summary>
        public void Activate()
        {
            machineManager = this;
            StartListeningEvent<IMGEventPreUpdateListener>();
            updateEnergyView();
        }


        public void Dispose()
        {
            IsDisposed = true;
            StopListeningAll();
            if (machineManager == this)
            {
                machineManager = null;
            }
            element.Clear();
        }

        /// <summary>
        /// ajoute un element ipath au machine manager
        /// </summary>
        /// <param name="_ipath"></param>
        public void AddElement(IPath _ipath)
        {
            element.Add(_ipath);

        }

        /// <summary>
        /// reture un element du machine manager
        /// </summary>
        /// <param name="_ipath"></param>
        public void RemoveElement(IPath _ipath)
        {
            elementToSup.Add(_ipath);
        }

        /// <summary>
        /// met a jour les element present dans le machine manager
        /// </summary>
        /// <param name="_i"></param>
        public void UpdateElement(IPath _i)
        {
                _i.Exit();//demande a tous les element de tenter de sortir un item
            if (typeof(Production).IsInstanceOfType(_i))
            {

                ((Production)_i).UpdateTic(10);//si l'element est de type production alors on met a jour son tic
            }
        }

        /// <summary>
        /// augmente la quantité max d'energie
        /// </summary>
        /// <param name="power"></param>
        public void increaseMaxPower(int power)
        {

            maxPower += power;
            updateEnergyView();

        }

        /// <summary>
        /// diminue la quantité max d'energie
        /// </summary>
        /// <param name="change"></param>
        public void decreaseMaxPower(int change)
        {
          
                maxPower += change;
                if (currentPower > maxPower)
                    currentPower = maxPower;
            
            updateEnergyView();
        }

        /// <summary>
        /// consomme un certain nombre de point d'energie
        /// </summary>
        /// <param name="power"></param>
        /// <returns></returns>
        public bool consume(int power)
        {
            if (currentPower >= power)
            {
             
                currentPower -= power;
                updateEnergyView();
                return true;
            }

            return false;
        }

        /// <summary>
        /// augment la quantité actuelle d'energie
        /// </summary>
        /// <param name="power"></param>
        /// <returns></returns>
        public bool stockPower(int power)
        {
            if (currentPower + power <= maxPower)
            {
                currentPower += power;
                updateEnergyView();
                return true;
               
            }

            return false;
        }

        /// <summary>
        /// demande la mise a jour du HUD
        /// </summary>
        private void updateEnergyView()
        {
            Hud.updatePowerHUD(currentPower, maxPower);
        }

        public void PreUptate(GameTime gameTime)
        {
            tic++;

            if (tic % 10 == 0)
            {

                element.ForEach(i => UpdateElement(i));

            }
            for (int i = 0; i < elementToSup.Count; i++)
                element.Remove(elementToSup[i]);
            elementToSup = new List<IPath>();
        }

        
    }
}
