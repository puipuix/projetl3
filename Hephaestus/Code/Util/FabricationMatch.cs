﻿using Hephaestus.Code.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.util
{
    class FabricationMatch
    {
        private FabricationMatch() { }

        /// <summary>
        /// return a list of itemType with items necessary for create the item in parameter 
        /// </summary>
        /// <param name="_it">item que l'on souhaite fabriquer</param>
        /// <returns></returns>
        public static Stockage Match(ItemType _it)
        {
            Stockage stock =  new Stockage(0) ;

            //ajouter les recettes dans un switch
            switch (_it)
            {
                case ItemType.ENERGY:stock = new Stockage(1); stock.Add(ItemType.MCHARBON); break;
                //minerais
                case ItemType.MCHARBON: break;
                case ItemType.MFER: break;
                case ItemType.PIERRE:break;
                case ItemType.MCUIVRE:break;
                case ItemType.MURANIUM:break;

                //items issue de minerais
                case ItemType.FER: stock = new Stockage(1); stock.Add(ItemType.MFER); break;
               // case ItemType.CUIVRE: stock = new Stockage(1); stock.Add(ItemType.MCUIVRE); break;
                case ItemType.PLAQUE_FER: stock = new Stockage(1); stock.Add(ItemType.FER); break;
                case ItemType.PLAQUE_CUIVRE:stock = new Stockage(1); stock.Add(ItemType.MCUIVRE);break;
                case ItemType.LITHIUM:stock = new Stockage(1);stock.Add(ItemType.MARGENT);break;
                case ItemType.URANIUM_238:stock = new Stockage(1);stock.Add(ItemType.MURANIUM,2); break;
                case ItemType.URANIUM_235:stock = new Stockage(1);stock.Add(ItemType.URANIUM_238, 10); break;
                

                //autre
                case ItemType.FIL_CUIVRE: stock = new Stockage(1); stock.Add(ItemType.PLAQUE_CUIVRE); break;

                case ItemType.CARTE_MERE:stock = new Stockage(4);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.PLAQUE_CUIVRE); stock.Add(ItemType.FIL_CUIVRE, 2); break;

                //tuyaux
                case ItemType.TUYAUX_COURBET1:
                case ItemType.TUYAUX_COURBET2:
                case ItemType.TUYAUX_CROIX:
                case ItemType.TUYAUX: stock = new Stockage(1);stock.Add(ItemType.FER);break;

                //machine
                case ItemType.FOUR: stock = new Stockage(2);stock.Add(ItemType.PLAQUE_FER,2);stock.Add(ItemType.LASER);break;
                case ItemType.USINE: stock = new Stockage(3);stock.Add(ItemType.CARTE_MERE, 2);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.FIL_CUIVRE, 2);break;
                case ItemType.POWER_PLANT:stock = new Stockage(3);stock.Add(ItemType.CONDENSATEUR);stock.Add(ItemType.CARTE_MERE);stock.Add(ItemType.MOTEUR);break;
                case ItemType.PILE:stock = new Stockage(2);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.BATTERIE);break;
                case ItemType.MINEUR:stock = new Stockage(3);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.FIL_CUIVRE);stock.Add(ItemType.CARTE_MERE);break;
                case ItemType.HEALWALLE:stock = new Stockage(1);stock.Add(ItemType.FER);break;
                case ItemType.LASER:stock = new Stockage(3);stock.Add(ItemType.CARTE_MERE);stock.Add(ItemType.CAPTEUR);stock.Add(ItemType.FIL_CUIVRE, 2);break;
                case ItemType.CAPTEUR:stock = new Stockage(2);stock.Add(ItemType.FER);stock.Add(ItemType.CARTE_MERE);break;
                case ItemType.MOTEUR:stock = new Stockage(2);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.CARTE_MERE, 2);break;
                case ItemType.BATTERIE:stock = new Stockage(3);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.LITHIUM, 5);stock.Add(ItemType.CUIVRE);break;
                case ItemType.CONDENSATEUR:stock = new Stockage(2);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.FIL_CUIVRE);break;

                //robot
                case ItemType.ROUE:stock = new Stockage(1);stock.Add(ItemType.FER);break;
                case ItemType.REVETEMENT:stock = new Stockage(2);stock.Add(ItemType.PLAQUE_CUIVRE);stock.Add(ItemType.CAPTEUR); break;
                case ItemType.WALLE:stock = new Stockage(4);stock.Add(ItemType.REVETEMENT);stock.Add(ItemType.ROUE, 6);stock.Add(ItemType.MOTEUR, 6);stock.Add(ItemType.BATTERIE, 2);break;

                case ItemType.REACTEUR:stock = new Stockage(2);stock.Add(ItemType.PLAQUE_FER, 10);stock.Add(ItemType.TURBINE);break;
                case ItemType.TURBINE:stock = new Stockage(3);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.CARTE_MERE);stock.Add(ItemType.FIL_CUIVRE);break;
                case ItemType.REACTEUR_NUCLEAIRE:stock = new Stockage(3);stock.Add(ItemType.COMPOSANT_NUCLEAIRE);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.FER);break;
                case ItemType.COMPOSANT_NUCLEAIRE:stock = new Stockage(2);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.URANIUM_235,5) ;break;
                case ItemType.COQUE:stock = new Stockage(4);stock.Add(ItemType.PLAQUE_FER);stock.Add(ItemType.CAPTEUR, 5);stock.Add(ItemType.CONDENSATEUR, 10);stock.Add(ItemType.CARTE_MERE, 5);break;
                case ItemType.EVE:stock = new Stockage(4);stock.Add(ItemType.COQUE);stock.Add(ItemType.REACTEUR);stock.Add(ItemType.REACTEUR_NUCLEAIRE);stock.Add(ItemType.BATTERIE, 10);break;

                //lampes
                case ItemType.LAMP: stock = new Stockage(3);stock.Add(ItemType.FIL_CUIVRE,2);stock.Add(ItemType.PLAQUE_FER,3);stock.Add(ItemType.PIERRE);break;
                case ItemType.TORCHE: stock = new Stockage(2);stock.Add(ItemType.MCHARBON);stock.Add(ItemType.FER,2);break;

                default: break;
            }

            return stock;
        }

       
    }
}
