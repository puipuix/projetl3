﻿using Microsoft.Xna.Framework;
using MGCE.Util;
using Hephaestus.Code.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Hephaestus.Code.Util
{
    [Serializable]
    public class BlockRelativePositionActor : CameraFollowerActor
    {
		

		public override Vector2 WorldPosition { get => BlockPositionToWorldPosition(BlockPosition); set => BlockPosition = WorldPositionToBlockPosition(value); }


        /// <summary>
        /// The block position in the world
        /// </summary>
        public Point BlockPosition { get; set; }

        public BlockRelativePositionActor(WorldCamera camera) : base(camera)
        {

        }

        protected BlockRelativePositionActor(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            BlockPosition = info.GetPoint("BlockPosition");
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("BlockPosition", BlockPosition);
        }

        /// <summary>
        /// Transform world position to a block position
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        public static Point WorldPositionToBlockPosition(Vector2 worldPosition)
        {
            Vector2 v = (worldPosition / Block.BLOCK_SIZE_PX);
            return new Point(MGMath.FastFloor(v.X), MGMath.FastFloor(v.Y));
        }

        /// <summary>
        /// Transform a block position to a world position
        /// </summary>
        /// <param name="blockPosition"></param>
        /// <returns></returns>
        public static Vector2 BlockPositionToWorldPosition(Point blockPosition)
        {
            return blockPosition.ToVector2() * Block.BLOCK_SIZE_PX;
        }


        /// <summary>
        /// Transform a screen position to a block position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public static Point ScreenPositionToBlockPosition(Vector2 position, WorldCamera camera)
        {
            return WorldPositionToBlockPosition(camera.ScreenPositionToWorldPosition(position));
        }

        /// <summary>
        /// Transform a block position to the screen position
        /// </summary>
        /// <param name="blockPosition"></param>
        /// <returns></returns>
        public static Vector2 BlockPositionToScreenPosition(Point blockPosition, WorldCamera camera)
        {
            return camera.WorldPositionToScreenPosition(BlockPositionToWorldPosition(blockPosition));
        }
    }
}
