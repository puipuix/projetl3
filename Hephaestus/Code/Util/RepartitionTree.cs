﻿using MGCE.Util;
using Hephaestus.Code.Util.Light;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Hephaestus.Code.Util
{
    [Serializable]
    public class RepartitionNode<T> : IMGSerializable
    {
        public T Value { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
        public RepartitionNode<T> Upper { get; set; }
        public RepartitionNode<T> Lower { get; set; }

        public RepartitionNode(T value, double min, double max)
        {
            Value = value;
            Min = min;
            Max = max;
        }

        private RepartitionNode(SerializationInfo info, StreamingContext context) : this(info.GetValue<T>("Value"), info.GetDouble("Min"), info.GetDouble("Max"))
        {
            Upper = info.GetValue("Upper", Upper);
            Lower = info.GetValue("Lower", Lower);
        }

        public void Serialize(SerializationInfo info, StreamingContext context)
        {
            info.AddValues(
                ("Value", Value),
                ("Min", Min),
                ("Max", Max),
                ("Upper", Upper),
                ("Lower", Lower)
                );
        }

        public void OnDeserialization(object sender) { }

        public T Find(double v, T defaultValue = default)
        {
            if (Min <= v && v < Max)
            {
                return Value;
            } else
            {
                RepartitionNode<T> target = v < Min ? Lower : Upper;
                return target is null ? defaultValue : target.Find(v, defaultValue);
            }
        }

        public bool Add(RepartitionNode<T> node)
        {
            if (node.Max < Min)
            {
                if (Lower is null)
                {
                    Lower = node;
                } else
                {
                    Lower.Add(node);
                }
                return true;
            } else if (Max <= node.Min)
            {
                if (Upper is null)
                {
                    Upper = node;
                } else
                {
                    Upper.Add(node);
                }
                return true;
            } else
            {
                return false;
            }
        }
    }

    public class RepartitionTreeEnumerator<T> : IEnumerator<T>
    {
        public RepartitionTree<T> Tree { get; private set; }

        public T Current { get; private set; }
        public Queue<RepartitionNode<T>> Nexts { get; private set; } = new Queue<RepartitionNode<T>>();

        private bool nextIsDefaultTreeValue = true;

        object IEnumerator.Current => Current;

        public RepartitionTreeEnumerator(RepartitionTree<T> tree)
        {
            Tree = tree;
            Reset();
        }

        public bool MoveNext()
        {
            if (nextIsDefaultTreeValue && Tree.DefaultValue is T)
            {
                nextIsDefaultTreeValue = false;
                Current = Tree.DefaultValue;
                return true;
            } else if (Nexts.IsEmpty())
            {
                return false;
            } else
            {
                var node = Nexts.Dequeue();
                if (node.Lower != null)
                {
                    Nexts.Enqueue(node.Lower);
                }
                if (node.Upper != null)
                {
                    Nexts.Enqueue(node.Upper);
                }
                Current = node.Value;
                return true;
            }
        }

        public void Reset()
        {
            Nexts.Clear();
            nextIsDefaultTreeValue = true;
            if (Tree.Head != null)
            {
                Nexts.Enqueue(Tree.Head);
            }
            Current = default;
        }

        public void Dispose()
        {
            Tree = null;
            Nexts.Clear();
            Nexts = null;
            Current = default;
        }
    }

    [Serializable]
    public class RepartitionTree<T> : IMGSerializable, IEnumerable<T>

    {
        public T DefaultValue { get; set; } = default;

        public RepartitionNode<T> Head { get; set; }


        public RepartitionTree(T defaultValue)
        {
            DefaultValue = defaultValue;
        }
        private RepartitionTree(SerializationInfo info, StreamingContext context) : this(info.GetValue<T>("DefaultValue"))
        {
            Head = info.GetValue("Head", Head);
        }

        public void Serialize(SerializationInfo info, StreamingContext context)
        {
            info.AddValues(
                ("DefaultValue", DefaultValue),
                ("Head", Head));
        }

        public void OnDeserialization(object sender) { }

        public T Find(double value)
        {
            return Head is null ? DefaultValue : Head.Find(value, DefaultValue);
        }

        public bool Add(T value, double min, double max)
        {
            return Add(new RepartitionNode<T>(value, min, max));
        }

        public bool Add(RepartitionNode<T> node)
        {
            if (Head is null)
            {
                Head = node;
                return true;
            } else
            {
                return Head.Add(node);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new RepartitionTreeEnumerator<T>(this);
        }
    }
}
