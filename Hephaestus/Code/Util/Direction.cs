﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Util
{
    public struct Direction
    {
        private const byte N = 0;               // 0000 0000
        private const byte U = 1;               // 0000 0001
        private const byte D = 2;               // 0000 0010
        private const byte L = 4;               // 0000 0100
        private const byte R = 8;               // 0000 1000
        private const byte A = U | D | L | R;   // 0000 1111

        public static readonly Direction ANY = new Direction(A);
        public static readonly Direction UP = new Direction(U);
        public static readonly Direction DOWN = new Direction(D);
        public static readonly Direction LEFT = new Direction(L);
        public static readonly Direction RIGHT = new Direction(R);
        public static readonly Direction NONE = new Direction(N);

        public byte Value { get; }

        private Direction(byte v)
        {
            Value = v;
        }

        /// <summary>
        /// return if a == b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Direction a, Direction b)
        {
            return a.Value == b.Value;
        }

        /// <summary>
        /// return if a != b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Direction a, Direction b)
        {
            return a.Value != b.Value;
        }

        /// <summary>
        /// return if a accept b 
        /// <br>((UP + DOWN) & DOWN -> true)</br>
        /// <br>(ANY & DOWN -> true)</br>
        /// <br>(NONE & DOWN -> false)</br>
        /// <br>(DOWN & ANY -> false)</br>
        /// <br>(DOWN & NONE -> false)</br>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator &(Direction a, Direction b)
        {
            if (b.Value == N)
            {
                return false;
            }
            else
            {
                return (a.Value & b.Value) == b.Value;
            }
        }

        /// <summary>
        /// return a + b
        /// <br>(a+b) & a -> true</br>
        /// <br>(a+b) & b -> true</br>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Direction operator +(Direction a, Direction b)
        {
            return new Direction((byte)(a.Value | b.Value));
        }

        /// <summary>
        /// return a - b
        /// <br>(ANY-b) & b -> false</br>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Direction operator -(Direction a, Direction b)
        {
            //   0011 & ~1010
            // = 0011 & 0101
            // = 0001
            return new Direction((byte)(a.Value & (~b.Value)));
        }

        /// <summary>
        /// return the opposit of a
        /// <br>left -> right + up + down</br>
        /// <br>up+right -> down+left</br>
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Direction operator !(Direction a)
        {
            return new Direction((byte)((~a.Value) & A));
        }

        /// <summary>
        /// return the reverse direction
        /// <br>up -> down</br>
        /// <br>up+right -> down+left</br>
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Direction operator -(Direction a)
        {
            switch (a.Value)
            {
                case U: return DOWN;
                case D: return UP;
                case L: return RIGHT;
                case R: return LEFT;
                default: return !a;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Direction direction &&
                   Value == direction.Value;
        }

        public override int GetHashCode()
        {
            return Value;
        }
    }
}
