﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MGCE.Engine;
using MGCE.Util;

namespace Hephaestus.Code.Util
{
    public abstract class Noise
    {
        /// <summary>
        /// The noise seed
        /// </summary>
        public virtual int Seed { get; set; }

        public Noise(int seed)
        {
            Seed = seed;
        }

        /// <summary>
        /// Return a value between [-1;1]
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="factor"></param>
        /// <returns></returns>
        public abstract double Get(double x, double y, double factor = 1.0);

        public abstract Noise Copy(int seed);

        /// <summary>
        /// Transform a noise value ([-1;1]) to a percent value ([0;1])
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static double GetPercentFromNoiseResult(double v)
        {
            return (v + 1.0) / 2.0;
        }

        public static bool GetBoolFromNoiseResult(double noise, double trueProbability)
        {
            return GetPercentFromNoiseResult(noise) <= trueProbability;
        }

        /// <summary>
        /// Transform a noise value ([-1;1]) to set it in range between [min;max]
        /// </summary>
        /// <param name="v"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static double GetInRangeFromNoiseResult(double v, double min, double max)
        {
            return MGMath.Lerp(min, max, GetPercentFromNoiseResult(v));
        }

        /// <summary>
        /// Transform a noise value ([-1;1]) to a color
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Color NoiseToColor(double v)
        {
            float col = (float)GetPercentFromNoiseResult(v) * 3;
            return new Color(0 <= col && col <= 1 ? col : 0, 1 <= col && col <= 2 ? col - 1 : 0, 2 <= col && col <= 3 ? col - 2 : 0);
        }

        /// <summary>
        /// Print the min and max value that can the noise return using random coordinant between 0 and maxValue
        /// </summary>
        /// <param name="noise"></param>
        /// <param name="iterations"></param>
        /// <param name="maxValueInput"></param>
        /// <param name="minValueInput"></param>
        public static void DebugMinMax(Noise noise, double maxValueInput = 1000, double minValueInput = -1000, int iterations = 1000)
        {
            double diff = maxValueInput - minValueInput;
            float min = float.PositiveInfinity, max = float.NegativeInfinity, avg = 0;
            Random r = new Random();
            for (int i = 0; i < iterations; i++)
            {
                float v = (float)noise.Get(minValueInput + r.NextDouble() * diff, minValueInput + r.NextDouble() * diff, 1);
                min = Math.Min(min, v);
                max = Math.Max(max, v);
                avg = (avg * i + v) / (i + 1);
            }
            MGDebug.PrintLine(noise.GetType().Name + " in [" + min + ";" + max + "] avg: " + avg);
        }

        public static SimplexNoise GetSimplexNoise(int seed)
        {
            return new SimplexNoise(seed);
        }

        public static PerlinNoise GetPerlinNoise(int seed, bool extendToScaleMinusOneOne = false)
        {
            return new PerlinNoise(seed, extendToScaleMinusOneOne);
        }

        public static RandomNoise GetRandomNoise(int seed)
        {
            return new RandomNoise(seed);
        }

        public static PatchNoise GetPatchNoise(int seed, double patchSize = 10)
        {
            return new PatchNoise(seed, patchSize);
        }

        public static PatchNoise GetPatchNoise(Noise noise, double patchSize = 10)
        {
            return new PatchNoise(noise, patchSize);
        }
    }

    /// <summary>
    /// https://github.com/devdad/SimplexNoise
    /// </summary>
    public class SimplexNoise : Noise
    {
        private byte[] perm;

        public override int Seed
        {
            get => base.Seed;
            set {
                base.Seed = value;
                perm = new byte[512];
                Random r = new Random(Seed);
                for (int i = 0; i < 256; i++)
                    perm[i] = (byte)i;

                for (int j = 0; j < 256; j++)
                {
                    int k = j + r.Next(256 - j);
                    byte l = perm[j];

                    perm[j + 256] = perm[k];
                    perm[j] = perm[k];

                    perm[k] = l;
                }
            }
        }

        public SimplexNoise(int seed) : base(seed)
        {

        }

        double Grad(int hash, double x, double y)
        {
            int h = hash & 7;                                   // Convert low 3 bits of hash code
            double u = h < 4 ? x : y;                           // into 8 simple gradient directions,
            double v = h < 4 ? y : x;                           // and compute the dot product with (x,y).
            return ((h & 1) == 1 ? -u : u) + ((h & 2) == 2 ? -2.0f * v : 2.0f * v);
        }

        public override double Get(double x, double y, double factor = 1)
        {
            x *= factor;
            y *= factor;

            double F2 = 0.366025403784438;                          // F2 = 0.5*(sqrt(3.0)-1.0)
            double G2 = 0.211324865405187;							// G2 = (3.0-Math.sqrt(3.0))/6.0

            double n0, n1, n2;                                  // Noise contributions from the three corners

            // Skew the input space to determine which simplex cell we're in

            double s = (x + y) * F2;                            // Hairy factor for 2D
            double xs = x + s;
            double ys = y + s;
            long i = MGMath.FastFloor(xs);
            long j = MGMath.FastFloor(ys);

            double t = (i + j) * G2;
            double X0 = i - t;                                  // Unskew the cell origin back to (x,y) space
            double Y0 = j - t;
            double x0 = x - X0;                                 // The x,y distances from the cell origin
            double y0 = y - Y0;

            // For the 2D case, the simplex shape is an equilateral triangle.
            // Determine which simplex we are in.
            long i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
            if (x0 > y0) { i1 = 1; j1 = 0; } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
            else { i1 = 0; j1 = 1; }      // upper triangle, YX order: (0,0)->(0,1)->(1,1)

            // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
            // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
            // c = (3-sqrt(3))/6

            double x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
            double y1 = y0 - j1 + G2;
            double x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
            double y2 = y0 - 1.0 + 2.0 * G2;

            // Wrap the integer indices at 256, to avoid indexing perm[] out of bounds
            long ii = i & 0xff;
            long jj = j & 0xff;

            // Calculate the contribution from the three corners
            double t0 = 0.5 - x0 * x0 - y0 * y0;
            if (t0 < 0.0f) n0 = 0.0f;
            else
            {
                t0 *= t0;
                n0 = t0 * t0 * Grad(perm[ii + perm[jj]], x0, y0);
            }

            double t1 = 0.5 - x1 * x1 - y1 * y1;
            if (t1 < 0.0) n1 = 0.0;
            else
            {
                t1 *= t1;
                n1 = t1 * t1 * Grad(perm[ii + i1 + perm[jj + j1]], x1, y1);
            }

            double t2 = 0.5 - x2 * x2 - y2 * y2;
            if (t2 < 0.0) n2 = 0.0;
            else
            {
                t2 *= t2;
                n2 = t2 * t2 * Grad(perm[ii + 1 + perm[jj + 1]], x2, y2);
            }

            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1]
            return 40.0 / 0.884343445 * (n0 + n1 + n2);	//accurate to e-9 so that values scale to [-1, 1], same acc as F2 G2.
        }

        public override Noise Copy(int seed)
        {
            return new SimplexNoise(seed);
        }
    }

    public class PatchNoise : Noise
    {
        public Noise UsedNoise { get; set; }

        private readonly Noise simplex;

        public double PatchSize { get; set; }

        public double OffSetX { get; set; }
        public double OffSetY { get; set; }

        public override int Seed
        {
            get => UsedNoise.Seed;
            set {
                if (UsedNoise!=null) UsedNoise.Seed = value;
                if (simplex != null) simplex.Seed = value;
                Random r = new Random(value);
                OffSetX = r.NextDouble() * 1000000;
                OffSetY = r.NextDouble() * 1000000;
            }
        }

        // use a random noise by default
        public PatchNoise(int seed, double patchSize = 10.0) : this(new RandomNoise(seed), patchSize)
        {

        }

        public PatchNoise(Noise noise, double patchSize = 10.0) : base(noise.Seed)
        {
            UsedNoise = noise;
            simplex = new SimplexNoise(noise.Seed);
            PatchSize = patchSize;
            Seed = noise.Seed;
        }

        private void FindCell(double x, double y, out MGVector2Double closeOne, out double closeDist)
        {
            // point in cell coor
            MGVector2Double pointPosition = new MGVector2Double(x / PatchSize, y / PatchSize);
            // cell coor
            MGVector2Double patchPosition = MGVector2Double.FastRound(pointPosition);

            closeDist = double.PositiveInfinity;
            closeOne = new MGVector2Double();

            for (double ix = -0.5; ix <= 0.6; ix++)
            {
                for (double iy = -0.5; iy <= 0.6; iy++)
                {
                    // point of the cell
                    MGVector2Double neighbour = patchPosition + new MGVector2Double(ix, iy);
                    MGVector2Double neiPoint = neighbour + new MGVector2Double(
                            simplex.Get(OffSetX + neighbour.X, OffSetX + neighbour.Y) / 2.0,
                        simplex.Get(OffSetY + neighbour.X, OffSetY + neighbour.Y) / 2.0);
                    double distance = MGVector2Double.DistanceSquare(neiPoint, pointPosition);
                    if (distance < closeDist)
                    {
                        closeDist = distance;
                        closeOne = neighbour;
                    }
                }
            }

            closeDist = Math.Sqrt(closeDist);
        }

        /// <summary>
        /// Return the noise value and set "patchPosition" to the closest patch position and "distance" to the distance between the given point and the patch
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="patchPosition"></param>
        /// <param name="distance"></param>
        /// <param name="factor"></param>
        /// <returns></returns>
        public double GetPointData(double x, double y, out MGVector2Double patchPosition, out double distance, double factor = 1.0)
        {
            FindCell(x, y, out patchPosition, out distance);
            patchPosition *= PatchSize;
            distance *= PatchSize;
            return UsedNoise.Get(patchPosition.X, patchPosition.Y, factor);
        }

        public override double Get(double x, double y, double factor = 1.0)
        {
            return GetPointData(x, y, out _, out _, factor);
        }

        /// <summary>
        /// Return the distance of the given point from the closest patch
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double GetDistance(double x, double y)
        {
            FindCell(x, y, out MGVector2Double _0, out double dist);
            return dist * PatchSize;
        }

        public override Noise Copy(int seed)
        {
            return new PatchNoise(UsedNoise.Copy(seed), PatchSize);
        }
    }

    /// <summary>
    /// https://github.com/thomasp85/ambient
    /// </summary>
    public class PerlinNoise : Noise
    {
               
        private readonly int[] GRAD_X =  {  1, -1, 1, -1,
                                            1, -1, 1, -1,
                                            0, 0, 0, 0};
        private readonly int[] GRAD_Y = {   1, 1, -1, -1,
                                            0, 0, 0, 0,
                                            1, -1, 1, -1};

        private byte[] perm;
        private byte[] perm12;

        bool ExtendToScaleMinusOneOne { get; set; }

        public PerlinNoise(int seed, bool extendToScaleMinusOneOne = false) : base(seed)
        {
            ExtendToScaleMinusOneOne = extendToScaleMinusOneOne;
        }

        public override int Seed
        {
            get => base.Seed;
            set {
                base.Seed = value;
                perm = new byte[512];
                perm12 = new byte[512];
                Random r = new Random(Seed);
                for (int i = 0; i < 256; i++)
                    perm[i] = (byte)i;

                for (int j = 0; j < 256; j++)
                {
                    int k = j + r.Next(256 - j);
                    byte l = perm[j];

                    perm[j + 256] = perm[k];
                    perm[j] = perm[k];

                    perm[k] = l;

                    l = (byte)(perm[j] % 12);
                    perm12[j + 256] = l;
                    perm12[j] = l;
                }
            }
        }



        public double GradCoord2D(long x, long y, double xd, double yd)
        {

            byte lutPos = perm12[(x & 0xff) + perm[(y & 0xff)]];

            return xd * GRAD_X[lutPos] + yd * GRAD_Y[lutPos];
        }

        public override double Get(double x, double y, double factor = 1)
        {
            x *= factor;
            y *= factor;

            long x0 = MGMath.FastFloor(x);
            long y0 = MGMath.FastFloor(y);
            long x1 = x0 + 1;
            long y1 = y0 + 1;

            double xs, ys;

            xs = x - x0;
            ys = y - y0;

            double xd0 = x - x0;
            double yd0 = y - y0;
            double xd1 = xd0 - 1;
            double yd1 = yd0 - 1;

            double xf0 = MGMath.Lerp(GradCoord2D(x0, y0, xd0, yd0), GradCoord2D(x1, y0, xd1, yd0), xs);
            double xf1 = MGMath.Lerp(GradCoord2D(x0, y1, xd0, yd1), GradCoord2D(x1, y1, xd1, yd1), xs);

            double v = MGMath.Lerp(xf0, xf1, ys);
            return ExtendToScaleMinusOneOne ? MGMath.Clamp(-1, 1.35 * v, 1) : v; // from [-0.7;0.7] to [1;1]
        }

        public override Noise Copy(int seed)
        {
            return new PerlinNoise(seed, ExtendToScaleMinusOneOne);
        }
    }

    public class RandomNoise : Noise
    {
        public RandomNoise(int seed) : base(seed)
        {
        }

        public override double Get(double x, double y, double factor = 1)
        {
            x *= factor;
            y *= factor;

            long seed = (long)(x) * 1087;
            seed ^= 0xE56FAA12;
            seed += (long)(y) * 2749;
            seed ^= 0x69628a2d;
            seed += Seed * 3433;
            seed ^= 0xa7b2c49a;
            if (seed < 0)
            {
                seed = -seed;
            }

            return (double)((seed % 2001) - 1000) / 1000.0f;
        }

        public override Noise Copy(int seed)
        {
            return new RandomNoise(seed);
        }
    }
}