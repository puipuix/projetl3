﻿using MGCE.Util;
using Hephaestus.Code.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MGCE.Engine;

namespace Hephaestus.Code.util
{
	[Serializable]
    public class Stockage
    {
        public List<Couple> items { get; set; }
        public readonly int slot;
        private int slotActuelle=0;
        private Filtre filtre;

        public static Stockage usine,four;

		/// <summary>
		/// retourne la quantité actuelle d'in certain element dans le stockage
		/// </summary>
		/// <param name="t">item recherché</param>
		/// <returns></returns>
		public int getCountOf(ItemType t)
		{
			int count = 0;
			foreach(Couple c in items)
			{
				if (c.GetItemType() == t)//si on trouve l'item on ajoute sa quantité au compteur
					count += c.GetQuantite();
			}
			return count;
		}

		/// <summary>
		/// initialise les stockage de reference
		/// </summary>
        public static void init()
        {
            usine = new Stockage(30);
            usine.Add(ItemType.FIL_CUIVRE);
			usine.Add(ItemType.PLAQUE_FER);
			usine.Add(ItemType.CARTE_MERE);
			usine.Add(ItemType.BATTERIE);
			usine.Add(ItemType.CAPTEUR);
			usine.Add(ItemType.CONDENSATEUR);
			usine.Add(ItemType.LASER);

			usine.Add(ItemType.COQUE);
			usine.Add(ItemType.EVE);
			usine.Add(ItemType.FOUR);
			
			usine.Add(ItemType.MOTEUR);
			usine.Add(ItemType.MINEUR);
			usine.Add(ItemType.POWER_PLANT);
			usine.Add(ItemType.REACTEUR);
			usine.Add(ItemType.REACTEUR_NUCLEAIRE);
			usine.Add(ItemType.REVETEMENT);
			usine.Add(ItemType.ROUE);
			usine.Add(ItemType.TURBINE);
			usine.Add(ItemType.TUYAUX);
			usine.Add(ItemType.TUYAUX_COURBET1);
			usine.Add(ItemType.TUYAUX_COURBET2);
			usine.Add(ItemType.TUYAUX_CROIX);
			usine.Add(ItemType.USINE);
			usine.Add(ItemType.WALLE);
			usine.Add(ItemType.HEALWALLE);
			usine.Add(ItemType.LAMP);
			usine.Add(ItemType.TORCHE);

			four = new Stockage(30);
            four.Add(ItemType.FER);
			four.Add(ItemType.PLAQUE_CUIVRE);
			four.Add(ItemType.COMPOSANT_NUCLEAIRE);
			four.Add(ItemType.LITHIUM);
			four.Add(ItemType.URANIUM_235);
			four.Add(ItemType.URANIUM_238);
			
			
        }
        


		public Stockage(int _slot, Filtre _filtre = null)
		{
			slot = _slot;
			filtre = _filtre == null ? new Filtre(null) : _filtre;
			items = new List<Couple>();
		}

		protected Stockage(SerializationInfo info, StreamingContext context)
		{
			slot = info.GetInt32("slot");
			filtre = info.GetValue("filtre", filtre);
			items = info.GetValue("items", items);
			slotActuelle = info.GetInt32("slotActuelle");
		}

		public void Serialize(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("slot", slot);
			info.AddValue("filtre", filtre);
			info.AddValue("items", items);
			info.AddValue("slotActuelle", slotActuelle);
		}

		public void OnDeserialization(object sender) { }

		public void DuplicateContente(List<Couple> _items)
		{
			items = _items;
		}

		public bool CanAdd(ItemType _T, int _q = 1)
		{

			return ResearchPlace(_T, _q) != -2;
		}

		public int ResearchPlace(ItemType _T, int _q = 1)
		{
			bool canAdd = false;
			int cpt = 0;
			while (!canAdd && cpt < items.Count)
			{
				if (items[cpt].GetItemType() == _T /*&& items[cpt].GetQuantite() + _q <= 64*/)
				{
					canAdd = true;
				}
				cpt++;
			}
			if (canAdd)
				cpt--;
			
			if (!canAdd)
			{
				cpt = -1;
				canAdd = slotActuelle < slot;
			}

			if (!canAdd)
			{
				cpt = -2;
			}
			return cpt;
		}

		public Boolean Add(ItemType _t, int _q = 1)
		{
			if (filtre.CanAdd(_t))
			{
				int rang = ResearchPlace(_t, _q);
				if (rang == -1)
				{
					items.Add(new Couple(_t, _q));
					slotActuelle++;
				} else if (rang != -2)
				{
					items[rang].Add(_q);
				} else
				{
					return false;
				}
			}
			else { return false; }
			return true;

		}

		public List<Couple> GetAll()
		{
			return items;
		}

		public List<ItemType> ToItemList()
		{
			List<ItemType> _return = new List<ItemType>();
			foreach (Couple _c in items)
				_return.Add(_c.GetItemType());
			return _return.Distinct().ToList();

		}

		/// <summary>
		/// retire une quantité du premier item
		/// </summary>
		/// <returns></returns>
		public ItemType Remove()
		{
			if (items.Count > 0)
			{
				if (items[0].Remove())
				{
					items.RemoveAt(0);
					return Remove();
				} else
				{
					return items[0].GetItemType();
				}

			}
			verification();
			return ItemType.NOTHING;
		}

		public bool Remove(ItemType _t, int _q = 1)
		{
		
			bool _finded = false;
			int i = 0;
			while (i < items.Count && !_finded)
			{
				if (items[i].GetItemType() == _t)
				{
					_finded = true;
					
					if (items[i].Remove(_q))
					{
						if (items[i].GetQuantite() == 0)
						{
							items.RemoveAt(i);
						}


					}
				}
				i++;
			}

			verification();
			return _finded;
		}

		public bool Remove(Stockage _s)
		{
			bool finded = false;
			
				foreach(Couple c in _s.GetAll())
				{
				if (Remove(c.GetItemType(), c.GetQuantite()))
					finded = true;
				}
				
			
			
				return finded;
			

		}

		private void verification()
		{
			slotActuelle = items.Count;
		}

		public bool Contains(Stockage _s)
		{
			bool valid = true;
			int rang = 0;
			while (valid && rang < _s.GetAll().Count)
			{
				valid = items.Contains(_s.GetAll().ElementAt(0));
				rang++;
			}

			return valid;

		}

		internal bool removeIngredient(Stockage ingredient)
		{
			bool retour = true ;
			foreach(Couple c in ingredient.GetAll())
			{
				if (getCountOf(c.GetItemType()) < c.GetQuantite())
				{
					retour = false;
				}
			}

			if (retour)
			{
				foreach (Couple c in ingredient.GetAll())
				{
					Remove(c.GetItemType(), c.GetQuantite());
				}
			}

			return retour;
		}
	}

	[Serializable]
	public class Couple : IMGSerializable
	{
		private ItemType itemType;
		private int quantite;

		public Couple(ItemType _i, int _q = 1)
		{
			itemType = _i;
			quantite = _q;
		}

		public Couple(SerializationInfo info, StreamingContext context) : this(info.GetValue<ItemType>("itemType"), info.GetInt32("quantite"))
		{
		}

		public void Serialize(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("quantite", quantite);
			info.AddValue("itemType", itemType);
		}

		public void OnDeserialization(object sender) { }

		public ItemType GetItemType()
		{
			return itemType;
		}

		public void Add(int _q = 1)
		{
			quantite += _q;
		}

		public bool Remove(int _q = 1)
		{
			if (quantite - _q >= 0)
			{
				quantite -= _q;
				return true;
			} else
			{
				return false;
			}
		}

		public int GetQuantite()
		{
			return quantite;
		}
	}
}

