﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Util;
using System.Runtime.Serialization;

namespace Hephaestus.Code.Util
{
	/// <summary>
	/// Apply Actor screen position based on Worldposition and the camera position 
	/// </summary>
	public class CameraFollowerActor : MGActor, IMGSerializable
	{
		/// <summary>
		/// The real position of this actor and not the screen position
		/// </summary>
		public virtual Vector2 WorldPosition { get; set; }
		public virtual WorldCamera Camera { get; set; }

		/// <summary>
		/// Tell if the block is dirty and should be redraw.
		/// </summary>
		public virtual bool IsDirtyState { get; set; }

		public override MGSpriteSheet SpriteSheet
		{
			get => base.SpriteSheet;
			set {
				base.SpriteSheet = value;
				if (value is MGSpriteSheet)
				{
					value.OnTextureChange += (o, args) => IsDirtyState = true;
				}
			}
		}

		public CameraFollowerActor(WorldCamera camera) : base()
		{
			Camera = camera;
		}
		protected CameraFollowerActor(SerializationInfo info, StreamingContext context) : base()
		{
			Camera = info.GetValue("Camera", Camera);
		}

		public virtual void Serialize(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Camera", Camera);
		}

		public virtual void OnDeserialization(object sender) { }

		/// <summary>
		/// Update the screen position looking at the world position
		/// </summary>
		public virtual void UpdateWorldPosition()
		{
			ScreenPosition = WorldPosition;
		}

		public virtual bool IsDirty()
		{
			return IsDirtyState || Camera.IsDirty;
		}

		public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
		{
			UpdateWorldPosition();
			if (IsDirty())
			{
				base.Draw(gameTime, spriteBatch);
				IsDirtyState = false;
			}
		}
	}
}
