﻿using Hephaestus.Code.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Interfaces
{/// <summary>
/// interface autorisant un block a resevoir et sortir des items
/// </summary>
    public interface IPath
    {
        bool IsRemoved();
        bool SetNext(IPath _next);
        bool Enter(Item _i,int quantite=1);
        bool Exit();
        bool Available(int _q);

        bool verifOrientation(IPath path);
    }

}
