﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hephaestus.Code.Items;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Util;

namespace Hephaestus.Code.Interfaces.Blocks
{
    public interface IContainer
    {
        bool Push(Item _item,int _q=1);
        bool Pop(ItemType _itemType,int _q=1);
    }
}
