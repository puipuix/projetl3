﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Input;
using MGCE.Util;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Hephaestus.Code.Util.Const;
using System.Runtime.Serialization;

namespace Hephaestus.Code
{
    public class ZoomEventArgs : EventArgs
    {
        public float Zoom { get; }
        public ZoomEventArgs(float zoom)
        {
            Zoom = zoom;
        }
    }

    [Serializable]
    public class WorldCamera : IMGDrawable, IDisposable, IMGSerializable
    {
        public const int MAX_BUFFER_SIZE = 4000;
        public const int MIN_BUFFER_SIZE = 100;
        public Game Game { get; }
        public bool IsDirty { get; set; }
        public Point BufferSize
        {
            get => _bufferSize;
            set {
                if (_bufferSize != value && !IsDisposed)
                {
                    _bufferSize = MGMath.Min(value, new Point(MAX_BUFFER_SIZE));
                    BufferCenter = _bufferSize.ToVector2() / 2;
                    RenderTarget?.Dispose();
                    OldRenderTarget?.Dispose();
                    try
                    {
                        RenderTarget = new RenderTarget2D(Game.GraphicsDevice, _bufferSize.X, _bufferSize.Y) { Name = "Current" };
                        OldRenderTarget = new RenderTarget2D(Game.GraphicsDevice, _bufferSize.X, _bufferSize.Y) { Name = "Old" };
                    } catch (Exception)
                    {

                        throw;
                    }

                }
                ReCenterMatrix();
            }
        }
        private Point _bufferSize;
        public Vector2 BufferCenter { get; private set; }

        public RenderTarget2D RenderTarget { get; private set; }
        public RenderTarget2D OldRenderTarget { get; private set; }
        public float MinZoom { get; set; } = 0.5f;
        public float MaxZoom { get; set; } = 2f;
        public float Zoom
        {
            get => _zoom;
            set {
                if (MinZoom <= value && value <= MaxZoom && !IsDisposed)
                {
                    _zoom = value;
                    BackBufferScale = Matrix.CreateScale(_zoom, _zoom, 1);
                    MoveMatrix();
                    MGTools.SafeInvoke(OnZoomChange, this, new ZoomEventArgs(value));
                }
            }
        }
        private float _zoom = 1;

        public event EventHandler<ZoomEventArgs> OnZoomChange;
        public Vector2 WorldPosition
        {
            get => _worldPosition;
            set {
                SoundManager.DistanceDivisor = Block.BLOCK_SIZE_PX * 2;
                SoundManager.ListenerWorldPosition = value;
                _worldPosition = value;
                MoveMatrix();
            }
        }
        private Vector2 _worldPosition;
        public Vector2 BufferTopLeftWorldPosition { get; private set; }
        public Vector2 BufferWorldPosition { get; private set; }

        public Matrix CameraMatrix { get; private set; }

        public Matrix BackBufferScale { get; private set; } = Matrix.CreateScale(1, 1, 1);
        public Matrix BackBufferMatrix { get; private set; }
        public Vector2 BackBufferTranslation { get; private set; }
        public bool IsDisposed { get; private set; }

        public Vector2 MouseWorldPosition => ScreenPositionToWorldPosition(MGMouse.Position.ToVector2());

        public WorldCamera()
        {
            Game = MyGame.Game;
            MGEngine.ToDraws.Add(this, WORLD_RENDER_DRAW_ORDER);
            BufferSize = new Point(1);
        }

        public WorldCamera(SerializationInfo info, StreamingContext context) : this() { }

        public void Serialize(SerializationInfo info, StreamingContext context) { }

        public void OnDeserialization(object sender) { }

        private void SetRenderTarget(object spritebatch, EventArgs args)
        {
            Game.GraphicsDevice.SetRenderTarget(RenderTarget);
            SpriteBatch spriteBatch = (SpriteBatch)spritebatch;
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, null);
            spriteBatch.Draw(OldRenderTarget, OldRenderTarget.Bounds, Color.White);
            spriteBatch.End();
        }

        public void ReCenterMatrix()
        {
            BufferWorldPosition = WorldPosition;
            BufferTopLeftWorldPosition = BufferWorldPosition - BufferCenter;
            CameraMatrix = Matrix.CreateTranslation(-BufferTopLeftWorldPosition.X, -BufferTopLeftWorldPosition.Y, 0);
            MGEngine.SpriteBatchParameters[TERRAIN_DRAW_ORDER] =
                MGEngine.SpriteBatchParameters[FOREGROUND_DRAW_ORDER] =
                  MGEngine.SpriteBatchParameters[GAZ_DRAW_ORDER] =
                   MGEngine.SpriteBatchParameters[LIQUID_DRAW_ORDER] =
                    new MGSpriteBatchParameter(matrix: CameraMatrix, sampler: SamplerState.PointClamp, beforeBegin: SetRenderTarget);
            MoveMatrix();
            IsDirty = true;
        }

        public void MoveMatrix()
        {
            BackBufferTranslation = WorldPosition - BufferWorldPosition + BufferCenter - MGEngine.BufferCenter / Zoom;
            BackBufferMatrix = Matrix.Multiply(Matrix.CreateTranslation(-BackBufferTranslation.X, -BackBufferTranslation.Y, 0), BackBufferScale);
            MGEngine.SpriteBatchParameters[ENTITY_DRAW_ORDER] = MGEngine.SpriteBatchParameters[PLAYER_DRAW_ORDER] = new MGSpriteBatchParameter(matrix: Matrix.Multiply(Matrix.CreateTranslation(-BackBufferTranslation.X - BufferTopLeftWorldPosition.X, -BackBufferTranslation.Y - BufferTopLeftWorldPosition.Y, 0), BackBufferScale), sampler: SamplerState.PointClamp);
        }

        public Vector2 ScreenPositionToWorldPosition(Vector2 position)
        {
            return ScreenSizeToWorldSize(position) + BufferTopLeftWorldPosition + BackBufferTranslation;
        }

        public Vector2 WorldPositionToScreenPosition(Vector2 worldPosition)
        {
            return WorldSizeToScreenSize(worldPosition - BufferTopLeftWorldPosition - BackBufferTranslation);
        }

        public Vector2 ScreenSizeToWorldSize(Vector2 screenSize)
        {
            return screenSize / Zoom;
        }

        public Vector2 WorldSizeToScreenSize(Vector2 worldSize)
        {
            return worldSize * Zoom;
        }


        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.End();
            spriteBatch.GraphicsDevice.SetRenderTarget(null);
            spriteBatch.Begin(transformMatrix: BackBufferMatrix, samplerState: SamplerState.PointClamp);
            spriteBatch.Draw(RenderTarget, RenderTarget.Bounds, Color.White);
            var next = OldRenderTarget;
            OldRenderTarget = RenderTarget;
            RenderTarget = next;
            IsDirty = false;
        }

        public void Dispose()
        {
            RenderTarget?.Dispose();
            OldRenderTarget?.Dispose();
            IsDisposed = true;
            MGEngine.ToDraws.Remove(this, WORLD_RENDER_DRAW_ORDER);
            MGEngine.SpriteBatchParameters[TERRAIN_DRAW_ORDER] =
               MGEngine.SpriteBatchParameters[FOREGROUND_DRAW_ORDER] =
                 MGEngine.SpriteBatchParameters[GAZ_DRAW_ORDER] =
                  MGEngine.SpriteBatchParameters[LIQUID_DRAW_ORDER] =
                   new MGSpriteBatchParameter(matrix: CameraMatrix, sampler: SamplerState.PointClamp);
        }


    }

    /// <summary>
    /// Base class for camera interactions
    /// </summary>
    public abstract class CameraActor : IMGEventUpdateListener, IMGDisposable
    {
        public bool IsEnable { get; private set; }
        public WorldCamera Camera { get; set; }

        public bool IsDisposed { get; private set; }

        protected CameraActor(WorldCamera camera)
        {
            Camera = camera;
        }

        public void Enable()
        {
            if (!IsEnable)
            {
                IsEnable = true;
                MGEvent<IMGEventUpdateListener>.Get().AddListener(this);
            }
        }

        public void Disable()
        {
            if (IsEnable)
            {
                IsEnable = false;
                MGEvent<IMGEventUpdateListener>.Get().RemoveListener(this);
            }
        }

        public abstract void Uptate(GameTime gameTime);

        public virtual void Dispose()
        {
            Disable();
            Camera = null;
            IsDisposed = true;
        }
    }

    /// <summary>
    /// Class that will handle camera zoom inputs
    /// </summary>
    public class CameraZoomActor : CameraActor
    {
        public float ZoomMul { get; set; } = 1.1f;
        public CameraZoomActor(WorldCamera camera) : base(camera)
        {
        }

        public override void Uptate(GameTime gameTime)
        {
            if (MGInputs.IsPressStart(MGInputNames.Add))
            {
                Camera.Zoom *= ZoomMul;
            }
            if (MGInputs.IsPressStart(MGInputNames.Subtract))
            {
                Camera.Zoom /= ZoomMul;
            }
        }
    }

    /// <summary>
    /// Base class to move smoothly the camera
    /// </summary>
    public class SmoothCameraActor : CameraActor
    {
        public Vector2 Target { get; set; }
        public float MaxDistance { get; set; } = 200;
        public float MinDistance { get; set; } = 5;
        public float Speed { get; set; } = 0.1f * Block.BLOCK_SIZE_PX;


        public SmoothCameraActor(WorldCamera camera) : base(camera)
        {
        }



        public override void Uptate(GameTime gameTime)
        {
            Vector2 camScreen = Camera.WorldSizeToScreenSize(Camera.WorldPosition);
            Vector2 tarScreen = Camera.WorldSizeToScreenSize(Target);
            float distance = Vector2.Distance(camScreen, tarScreen);
            Vector2 dir = (tarScreen - camScreen) / distance;
            if (distance > MaxDistance)
            {
                Camera.WorldPosition += Camera.ScreenSizeToWorldSize(dir * (distance - MaxDistance));
            } else if (distance > MinDistance)
            {
                Camera.WorldPosition += Camera.ScreenSizeToWorldSize(dir * Speed * (distance / MaxDistance));
            }
        }
    }

    /// <summary>
    /// class that will folow and move the camera on an actor
    /// </summary>
    public class ActorFollowerCameraActor : SmoothCameraActor
    {
        public CameraFollowerActor Actor { get; set; }


        public ActorFollowerCameraActor(CameraFollowerActor actor) : base(actor.Camera)
        {
            Actor = actor;
        }

        public override void Uptate(GameTime gameTime)
        {
            Target = Actor.WorldPosition;
            base.Uptate(gameTime);
        }

        public override void Dispose()
        {
            base.Dispose();
            Actor = null;
        }
    }

    public class FreeCameraActor : CameraActor
    {
        public FreeCameraActor(WorldCamera camera) : base(camera)
        {
        }

        public float Speed { get; set; } = 1.0f;

        public override void Uptate(GameTime gameTime)
        {
            Camera.WorldPosition += new Vector2(MGInputs.GetState(MGInputNames.Right) - MGInputs.GetState(MGInputNames.Left), MGInputs.GetState(MGInputNames.Down) - MGInputs.GetState(MGInputNames.Up)) * Speed;
        }
    }
}
