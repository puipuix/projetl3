﻿using MGCE.Actor;
using MGCE.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Character;
using Hephaestus.Code.Items;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Hephaestus.Code.Blocks.Foreground
{
    /// <summary>
    /// Block that posses a playable character.
    /// </summary>
    [Serializable]
    public class CharacterBlock : ForegroundBlock
    {
        public override bool IsNatural => false;

        public CharacterDisplay Character { get; set; }
        public CharacterBlock(ItemType character, Point blockPosition, World world) : base(blockPosition, world)
        {
            Character = character switch
            {
                ItemType.EVE => new EveCharacter(world),
                ItemType.WALLE => new WallECharacter(world),
                _ => throw new ArgumentException(Character + " is not a valid character item")
            };
            Character.WorldPosition = this.WorldPosition + new Vector2(BLOCK_SIZE_PX / 2);
        }

        public CharacterBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Character = info.GetValue("Character", Character);
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("Character", Character);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet();
        }

        public override void OnClick()
        {
            base.OnClick();
            // switch player's and block's character
            Character = World.GameSave.ChangeCharacter(Character);
            Character.Disactivate();
            Character.WorldPosition = WorldPosition + new Vector2(BLOCK_SIZE_PX / 2);
            if (!Visible)
            {
                Character.Hide();
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            Character.Dispose();
        }

        public override void Hide()
        {
            base.Hide();
            Character.Hide();
        }

        public override void Show(int? order = null)
        {
            base.Show(order);
            Character.Show();
        }
    }
}
