﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Blocks.Fluids;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Util;
using MGCE.Shapes;
using static Hephaestus.Code.Util.Const;
using Hephaestus.Code.Util;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework.Media;

namespace Hephaestus.Code.Blocks.Foreground
{
    [Serializable]
    public abstract class ForegroundBlock : Block
    {
        public const float FULL_BLOCK_MAX_FLUID = 1000;
        public const float LIGHT_COEF = 0.8f;
        public const int BLOCK_HEAL = 100;
        public static readonly MGCube DEFAULT_HITBOX = new MGCube(BLOCK_SIZE_PX);

        public static List<MGTexture2D> DESTROY_TEXTURES { get; private set; }

        public float MaxFluidAmount { get; protected set; } = FULL_BLOCK_MAX_FLUID;
        public float FluidAmout { get => (Liquid is null ? 0 : Liquid.Amount) + (Gaz is null ? 0 : Gaz.Amount); }
        public bool AllowFluids => MaxFluidAmount >= FluidBlock.MIN_AMOUNT_FOR_REMOVE;
        public LiquidBlock Liquid { get; set; }
        public GazBlock Gaz { get; set; }
        public virtual Vector3 BlockLightTransparancy { get; set; } = new Vector3(0.99f);
        public virtual Vector3 LightTransparancy => BlockLightTransparancy * (Liquid is null ? Vector3.One : Liquid.LightTransparency) * (Gaz is null ? Vector3.One : Gaz.LightTransparency);

        public override Vector3 PMMaxLight { get; } = new Vector3(0.8f);
        public override Vector3 Light
        {
            get => base.Light;
            set {
                base.Light = value * LIGHT_COEF;
                if (Liquid is LiquidBlock)
                    Liquid.Light = value;
                if (Gaz is GazBlock)
                    Gaz.Light = value;
            }
        }

        public override bool IsNatural => (Liquid?.IsNatural ?? true) && (Gaz?.IsNatural ?? true) && (Heal == MaxHeal);

        public override bool IsForgetable => base.IsForgetable && (Liquid?.IsForgetable ?? true) && (Gaz?.IsForgetable ?? true);

        public virtual MGPolygon HitBox { get; set; } = MGPolygon.EMPTY;

        public string DestroySound { get; set; } = null;
        public float DestroySoundVolume { get; set; } = 1;
        public virtual int MaxHeal { get; set; } = BLOCK_HEAL;
        public virtual int Heal
        {
            get => _heal; set {
                if (_heal != value)
                {
                    _heal = value;
                    IsDirtyState = true;
                }
            }
        }
        protected int _heal = BLOCK_HEAL;

        /// <summary>
        /// What the block will loot when destroyed
        /// </summary>
        public virtual ItemType Loot => ItemType.NOTHING;

        [Obsolete]
        public ForegroundBlock(MGSpriteSheet spriteSheet, Point blockPosition, World world) : base(spriteSheet, blockPosition, world)
        {
            DrawOrder = FOREGROUND_DRAW_ORDER;
        }

        public ForegroundBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
            DrawOrder = FOREGROUND_DRAW_ORDER;
        }

        public ForegroundBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            DrawOrder = FOREGROUND_DRAW_ORDER;
            Liquid = info.GetValue("Liquid", Liquid);
            Gaz = info.GetValue("Gaz", Gaz);
            Heal = info.GetValue("Heal", Heal);
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("Liquid", Liquid);
            info.AddValue("Gaz", Gaz);
            info.AddValue("Heal", Heal);
        }

        public override void Show(int? order = null)
        {
            base.Show();
            Liquid?.Show();
            Gaz?.Show();
        }

        public override void Hide()
        {
            base.Hide();
            Liquid?.Hide();
            Gaz?.Hide();
        }

        public override void Dispose()
        {
            base.Dispose();
            Liquid?.Dispose();
            Gaz?.Dispose();
        }

        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            base.OnNeibourgChange(originDirection, neibourg);
            Liquid?.OnNeibourgChange(originDirection, neibourg);
            Gaz?.OnNeibourgChange(originDirection, neibourg);
        }

        public override bool IsDirty()
        {
            return (Liquid?.IsDirty() ?? false) || (Gaz?.IsDirty() ?? false) || base.IsDirty();
        }

        public override void OnClick()
        {
            base.OnClick();
            Liquid?.OnClick();
            Gaz?.OnClick();
        }

        public override void Removed()
        {
            if (DestroySound is string)
            {
                SoundManager.PlayUniqueEffect(DestroySound, AudioEmitter, DestroySoundVolume);
            }
            base.Removed();
        }

        public void DrawDestroyEffect(SpriteBatch spriteBatch)
        {
            if (Heal < MaxHeal)
            {
                MGTexture2D texture = GetDestroyTexture(MaxHeal, Heal);
                if (texture != null)
                {
                    spriteBatch.Draw(texture, this, 1);
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            bool isDirty = IsDirty();
            base.Draw(gameTime, spriteBatch);
            if (isDirty)
            {
                DrawDestroyEffect(spriteBatch);
            }
        }

        /// <summary>
        /// Load destroy textures
        /// </summary>
        public static void Initialize()
        {
            DESTROY_TEXTURES = MGSpriteAnimation.TileTexture(RessourceManager.Load<Texture2D>("Environnement/destroy"), new Point(32));
        }

        public static MGTexture2D GetDestroyTexture(float maxHeal, float heal)
        {
            return DESTROY_TEXTURES.Count > 0 ? DESTROY_TEXTURES[(int)((1.0f - heal / maxHeal) * (DESTROY_TEXTURES.Count - 1))] : null;
        }
    }

    /// <summary>
    /// Transparent block
    /// </summary>
    [Serializable]
    public class AirBlock : ForegroundBlock
    {
        public override bool IsNatural => base.IsNatural && IsNaturalGeneration;
        public bool IsNaturalGeneration { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blockPosition"></param>
        /// <param name="world"></param>
        /// <param name="isNaturalGeneration">if the block is created by a world generator</param>
        public AirBlock(Point blockPosition, World world, bool isNaturalGeneration) : base(blockPosition, world)
        {
            IsNaturalGeneration = isNaturalGeneration;
            DestroySound = null;
        }

        public AirBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            IsNaturalGeneration = info.GetBoolean("IsNaturalGeneration");
            DestroySound = null;
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("IsNaturalGeneration", IsNaturalGeneration);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            IsDirtyState = false;
        }
    }

    /// <summary>
    /// A full block size block.
    /// </summary>
    [Serializable]
    public abstract class FullBlock : ForegroundBlock
    {
        [Obsolete]
        public FullBlock(MGSpriteSheet spriteSheet, Point blockPosition, World world) : base(spriteSheet, blockPosition, world)
        {
            BlockLightTransparancy = new Vector3(0);
            MaxFluidAmount = 0;
            HitBox = DEFAULT_HITBOX;
        }

        public FullBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
            BlockLightTransparancy = new Vector3(0f);
            MaxFluidAmount = 0;
            HitBox = DEFAULT_HITBOX;
        }

        public FullBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            BlockLightTransparancy = new Vector3(0f);
            MaxFluidAmount = 0;
            HitBox = DEFAULT_HITBOX;
        }
    }

    [Serializable]
    public class StoneForegroundBlock : FullBlock
    {
        public override ItemType Loot => ItemType.PIERRE;
        public override bool IsNatural => false;

        public StoneForegroundBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
        }

        public StoneForegroundBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/pierre"));
        }
    }

    /// <summary>
    /// A block that will use his terrain's spritesheet
    /// </summary>
    [Serializable]
    public class CopyTerrainForegroundBlock : FullBlock
    {
        private bool _protectSpriteSheet = false;
        public override MGSpriteSheet SpriteSheet { get => _protectSpriteSheet ? null : World?.GetOrNull(BlockPosition)?.SpriteSheet; set { } }

        public override ItemType Loot => ItemType.PIERRE;

        public CopyTerrainForegroundBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
        }

        public CopyTerrainForegroundBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }


        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsDirty())
            {
                UpdateWorldPosition();
                SpriteSheet.SetAnimation(DEFAULT_ANIMATION, false);
                if (MGSpriteSheet.IsReady(SpriteSheet))
                {
                    spriteBatch.Draw(SpriteSheet, this, 1);
                }
                SpriteSheet.SetAnimation(FOREGROUND_ANIMATION, false);
                if (MGSpriteSheet.IsReady(SpriteSheet))
                {
                    spriteBatch.Draw(SpriteSheet, this, 1);
                }
                DrawDestroyEffect(spriteBatch);
                IsDirtyState = false;
            }
        }

        public override void Dispose()
        {
            _protectSpriteSheet = true;
            base.Dispose();
            _protectSpriteSheet = false;
        }

        protected override MGSpriteSheet CreateSpriteSheet() { return SpriteSheet; }
    }
}
