﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Hephaestus.Code.Blocks.Machine
{/// <summary>
/// permet de créer un four
/// </summary>
    [Serializable]
    class Four : Production
    {
        public override ItemType Loot => ItemType.FOUR;
        public Four(Point blockPosition, World world, ItemType _it,int speed) : base(blockPosition, world, _it, speed)
        {
            MachineManager.GetMachineManager().AddElement(this);
        }

        public Four(SerializationInfo info, StreamingContext context) : base(info, context, 100)
        {

        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTileTexture(RessourceManager.Load<Texture2D>(RessourceManager.MatchImage(ItemType.FOUR, "machines")), new Point(32), 12, name: DEFAULT_ANIMATION);
        }
    }
}
