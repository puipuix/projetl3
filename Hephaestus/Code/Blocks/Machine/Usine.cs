﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Items;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Hephaestus.Code.util;

namespace Hephaestus.Code.Blocks.Machine
{
    [Serializable]
    class Usine : Production
    {
        public override ItemType Loot => ItemType.USINE;
        public Usine(Point blockPosition, World world, ItemType _sortie, int _speed = 100) : base(blockPosition, world, _sortie, _speed)
        {
            MachineManager.GetMachineManager().AddElement(this);
        }

        public Usine(SerializationInfo info, StreamingContext context) : base(info, context, 100)
        {

        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTileTexture(RessourceManager.Load<Texture2D>(RessourceManager.MatchImage(ItemType.USINE, "machines")), new Point(32), 6);
        }
    }
}
