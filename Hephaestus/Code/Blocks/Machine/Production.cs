﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Shapes;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Blocks.Tuyau_Objet;
using Hephaestus.Code.Character;
using System.Runtime.Serialization;
using MGCE.Util;

namespace Hephaestus.Code.Blocks.Machine
{/// <summary>
/// classe permettant d'implmenter un modele de production
/// </summary>
    [Serializable]
    public abstract class Production : ForegroundBlock, IPath
    {
        public bool destroyed=false;//bloc detruit ou non
        public ItemType sortie;//type de l'item produit
        public Stockage stockage;//stockage actuel de la machine
       public int speed, actualTic, actualProd;//variable utile pour la production
        IPath next;//structure a qui transferer la production
       protected readonly Stockage ingredient;//ingredient pour fabriquer l'item demandé
        public override bool IsNatural => false;

        public Production(Point blockPosition, World world, ItemType _sortie, int _speed = 100) : base(blockPosition, world)
        {
            ingredient = FabricationMatch.Match(_sortie);
            stockage = new Stockage(20, new Filtre(ingredient.ToItemList()));
            sortie = _sortie;
            speed = _speed;
            actualTic = 0;
            actualProd = 0;
        }

        public Production(SerializationInfo info, StreamingContext context, int _speed = 100) : base(info, context)
        {
            sortie = info.GetValue("sortie", sortie);
            ingredient = info.GetValue("ingredient", ingredient);
            stockage = info.GetValue("stockage", stockage);
            next = info.GetValue("next", next);
            speed = _speed;
            actualTic = 0;
            actualProd = 0;
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValues(("stockage", stockage), ("sortie", sortie), ("ingredient", ingredient), ("next", next));
        }


        public Stockage getIngredient()
        {

            return ingredient;
        }

        //permet d'ajouter la structure a qui passer l'item produit
        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            base.OnNeibourgChange(originDirection, neibourg);
            neibourg = Player.player.world[BlockPosition + originDirection].ForegroundBlock;
            if (originDirection.X == -1 && originDirection.Y == 0)//cas a gauche
            {
                if (typeof(TuyauObjet).IsInstanceOfType(neibourg))//cas a gauche
                {


                    //tuyaux t1 et t2
                    if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neibourg))
                    {
                        if (((TuyauObjetCoudeT1)neibourg).orientation == Orientation.DOWN)
                            SetNext((IPath)neibourg);
                        if (((TuyauObjetCoudeT1)neibourg).orientation == Orientation.RIGHT)
                            ((IPath)neibourg).SetNext(this);
                    }
                    else if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neibourg))
                    {
                        if (((TuyauObjetCoudeT2)neibourg).orientation == Orientation.RIGHT)
                            ((IPath)neibourg).SetNext(this);
                        if (((TuyauObjetCoudeT2)neibourg).orientation == Orientation.UP)
                            SetNext((IPath)neibourg);
                    }
                    else if (typeof(TuyauObjetCroix).IsInstanceOfType(neibourg))//tuyaux croix
                    {
                        ((TuyauObjetCroix)neibourg).SetNext(this);
                    }
                    else if (((TuyauObjet)neibourg).orientation == Orientation.LEFT)
                        SetNext((IPath)neibourg);
                    else if (((TuyauObjet)neibourg).orientation == Orientation.RIGHT)
                        ((IPath)neibourg).SetNext(this);
                }
            }
            else if (originDirection.X == 1 && originDirection.Y == 0)//cas a droite
            {
                if (typeof(TuyauObjet).IsInstanceOfType(neibourg))
                {

                    //tuyaux t1 et t2
                    if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neibourg))
                    {
                        if (((TuyauObjetCoudeT1)neibourg).orientation == Orientation.UP)
                            SetNext((IPath)neibourg);
                        if (((TuyauObjetCoudeT1)neibourg).orientation == Orientation.LEFT)
                            ((IPath)neibourg).SetNext(this);
                    }
                    else if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neibourg))
                    {
                        if (((TuyauObjetCoudeT2)neibourg).orientation == Orientation.LEFT)
                            ((IPath)neibourg).SetNext(this);
                        if (((TuyauObjetCoudeT2)neibourg).orientation == Orientation.DOWN)
                            SetNext((IPath)neibourg);
                    }
                    else if (typeof(TuyauObjetCroix).IsInstanceOfType(neibourg))//tuyaux croix
                    {
                        ((TuyauObjetCroix)neibourg).SetNext(this);
                    }
                    else if (((TuyauObjet)neibourg).orientation == Orientation.RIGHT)
                        SetNext((IPath)neibourg);
                    else if (((TuyauObjet)neibourg).orientation == Orientation.LEFT)
                        ((IPath)neibourg).SetNext(this);
                }
            }

        }

        //update du tic pour la production
        public void UpdateTic(int _tic = 10)
        {
            actualTic += _tic;
            if (actualTic > speed)
            {
                actualTic = 0;
                fabriquer();
            }


        }

        /// <summary>
        /// permet de fabriquer un item si les conditions sont remplies
        /// </summary>
        public virtual void fabriquer()
        {
           
           
            if (actualProd < 64 && MachineManager.GetCurrentPower()>0 && stockage.removeIngredient(ingredient))
            {
                MachineManager.GetMachineManager().consume(1);
              
                actualProd++;
               
            }
            else
            {
               
            }
        }

        public void Update()
        {

        }

        public bool SetNext(IPath _next)
        {

            next = _next;
            return true;

        }


        /// <summary>
        /// permet de faire entrer un item valide dans le stockage
        /// </summary>
        /// <param name="_i">item</param>
        /// <param name="quantite">quantité, 1 par defaut </param>
        /// <returns></returns>
        public bool Enter(Item _i,int quantite=1)
        {
            
            return stockage.Add(_i.GetItemType(),quantite);
        }

        /// <summary>
        /// permet d'ejecter l'item produit si les conditions sont remplie
        /// </summary>
        /// <returns></returns>
        public bool Exit()
        {
            //MGDebug.PrintLine("called");
            if (next != null && !next.IsRemoved()&& actualProd > 0)
            {
                actualProd--;
                // MGDebug.PrintLine("excited");
                return next.Enter(new Item(sortie));

            }

            return false;
        }

        /// <summary>
        /// permet de savoir si disponible a l'ajout d'item
        /// </summary>
        /// <param name="_q">quantité a ajouter</param>
        /// <returns></returns>
        public bool Available(int _q = 1)
        {
            return stockage.CanAdd(sortie, _q);
        }

        /// <summary>
        /// verifie l'orientation d'un Ipath afin de savoir si il doit etre ajouter au next
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool verifOrientation(IPath path)
        {
            if (typeof(TuyauObjet).IsInstanceOfType(path))
                return ((TuyauObjet)path).orientation == Orientation.LEFT || ((TuyauObjet)path).orientation == Orientation.RIGHT;
            if (typeof(Production).IsInstanceOfType(path))
                return true;
            return false;
        }

        /// <summary>
        /// override de remove de block pour desactiver la production
        /// </summary>
        public override void Removed()
        {
            MachineManager.GetMachineManager().RemoveElement(this);
            next = null;
            destroyed = true;
            base.Removed();
        }

        public bool IsRemoved()
        {
            return destroyed;
        }
    }
}
