﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Hephaestus.Code.Blocks.Machine
{
    [Serializable]
    class Pompe : Production
    {
        public override ItemType Loot => ItemType.NOTHING;
        public Pompe(Point blockPosition, World world,ItemType _it) : base(blockPosition, world,_it,100)
        {
            MachineManager.GetMachineManager().AddElement(this);
        }

        public Pompe(SerializationInfo info, StreamingContext context) : base(info, context, 100)
        {

        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet();
        }
    }
}
