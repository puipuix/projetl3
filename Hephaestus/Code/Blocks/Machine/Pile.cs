﻿using MGCE.Actor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using Hephaestus.Code.View;
using System;
using System.Collections.Generic;

using System.Text;
using System.Runtime.Serialization;

namespace Hephaestus.Code.Blocks.Machine
{
    [Serializable]
    public class Pile : ForegroundBlock
    {
        private Point p;
        public override ItemType Loot => ItemType.PILE;
        public override bool IsNatural => false;

        public Pile(Point blockPosition, World world) : base(blockPosition, world)
        {
            MachineManager.GetMachineManager().increaseMaxPower(10);
            Hud.updatePowerHUD(MachineManager.GetCurrentPower(), MachineManager.GetMaxPower());
        }

        public Pile(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTileTexture(RessourceManager.Load<Texture2D>(RessourceManager.MatchImage(ItemType.PILE, "machines")), new Point(32), 12, name: DEFAULT_ANIMATION);
        }

        /// <summary>
        /// lorsque retiré, on descend le nombre d'énergie max de 10 points
        /// </summary>
        public override void Removed()
        {
            MachineManager.GetMachineManager().decreaseMaxPower(10);
            base.Removed();
        }
    }
}
