﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using MGCE.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Hephaestus.Code.Util;
using MGCE.Engine;

namespace Hephaestus.Code.Blocks.Machine
{/// <summary>
/// permet de créer un mineur
/// </summary>
    [Serializable]
    class Mineur : Production
    {
        RessourceTerrain ressource;
        public override ItemType Loot => ItemType.MINEUR;
        public Mineur(Point blockPosition, World world, RessourceTerrain _it) : base(blockPosition, world,_it.Stockage.ToItemList().First(),100)
        {
            ressource = _it;
            MachineManager.GetMachineManager().AddElement(this);
        }

        public Mineur(SerializationInfo info, StreamingContext context) : base(info, context, 100)
        {
            info.GetValue("Terrain", out ressource);
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("Terrain", ressource);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTileTexture(RessourceManager.Load<Texture2D>(RessourceManager.MatchImage(ItemType.MINEUR, "machines")), new Point(32), 12, name: DEFAULT_ANIMATION);
        }

        /// <summary>
        /// override de la fonction fabriquer car le mineur ne consomme pas de resources
        /// </summary>
        public override void fabriquer()
        {
            if ( actualProd < 100)
            {
                actualProd++;
            }
               
           
           
        }
    }
}
