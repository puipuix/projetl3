﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code;
using Hephaestus.Code.View;
using MGCE.Engine;
using System.Runtime.Serialization;

namespace Hephaestus.Code.Blocks.Machine
{
    [Serializable]
    public class PowerPlant : Production
    {
        public override ItemType Loot => ItemType.POWER_PLANT;
        String current = DEFAULT_ANIMATION;
        public PowerPlant(Point blockPosition, World world, ItemType _it) : base(blockPosition, world, _it, 100)
        {
            Hud.updatePowerHUD(MachineManager.GetCurrentPower(), MachineManager.GetMaxPower());
            MachineManager.GetMachineManager().AddElement(this);
        }

        public PowerPlant(SerializationInfo info, StreamingContext context) : base(info, context, 100)
        {

        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            var spritesheet = MGSpriteSheet.NewSingleAnimationFromTileTexture(RessourceManager.Load<Texture2D>(RessourceManager.MatchImage(ItemType.POWER_PLANT, "machines")), new Point(32), 12, name: current);
            spritesheet.AddAnimation(MGSpriteAnimation.NewFromTileTexture("2", RessourceManager.Load<Texture2D>(RessourceManager.MatchImage(ItemType.POWER_PLANT, "machines", "_on")), new Point(32), 12), true);
            return spritesheet;
        }
        /// <summary>
        /// override de fabriquer,produit tant que le nombre de points max pour l'energie n'est pas atteint
        /// </summary>
        public override void fabriquer()
        {

            if (MachineManager.GetCurrentPower() < MachineManager.GetMaxPower() && stockage.Remove(ingredient))
            {
              
                SpriteSheet.SetAnimation("2");//change l'animation pour mettre celle d'activation
                SpriteSheet.PlayAnimation("2");
                //SpriteSheet.CurrentAnimation.CycleCount = int.MaxValue;
                MachineManager.GetMachineManager().stockPower(5);
            } else
            {
                SpriteSheet.SetAnimation(current);//change l'animation pour mettre celle desactivé
            }
        }

       

    }
}
