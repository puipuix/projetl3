﻿using Hephaestus.Code.Util;
using MGCE.Actor;
using MGCE.Engine;
using Microsoft.Xna.Framework;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util.Light;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Hephaestus.Code.Blocks.Utilitaire
{
    /// <summary>
    /// Base class for light block
    /// </summary>
    [Serializable]
    public abstract class LightBlock : ForegroundBlock, ILightSource
    {
        public const string OFF_ANIMATION = "off_anim";
        public const string ON_ANIMATION = "on_anim";

        public List<TerrainBlock> AffectedBlocks { get; } = new List<TerrainBlock>();

        public int VisibleAffectedBlock { get; set; }

        public float LightAngle => MathHelper.Pi;

        public Vector2 LightDirection => Vector2.UnitX;

        public bool NeedReAffect { get; protected set; } = true;
        public bool IsProtectingBlock => true;
        public virtual Vector2 LightWorldPosition => WorldPosition + new Vector2(BLOCK_SIZE_PX / 2);
        public override bool IsNatural => false;
        public virtual bool Enable {
            get => _enable;
            set {
                if (value && !_enable)
                {
                    SetOn();
                } else if (!value && _enable)
                {
                    SetOff();
                }
                _enable = value;
            }
        }

        public abstract Color LightColor { get; protected set; }
        public abstract float Attenuation { get; protected set; }
        public abstract float Range { get; protected set; }

        protected bool _enable;

        public string EnabeledSound { get; set; } = null;
        public float EnabeledSoundVolume { get; set; } = 1f;

        public LightBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
            SpriteSheet?.PlayAnimation(OFF_ANIMATION);
            Enable = false;
            Heal = 30;
            MaxHeal = 30;
            World.OnGC += World_OnGC;
        }

        private void World_OnGC(object sender, EventArgs e)
        {
            NeedReAffect = true;
        }

        public LightBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            World.OnGC += World_OnGC;
            _enable = !info.GetBoolean("Enable");
            Enable = !_enable; // start good animation
            Heal = 30;
            MaxHeal = 30;
        }

        protected virtual void SetOn()
        {
            StartListeningEvent<ILightPreUpdate>();
            NeedReAffect = true;
            SpriteSheet?.PlayAnimation(ON_ANIMATION);
        }

        protected virtual void SetOff()
        {
            StopListeningEvent<ILightPreUpdate>();
            UnAffectBlocks();
            SpriteSheet?.PlayAnimation(OFF_ANIMATION);
            if (EnabeledSound is string)
            {
                SoundManager.RemoveUniqueEffectEmitter(EnabeledSound, AudioEmitter);
            }
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("Enable", Enable);
        }


        public override void OnClick()
        {
            base.OnClick();
            Enable = !Enable;
        }

        public virtual void LightPreUpdate()
        {
            if (Enable && EnabeledSound is string && Visible)
            {
                SoundManager.PlayUniqueEffect(EnabeledSound, AudioEmitter, EnabeledSoundVolume);
            }
            if (NeedReAffect)
            {
                AffectBlocks();
                NeedReAffect = false;
            }
        }

        public override void Dispose()
        {
            World.OnGC -= World_OnGC;
            SetOff();
            base.Dispose();
            UnAffectBlocks();
        }

        public virtual void RequestLightUpdate()
        {
            NeedReAffect = true;
        }

        public void UnAffectBlocks()
        {
            ILightSource.DefaultUnAffectBlocks(this);
        }

        public virtual void AffectBlocks()
        {
            ILightSource.DefaultAffectBlocks(this);
        }

        public virtual void CheckNeedPreUpdate()
        {
            ILightSource.DefaultCheckNeedPreUpdate(this);
        }

        public virtual void IncreaseVisibleAffectedBlock()
        {
            ILightSource.DefaultIncreaseVisibleAffectedBlock(this);
        }

        public virtual void DecreaseVisibleAffectedBlock()
        {
            ILightSource.DefaultDecreaseVisibleAffectedBlock(this);
        }

        public virtual float LightPower(LightData data)
        {
            return ILightSource.DefaultLightPower(this, data);
        }

        public virtual Vector3 GetLightEmission(Vector2 position, LightData data)
        {
            return ILightSource.DefaultGetLightEmission(this, position, data);
        }
    }
}
