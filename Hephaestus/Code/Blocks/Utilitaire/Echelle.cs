﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Hephaestus.Code.Blocks.Utilitaire
{
    [Obsolete]
    class Echelle : Block
    {
        public Echelle(Point blockPosition, World world) : base(new MGSpriteSheet(), blockPosition, world)
        {
            BlockPosition = blockPosition;
            World = world;

           
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet();
        }
    }
}
