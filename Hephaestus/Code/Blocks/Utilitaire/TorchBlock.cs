﻿using MGCE.Actor;
using MGCE.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Blocks.Fluids;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util;
using Hephaestus.Code.Util.Light;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Hephaestus.Code.Items;

namespace Hephaestus.Code.Blocks.Utilitaire
{
    [Serializable]
    public class TorchBlock : LightBlock
    {
        private const int LIGHT_UPDATE_DELAY = 3;
        private const float MAX_WATER_COEF = 0.25f;

        private int tickCount;

        public override Color LightColor { get; protected set; } = Color.White;
        public override float Attenuation { get => 1.2f; protected set { } }
        public override float Range { get => BLOCK_SIZE_PX * 7; protected set { } }

        public override ItemType Loot => ItemType.TORCHE;

        public TorchBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
            EnabeledSound = "Sound/fire";
            EnabeledSoundVolume = 0.1f;
        }

        public TorchBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            EnabeledSound = "Sound/fire";
            EnabeledSoundVolume = 0.1f;
        }

        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            base.OnNeibourgChange(originDirection, neibourg);
            if (Enable && Liquid is Water && Liquid.Amount > MaxFluidAmount * MAX_WATER_COEF)
            {
                Enable = false;
            }
        }

        protected override void SetOn()
        {
            base.SetOn();
            if (Liquid is Water && Liquid.Amount > MaxFluidAmount * MAX_WATER_COEF)
            {
                SetOff();
            }
        }

        public override void LightPreUpdate()
        {
            base.LightPreUpdate();
            if (tickCount++ > LIGHT_UPDATE_DELAY && VisibleAffectedBlock > 0)
            {
                tickCount = MGTools.RNG.Next(0, LIGHT_UPDATE_DELAY / 2);
                LightColor = new Color(new Vector3(1f, MGMath.Lerp(0.7f, 0.8f, MGTools.RNG.NextFloat()), 0.6f) * MGMath.Lerp(0.7f, 1f, MGTools.RNG.NextFloat()));
                foreach (TerrainBlock block in AffectedBlocks)
                {
                    block.NotifyLightChange();
                }
            }
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet(
                MGSpriteAnimation.NewFromTileTexture(ON_ANIMATION, RessourceManager.Load<Texture2D>("Outils/torche_on"), new Point(32), 20),
                MGSpriteAnimation.NewSingleFrameFromTexture(OFF_ANIMATION, RessourceManager.Load<Texture2D>("Outils/torche_off"))
            );
        }
    }
}
