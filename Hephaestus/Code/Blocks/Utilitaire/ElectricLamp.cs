﻿using MGCE.Actor;
using MGCE.Input;
using MGCE.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Hephaestus.Code.util;
using MGCE.Engine;
using Hephaestus.Code.Items;

namespace Hephaestus.Code.Blocks.Utilitaire
{
    [Serializable]
    public class ElectricLamp : LightBlock
    {
        public override Color LightColor { get; protected set; } = Color.White;
        public override float Attenuation { get; protected set; } = 3f;
        public override float Range { get => BLOCK_SIZE_PX * 10; protected set { } }
        public override ItemType Loot => ItemType.LAMP;

        private const int LIGHT_SIZZLE_DELAY = 300;
        private const int LIGHT_SIZZLE_MAX_DURATION = 50;
        private int tickCount;
        private bool _sizzle;
        public ElectricLamp(Point blockPosition, World world) : base(blockPosition, world)
        {
            EnabeledSound = "Sound/neon";
            EnabeledSoundVolume = 0.5f;
        }

        public ElectricLamp(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Attenuation = info.GetSingle("Attenuation");
            EnabeledSound = "Sound/neon";
            EnabeledSoundVolume = 0.5f;
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("Attenuation", Attenuation);
        }

        public override void OnClick()
        {
            if (MGInputs.IsPressed(MGInputNames.LeftControl))
            {
                Attenuation = (((int)Attenuation) % 5) + 1;
                NeedReAffect = true;
            } else
            {
                base.OnClick();
            }
        }

        private void CheckSizzleStart()
        {
            if (tickCount++ > LIGHT_SIZZLE_DELAY && VisibleAffectedBlock > 0)
            {
                SpriteSheet?.PlayAnimation(OFF_ANIMATION);
                tickCount = MGTools.RNG.Next(0, LIGHT_SIZZLE_MAX_DURATION);
                LightColor = new Color(new Vector3(100f / 255f));
                _sizzle = true;
                foreach (TerrainBlock block in AffectedBlocks)
                {
                    block.NotifyLightChange();
                }
            }
        }

        private void CheckSizzleStop()
        {
            if (tickCount++ > LIGHT_SIZZLE_MAX_DURATION && VisibleAffectedBlock > 0)
            {
                SpriteSheet?.PlayAnimation(ON_ANIMATION);
                tickCount = MGTools.RNG.Next(0, LIGHT_SIZZLE_DELAY / 4);
                LightColor = Color.White;
                _sizzle = false;
                foreach (TerrainBlock block in AffectedBlocks)
                {
                    block.NotifyLightChange();
                }
            }
        }

        public override void LightPreUpdate()
        {
            base.LightPreUpdate();
            if (MGEngine.TickCount % 500 == 0)
            {
                MachineManager.GetMachineManager().consume(1);
            }
            if (_sizzle) CheckSizzleStop(); else CheckSizzleStart();
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet(
                MGSpriteAnimation.NewSingleFrameFromTexture(ON_ANIMATION, RessourceManager.Load<Texture2D>("Outils/lamp_on")),
                MGSpriteAnimation.NewSingleFrameFromTexture(OFF_ANIMATION, RessourceManager.Load<Texture2D>("Outils/lamp_off"))
            );
        }
    }
}
