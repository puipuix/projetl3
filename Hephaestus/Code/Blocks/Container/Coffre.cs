﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;

namespace Hephaestus.Code.Blocks.Container
{
    [Obsolete]
    class Coffre : Block,IContainer
    {
        private Stockage stockage;

        public Coffre(Point blockPosition, World world,int _quantite) : base(new MGSpriteSheet(), blockPosition, world)
        {
            BlockPosition = blockPosition;
            World = world;
            stockage = new Stockage(_quantite);
          
        }     

        public bool Push(Item _item,int _q=1)
        {
            return stockage.Add(_item.GetItemType(),_q);
        }

        public bool Pop(ItemType _itemType,int _q=1)
        {
           return stockage.Remove(_itemType,_q);
        }

        public Stockage GetStockage()
        {
            return stockage;
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet();
        }
    }
}
