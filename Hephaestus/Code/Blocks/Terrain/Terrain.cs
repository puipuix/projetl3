﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Events;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Util;
using Hephaestus.Code.Util.Light;
using MGCE.Util;
using static Hephaestus.Code.Util.Const;
using System.Runtime.Serialization;
using MGCE.Input;
using Hephaestus.Code.Items;

namespace Hephaestus.Code.Blocks.Terrain
{
    /// <summary>
    /// Base class for background block
    /// </summary>
    [Serializable]
    public abstract class TerrainBlock : Block, ILightUpdate
    {
        public const float MIN_LIGHT = 0.2f;

        public override bool IsNatural => ForegroundBlock?.IsNatural ?? true;

        public override bool IsForgetable => base.IsForgetable && (ForegroundBlock?.IsForgetable ?? true) && LightSources.None(p=>p.Key.IsProtectingBlock);

        public ForegroundBlock ForegroundBlock
        {
            get => _foreground;
            set {
                _foreground = value;
                _foreground.Light = Light;
            }
        }
        private ForegroundBlock _foreground;

        public override Vector3 NPMMinLight { get; } = new Vector3(0.3f);

        public override Vector3 Light
        {
            get => base.Light;
            set {
                _light = value;
                Color = new Color(MGMath.Max(value, NPMMinLight) * ILightSource.GlobalBrightness);
                if (ForegroundBlock is ForegroundBlock) ForegroundBlock.Light = value;
                IsDirtyState = true;
            }
        }

        public Dictionary<ILightSource, LightData> LightSources { get; } = new Dictionary<ILightSource, LightData>();

        public TerrainBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
            DrawOrder = TERRAIN_DRAW_ORDER;
        }

        [Obsolete]
        public TerrainBlock(MGSpriteSheet spriteSheet, Point blockPosition, World world) : base(spriteSheet, blockPosition, world)
        {
            DrawOrder = TERRAIN_DRAW_ORDER;
        }

        protected TerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            DrawOrder = TERRAIN_DRAW_ORDER;
            ForegroundBlock = info.GetValue("ForegroundBlock", ForegroundBlock);
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("ForegroundBlock", ForegroundBlock);
        }

        public void AddLightSource(ILightSource source, LightData data)
        {
            LightSources[source] = data;
            NotifyLightChange();
        }

        public void RemoveLightSource(ILightSource source)
        {
            LightSources.Remove(source);
            NotifyLightChange();
        }

        /// <summary>
        /// Start Listening ILightUpdate Event
        /// </summary>
        public void NotifyLightChange()
        {
            StartListeningEvent<ILightUpdate>();
        }

        public virtual void LightUpdate()
        {
            Vector3 max = Vector3.Zero;
            foreach (KeyValuePair<ILightSource, LightData> pair in LightSources)
            {
                ILightSource source = pair.Key;
                LightData data = pair.Value;
                Vector3 current = source.GetLightEmission(WorldPosition + new Vector2(BLOCK_SIZE_PX / 2), data);
                max = MGMath.Max(current, max);
            }
            Light = max;
            StopListeningEvent<ILightUpdate>();
        }

        public void ReplaceForegroundBlock(ForegroundBlock _newOne)
        {
            var old = ForegroundBlock;
            ForegroundBlock = _newOne;
            if (old.Visible)
            {
                ForegroundBlock.Show();
            }
            old?.Removed();
            NotifyNeibourgs();
        }

        public void DestroyForegroundBlock()
        {
            ReplaceForegroundBlock(new AirBlock(BlockPosition, World, false));
        }

        /// <summary>
        /// Damage the foreground block and return the block loot or nothing if it's not destroyed.
        /// </summary>
        /// <param name="damage"></param>
        /// <returns></returns>
        public ItemType DamageForegroundBlock(int damage = 1)
        {
            if (ForegroundBlock is ForegroundBlock && !(ForegroundBlock is AirBlock))
            {
                ForegroundBlock.Heal -= damage;
                if (ForegroundBlock.Heal <= 0)
                {
                    var item = ForegroundBlock.Loot;
                    DestroyForegroundBlock();
                    return item;
                } else
                {
                    return ItemType.NOTHING;
                }
            } else
            {
                return ItemType.NOTHING;
            }
        }

        public override void Show(int? order = null)
        {
            base.Show();
            ForegroundBlock?.Show();
            LightSources.Keys.ForEach(s => s.IncreaseVisibleAffectedBlock());
        }

        public override void Hide()
        {
            base.Hide();
            ForegroundBlock?.Hide();
            LightSources.Keys.ForEach(s => s.DecreaseVisibleAffectedBlock());
        }

        public override void Dispose()
        {
            base.Dispose();
            ForegroundBlock?.Dispose();
            LightSources.Keys.ForEach(l =>
            {
                l.AffectedBlocks.Remove(this);
            });
            LightSources.Clear();
        }

        public override void NotifyNeibourgs()
        {
            base.NotifyNeibourgs();
            ILightSource.RequestAllLightsUpdate(LightSources.Keys);
        }

        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            base.OnNeibourgChange(originDirection, neibourg);
            ForegroundBlock?.OnNeibourgChange(originDirection, neibourg);
            ILightSource.RequestAllLightsUpdate(LightSources.Keys);
        }

        public override bool IsDirty()
        {
            return (ForegroundBlock?.IsDirty() ?? false) || base.IsDirty();
        }

        public override void OnClick()
        {
            base.OnClick();
            ForegroundBlock?.OnClick();
        }
    }

    [Serializable]
    public class SkyTerrainBlock : TerrainBlock
    {
        public SkyTerrainBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
        }

        protected SkyTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/air"));
        }
    }

    [Serializable]
    public class SnowTerrainBlock : TerrainBlock
    {
        public SnowTerrainBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
        }

        protected SnowTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/top"));
        }
    }

    [Serializable]
    public class DirtTerrainBlock : TerrainBlock
    {
        public DirtTerrainBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
        }

        protected DirtTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/terre"));
        }
    }

    [Serializable]
    public class StoneTerrainBlock : TerrainBlock
    {
        public StoneTerrainBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
        }

        protected StoneTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/pierre"));
        }
    }

    [Serializable]
    public class MagmaTerrainBlock : TerrainBlock
    {
        public override Vector3 NPMMinLight { get; } = new Vector3(0.7f);

        public MagmaTerrainBlock(Point blockPosition, World world) : base(blockPosition, world)
        {
        }

        protected MagmaTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/magma"));
        }
    }
}
