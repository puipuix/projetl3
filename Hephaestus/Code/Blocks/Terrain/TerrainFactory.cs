﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Shapes;
using MGCE.Util;
using Hephaestus.Code.Blocks.Fluids;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Items;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hephaestus.Code.util;
using System.Runtime.Serialization;
using System.Security.Authentication.ExtendedProtection;
using System.Reflection.Metadata.Ecma335;
using MGCE.Util.Caster;
using Hephaestus.Code.Saves;

namespace Hephaestus.Code.Blocks.Terrain
{
    /// <summary>
    /// Base class for terrain factory
    /// </summary>
    [Serializable]
    public abstract class TerrainFactory : IMGSerializable
    {
        public const float PATCH_SIZE = 30.0f;
        public const int OCEAN_LEVEL = -30;
        public const float OCEAN_PROB = 0.1f;
        public const float OCEAN_FACTOR = 0.001f;
        public const int SKY_LIMIT_Y = -50;
        public const int UNDERGROUND_LIMIT_Y = 0;
        public const int RESSOURCE_LIMIT_Y = 10;
        public readonly Point SPAWN_POINT_COAL = new Vector2(GameSave.SPAWN_POINT.X - PATCH_SIZE, RESSOURCE_LIMIT_Y + PATCH_SIZE * 0.5f).ToPoint();
        public readonly Point SPAWN_POINT_IRON = new Vector2(GameSave.SPAWN_POINT.X + PATCH_SIZE, RESSOURCE_LIMIT_Y + PATCH_SIZE * 1.5f).ToPoint();

        /// <summary>
        /// Size of the patch
        /// </summary>
        public double SizeCoef { get; set; } = double.PositiveInfinity;
        public int MinDepthToSpawn { get; set; } = RESSOURCE_LIMIT_Y;

        protected TerrainFactory() { }

        protected TerrainFactory(SerializationInfo info, StreamingContext context)
        {
            SizeCoef = info.GetDouble("SizeCoef");
        }

        public virtual void Serialize(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SizeCoef", SizeCoef);
        }

        public virtual void OnDeserialization(object sender) { }

        private bool ForcePatch(MGPoint patchPosition, MGPoint spawnPoint)
        {
            var patch_size = new MGPoint((int)(PATCH_SIZE));
            var zone = new Rectangle(spawnPoint - (patch_size / 2 + 1), patch_size);
            return zone.Contains(patchPosition);
        }

        private bool ForceCoal(Point patchPosition)
        {
            return ForcePatch(patchPosition, SPAWN_POINT_COAL);
        }

        private bool ForceIron(Point patchPosition)
        {
            return ForcePatch(patchPosition, SPAWN_POINT_IRON);
        }

        /// <summary>
        /// Will create a new terrain
        /// </summary>
        /// <param name="patchPosition">The closest patch position</param>
        /// <param name="blockPosition">The block position</param>
        /// <param name="distance">The distance between thoses 2 points</param>
        /// <returns></returns>
        public virtual TerrainBlock GetNewTerrain(Point patchPosition, Point blockPosition, bool cave, World world, double distance, WorldGenerator generator)
        {
            // we are above spawn ressources limit
            if (patchPosition.Y < RESSOURCE_LIMIT_Y)
            {
                return generator.Tree.DefaultValue.Generate(patchPosition, blockPosition, cave, world, distance, generator);
            } else
            {
                TerrainBlock terrain = null;
                // we look if we need to create a coal or iron patch
                if (ForceCoal(patchPosition))
                {
                    foreach (var factory in generator.Tree)
                    {
                        if (factory is CoalTerrainFactory)
                        {
                            if (distance / PATCH_SIZE > factory.SizeCoef)
                            {
                                terrain = generator.Tree.DefaultValue.Generate(patchPosition, blockPosition, cave, world, distance, generator);
                            } else
                            {
                                terrain = factory.Generate(patchPosition, blockPosition, cave, world, distance, generator);
                            }
                            break;
                        }
                    }
                } else if (ForceIron(patchPosition))
                {
                    foreach (var factory in generator.Tree)
                    {
                        if (factory is IronTerrainFactory)
                        {
                            if (distance / PATCH_SIZE > factory.SizeCoef)
                            {
                                terrain = generator.Tree.DefaultValue.Generate(patchPosition, blockPosition, cave, world, distance, generator);
                            } else
                            {
                                terrain = factory.Generate(patchPosition, blockPosition, cave, world, distance, generator);
                            }
                            break;
                        }
                    }
                }

                // normal generation
                if (terrain is null)
                {
                    if (patchPosition.Y < MinDepthToSpawn || distance / PATCH_SIZE > SizeCoef)
                    {
                        return generator.Tree.DefaultValue.GetNewTerrain(patchPosition, blockPosition, cave, world, distance, generator);
                    } else
                    {
                        return Generate(patchPosition, blockPosition, cave, world, distance, generator);
                    }
                } else
                {
                    return terrain;
                }
            }
        }

        protected abstract TerrainBlock Generate(Point patchPosition, Point blockPosition, bool cave, World world, double distance, WorldGenerator generator);
    }

    [Serializable]
    public class DefaultTerrainFactory : TerrainFactory
    {
        public DefaultTerrainFactory() : base() { }

        protected DefaultTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        private bool IsOcean(Point patchPosition, WorldGenerator generator)
        {
            return Noise.GetBoolFromNoiseResult(generator.OceanNoise.Get(patchPosition.X, OCEAN_LEVEL, OCEAN_FACTOR), OCEAN_PROB);
        }

        private bool IsSkyBlock(Point patchPosition, Point blockPosition, WorldGenerator generator)
        {
            return patchPosition.Y < SKY_LIMIT_Y || (IsOcean(patchPosition, generator) && blockPosition.Y < OCEAN_LEVEL);
        }

        protected override TerrainBlock Generate(Point patchPosition, Point blockPosition, bool cave, World world, double distance, WorldGenerator generator)
        {
            // to high so sky
            if (patchPosition.Y < SKY_LIMIT_Y)
            {
                TerrainBlock sky = new SkyTerrainBlock(blockPosition, world) {
                    ForegroundBlock = new AirBlock(blockPosition, world, true),
                };
                // if under ocean level
                if (IsOcean(patchPosition, generator) && blockPosition.Y >= OCEAN_LEVEL)
                {
                    WaterTerrainFactory.CreateWaterBlock(sky, false, blockPosition.Y == OCEAN_LEVEL ? ForegroundBlock.FULL_BLOCK_MAX_FLUID * 0.1f : ForegroundBlock.FULL_BLOCK_MAX_FLUID, world);
                }
                return sky;
            } else
            // if we generate the top of the world
            if (patchPosition.Y < UNDERGROUND_LIMIT_Y)
            {
                // if ocean
                if (IsOcean(patchPosition, generator))
                {
                    TerrainBlock sky = new SkyTerrainBlock(blockPosition, world) {
                        ForegroundBlock = new AirBlock(blockPosition, world, true),
                    };
                    // if under ocean level
                    if (blockPosition.Y >= OCEAN_LEVEL)
                    {
                        WaterTerrainFactory.CreateWaterBlock(sky, false, blockPosition.Y == OCEAN_LEVEL ? ForegroundBlock.FULL_BLOCK_MAX_FLUID * 0.1f : ForegroundBlock.FULL_BLOCK_MAX_FLUID, world);
                    }
                    return sky;
                } else // dirt
                {
                    // we look the block above
                    Point above = blockPosition - new Point(0, 1);
                    generator.GetGenerationInfo(above, out var _0, out Point patch, out var _1);
                    // if the patch of the block above is above sky limit
                    if (IsSkyBlock(patch, above, generator))
                    {
                        // we place a snow block
                        return new SnowTerrainBlock(blockPosition, world) {
                            ForegroundBlock = cave ? (ForegroundBlock)new AirBlock(blockPosition, world, true) : new CopyTerrainForegroundBlock(blockPosition, world),
                        };
                    } else
                    {
                        // else a dirt block
                        return new DirtTerrainBlock(blockPosition, world) {
                            ForegroundBlock = cave ? (ForegroundBlock)new AirBlock(blockPosition, world, true) : new CopyTerrainForegroundBlock(blockPosition, world),
                        };
                    }
                }
            } else // we are underground
            {
                if (Noise.GetBoolFromNoiseResult(generator.MagmaNoise.Get(patchPosition.X, patchPosition.Y), (double)patchPosition.Y / World.EARTH_SIZE))
                {
                    TerrainBlock terrain = new MagmaTerrainBlock(blockPosition, world) {
                        ForegroundBlock = cave ? (ForegroundBlock)new AirBlock(blockPosition, world, true) : new CopyTerrainForegroundBlock(blockPosition, world),
                    };
                    return terrain;
                } else
                {
                    return new StoneTerrainBlock(blockPosition, world) {
                        ForegroundBlock = cave ? (ForegroundBlock)new AirBlock(blockPosition, world, true) : new CopyTerrainForegroundBlock(blockPosition, world),
                    };
                }
            }
        }
    }

    [Serializable]
    public abstract class GazTerrainFactory : TerrainFactory
    {
        public GazTerrainFactory() : base() { }

        protected GazTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        protected override TerrainBlock Generate(Point patchPosition, Point blockPosition, bool cave, World world, double distance, WorldGenerator generator)
        {
            TerrainBlock terrain = generator.Tree.DefaultValue.GetNewTerrain(patchPosition, blockPosition, true, world, distance, generator);
            CreateGaz(terrain, blockPosition, cave, terrain.ForegroundBlock.MaxFluidAmount, world);
            return terrain;
        }

        protected abstract void CreateGaz(TerrainBlock terrain, Point blockPosition, bool alive, float amount, World world);
    }

    [Serializable]
    public class SO2TerrainFactory : GazTerrainFactory
    {
        protected SO2TerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public SO2TerrainFactory() : base()
        {
            SizeCoef = 5 / PATCH_SIZE;
        }

        protected override void CreateGaz(TerrainBlock terrain, Point blockPosition, bool alive, float amount, World world)
        {
            new SO2(terrain, world, amount, alive, true);
        }
    }

    [Serializable]
    public abstract class LiquidTerrainFactory : TerrainFactory
    {
        protected LiquidTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public LiquidTerrainFactory() : base() { }

        protected override TerrainBlock Generate(Point patchPosition, Point blockPosition, bool cave, World world, double distance, WorldGenerator generator)
        {
            TerrainBlock terrain = generator.Tree.DefaultValue.GetNewTerrain(patchPosition, blockPosition, true, world, distance, generator);
            if (patchPosition.Y <= blockPosition.Y)
            {
                CreateLiquid(terrain, blockPosition, cave, terrain.ForegroundBlock.MaxFluidAmount * (patchPosition.Y == blockPosition.Y ? 0.5f : 1f), world);
            }
            return terrain;
        }

        protected abstract void CreateLiquid(TerrainBlock terrain, Point blockPosition, bool alive, float amount, World world);
    }

    [Serializable]
    public class WaterTerrainFactory : LiquidTerrainFactory
    {
        protected WaterTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public WaterTerrainFactory() : base()
        {
            SizeCoef = 15 / PATCH_SIZE;
        }

        protected override void CreateLiquid(TerrainBlock terrain, Point blockPosition, bool alive, float amount, World world)
        {
            CreateWaterBlock(terrain, alive, amount, world);
        }

        public static void CreateWaterBlock(TerrainBlock terrain, bool alive, float amount, World world)
        {
            new Water(terrain, world, amount, alive, true);
        }
    }

    [Serializable]
    public class PetrolTerrainFactory : LiquidTerrainFactory
    {
        protected PetrolTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public PetrolTerrainFactory() : base()
        {
            SizeCoef = 30 / PATCH_SIZE;
            MinDepthToSpawn = 1500;
        }

        protected override void CreateLiquid(TerrainBlock terrain, Point blockPosition, bool alive, float amount, World world)
        {
            new Petrol(terrain, world, amount, alive, true);
            SoundManager.PlayPetrol();
        }
    }

    [Serializable]
    public class LavaTerrainFactory : LiquidTerrainFactory
    {
        protected LavaTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public LavaTerrainFactory() : base()
        {
            SizeCoef = 10 / PATCH_SIZE;
        }

        protected override void CreateLiquid(TerrainBlock terrain, Point blockPosition, bool alive, float amount, World world)
        {
            new Lava(terrain, world, world.AutoLoadedZone.Contains(blockPosition), amount, alive, true);
            SoundManager.PlayLava();
        }
    }

    [Serializable]
    public abstract class RessourcesTerrainFactory : TerrainFactory
    {

        /// <summary>
        /// Items that can be mined from this terrain
        /// Item, min quantity, max quantity
        /// </summary>
        public List<(ItemType, int, int)> ItemsMinMaxValues { get; set; }

        public RessourcesTerrainFactory() : base() { }

        protected RessourcesTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            ItemsMinMaxValues = info.GetValue("ItemsMinMaxValues", ItemsMinMaxValues);
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("ItemsMinMaxValues", ItemsMinMaxValues);
        }

        protected override TerrainBlock Generate(Point patchPosition, Point blockPosition, bool cave, World world, double distance, WorldGenerator generator)
        {
            List<Couple> items = new List<Couple>();
            foreach (var (type, min, max) in ItemsMinMaxValues)
            {
                int nb = (int)MGMath.Lerp(max, min, distance / (PATCH_SIZE * Math.Min(SizeCoef, 1)));
                items.Add(new Couple(type, nb));
            }
            TerrainBlock t = Generate(blockPosition, world, items);
            t.ForegroundBlock = cave ? (ForegroundBlock)new AirBlock(blockPosition, world, true) : new CopyTerrainForegroundBlock(blockPosition, world);
            return t;
        }

        public abstract TerrainBlock Generate(Point blockPosition, World world, List<Couple> items);
    }

    [Serializable]
    public class IronTerrainFactory : RessourcesTerrainFactory
    {
        protected IronTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public IronTerrainFactory()
        {
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MFER, 50, 500)
            };
            SizeCoef = 9 / PATCH_SIZE;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return CreateIronBlock(blockPosition, world, items);
        }

        public static IronRessourceTerrainBlock CreateIronBlock(Point blockPosition, World world, List<Couple> items)
        {
            return new IronRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class CopperTerrainFactory : RessourcesTerrainFactory
    {
        protected CopperTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public CopperTerrainFactory()
        {
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MCUIVRE, 50, 500)
            };
            SizeCoef = 8 / PATCH_SIZE;
            MinDepthToSpawn = 100;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new CopperRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class CoalTerrainFactory : RessourcesTerrainFactory
    {
        protected CoalTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public CoalTerrainFactory()
        {
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MCHARBON, 50, 500)
            };
            SizeCoef = 12 / PATCH_SIZE;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return CreateCoalBlock(blockPosition, world, items);
        }

        public static CoalRessourceTerrainBlock CreateCoalBlock(Point blockPosition, World world, List<Couple> items)
        {
            return new CoalRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class DiamondTerrainFactory : RessourcesTerrainFactory
    {
        protected DiamondTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public DiamondTerrainFactory()
        {
            SizeCoef = 2 / PATCH_SIZE;
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MDIAMANT, 5, 50)
            };
            MinDepthToSpawn = 600;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new DiamondRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class GoldTerrainFactory : RessourcesTerrainFactory
    {
        protected GoldTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public GoldTerrainFactory()
        {
            SizeCoef = 6 / PATCH_SIZE;
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MOR, 50, 500)
            };
            MinDepthToSpawn = 2000;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new GoldRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class TungstenTerrainFactory : RessourcesTerrainFactory
    {
        protected TungstenTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public TungstenTerrainFactory()
        {
            SizeCoef = 7 / PATCH_SIZE;
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MTUNGSTEN, 50, 500)
            };
            MinDepthToSpawn = 500;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new TungstenRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class SilverTerrainFactory : RessourcesTerrainFactory
    {
        protected SilverTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public SilverTerrainFactory()
        {
            SizeCoef = 6 / PATCH_SIZE;
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MARGENT, 25, 300)
            };

            MinDepthToSpawn = 2000;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new SilverRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class PlatinumTerrainFactory : RessourcesTerrainFactory
    {
        protected PlatinumTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public PlatinumTerrainFactory()
        {
            SizeCoef = 4 / PATCH_SIZE;
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MPLATINE, 5, 100)
            };
            MinDepthToSpawn = 3000;
        }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new PlatiniumRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class UraniumTerrainFactory : RessourcesTerrainFactory
    {
        public UraniumTerrainFactory()
        {
            SizeCoef = 8 / PATCH_SIZE;
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MURANIUM, 50, 500)
            };
            MinDepthToSpawn = 100;
        }
        protected UraniumTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new UraniumRessourceTerrainBlock(blockPosition, world, items);
        }
    }

    [Serializable]
    public class ZirconTerrainFactory : RessourcesTerrainFactory
    {
        public ZirconTerrainFactory()
        {
            SizeCoef = 5 / PATCH_SIZE;
            ItemsMinMaxValues = new List<(ItemType, int, int)>()
            {
                (ItemType.MZIRCON, 50, 500)
            };
            MinDepthToSpawn = 100;
        }
        protected ZirconTerrainFactory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public override TerrainBlock Generate(Point blockPosition, World world, List<Couple> items)
        {
            return new ZirconRessourceTerrainBlock(blockPosition, world, items);
            ;
        }
    }
}
