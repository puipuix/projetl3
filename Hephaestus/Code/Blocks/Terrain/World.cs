﻿//#define OFFSCREEN_VISIBLE

using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Events;
using MGCE.Control;
using MGCE.Engine;
using MGCE.Shapes;
using MGCE.Util;
using MGCE.Actor;
using Hephaestus.Code.Util;
using System.Runtime.Serialization;
using Hephaestus.Code.Util.Light;
using MGCE.Input;
using Hephaestus.Code.Saves;
using Hephaestus.Code.View;
using Hephaestus.Code.util;
using Microsoft.Xna.Framework.Input;
using Hephaestus.Code.Character;

namespace Hephaestus.Code.Blocks.Terrain
{
    public class BlockEventArgs : EventArgs
    {
        public TerrainBlock Block { get; }
       

        public BlockEventArgs(TerrainBlock block)
        {
            Block = block;
        }
    }

    [Serializable]
    public class World : MGEventsListenerActor, IMGSerializable, IMGDisposable, IMGEventPostUpdateListener
    {
        public const int EARTH_SIZE = 6371000;
        public const float OFF_SCREEN_AUTOLOAD_DISTANCE_FACTOR = 1.3f;

        public WorldGenerator Generator { get; set; }

        public MGClickableManager ClickManager { get; set; }

        /// <summary>
        /// If you can click on the world
        /// </summary>
        public bool IsWorldClickable { get; set; } = true;

        /// <summary>
        /// If the world isn't a decorative world
        /// </summary>
        public bool IsWorldPlayable { get; set; } = true;

        public Rectangle AutoLoadedZone { get; set; }

        public Vector2 AutoLoadDistance => MGEngine.BufferCenter / Block.BLOCK_SIZE_PX;

        public float Gravity { get; set; } = 9.807f;

        public Dictionary<Point, TerrainBlock> LoadedTerrain { get; set; }

        /// <summary>
        /// Called when a new terrain is generated
        /// </summary>
        public event EventHandler<BlockEventArgs> OnTerrainGenerated;
        public EventHandler<BlockEventArgs> GetOnTerrainGenerated() { return OnTerrainGenerated; }

        public WorldCamera Camera { get; set; }

        public SunLight SunLight { get; set; }

        public bool IsDisposed { get; private set; }

        public GameSave GameSave { get; set; }

        public event EventHandler OnGC;

        public TerrainBlock this[Point p]
        {
            get => LoadedTerrain.GetOrAdd(p, () => Generator.Generate(p, this));
            set => LoadedTerrain.Add(p, value);
        }

        private World()
        {
            MGDebug.PrintLine("WORLD CREATE");
            MGEngine.OnGC += MGEngine_OnGC;
            AutoLoadedZone = Rectangle.Empty;
            ClickManager = new MGClickableManager() {
                HitBox = new MGRectangle(MGEngine.BufferSize.ToVector2() - new Vector2(0, 32 + 110)),
            };

            MGEngine.OnBufferSizeChange += (o, a) => {
                ClickManager.HitBox = new MGRectangle(MGEngine.BufferSize.ToVector2() - new Vector2(0, 32 + 110));
                AutoLoad(ComputeAutoLoadZone());
            };

            ClickManager.OnMouseClickEnd += (obj, args) => {
                if (args.Button == MGInputNames.M_Left)
                {
                    Point p = BlockRelativePositionActor.WorldPositionToBlockPosition(Camera.MouseWorldPosition);
                    this[p].OnClick();
                }
            };

        }

        private void MGEngine_OnGC(object sender, EventArgs e)
        {
            GC();
        }

        public World(WorldGenerator generator) : this()
        {
            Generator = generator;
            LoadedTerrain = new Dictionary<Point, TerrainBlock>();
            Camera = new WorldCamera();
            SunLight = new SunLight(this);
            OnDeserialization(this);
        }

        protected World(SerializationInfo info, StreamingContext context) : this()
        {
            Generator = info.GetValue("Generator", Generator);
            Gravity = info.GetValue<float>("Gravity");
            LoadedTerrain = info.GetValue("LoadedTerrain", LoadedTerrain);
            Camera = info.GetValue("Camera", Camera);
            SunLight = new SunLight(this) {
                CurrentDuration = new TimeSpan(info.GetInt64("DayTime"))
            };
        }

        public virtual void Serialize(SerializationInfo info, StreamingContext context)
        {
            info.AddValues(
                ("Generator", Generator),
                ("Gravity", Gravity),
                ("LoadedTerrain", LoadedTerrain),
                ("Camera", Camera),
                ("DayTime", SunLight.CurrentDuration.Ticks));
        }

        public virtual void OnDeserialization(object sender)
        {
            float max = Math.Max(MyGame.Game.GraphicsDevice.Viewport.Width, MyGame.Game.GraphicsDevice.Viewport.Height);
            Camera.MinZoom = max * OFF_SCREEN_AUTOLOAD_DISTANCE_FACTOR / WorldCamera.MAX_BUFFER_SIZE;
            Camera.MaxZoom = max * OFF_SCREEN_AUTOLOAD_DISTANCE_FACTOR / WorldCamera.MIN_BUFFER_SIZE;
            Camera.OnZoomChange += (obj, args) => AutoLoad(ComputeAutoLoadZone());
        }

        /// <summary>
        /// return if the block is loaded and, if true, set his value into block 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="block">the block if exist</param>
        /// <returns>If the block exist</returns>
        public bool GetIfExist(Point p, out TerrainBlock block)
        {
            return LoadedTerrain.TryGetValue(p, out block);
        }

        /// <summary>
        /// Return the block if loaded or null
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public TerrainBlock GetOrNull(Point p)
        {
            return LoadedTerrain is null ? null : LoadedTerrain.TryGetValue(p, out TerrainBlock block) ? block : null;
        }

        /// <summary>
        /// Load a block
        /// </summary>
        /// <param name="p"></param>
        public void Load(Point p)
        {
            this[p].Show();
        }

        /// <summary>
        /// Unload a block
        /// </summary>
        /// <param name="p"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool UnLoad(Point p, TerrainBlock t)
        {
            t.Dispose();
            return LoadedTerrain.Remove(p);
        }
        /// <summary>
        /// Unload a block
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool UnLoad(Point p)
        {
            return LoadedTerrain.TryGetValue(p, out TerrainBlock t) ? UnLoad(p, t) : false;
        }

        /// <summary>
        /// Unload a block if it can be destroyed
        /// </summary>
        /// <param name="p"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool TryUnLoad(Point p, Func<TerrainBlock, Point, bool> canDestroy)
        {
            if (LoadedTerrain.TryGetValue(p, out TerrainBlock t))
            {
                if (canDestroy(t, p))
                {
                    return UnLoad(p, t);
                } else
                {
#if !OFFSCREEN_VISIBLE
                    t.Hide();
#endif
                    return false;
                }
            } else
            {
                return false;
            }
        }

        /// <summary>
        /// Load a zone
        /// </summary>
        /// <param name="r"></param>
        public void Load(Rectangle r)
        {
            for (int x = r.Left; x < r.Right; x++)
            {
                for (int y = r.Top; y < r.Bottom; y++)
                {
                    Load(new Point(x, y));
                }
            }
        }

        /// <summary>
        /// Unload a zone
        /// </summary>
        /// <param name="r"></param>
        public void UnLoad(Rectangle r)
        {
            for (int x = r.Left; x < r.Right; x++)
            {
                for (int y = r.Top; y < r.Bottom; y++)
                {
                    UnLoad(new Point(x, y));
                }
            }
        }

        /// <summary>
        /// Return if need to make a autoload (the screen will contain non-loaded blocks)
        /// </summary>
        /// <returns></returns>
        public bool NeedAutoLoad()
        {
            Vector2 distance = AutoLoadDistance * Math.Max(OFF_SCREEN_AUTOLOAD_DISTANCE_FACTOR - 1, 0) / Camera.Zoom;
            Point position = BlockRelativePositionActor.WorldPositionToBlockPosition(Camera.WorldPosition);
            bool x = Math.Abs(AutoLoadedZone.Center.X - position.X) > distance.X;
            bool y = Math.Abs(AutoLoadedZone.Center.Y - position.Y) > distance.Y;
            return AutoLoadedZone.IsEmpty || x || y;
        }

        /// <summary>
        /// Return the zone that is visible on screen
        /// </summary>
        /// <returns></returns>
        public Rectangle ComputeAutoLoadZone()
        {
            Vector2 distance = AutoLoadDistance * OFF_SCREEN_AUTOLOAD_DISTANCE_FACTOR / Camera.Zoom;
            return new Rectangle(BlockRelativePositionActor.WorldPositionToBlockPosition(Camera.WorldPosition) - distance.ToPoint() - new Point(1), (distance * 2).ToPoint() + new Point(2));
        }
                
        public void AutoLoad(Rectangle zone)
        {
            // LOAD
            for (int x = zone.Left; x < zone.Right; x++)
            {
                for (int y = zone.Top; y < zone.Bottom; y++)
                {
                    if (!AutoLoadedZone.Contains(x, y))
                    {
                        Load(new Point(x, y));
                    }
                }
            }

            // UNLOAD
            for (int x = AutoLoadedZone.Left; x < AutoLoadedZone.Right; x++)
            {
                for (int y = AutoLoadedZone.Top; y < AutoLoadedZone.Bottom; y++)
                {
                    Point p = new Point(x, y);
                    if (!zone.Contains(p))
                    {
                        TryUnLoad(p, (t, p) => t.IsForgetable);
                    }
                }
            }
            AutoLoadedZone = zone;
            Camera.BufferSize = (zone.Size.ToVector2() * Block.BLOCK_SIZE_PX).ToPoint();
        }

        int x = 0, y = 0;

        public void PostUptate(GameTime gameTime)
        {
            if (NeedAutoLoad())
            {
                AutoLoad(ComputeAutoLoadZone());
            }
            if (IsWorldPlayable)
            {
                if (IsWorldClickable)
                {
                    ClickManager.Update();
                    if (MGInputNames.R.IsPressEnd())
                    {
                        Hud.IncreaseRotation();
                    }

                    if (MGInputNames.I.IsPressEnd())
                    {
                        Hud.InventoryView(x, y);
                    }
                }
                Hud.updateLifeHud();
                if (MGInputNames.Escape.IsPressEnd())
                {
                    PauseMenu.SwitchVisible(GameSave);
                }
            }
        }

        private readonly Stack<Point> toGc = new Stack<Point>();
        /// <summary>
        /// Will remove a lot of blocks
        /// </summary>
        /// <param name="clearMax">If <see langword="true"/> will remove all natural blocks</param>
        /// <param name="callOnGc">Call the event onGC</param>
        public void GC(bool clearMax = false, bool callOnGc = true)
        {
            int nb = LoadedTerrain.Count;
            LoadedTerrain.Where(
                p => clearMax ? p.Value.IsNatural : p.Value.IsForgetable && !AutoLoadedZone.Contains(p.Key)
                ).ForEach(p => toGc.Push(p.Key));

            while (!toGc.IsEmpty())
            {
                UnLoad(toGc.Pop());
            }
            if (clearMax)
            {
                AutoLoadedZone = Rectangle.Empty;
            }
            MGDebug.PrintLine((nb - LoadedTerrain.Count) + " blocks unloaded out of " + nb + "->" + LoadedTerrain.Count);
            if (callOnGc)
            {
                MGDebug.PrintLine("call? " + OnGC.Target);
                OnGC?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Enable the world's update
        /// </summary>
        public void Activate()
        {
            SunLight?.Refresh();
            SunLight?.Activate();
            StartListeningEvent<IMGEventPostUpdateListener>();
        }

        public void Dispose()
        {
            MGEngine.OnGC -= MGEngine_OnGC;
            StopListeningAll();
            foreach (var p in LoadedTerrain.ToArray())
            {
                UnLoad(p.Key, p.Value);
            }
            Camera.Dispose();
            SunLight?.Disactivate();
            IsDisposed = true;
            OnGC = null;
        }
    }
}
