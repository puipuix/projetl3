﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Interfaces.Blocks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Util;
using MGCE.Engine;
using MGCE.Events;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using System.Runtime.Serialization;
using Hephaestus.Code.Util;

namespace Hephaestus.Code.Blocks.Terrain
{
	/// <summary>
	/// Terrain with an inventory.
	/// </summary>
	[Serializable]
	public abstract class RessourceTerrain : TerrainBlock
	{
		public Stockage Stockage { get; set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="blockPosition"></param>
		/// <param name="world"></param>
		/// <param name="items">What are the block's items/param>
		public RessourceTerrain(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world)
		{
			SpriteSheet?.PlayAnimation(FOREGROUND_ANIMATION, false);

			Stockage = new Stockage(items.Sum(c => c.GetQuantite() / 60 + 1),
				new Filtre(new List<ItemType>() { ItemType.NOTHING })) {
				items = items
			};
		}

		public RessourceTerrain(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			Stockage = info.GetValue("Stockage", Stockage);
			SpriteSheet?.PlayAnimation(FOREGROUND_ANIMATION, false);
		}

		public override void Serialize(SerializationInfo info, StreamingContext context)
		{
			base.Serialize(info, context);
			info.AddValue("Stockage", Stockage);
		}

		public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
		{
			if (IsDirty())
			{
				SpriteSheet.SetAnimation(DEFAULT_ANIMATION, false);
				base.Draw(gameTime, spriteBatch);
				SpriteSheet.SetAnimation(FOREGROUND_ANIMATION, false);
				if (MGSpriteSheet.IsReady(SpriteSheet))
				{
					spriteBatch.Draw(SpriteSheet, this, 1);
				}
				IsDirtyState = false;
			}
		}

		protected override MGSpriteSheet CreateSpriteSheet()
		{
			var ss = MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/pierre", MyGame.Game.Content));
			ss.AddAnimation(CreateForegroundAnimation(FOREGROUND_ANIMATION));
			return ss;
		}

		protected abstract MGSpriteAnimation CreateForegroundAnimation(string animationName);
	}

	[Serializable]
	public class IronRessourceTerrainBlock : RessourceTerrain
	{
		public IronRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public IronRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/fer"));
		}
	}

	[Serializable]
	public class CopperRessourceTerrainBlock : RessourceTerrain
	{
		public CopperRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public CopperRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/cuivre"));
		}
	}

	[Serializable]
	public class CoalRessourceTerrainBlock : RessourceTerrain
	{
		public CoalRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public CoalRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/charbon"));
		}
	}

	[Serializable]
	public class DiamondRessourceTerrainBlock : RessourceTerrain
	{
		public DiamondRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public DiamondRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/diamant"));
		}
	}

	[Serializable]
	public class GoldRessourceTerrainBlock : RessourceTerrain
	{
		public GoldRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public GoldRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/or"));
		}
	}

	[Serializable]
	public class TungstenRessourceTerrainBlock : RessourceTerrain
	{
		public TungstenRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public TungstenRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/tungstene"));
		}
	}

	[Serializable]
	public class SilverRessourceTerrainBlock : RessourceTerrain
	{
		public SilverRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public SilverRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/argent"));
		}
	}

	[Serializable]
	public class PlatiniumRessourceTerrainBlock : RessourceTerrain
	{
		public PlatiniumRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public PlatiniumRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/platine"));
		}
	}

	[Serializable]
	public class UraniumRessourceTerrainBlock : RessourceTerrain
	{
		public UraniumRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public UraniumRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/uranium"));
		}
	}

	[Serializable]
	public class ZirconRessourceTerrainBlock : RessourceTerrain
	{
		public ZirconRessourceTerrainBlock(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public ZirconRessourceTerrainBlock(Point blockPosition, World world, List<Couple> items) : base(blockPosition, world, items)
		{
		}

		protected override MGSpriteAnimation CreateForegroundAnimation(string animationName)
		{
			return MGSpriteAnimation.NewSingleFrameFromTexture(animationName, RessourceManager.Load<Texture2D>("Environnement/zircon"));
		}
	}
}
