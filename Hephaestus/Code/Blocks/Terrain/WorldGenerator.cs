﻿using Microsoft.Xna.Framework;
using MGCE.Engine;
using MGCE.Util;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Interfaces.Blocks;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;

namespace Hephaestus.Code.Blocks.Terrain
{
    [Serializable]
    public class WorldGenerator : IMGSerializable
    {
        public const float REPARTITION_MAX_VALUE = 100.0f;

        public static RepartitionTree<TerrainFactory> DefaultTree { get; private set; }

        static WorldGenerator()
        {
            DefaultTree = new RepartitionTree<TerrainFactory>(new DefaultTerrainFactory());

            // All ressources repartitions
            List<Tuple<TerrainFactory, float>> factories = new List<Tuple<TerrainFactory, float>>
            {
                new Tuple<TerrainFactory, float>(new DiamondTerrainFactory(), 0.1f),
                new Tuple<TerrainFactory, float>(new IronTerrainFactory(), 3f),
                new Tuple<TerrainFactory, float>(new CopperTerrainFactory(), 2f),
                new Tuple<TerrainFactory, float>(new CoalTerrainFactory(), 5f),
                new Tuple<TerrainFactory, float>(new GoldTerrainFactory(), 0.5f),
                new Tuple<TerrainFactory, float>(new TungstenTerrainFactory(), 1.2f),
                new Tuple<TerrainFactory, float>(new SilverTerrainFactory(), 0.6f),
                new Tuple<TerrainFactory, float>(new ZirconTerrainFactory(), 0.3f),
                new Tuple<TerrainFactory, float>(new PlatinumTerrainFactory(), 0.2f),
                new Tuple<TerrainFactory, float>(new UraniumTerrainFactory(), 1f),

                new Tuple<TerrainFactory, float>(new WaterTerrainFactory(), 2f),
                new Tuple<TerrainFactory, float>(new PetrolTerrainFactory(), 0.1f),
                new Tuple<TerrainFactory, float>(new LavaTerrainFactory(), 0.5f),

                new Tuple<TerrainFactory, float>(new SO2TerrainFactory(), 1f),
            };

            // All all ressources in the tree
            float ressourceRate = factories.Sum(t => t.Item2);
            float inc = (REPARTITION_MAX_VALUE - ressourceRate) / (factories.Count + 1);
            float start = inc;
            foreach (Tuple<TerrainFactory, float> tuple in factories)
            {
                DefaultTree.Add(tuple.Item1, start, start + tuple.Item2);
                start += tuple.Item2 + inc;
            }
            MGDebug.PrintLine("SpawnRate " + ressourceRate);
        }

        public RepartitionTree<TerrainFactory> Tree { get; set; }

        /// <summary>
        /// The noise used to generate the world
        /// </summary>
        public PatchNoise TerrainNoise { get; set; }
        public (SimplexNoise, SimplexNoise)[] CaveNoises { get; set; }
        public SimplexNoise OceanNoise { get; set; }
        public RandomNoise MagmaNoise { get; set; }
        public double CaveProbability { get; set; } = 0.05;
        public double CaveFactor { get; set; } = 0.005;
        public double CaveCenter { get; set; } = 0.5;
        public int Seed => TerrainNoise.Seed;

        public WorldGenerator(int seed, RepartitionTree<TerrainFactory> tree, int cavePairNoise = 5)
        {
            TerrainNoise = new PatchNoise(seed, TerrainFactory.PATCH_SIZE);
            OceanNoise = new SimplexNoise(seed);
            MagmaNoise = new RandomNoise(seed);
            Tree = tree;

            CaveNoises = new (SimplexNoise, SimplexNoise)[cavePairNoise];
            Random r = new Random(seed);
            for (int i = 0; i < cavePairNoise; i++)
            {
                CaveNoises[i] = (new SimplexNoise(r.Next()), new SimplexNoise(r.Next()));
            }            
        }

        public WorldGenerator(SerializationInfo info, StreamingContext context) : this(info.GetInt32("Seed"), info.GetValue<RepartitionTree<TerrainFactory>>("Tree"))
        {
            CaveProbability = info.GetDouble("CaveProbability");
            CaveFactor = info.GetDouble("CaveFactor");
        }

        public void Serialize(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Seed", Seed);
            info.AddValue("Tree", Tree);
            info.AddValue("CaveProbability", CaveProbability);
            info.AddValue("CaveFactor", CaveFactor);
        }

        public void OnDeserialization(object sender) { }

        private bool InRangeCave(Point point)
        {
            bool cave = false;
            for (int i = 0; i < CaveNoises.Length && !cave; i++)
            {
                double p1 = Noise.GetPercentFromNoiseResult(CaveNoises[i].Item1.Get(point.X, point.Y, CaveFactor));
                double p2 = Noise.GetPercentFromNoiseResult(CaveNoises[i].Item2.Get(point.X, point.Y, CaveFactor));
                cave = CaveCenter - (CaveProbability / 2.0) < p1 && p1 < CaveCenter + (CaveProbability / 2.0)
                    && CaveCenter - (CaveProbability / 2.0) < p2 && p2 < CaveCenter + (CaveProbability / 2.0);
            }
            return cave;
        }

        public void GetGenerationInfo(Point point, out TerrainFactory factory, out Point patch, out double dist)
        {
            factory = Tree.Find(Noise.GetPercentFromNoiseResult(TerrainNoise.GetPointData(point.X, point.Y, out MGVector2Double patchVec, out dist)) * REPARTITION_MAX_VALUE);
            patch = patchVec.ToVector2().ToPoint();
        }

        public TerrainBlock Generate(Point point, World world)
        {
            GetGenerationInfo(point, out TerrainFactory factory, out Point patch, out double dist);
            bool cave = InRangeCave(point);
            TerrainBlock t = factory.GetNewTerrain(patch, point, cave, world, dist, this);
            t.Light = Vector3.Zero;
            MGTools.SafeInvoke(world.GetOnTerrainGenerated(), world, new BlockEventArgs(t));
            return t;
        }
    }
}
