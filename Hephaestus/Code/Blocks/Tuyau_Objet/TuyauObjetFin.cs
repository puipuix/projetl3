﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Blocks.Terrain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Blocks.Tuyau_Objet
{
    [Serializable]
    class TuyauObjetFin : TuyauObjet
    {
        public TuyauObjetFin(Point blockPosition,Orientation o, World world) : base(blockPosition, world,o)
        {

        }
    }
}
