﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Interfaces.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Actor;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Util;
using Hephaestus.Code.Blocks.Foreground;
using MGCE.Engine;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.Character;
using System.Runtime.Serialization;
using MGCE.Util;
using System.Windows.Forms;

namespace Hephaestus.Code.Blocks.Tuyau_Objet
{
    [Serializable]
    class TuyauObjet : ForegroundBlock, IPath
    {
        
        private bool destroyed=false;
        private static int idgiver=0;
        public int id;
        public List<Item> listeItem;
        int max;
        public override bool IsNatural => false;
        public IPath next;
        public Orientation orientation;
        public override ItemType Loot => ItemType.TUYAUX;

        public TuyauObjet(Point blockPosition, World world, Orientation o, int _q = 2, string lt = "Transports/tuyaux_l", string rt = "Transports/tuyaux_r", string dt = "Transports/tuyaux_d", string ut = "Transports/tuyaux_u") : base(blockPosition, world)
        {
            id = idgiver;
            idgiver++;
            listeItem = new List<Item>();
            orientation = o;
            max = _q;

            SpriteSheet.AddAnimation(orientation switch//ajoute la bonne image au tuyaux
            {
                Orientation.LEFT => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(lt)),
                Orientation.RIGHT => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(rt)),
                Orientation.DOWN => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(dt)),
                Orientation.UP => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(ut)),
                _ => new MGSpriteAnimation(DEFAULT_ANIMATION),
            }, true) ;

            MachineManager.GetMachineManager().AddElement(this);
        }
        public TuyauObjet(SerializationInfo info, StreamingContext context, int _q = 2, string lt = "Transports/tuyaux_l", string rt = "Transports/tuyaux_r", string dt = "Transports/tuyaux_d", string ut = "Transports/tuyaux_u") : base(info, context)
        {
            max = _q;
            orientation = info.GetValue("orientation", orientation);
            next = info.GetValue("next", next);
            listeItem = info.GetValue("listeItem", listeItem);
            SpriteSheet.AddAnimation(orientation switch
            {
                Orientation.LEFT => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(lt)),
                Orientation.RIGHT => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(rt)),
                Orientation.DOWN => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(dt)),
                Orientation.UP => MGSpriteAnimation.NewSingleFrameFromTexture(DEFAULT_ANIMATION, RessourceManager.Load<Texture2D>(ut)),
                _ => new MGSpriteAnimation(DEFAULT_ANIMATION),
            }, true);

        }

        public TuyauObjet(SerializationInfo info, StreamingContext context) : this(info, context, 2) { }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValues(("orientation", orientation), ("next", next), ("listeItem", listeItem));
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return new MGSpriteSheet();
        }

        public void setPlace(World world, Point point)
        {


        }

        
        /// <summary>
        /// si le tuyaux est orienté a droite
        /// </summary>
        /// <param name="point">coordonnée dans le monde</param>
        /// <param name="neighbour">type du voisin</param>
        public void rightOrientation(Point point, Block neighbour)
        {
            //point.X = point.X + 1; if (typeof(IPath).IsInstanceOfType(world[point].ForegroundBlock)) ((IPath)world[point].ForegroundBlock).SetNext(this);
            MGDebug.PrintLine( ((IPath)neighbour).verifOrientation(this));

            if (typeof(IPath).IsInstanceOfType(neighbour) && point.X == 1 && ((IPath)neighbour).verifOrientation(this))
            {

                SetNext((IPath)neighbour);
            }
            else if (typeof(IPath).IsInstanceOfType(neighbour) && point.X == -1 && ((IPath)neighbour).verifOrientation(this))
            {
                ((IPath)neighbour).SetNext(this);
            }

        }

        /// <summary>
        /// si le tuyaux est orienté a gauche
        /// </summary>
        /// <param name="point">coordonnée dans le monde</param>
        /// <param name="neighbour">type du voisin</param>
        public void leftOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour) && point.X == -1 && ((IPath)neighbour).verifOrientation(this))
            {
                SetNext((IPath)neighbour);
            }
            else if (typeof(IPath).IsInstanceOfType(neighbour) && point.X == 1 && ((IPath)neighbour).verifOrientation(this))
            {
                ((IPath)neighbour).SetNext(this);
            }
            MGDebug.PrintLine("done");
        }

        /// <summary>
        /// si le tuyaux est orienté en haut
        /// </summary>
        /// <param name="point">coordonnée dans le monde</param>
        /// <param name="neighbour">type du voisin</param>
        public void upOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour) && point.Y == -1 && ((IPath)neighbour).verifOrientation(this))
            {
                SetNext((IPath)neighbour);
            }
            else if (typeof(IPath).IsInstanceOfType(neighbour) && point.Y == 1 && ((IPath)neighbour).verifOrientation(this))
            {
                ((IPath)neighbour).SetNext(this);
            }
            MGDebug.PrintLine("done");
        }
        /// <summary>
        /// si le tuyaux est orienté en bas
        /// </summary>
        /// <param name="point">coordonnée dans le monde</param>
        /// <param name="neighbour">type du voisin</param>
        public void downOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour) && point.Y == 1 && ((IPath)neighbour).verifOrientation(this))
            {
                SetNext((IPath)neighbour);
            }
            else if (typeof(IPath).IsInstanceOfType(neighbour) && point.Y == -1 && ((IPath)neighbour).verifOrientation(this))
            {
                ((IPath)neighbour).SetNext(this);
            }
            MGDebug.PrintLine("done");
        }

        /// <summary>
        /// override permettant de verifier si le voisin ajouté doit ou non etre set en structure suivante
        /// </summary>
        /// <param name="originDirection"></param>
        /// <param name="neibourg"></param>
        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            base.OnNeibourgChange(originDirection, neibourg);
            neibourg = Player.player.world[BlockPosition + originDirection].ForegroundBlock;
            if(typeof(IPath).IsInstanceOfType(neibourg))
            switch (orientation)//choix de la fonction selon l'orientation actuel du block
            {
                case Orientation.LEFT: leftOrientation(originDirection, neibourg); break;
                case Orientation.RIGHT: rightOrientation(originDirection, neibourg); break;
                case Orientation.DOWN: downOrientation(originDirection, neibourg); break;
                case Orientation.UP: upOrientation(originDirection, neibourg); break;

                default: break;
            }
        }

        /// <summary>
        /// permet de verifier que l'orientation actuelle et compatible pour la connection a une structure
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool verifOrientation(IPath path)
        {
            if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(this))
                return ((TuyauObjetCoudeT1)this).verifOrientation(path);
            if (typeof(Production).IsInstanceOfType(path))
                return Orientation.LEFT == orientation || Orientation.RIGHT == orientation;
            if (typeof(TuyauObjet).IsInstanceOfType(path))
                return ((TuyauObjet)path).orientation == orientation;

            return false;
        }


        /// <summary>
        /// permet de faire entrer un item
        /// </summary>
        /// <param name="_i"></param>
        /// <param name="quantite"></param>
        /// <returns></returns>
        public bool Enter(Item _i,int quantite=1)
        {
            
            if (listeItem.Count < max)
            {
                listeItem.Add(_i);
                return true;
            }
            else
            {
              /*  if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(this))
                    MGDebug.PrintLine("Can't add because max = " + max + " quantity actual = " + listeItem.Count);*/
                return false;
            }
        }

        /// <summary>
        /// retourne la liste actuel des item dans le tuyaux
        /// </summary>
        /// <returns></returns>
        public List<Item> GetItems()
        {
            return listeItem;
        }

        /// <summary>
        /// permet de faire sortir un item du tuyaux
        /// </summary>
        /// <returns></returns>
        public virtual bool Exit()
        {

            if (listeItem.Count > 0 && next != null && !next.IsRemoved() && next.Enter(listeItem.ElementAt(0)))
            {
                listeItem.RemoveAt(0);
                return true;
            }

            else return false;
        }

        public void Update()
        {
            //??
        }

        public virtual bool SetNext(IPath _next)
        {
            next = _next;
            return true;
        }

        public bool Available(int _q)
        {
            return listeItem.Count + _q <= max;
        }

        public bool IsRemoved()
        {
            return destroyed;
        }

        public override void Removed()
        {
            next = null;
            destroyed = true;
            base.Removed();
        }
    }

    public enum Orientation
    {
        LEFT, RIGHT, UP, DOWN
    }

   
}
