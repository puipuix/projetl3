﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Blocks.Tuyau_Objet
{

    [Serializable]
    class TuyauObjetT : TuyauObjet
    {


        public TuyauObjetT(Point blockPosition, World world,Orientation o) : base(blockPosition, world,o, 1)
        {
           
         
        }

    }
}
