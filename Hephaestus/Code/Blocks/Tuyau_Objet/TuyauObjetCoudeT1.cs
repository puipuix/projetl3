using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Character;
using MGCE.Engine;
using System.Runtime.Serialization;
using Hephaestus.Code.Items;

namespace Hephaestus.Code.Blocks.Tuyau_Objet
{
    [Serializable]
    class TuyauObjetCoudeT1 : TuyauObjet
    {
        public override ItemType Loot => ItemType.TUYAUX_COURBET1;
        public TuyauObjetCoudeT1(Point blockPosition, World world, Orientation o) : base(blockPosition, world, o, 2, "Transports/tuyaux_courbe_ldl", "Transports/tuyaux_courbe_urr", "Transports/tuyaux_courbe_rdd", "Transports/tuyaux_courbe_ulu")
        {

        }

        public TuyauObjetCoudeT1(SerializationInfo info, StreamingContext context) : base(info, context, 2, "Transports/tuyaux_courbe_ldl", "Transports/tuyaux_courbe_urr", "Transports/tuyaux_courbe_rdd", "Transports/tuyaux_courbe_ulu")
        {

        }

        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            MGDebug.PrintLine("started");
            base.OnNeibourgChange(originDirection, neibourg);
            neibourg = Player.player.world[BlockPosition + originDirection].ForegroundBlock;
            if (originDirection.X == 1 && originDirection.Y == 0)
                rightOrientation(originDirection, neibourg);
            else if (originDirection.X == -1 && originDirection.Y == 0)
                leftOrientation(originDirection, neibourg);
            else if (originDirection.X == 0 && originDirection.Y == 1)
                downOrientation(originDirection, neibourg);
            else if (originDirection.X == 0 && originDirection.Y == -1)
                upOrientation(originDirection, neibourg);
        }
        /// <summary>
        /// personne a gauche
        /// </summary>
        /// <param name="point"></param>
        /// <param name="neighbour"></param>
        public new void leftOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {   //si sortie a gauche
                if (orientation == Orientation.LEFT)
                {
                    //si usine
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.DOWN) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.LEFT))
                    {
                        MGDebug.PrintLine("here");
                        SetNext((IPath)neighbour);
                    }
                }
                else if (orientation == Orientation.UP)
                {
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.RIGHT) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.RIGHT))
                    {
                        SetNext((IPath)neighbour);
                    }
                }
            }
        }

        public new void rightOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {   //si sortie a gauche
                if (orientation == Orientation.RIGHT)
                {
                    //si usine
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.UP) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.RIGHT))
                    {
                        SetNext((IPath)neighbour);
                    }
                }
                else if (orientation == Orientation.DOWN)
                {
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.LEFT) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.LEFT))
                    {
                        SetNext((IPath)neighbour);
                    }
                }
            }
        }

        public new void upOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {   //si sortie en haut
                if (orientation == Orientation.UP)
                {
                    //si usine ou tuyaux
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.LEFT) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.UP))
                    {
                        MGDebug.PrintLine("here");
                        SetNext((IPath)neighbour);
                    }
                }
                else if (orientation == Orientation.RIGHT)
                {
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.DOWN) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.DOWN))
                    {
                        SetNext((IPath)neighbour);
                    }
                }
            }
        }

        public new void downOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {   //si sortie a gauche
                if (orientation == Orientation.DOWN)
                {
                    //si usine
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.RIGHT) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.DOWN))
                    {
                        SetNext((IPath)neighbour);
                    }
                }
                else if (orientation == Orientation.LEFT)
                {
                    if (typeof(Usine).IsInstanceOfType(neighbour) || (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour) && ((TuyauObjetCoudeT1)neighbour).orientation == Orientation.UP) ||
                        (typeof(TuyauObjet).IsInstanceOfType(neighbour) && ((TuyauObjet)neighbour).orientation == Orientation.UP))
                    {
                        SetNext((IPath)neighbour);
                    }
                }
            }
        }

        public new bool verifOrientation(IPath path)
        {
            if (typeof(TuyauObjet).IsInstanceOfType(path))
            {
                if (((TuyauObjet)path).orientation == orientation)
                    return true;

                switch (orientation)
                {
                    case Orientation.UP: return ((TuyauObjet)path).orientation == Orientation.RIGHT;
                    case Orientation.LEFT: return ((TuyauObjet)path).orientation == Orientation.UP;
                    case Orientation.DOWN: return ((TuyauObjet)path).orientation == Orientation.LEFT;
                    case Orientation.RIGHT: return ((TuyauObjet)path).orientation == Orientation.DOWN;
                }
            }
            return false;
        }

    }
}

