﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hephaestus.Code.Interfaces;
using Hephaestus.Code.Character;
using Hephaestus.Code.Blocks.Machine;
using Hephaestus.Code.Blocks.Tuyau_Objet;
using MGCE.Engine;
using System.Runtime.Serialization;
using MGCE.Util;
using Hephaestus.Code.Items;

namespace Hephaestus.Code.Blocks.Tuyau_Objet
{
    [Serializable]
    class TuyauObjetCroix : TuyauObjet
    {
        public override ItemType Loot => ItemType.TUYAUX_CROIX;
        List<IPath> nextListe = new List<IPath>();

        public TuyauObjetCroix(Point blockPosition, World world, Orientation o) : base(blockPosition, world, o, 2, "Transports/tuyaux_croix", "Transports/tuyaux_croix", "Transports/tuyaux_croix", "Transports/tuyaux_croix")
        {

        }

        public TuyauObjetCroix(SerializationInfo info, StreamingContext context) : base(info, context, 2, "Transports/tuyaux_croix", "Transports/tuyaux_croix", "Transports/tuyaux_croix", "Transports/tuyaux_croix")
        {
            nextListe = info.GetValue("nextListe", nextListe);
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("nextListe", nextListe);
        }


        public override bool SetNext(IPath _next)
        {
            nextListe.Add(_next);
            return true;
        }
      
        public override bool Exit()
        {
            bool oui = listeItem.Count > 0 && nextListe.Count > 0 && nextListe.ElementAt(0).Enter(listeItem.ElementAt(0));
            if (oui)
            {             
                listeItem.RemoveAt(0);
                IPath tmp = nextListe.First(); 
                nextListe.RemoveAt(0);
                nextListe.Add(tmp);
                return true;
            }

            else return false;
        }

        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {

            base.OnNeibourgChange(originDirection, neibourg);
            neibourg = Player.player.world[BlockPosition + originDirection].ForegroundBlock;
            if (originDirection.X == 1 && originDirection.Y == 0)
                rightOrientation(originDirection, neibourg);
            else if (originDirection.X == -1 && originDirection.Y == 0)
                leftOrientation(originDirection, neibourg);
            else if (originDirection.X == 0 && originDirection.Y == 1)
                downOrientation(originDirection, neibourg);
            else if (originDirection.X == 0 && originDirection.Y == -1)
                upOrientation(originDirection, neibourg);
        }

        public new void leftOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {

                //si usine
                if (typeof(Usine).IsInstanceOfType(neighbour))
                {
                    SetNext((IPath)neighbour);
                }
                else if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.DOWN)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.RIGHT)
                    {
                        ((TuyauObjetCoudeT1)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCoudeT2).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.UP)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.RIGHT)
                    {
                        ((TuyauObjetCoudeT2)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCroix).IsInstanceOfType(neighbour))
                {
                    
                }
                else if (typeof(TuyauObjet).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjet)neighbour).orientation == Orientation.LEFT)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjet)neighbour).orientation == Orientation.RIGHT)
                    {
                        ((TuyauObjet)neighbour).SetNext(this);
                    }
                }
            }
        }

        public new void rightOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {

                //si usine
                if (typeof(Usine).IsInstanceOfType(neighbour))
                {
                    SetNext((IPath)neighbour);
                }
                else if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.UP)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.LEFT)
                    {
                        ((TuyauObjetCoudeT1)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCoudeT2).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.DOWN)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.LEFT)
                    {
                        ((TuyauObjetCoudeT2)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCroix).IsInstanceOfType(neighbour))
                {

                }
                else if (typeof(TuyauObjet).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjet)neighbour).orientation == Orientation.RIGHT)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjet)neighbour).orientation == Orientation.LEFT)
                    {
                        ((TuyauObjet)neighbour).SetNext(this);
                    }
                }
            }
        }

        public new void upOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {

                //si usine
                if (typeof(Usine).IsInstanceOfType(neighbour))
                {
                    SetNext((IPath)neighbour);
                }
                else if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.LEFT)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.DOWN)
                    {
                        ((TuyauObjetCoudeT1)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCoudeT2).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.RIGHT)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.DOWN)
                    {
                        ((TuyauObjetCoudeT2)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCroix).IsInstanceOfType(neighbour))
                {

                }
                else if (typeof(TuyauObjet).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjet)neighbour).orientation == Orientation.UP)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjet)neighbour).orientation == Orientation.DOWN)
                    {
                        ((TuyauObjet)neighbour).SetNext(this);
                    }
                }
            }
        }

        public new void downOrientation(Point point, Block neighbour)
        {
            if (typeof(IPath).IsInstanceOfType(neighbour))
            {

                if (typeof(TuyauObjetCoudeT1).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.RIGHT)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT1)neighbour).orientation == Orientation.UP)
                    {
                        ((TuyauObjetCoudeT1)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCoudeT2).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.LEFT)
                    {
                        SetNext((IPath)neighbour);
                    }
                    else if (((TuyauObjetCoudeT2)neighbour).orientation == Orientation.UP)
                    {
                        ((TuyauObjetCoudeT2)neighbour).SetNext(this);
                    }
                }
                else if (typeof(TuyauObjetCroix).IsInstanceOfType(neighbour))
                {

                }
                else if (typeof(TuyauObjet).IsInstanceOfType(neighbour))
                {
                    if (((TuyauObjet)neighbour).orientation == Orientation.DOWN)
                    {
                        SetNext((IPath)neighbour);MGDebug.PrintLine("done");
                    }
                    else if (((TuyauObjet)neighbour).orientation == Orientation.UP)
                    {
                        ((TuyauObjet)neighbour).SetNext(this);
                    }
                }
            }
        }

        public new bool verifOrientation(IPath path)
        {
            if (typeof(TuyauObjet).IsInstanceOfType(path))
            {
                if (((TuyauObjet)path).orientation == orientation)
                    return true;

                switch (orientation)
                {
                    case Orientation.UP: return ((TuyauObjet)path).orientation == Orientation.RIGHT;
                    case Orientation.LEFT: return ((TuyauObjet)path).orientation == Orientation.UP;
                    case Orientation.DOWN: return ((TuyauObjet)path).orientation == Orientation.LEFT;
                    case Orientation.RIGHT: return ((TuyauObjet)path).orientation == Orientation.DOWN;
                }
            }
            return false;
        }

    }
}
