using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Input;
using MGCE.Util;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework.Audio;
using Hephaestus.Code.Util;
using System.Drawing.Imaging;

namespace Hephaestus.Code.Blocks.Fluids
{
    public interface IFluidUpdate : IMGEventListener
    {
        void FluidUpdate(GameTime gameTime);
    }

    /// <summary>
    /// Base class for all fluids
    /// </summary>
    [Serializable]
    public abstract class FluidBlock : Block, IFluidUpdate
    {
        public const float CURRENT_TIME_RESET_MASLUS_COEF = 0.5f;
        public const float MIN_AMOUNT_FOR_REMOVE = 1.0f;

        public float Amount
        {
            get => _amount;
            set {
                if (_amount != value)
                {
                    _amount = value;
                    IsDirtyState = true;
                }
            }
        }
        protected float _amount;
        public float Density { get; }
        public float FlowPerTransfer { get; }
        public TimeSpan FlowDelay { get; }
        public TimeSpan BalanceDelay { get; }
        protected TimeSpan CurrentFlowTime { get; set; }
        protected TimeSpan CurrentBalanceTime { get; set; }

        public Vector3 FullLightTransparency { get; }
        public abstract Vector3 LightTransparency { get; }

        public ForegroundBlock MyBlock => Terrain?.ForegroundBlock;
        public TerrainBlock Terrain { get; protected set; }

        public override bool IsNatural => IsNaturalGeneration;
        public bool IsNaturalGeneration { get; set; }

        public float MaxPlayerDamage { get; set; } = 0f;
        public float PlayerDamage => MaxPlayerDamage * _amount / ForegroundBlock.FULL_BLOCK_MAX_FLUID;

        public override bool IsDirtyState
        {
            get => base.IsDirtyState; 
            set {
                if (value != base.IsDirtyState)
                {
                    base.IsDirtyState = value;
                    if (MyBlock is ForegroundBlock)
                    {
                        if (MyBlock.Liquid is LiquidBlock l)
                        {
                            l.IsDirtyState = true;
                        }
                        if (MyBlock.Gaz is GazBlock g)
                        {
                            g.IsDirtyState = true;
                        }
                        MyBlock.IsDirtyState = true;
                    }
                }
            }
        }


        public string FlowSound { get; set; } = null;
        public float FlowSoundVolume { get; set; } = 1;
        public string RestSound { get; set; } = null;
        public float RestSoundVolume { get; set; } = 1;

        protected float lastTransferAmount = 0;
        protected float FlowSoundAmout = ForegroundBlock.FULL_BLOCK_MAX_FLUID * 0.01f;
        protected bool iddleTransfer = false;
        protected bool iddleBalance = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="density">weight of the liquid, heavy fluids will be under light fluids</param>
        /// <param name="flowPerSec">How many can move each second</param>
        /// <param name="flowTime">Time between fluid movement</param>
        /// <param name="flowBalanceTime">Time to look the fluid under to see if it's lighter</param>
        /// <param name="fluidColor"></param>
        /// <param name="drawOrder"></param>
        /// <param name="block"></param>
        /// <param name="world"></param>
        /// <param name="amount"></param>
        /// <param name="alive">If the block should update himself</param>
        /// <param name="isNaturalGeneration">If the block spawn from a world generator</param>
        protected FluidBlock(float density, float flowPerSec, TimeSpan flowTime, TimeSpan flowBalanceTime, Vector3 fluidColor, int drawOrder, TerrainBlock block, World world, float amount, bool alive, bool isNaturalGeneration) : base(block.BlockPosition, world)
        {
            Amount = amount;
            Terrain = block;
            Light = Terrain.Light;
            DrawOrder = drawOrder;
            IsNaturalGeneration = isNaturalGeneration;
            if (MyBlock.Visible)
            {
                Show();
            }
            Density = density;
            FlowDelay = flowTime;
            FlowPerTransfer = Math.Max(MIN_AMOUNT_FOR_REMOVE, flowPerSec * (float)flowTime.TotalSeconds);
            BalanceDelay = flowBalanceTime;
            FullLightTransparency = fluidColor;


            ResetFlow();
            ResetBalance();
            if (alive)
            {
                StartListeningEvent<IFluidUpdate>();
            }
        }


        protected FluidBlock(SerializationInfo info, StreamingContext context, float density, float flowPerSec, TimeSpan flowTime, TimeSpan flowBalance, Vector3 lightAbsorption, int drawOrder) : base(info, context)
        {
            Amount = info.GetValue("Amount", Amount);
            Terrain = info.GetValue("Terrain", Terrain);
            IsNaturalGeneration = false;

            Light = Terrain.Light;
            DrawOrder = drawOrder;
            Density = density;
            FlowDelay = flowTime;
            FlowPerTransfer = Math.Max(MIN_AMOUNT_FOR_REMOVE, flowPerSec * (float)flowTime.TotalSeconds);
            BalanceDelay = flowBalance;
            FullLightTransparency = lightAbsorption;

            ResetFlow();
            ResetBalance();
            StartListeningEvent<IFluidUpdate>();
        }

        public override void Serialize(SerializationInfo info, StreamingContext context)
        {
            base.Serialize(info, context);
            info.AddValue("Amount", Amount);
            info.AddValue("Terrain", Terrain);
        }

        private void ResetFlow()
        {
            CurrentFlowTime = TimeSpan.FromSeconds(-FlowDelay.TotalSeconds * MGTools.RNG.NextDouble() * CURRENT_TIME_RESET_MASLUS_COEF);
        }

        private void ResetBalance()
        {
            CurrentBalanceTime = TimeSpan.FromSeconds(-BalanceDelay.TotalSeconds * MGTools.RNG.NextDouble() * CURRENT_TIME_RESET_MASLUS_COEF);
        }

        public void FluidUpdate(GameTime gameTime)
        {
            if (IsDisposed)
            {
                Dispose();
            } else
            {
                base.Uptate(gameTime);
                CurrentBalanceTime += gameTime.ElapsedGameTime;
                CurrentFlowTime += gameTime.ElapsedGameTime;
                bool balance = false, transfer = false;
                if (CurrentFlowTime > FlowDelay)
                {
                    lastTransferAmount = 0;
                    transfer = CheckTransfer(gameTime);
                    iddleTransfer = !transfer;
                    ResetFlow();
                }
                if (CurrentBalanceTime > BalanceDelay)
                {
                    balance = CheckBalance(gameTime);
                    iddleBalance = !balance;
                    ResetBalance();
                }
                // vide
                if (Amount < MIN_AMOUNT_FOR_REMOVE)
                {
                    Removed();
                }
                // si le dernier transfert et balance on ete sans succes
                else if (iddleTransfer && iddleBalance)
                {
                    StopListeningEvent<IFluidUpdate>();
                }
                // si une balance ou un transfert a ete fait
                else if (balance || transfer)
                {
                    IsNaturalGeneration = false;
                    World[BlockPosition].NotifyNeibourgs();

                    if (balance)
                    {
                        World[BlockPosition + NUNITY].NotifyNeibourgs();
                    }
                }

                UpdateSound();

            }
        }

        protected void UpdateSound()
        {
            if (Visible)
            {
                if (lastTransferAmount < FlowSoundAmout)
                {
                    if (RestSound is string)
                    {
                        SoundManager.PlayUniqueEffect(RestSound, AudioEmitter, RestSoundVolume);
                        if (FlowSound != RestSound && FlowSound is string)
                        {
                            SoundManager.RemoveUniqueEffectEmitter(FlowSound, AudioEmitter);
                        }
                    }
                } else
                {
                    if (FlowSound is string)
                    {
                        SoundManager.PlayUniqueEffect(FlowSound, AudioEmitter, FlowSoundVolume);
                        if (FlowSound != RestSound && RestSound is string)
                        {
                            SoundManager.RemoveUniqueEffectEmitter(RestSound, AudioEmitter);
                        }
                    }
                }
            }
        }

        public override void Show(int? order = null)
        {
            base.Show(order);
            UpdateSound();
        }

        public override void Hide()
        {
            base.Hide();
            if (FlowSound is string)
            {
                SoundManager.RemoveUniqueEffectEmitter(FlowSound, AudioEmitter);
            }
            if (RestSound is string)
            {
                SoundManager.RemoveUniqueEffectEmitter(RestSound, AudioEmitter);
            }
        }

        public override void Removed()
        {
            IsDirtyState = true;
            base.Removed();
        }
        protected virtual void ResetIddleNeedCheck()
        {
            iddleBalance = false;
            iddleTransfer = false;
            StartListeningEvent<IFluidUpdate>();
        }

        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            if (!IsDisposed)
            {
                base.OnNeibourgChange(originDirection, neibourg);
                ResetIddleNeedCheck();
            }
        }

        /// <summary>
        /// Check for fluid movement
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        protected abstract bool CheckTransfer(GameTime gameTime);

        /// <summary>
        /// Check if the block under is lighter and move if so one
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        protected abstract bool CheckBalance(GameTime gameTime);

        /// <summary>
        /// Try empty this block
        /// </summary>
        /// <param name="amount"></param>
        public abstract void TryEspape(float amount);
    }
}
