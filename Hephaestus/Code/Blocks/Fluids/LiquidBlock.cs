﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Util;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Util.Light;
using static Hephaestus.Code.Util.Const;
using System.Runtime.Serialization;
using Hephaestus.Code.Util;

namespace Hephaestus.Code.Blocks.Fluids
{
    [Serializable]
    public abstract class LiquidBlock : FluidBlock
    {
        public const float MIN_AMOUNT_FOR_LIQUID_TRANSFER = ForegroundBlock.FULL_BLOCK_MAX_FLUID / 10;
        public const float MIN_DIFF_FOR_TRANSFER = 1;

        public override Vector3 LightTransparency => Vector3.Lerp(Vector3.One, FullLightTransparency, Amount / MyBlock.MaxFluidAmount);

        public float MovementSpeedCoef { get; set; } = 1f;

        public LiquidBlock(float density, float flowPerSec, TimeSpan flowTime, TimeSpan flowBalance, float opacity, Vector3 fluidColor, TerrainBlock blockPosition, World world, float amount, bool alive, bool isNaturalGeneration) : base(density, flowPerSec, flowTime, flowBalance, fluidColor, LIQUID_DRAW_ORDER, blockPosition, world, amount, alive, isNaturalGeneration)
        {
            Opacity = opacity;
            MyBlock.Liquid = this;
        }

        protected LiquidBlock(SerializationInfo info, StreamingContext context, float density, float flowPerSec, TimeSpan flowTime, TimeSpan flowBalance, float opacity, Vector3 fluidColor) : base(info, context, density, flowPerSec, flowTime, flowBalance, fluidColor, LIQUID_DRAW_ORDER)
        {
            Opacity = opacity;
        }

        protected override bool CheckBalance(GameTime gameTime)
        {
            TerrainBlock terrain = World[BlockPosition + UNITY];
            ForegroundBlock otherBlock = terrain.ForegroundBlock;
            if (otherBlock.AllowFluids && otherBlock.Liquid is LiquidBlock && otherBlock.Liquid.Density < Density)
            {
                LiquidBlock otherLiquid = otherBlock.Liquid;

                // echange liquide du block
                MyBlock.Liquid = otherLiquid;
                otherBlock.Liquid = this;

                // echangle block du liquide
                otherLiquid.Terrain = Terrain;
                Terrain = terrain;

                // recuperation des position
                BlockPosition = MyBlock.BlockPosition;
                otherLiquid.BlockPosition = otherLiquid.MyBlock.BlockPosition;

                // reset timer
                otherLiquid.CurrentBalanceTime = TimeSpan.Zero;
                otherLiquid.CurrentFlowTime = TimeSpan.Zero;
                CurrentFlowTime = TimeSpan.Zero;

                if (MyBlock.FluidAmout > MyBlock.MaxFluidAmount)
                {
                    MyBlock.Gaz?.TryEspape(MyBlock.FluidAmout - MyBlock.MaxFluidAmount);
                }

                if (otherLiquid.MyBlock.FluidAmout > otherLiquid.MyBlock.MaxFluidAmount)
                {
                    otherLiquid.MyBlock.Gaz?.TryEspape(otherLiquid.MyBlock.FluidAmout - otherLiquid.MyBlock.MaxFluidAmount);
                }

                return true;
            } else
            {
                return false;
            }
        }

        private float GetPossibleTransfer(Point direction, bool checkMinAmount)
        {
            if (Amount > MIN_AMOUNT_FOR_LIQUID_TRANSFER || !checkMinAmount)
            {
                ForegroundBlock target = World[BlockPosition + direction].ForegroundBlock;
                if (target.AllowFluids && (target.Liquid is null || target.Liquid.GetType() == GetType()))
                {
                    float otherAmount = target.Liquid is null ? 0 : target.Liquid.Amount;
                    float diff = Amount - otherAmount;
                    if (target.MaxFluidAmount > otherAmount)
                    {
                        if (checkMinAmount)
                        {

                            if (diff > MIN_DIFF_FOR_TRANSFER)

                            {
                                return Math.Min(diff, target.MaxFluidAmount - otherAmount);
                            } else
                            {
                                return 0;
                            }
                        } else
                        {
                            return Math.Min(Amount, target.MaxFluidAmount - otherAmount);
                        }
                    } else
                    {
                        return 0;
                    }
                } else
                {
                    return 0;
                }
            } else
            {
                return 0;
            }
        }

        private void MakeTransfer(Point direction, float amount)
        {
            if (amount > 0)
            {
                TerrainBlock terrain = World[BlockPosition + direction];
                ForegroundBlock target = terrain.ForegroundBlock;

                if (target.Liquid is null)
                {
                    CreateEmptyCopy(terrain, World);
                }
                target.Liquid.Amount += amount;
                Amount -= amount;
                lastTransferAmount += amount;

                if (target.FluidAmout > target.MaxFluidAmount)
                {
                    target.Gaz?.TryEspape(target.FluidAmout - target.MaxFluidAmount);
                }
            }
        }

        protected override bool CheckTransfer(GameTime gameTime)
        {
            float flowLeft = FlowPerTransfer;
            float forUnder = GetPossibleTransfer(UNITY, false);
            if (forUnder > flowLeft)
            {
                forUnder = flowLeft;
            }
            MakeTransfer(UNITY, forUnder);
            flowLeft -= forUnder;

            float forLeft = GetPossibleTransfer(NUNITX, true);
            float forRight = GetPossibleTransfer(UNITX, true);
            float sum = forLeft + forRight;
            float avg = sum / 3;
            float ratio = avg / sum; // ration of transfer
            float ttl = sum * ratio; // transfer total

            // if tranfer > max flow
            if (ttl > flowLeft)
            {
                ratio *= flowLeft / ttl; // we create a new ratio to make flow = ttl
            }

            forLeft *= ratio;
            forRight *= ratio;
            MakeTransfer(NUNITX, forLeft);
            MakeTransfer(UNITX, forRight);

            return forUnder + forLeft + forRight > 0;
        }

        public override void Dispose()
        {
            base.Dispose();
            if (MyBlock?.Liquid == this)
            {
                MyBlock.Liquid = null;
            }

            Terrain = null;
        }

        public override void TryEspape(float amount)
        {
            float objective = Amount - amount;
            List<Point> directions = new List<Point>()
            {
                UNITX, UNITY, NUNITX, NUNITY
            };
            List<Tuple<Point, float>> accepted = new List<Tuple<Point, float>>();
            foreach (Point dir in directions)
            {
                ForegroundBlock target = World[BlockPosition + dir].ForegroundBlock;
                if (target.AllowFluids && target.MaxFluidAmount > target.FluidAmout)
                {
                    if (target.Liquid is null || target.Liquid.GetType() == GetType())
                    {
                        accepted.Add(new Tuple<Point, float>(dir, Math.Min(Amount, target.MaxFluidAmount - target.FluidAmout)));
                    }
                }
            }

            if (accepted.Count > 0)
            {
                float sum = accepted.Sum(a => a.Item2); // sum of transfer
                float avg = sum / (accepted.Count + 1); // average counting this block as want 0 of transfer
                                                        // used to equalize transfer between directions and this block
                float ratio = avg / sum; // ration of transfer
                float ttl = sum * ratio; // transfer total
                if (ttl > amount)
                {
                    ratio *= amount / ttl;
                }
                foreach (var tup in accepted)
                {
                    Point dir = tup.Item1;
                    float amountNeeded = tup.Item2 * ratio;
                    MakeTransfer(dir, amountNeeded);
                }
            }
            if (objective > Amount)
            {
                Amount = objective;
            }
            NotifyNeibourgs();
            ResetIddleNeedCheck();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            UpdateWorldPosition();
            if (IsDirty() && MGSpriteSheet.IsReady(SpriteSheet))
            {
                if (World[BlockPosition + NUNITY].ForegroundBlock.Liquid is null || World[BlockPosition + NUNITY].ForegroundBlock.Liquid.GetType() == GetType())
                {
                    float ratio = Math.Min(1.0f, Amount / MyBlock.MaxFluidAmount);
                    int cut = (int)(BLOCK_SIZE_PX * (1.0f - ratio));
                    Point size = new Point(SpriteSheet.Texture.Size.X, (int)(SpriteSheet.Texture.Size.Y * ratio));
                    Point destSize = this.GetDestinationSize();
                    spriteBatch.Draw(SpriteSheet.Texture, new Rectangle((int)ScreenPosition.X, (int)ScreenPosition.Y + cut, destSize.X, destSize.Y - cut), new Rectangle(SpriteSheet.Texture.Location, size), Color * Opacity, Rotation, Origin, Effects, 1);
                } else
                {
                    spriteBatch.Draw(SpriteSheet, this, 1);
                }
                IsDirtyState = false;
            }
        }

        protected abstract LiquidBlock CreateEmptyCopy(TerrainBlock blockPosition, World world);
    }

    [Serializable]
    public class Water : LiquidBlock
    {
        public const float LAVA_LOSS_COEF = 0.5f;
        public Water(TerrainBlock blockPosition, World world, float amount = 0, bool alive = true, bool isNaturalGeneration = false) : base(1000, ForegroundBlock.FULL_BLOCK_MAX_FLUID, TimeSpan.FromSeconds(0.1), TimeSpan.FromSeconds(2), 0.3f, new Vector3(0.9f), blockPosition, world, amount, alive, isNaturalGeneration)
        {
            FlowSound = "Sound/water_flow";
            RestSound = "Sound/water_rest";
            RestSoundVolume = 0.3f;
            MovementSpeedCoef = 0.9f;
        }

        public Water(SerializationInfo info, StreamingContext context) : base(info, context, 1000, ForegroundBlock.FULL_BLOCK_MAX_FLUID, TimeSpan.FromSeconds(0.1), TimeSpan.FromSeconds(2), 0.3f, new Vector3(0.9f))
        {
            FlowSound = "Sound/water_flow";
            RestSound = "Sound/water_rest";
            RestSoundVolume = 0.3f;
            MovementSpeedCoef = 0.9f;
        }

        protected override LiquidBlock CreateEmptyCopy(TerrainBlock blockPosition, World world)
        {
            return new Water(blockPosition, world);
        }

        /// <summary>
        /// change to steam if above lava
        /// </summary>
        private void CheckLava()
        {
            if (World[BlockPosition + UNITY]?.ForegroundBlock?.Liquid is Lava)
            {
                float old = Amount;
                Amount = Math.Max(0, Amount - ForegroundBlock.FULL_BLOCK_MAX_FLUID * LAVA_LOSS_COEF);
                float diff = old - Amount;
                if (diff > MIN_AMOUNT_FOR_REMOVE)
                {
                    if (MyBlock is ForegroundBlock block)
                    {
                        if (block.Gaz is null)
                        {
                            new Steam(World[BlockPosition], World, diff, true);
                            if (Visible)
                            {
                                SoundManager.PlayEffect("Sound/steam", AudioEmitter, volume: 0.6f);
                            }
                        } else if (block.Gaz is Steam steam)
                        {
                            steam.Amount += diff;
                            if (Visible)
                            {
                                SoundManager.PlayEffect("Sound/steam", AudioEmitter, volume: 0.3f);
                            }
                        }
                    }
                }
            }
        }

        protected override bool CheckTransfer(GameTime gameTime)
        {
            bool tmp = base.CheckTransfer(gameTime);
            CheckLava();
            return tmp;
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/eau"));
        }
    }

    [Serializable]
    public class Petrol : LiquidBlock
    {
        public Petrol(TerrainBlock blockPosition, World world, float amount = 0, bool alive = true, bool isNaturalGeneration = false) : base(700, ForegroundBlock.FULL_BLOCK_MAX_FLUID, TimeSpan.FromSeconds(0.15), TimeSpan.FromSeconds(2.5), 0.6f, new Vector3(0.1f), blockPosition, world, amount, alive, isNaturalGeneration)
        {
            FlowSound = "Sound/water_flow";
            RestSound = "Sound/water_rest";
            RestSoundVolume = 0.2f;
            MovementSpeedCoef = 0.8f;
        }

        public Petrol(SerializationInfo info, StreamingContext context) : base(info, context, 700, ForegroundBlock.FULL_BLOCK_MAX_FLUID, TimeSpan.FromSeconds(0.15), TimeSpan.FromSeconds(2.5), 0.6f, new Vector3(0.1f))
        {
            FlowSound = "Sound/water_flow";
            RestSound = "Sound/water_rest";
            RestSoundVolume = 0.4f;
            MovementSpeedCoef = 0.8f;
        }

        protected override LiquidBlock CreateEmptyCopy(TerrainBlock blockPosition, World world)
        {
            return new Petrol(blockPosition, world);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/petrol"));
        }
    }

    [Serializable]
    public class Lava : LiquidBlock, ILightSource
    {
        private const int LIGHT_UPDATE_DELAY = 30;
        public List<TerrainBlock> AffectedBlocks { get; } = new List<TerrainBlock>();
        public int VisibleAffectedBlock { get; set; }
        public virtual Vector2 LightWorldPosition => WorldPosition + new Vector2(BLOCK_SIZE_PX / 2);

        public float LightAngle => MathHelper.Pi;

        public Vector2 LightDirection => Vector2.UnitX;

        public override Vector3 NPMMinLight { get; } = new Vector3(0.8f);

        public Color LightColor { get; set; }
        public float Attenuation => 0.5f;
        public float Range => BLOCK_SIZE_PX * 7;
        public bool NeedReAffect { get; protected set; }

        public bool IsProtectingBlock => true;

        private int tickCount;

        public Lava(TerrainBlock blockPosition, World world, bool inVisibleZone, float amount = 0, bool alive = true, bool isNaturalGeneration = false) : base(2500, ForegroundBlock.FULL_BLOCK_MAX_FLUID / 3, TimeSpan.FromSeconds(0.5), TimeSpan.FromSeconds(1), 0.7f, new Vector3(0.5f), blockPosition, world, amount, alive, isNaturalGeneration)
        {
            NeedReAffect = inVisibleZone;
            StartListeningEvent<ILightPreUpdate>();
            FlowSound = RestSound = "Sound/lava";
            FlowSoundVolume = RestSoundVolume = 0.5f;
            MovementSpeedCoef = 0.7f;
            MaxPlayerDamage = 5;
        }

        public Lava(SerializationInfo info, StreamingContext context) : base(info, context, 2500, ForegroundBlock.FULL_BLOCK_MAX_FLUID / 3, TimeSpan.FromSeconds(0.5), TimeSpan.FromSeconds(1), 0.7f, new Vector3(0.5f))
        {
            StartListeningEvent<ILightPreUpdate>();
            FlowSound = RestSound = "Sound/lava";
            FlowSoundVolume = RestSoundVolume = 0.5f;
            MovementSpeedCoef = 0.7f;
            MaxPlayerDamage = 5;
        }

        public override void OnDeserialization(object sender)
        {
            base.OnDeserialization(sender);
            NeedReAffect = World.AutoLoadedZone.Contains(BlockPosition);
        }

        protected override LiquidBlock CreateEmptyCopy(TerrainBlock blockPosition, World world)
        {
            return new Lava(blockPosition, world, world.AutoLoadedZone.Contains(blockPosition.BlockPosition));
        }

        public void LightPreUpdate()
        {
            if (NeedReAffect)
            {
                AffectBlocks();
                NeedReAffect = false;
            }
            if (tickCount++ > LIGHT_UPDATE_DELAY && VisibleAffectedBlock > 0)
            {
                tickCount = MGTools.RNG.Next(0, LIGHT_UPDATE_DELAY / 2);
                int red = MGTools.RNG.Next(100) + 150;
                LightColor = new Color(new Vector3(red / 255f, MGTools.RNG.Next(0, 100) / 255f, 0) * 0.6f);
                foreach (TerrainBlock block in AffectedBlocks)
                {
                    block.NotifyLightChange();
                }
            }
        }

        public override void OnNeibourgChange(Point originDirection, Block neibourg)
        {
            base.OnNeibourgChange(originDirection, neibourg);
            RequestLightUpdate();
        }

        public override void Dispose()
        {
            base.Dispose();
            UnAffectBlocks();
        }

        public void RequestLightUpdate()
        {
            NeedReAffect = true;
        }

        public override void Show(int? order = null)
        {
            base.Show();
            RequestLightUpdate();
        }

        public void UnAffectBlocks()
        {
            ILightSource.DefaultUnAffectBlocks(this);
        }

        public void AffectBlocks()
        {
            ILightSource.DefaultAffectBlocks(this);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/lave"));
        }
    }
}
