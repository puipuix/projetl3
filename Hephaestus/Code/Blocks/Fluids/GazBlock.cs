﻿using Microsoft.Xna.Framework;
using MGCE.Actor;
using MGCE.Util;
using Hephaestus.Code.Blocks.Foreground;
using Hephaestus.Code.Blocks.Terrain;
using System;
using System.Collections.Generic;
using System.Linq;
using static Hephaestus.Code.Util.Const;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework.Graphics;
using Hephaestus.Code.Util;

namespace Hephaestus.Code.Blocks.Fluids
{
    [Serializable]
    public abstract class GazBlock : FluidBlock
    {
        public const float MIN_AMOUNT_FOR_GAZ_TRANSFER = ForegroundBlock.FULL_BLOCK_MAX_FLUID / 100;
        public const float ATHMOS_LOSS = 0.1f;
        public const float MIN_DIFF_FOR_TRANSFER = ForegroundBlock.FULL_BLOCK_MAX_FLUID * 0.01f;

        public float MinOpacity { get; }
        public float MaxOpacity { get; }
        public override float Opacity => MGMath.Lerp(MinOpacity, MaxOpacity, Amount / MyBlock.MaxFluidAmount);

        public override Vector3 LightTransparency => Vector3.Lerp(Vector3.One, FullLightTransparency, Opacity);

        protected GazBlock(float density, float flowPerSec, TimeSpan flowTime, TimeSpan flowBalance, Vector3 fluidColor, float minOpacity, float maxOpacity, TerrainBlock blockPosition, World world, float amount, bool alive, bool isNaturalGeneration) : base(density, flowPerSec, flowTime, flowBalance, fluidColor, GAZ_DRAW_ORDER, blockPosition, world, amount, alive, isNaturalGeneration)
        {
            MyBlock.Gaz = this;
            MinOpacity = minOpacity;
            MaxOpacity = maxOpacity;
        }

        protected GazBlock(SerializationInfo info, StreamingContext context, float density, float flowPerSec, TimeSpan flowTime, TimeSpan flowBalance, Vector3 fluidColor, float minOpacity, float maxOpacity) : base(info, context, density, flowPerSec, flowTime, flowBalance, fluidColor, GAZ_DRAW_ORDER)
        {
            MinOpacity = minOpacity;
            MaxOpacity = maxOpacity;
        }

        protected override bool CheckBalance(GameTime gameTime)
        {
            TerrainBlock terrain = World[BlockPosition + UNITY];
            ForegroundBlock otherBlock = terrain.ForegroundBlock;
            GazBlock otherGaz = otherBlock.Gaz;
            float otherDens = otherGaz?.Density ?? 0;
            float otherAmount = otherGaz?.Amount ?? 0;

            if (otherBlock.AllowFluids && otherDens < Density)
            {
                if (otherBlock.MaxFluidAmount >= otherBlock.FluidAmout - otherAmount + Amount && MyBlock.MaxFluidAmount >= MyBlock.FluidAmout - Amount + otherAmount)
                {

                    // echange gaz du block
                    MyBlock.Gaz = otherGaz;
                    otherBlock.Gaz = this;

                    // echangle block du gaz
                    if (otherGaz is GazBlock)
                    {
                        otherGaz.Terrain = Terrain;
                    }

                    Terrain = terrain;

                    // recuperation des position
                    BlockPosition = MyBlock.BlockPosition;
                    if (otherGaz is GazBlock)
                    {
                        otherGaz.BlockPosition = otherGaz.MyBlock.BlockPosition;
                    }

                    // reset timer
                    if (otherGaz is GazBlock)
                    {
                        otherGaz.CurrentBalanceTime = TimeSpan.Zero;
                        otherGaz.CurrentFlowTime = TimeSpan.Zero;
                    }
                    CurrentFlowTime = TimeSpan.Zero;

                    return true;
                } else
                {
                    return false;
                }
            } else
            {
                return false;
            }
        }

        private bool AthmosLoss()
        {
            if (BlockPosition.Y < TerrainFactory.SKY_LIMIT_Y)
            {
                Amount *= 1f - ATHMOS_LOSS;
                return true;
            } else
            {
                return false;
            }
        }

        private bool MakeTransfer(float maxFlow, int myWeightAvg, List<Tuple<TerrainBlock, float>> accepted)
        {
            if (accepted.Count > 0 && maxFlow > 0)
            {
                float sum = accepted.Sum(a => a.Item2); // sum of transfer
                float avg = sum / (accepted.Count + myWeightAvg); // average counting this block as want 0 of transfer
                                                                  // used to equalize transfer between directions and this block
                float ratio = avg / sum; // ration of transfer
                float ttl = sum * ratio; // transfer total
                                         // if tranfer > max flow
                if (ttl > maxFlow)
                {
                    ratio *= maxFlow / ttl; // we create a new ratio to make flow = ttl
                }

                foreach (var tup in accepted)
                {
                    TerrainBlock target = tup.Item1;
                    float amount = tup.Item2 * ratio;

                    if (target.ForegroundBlock.Gaz is null)
                    {
                        CreateEmptyCopy(target, World);
                    }
                    target.ForegroundBlock.Gaz.Amount += amount;
                    Amount -= amount;
                    lastTransferAmount += amount;
                }

                return true;
            } else
            {
                return AthmosLoss();
            }
        }

        protected override bool CheckTransfer(GameTime gameTime)
        {
            if (Amount >= MIN_AMOUNT_FOR_GAZ_TRANSFER)
            {
                List<Point> directions = new List<Point>()
                {
                    UNITX, UNITY, NUNITX, NUNITY
                };
                List<Tuple<TerrainBlock, float>> accepted = new List<Tuple<TerrainBlock, float>>();
                foreach (Point dir in directions)
                {
                    TerrainBlock terrain = World[BlockPosition + dir];
                    ForegroundBlock target = terrain.ForegroundBlock;
                    if (target.AllowFluids && target.MaxFluidAmount > target.FluidAmout)
                    {
                        if (target.Gaz is null || target.Gaz.GetType() == GetType())
                        {
                            float diff = Amount - (target.Gaz is null ? 0 : target.Gaz.Amount);
                            if (diff > MIN_DIFF_FOR_TRANSFER)
                            {
                                accepted.Add(new Tuple<TerrainBlock, float>(terrain, Math.Min(diff, target.MaxFluidAmount - target.FluidAmout)));
                            }
                        }
                    }
                }
                return MakeTransfer(FlowPerTransfer, 1, accepted);
            } else
            {
                return AthmosLoss();
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            if (MyBlock?.Gaz == this)
            {
                MyBlock.Gaz = null;
            }

            Terrain = null;
        }

        public override void TryEspape(float amount)
        {
            float objective = Amount - amount;
            List<Point> directions = new List<Point>()
            {
                UNITX, UNITY, NUNITX, NUNITY
            };
            List<Tuple<TerrainBlock, float>> accepted = new List<Tuple<TerrainBlock, float>>();
            foreach (Point dir in directions)
            {
                TerrainBlock terrain = World[BlockPosition + dir];
                ForegroundBlock target = terrain.ForegroundBlock;
                if (target.AllowFluids && target.MaxFluidAmount > target.FluidAmout)
                {
                    if (target.Gaz is null || target.Gaz.GetType() == GetType())
                    {
                        accepted.Add(new Tuple<TerrainBlock, float>(terrain, Math.Min(Amount, target.MaxFluidAmount - target.FluidAmout)));
                    }
                }
            }

            MakeTransfer(amount, 0, accepted);
            if (objective > Amount)
            {
                Amount = objective;
            }
            NotifyNeibourgs();
            ResetIddleNeedCheck();
        }

        protected abstract GazBlock CreateEmptyCopy(TerrainBlock blockPosition, World world);
    }

    [Serializable]
    public class SO2 : GazBlock
    {
        public SO2(TerrainBlock blockPosition, World world, float amount = 0, bool alive = true, bool isNaturalGeneration = false) : base(2.63f, ForegroundBlock.FULL_BLOCK_MAX_FLUID * 5, TimeSpan.FromSeconds(0.05), TimeSpan.FromSeconds(5), new Vector3(0.8f, 1, 0.9f), 0.1f, 0.6f, blockPosition, world, amount, alive, isNaturalGeneration)
        {
            MaxPlayerDamage = 0.1f;
        }

        public SO2(SerializationInfo info, StreamingContext context) : base(info, context, 2.63f, ForegroundBlock.FULL_BLOCK_MAX_FLUID * 5, TimeSpan.FromSeconds(0.05), TimeSpan.FromSeconds(5), new Vector3(0.8f, 1, 0.9f), 0.1f, 0.6f)
        {
            MaxPlayerDamage = 0.1f;
        }

        protected override GazBlock CreateEmptyCopy(TerrainBlock blockPosition, World world)
        {
            return new SO2(blockPosition, world);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/souffre"));
        }
    }

    [Serializable]
    public class CO2 : GazBlock
    {
        public CO2(TerrainBlock blockPosition, World world, float amount = 0, bool alive = true, bool isNaturalGeneration = false) : base(1.87f, ForegroundBlock.FULL_BLOCK_MAX_FLUID * 8, TimeSpan.FromSeconds(0.05), TimeSpan.FromSeconds(7), new Vector3(0.6f), 0.1f, 0.8f, blockPosition, world, amount, alive, isNaturalGeneration)
        {
        }

        public CO2(SerializationInfo info, StreamingContext context) : base(info, context, 1.87f, ForegroundBlock.FULL_BLOCK_MAX_FLUID * 8, TimeSpan.FromSeconds(0.05), TimeSpan.FromSeconds(7), new Vector3(0.6f), 0.1f, 0.8f) { }

        protected override GazBlock CreateEmptyCopy(TerrainBlock blockPosition, World world)
        {
            return new CO2(blockPosition, world);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/co2"));
        }
    }

    [Serializable]
    public class Steam : GazBlock
    {
        private const float CONDENSATION = 0.01f;
        public Steam(TerrainBlock blockPosition, World world, float amount = 0, bool alive = true, bool isNaturalGeneration = false) : base(1.67f, ForegroundBlock.FULL_BLOCK_MAX_FLUID * 9, TimeSpan.FromSeconds(0.05), TimeSpan.FromSeconds(8), new Vector3(0.8f), 0.2f, 0.9f, blockPosition, world, amount, alive, isNaturalGeneration)
        {
        }

        public Steam(SerializationInfo info, StreamingContext context) : base(info, context, 1.67f, ForegroundBlock.FULL_BLOCK_MAX_FLUID * 9, TimeSpan.FromSeconds(0.05), TimeSpan.FromSeconds(8), new Vector3(0.8f), 0.2f, 0.9f)
        {
        }

        protected override bool CheckBalance(GameTime gameTime)
        {
            base.CheckBalance(gameTime);

            // check the condensation
            float old = Amount;
            Amount -= ForegroundBlock.FULL_BLOCK_MAX_FLUID * CONDENSATION;
            float diff = old - Amount;
            if (diff > MIN_AMOUNT_FOR_REMOVE)
            {
                if (MyBlock is ForegroundBlock block)
                {
                    if (block.Liquid is null)
                    {
                        new Water(World[BlockPosition], World, diff, true);
                    } else if (block.Liquid is Water water)
                    {
                        water.Amount += diff;
                    }
                }
            }
            return true;
        }

        protected override GazBlock CreateEmptyCopy(TerrainBlock blockPosition, World world)
        {
            return new Steam(blockPosition, world);
        }

        protected override MGSpriteSheet CreateSpriteSheet()
        {
            return MGSpriteSheet.NewSingleAnimationFromTexture(RessourceManager.Load<Texture2D>("Environnement/vapeur"));
        }
    }
}
