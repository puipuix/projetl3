﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Items
{
    /// <summary>
    /// liste de tout les items present dans le jeu
    /// </summary>
    [Serializable]
    public enum ItemType
    {
        NOTHING,ENERGY,

        //minerais
        PIERRE, EAU, LAVE, PETROL, MOR, MTUNGSTEN, MARGENT, MURANIUM, MZIRCON, MPLATINE, MDIAMANT,

        MCHARBON, MFER, MCUIVRE,

        //items issue de minerais
        FER,CUIVRE,PLAQUE_FER, PLAQUE_CUIVRE, URANIUM_238, URANIUM_235,LITHIUM,

        //autre
        FIL_CUIVRE, CARTE_MERE,MOTEUR,LASER,CAPTEUR,REVETEMENT,BATTERIE,CONDENSATEUR,ROUE,REACTEUR_NUCLEAIRE,REACTEUR,COMPOSANT_NUCLEAIRE,TURBINE,COQUE,HEALWALLE,


        //objet====
        //machine
        MINEUR, USINE, POWER_PLANT,FOUR,PILE,

        //transports
        TUYAUX,TUYAUX_CROIX,TUYAUX_COURBET1,TUYAUX_COURBET2,

        //outil
        LAMP, TORCHE,

        // character
        EVE, WALLE
    }
}
