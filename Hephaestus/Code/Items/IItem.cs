﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Items
{
    public interface IItem
    {
        string Name { get; }
        int Count { get; set; }

        IItem Copy();
    }
}
