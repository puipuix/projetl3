﻿using MGCE.Util;
using Hephaestus.Code.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Hephaestus.Code.Items
{
    [DataContract]
    public class Item
    {
        [DataMember]
        public ItemType type;

     

        //ajouter une image 
        public Item(ItemType name)
        {
            type = name;
        }

        public ItemType GetItemType()
        {
            return type;
        }

        public string GetName()
        {
            return MGLanguage.GetOrName(type.ToString());
        }
    }
}
