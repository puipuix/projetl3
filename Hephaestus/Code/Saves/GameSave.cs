﻿using MGCE.Util;
using Microsoft.Xna.Framework;
using Hephaestus.Code;
using Hephaestus.Code.Blocks;
using Hephaestus.Code.Blocks.Terrain;
using Hephaestus.Code.Character;
using Hephaestus.Code.Items;
using Hephaestus.Code.util;
using Hephaestus.Code.Util;
using Hephaestus.Code.Util.Light;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Linq;
using MGCE.Engine;
using Hephaestus.Code.View;

namespace Hephaestus.Code.Saves
{
    [Serializable]
    public class GameSave : IMGSerializable, IMGDisposable
    {
        public const bool ADMIN_INVENTORY = false;
        public const string SAVES_DIRECTORY = "Saves";
        public const string WORLD_FILE_NAME = "world.bin";
        public static readonly Vector2 SPAWN_POINT = new Vector2(0, -10 * Block.BLOCK_SIZE_PX);

        public static SurrogateSelector GameSurrogate { get; } = new SurrogateSelector();

        static GameSave()
        {
            GameSurrogate.AddSurrogate(typeof(Point), new StreamingContext(StreamingContextStates.All), new MGContractSurrogate());
            GameSurrogate.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new MGContractSurrogate());
            GameSurrogate.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), new MGContractSurrogate());
            GameSurrogate.AddSurrogate(typeof(Vector4), new StreamingContext(StreamingContextStates.All), new MGContractSurrogate());
            GameSurrogate.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new MGContractSurrogate());
            GameSurrogate.AddSurrogate(typeof(Item), new StreamingContext(StreamingContextStates.All), new MGContractSurrogate());
        }



        public string Name { get; private set; }

        public World World { get; private set; }

        public bool IsDisposed { get; private set; }

        public CharacterDisplay Character { get; set; }
        public Stockage Stockage { get; set; }
        public Player Player { get; set; }
        public ActorFollowerCameraActor ActorFollower { get; private set; }
        public WorldActorLight ActorLight { get; private set; }
        public CameraZoomActor CameraZoom { get; private set; }
        public MachineManager MachineManager { get; private set; }

        private GameSave(string name, int seed)
        {
            Name = CreateWorldName(string.IsNullOrWhiteSpace(name) ? "World" : name);
            WorldGenerator generator = new WorldGenerator(seed, WorldGenerator.DefaultTree);
            World = new World(generator);
            MachineManager = new MachineManager();
            Character = new WallECharacter(World) { WorldPosition = SPAWN_POINT };
            Stockage = new Stockage(30);
            Stockage.Add(ItemType.MINEUR, 2);
            Stockage.Add(ItemType.USINE);
            Stockage.Add(ItemType.FOUR);
            Stockage.Add(ItemType.POWER_PLANT);
            Stockage.Add(ItemType.FOUR);
            Stockage.Add(ItemType.PILE, 12);

#pragma warning disable CS0162 // Code inaccessible détecté
            if (ADMIN_INVENTORY)
            {
                Stockage.Add(ItemType.EVE);
                Stockage.Add(ItemType.WALLE);
                Stockage.Add(ItemType.LAMP);
                Stockage.Add(ItemType.TORCHE);
                Stockage.Add(ItemType.EAU);
          
         
                Stockage.Add(ItemType.PIERRE);
                Stockage.Add(ItemType.TUYAUX);
                Stockage.Add(ItemType.TUYAUX_COURBET1);
                //Stockage.Add(ItemType.TUYAUX_COURBET2);
                Stockage.Add(ItemType.TUYAUX_CROIX);
            }
#pragma warning restore CS0162 // Code inaccessible détecté
        }

        protected GameSave(SerializationInfo info, StreamingContext context)
        {
            World = info.GetValue<World>("World");
            Character = info.GetValue<CharacterDisplay>("Character");
            Stockage = info.GetValue<Stockage>("Stockage");
            MachineManager = info.GetValue<MachineManager>("MachineManager");
        }

        private string CreateWorldName(string baseName)
        {
            if (Directory.Exists(GetGameSaveDirectory(baseName)))
            {
                int i = 0;
                string name;
                do
                {
                    i++;
                    name = baseName + "_" + i;
                } while (Directory.Exists(GetGameSaveDirectory(name)));
                return name;
            } else
            {
                return baseName;
            }
        }

        public void Serialize(SerializationInfo info, StreamingContext context)
        {
            if (!IsDisposed)
            {
                info.AddValue("World", World);
                info.AddValue("Character", Character);
                info.AddValue("MachineManager", MachineManager);
                info.AddValue("Stockage", Stockage);
            }
        }

        public void OnDeserialization(object sender) { }

        private void SpawnPlayerSafe(Vector2 worldPosition, CharacterDisplay charac)
        {
            while (CharacterDisplay.ColideBlock(charac.HitBox, World, worldPosition))
            {
                worldPosition.Y -= 0.5f;
            }
            charac.WorldPosition = worldPosition;
        }

        private void PostCreate()
        {
            MachineManager.Activate();
            World.Activate();
            World.GameSave = this;
            SpawnPlayerSafe(Character.WorldPosition, Character);
            Character.Activate();
            Character.Show();

            ActorFollower = new ActorFollowerCameraActor(Character);
            ActorFollower.Enable();

            ActorLight = new WorldActorLight(Character, new Color(255, 255, 255), 2f, 15 * Block.BLOCK_SIZE_PX, World) { DistanceToMove = Block.BLOCK_SIZE_PX };
            ActorLight.Enable();

            CameraZoom = new CameraZoomActor(World.Camera);
            CameraZoom.Enable();

            Player = new Player(MyGame.Game, World, Character, Stockage);
        }

        private static string GetWorldFile(string name)
        {
            return string.Concat(SAVES_DIRECTORY, "/", name, "/", WORLD_FILE_NAME);
        }

        private static string GetGameSaveDirectory(string name)
        {
            return string.Concat(SAVES_DIRECTORY, "/", name);
        }

        /// <summary>
        /// Save the game
        /// </summary>
        public void Save()
        {
            if (!IsDisposed)
            {
                var bf = new BinaryFormatter();
                bf.SurrogateSelector = GameSurrogate;
                Directory.CreateDirectory(GetGameSaveDirectory(Name));
                using Stream st = File.OpenWrite(GetWorldFile(Name));
                World.GC(true);
                bf.Serialize(st, this);
                st.Flush();
                World.AutoLoad(World.ComputeAutoLoadZone());
                ActorLight.RequestLightUpdate();
            }
        }

        /// <summary>
        /// Change the character of the player
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public CharacterDisplay ChangeCharacter(CharacterDisplay character)
        {
            var ctmp = Character;
            Character = character;
            Character.Activate();
            ctmp.Disactivate();

            Character.Show();
            Character.WorldPosition = ctmp.WorldPosition;
            Player.Character = Character;

            ActorFollower.Actor = Character;
            ActorLight.Target = Character;

            return ctmp;
        }

        /// <summary>
        /// Load a game
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static GameSave Load(string name)
        {
            MGDebug.PrintLine("load");
            var bf = new BinaryFormatter {
                SurrogateSelector = GameSurrogate
            };
            using Stream st = File.OpenRead(GetWorldFile(name));
            GameSave save = (GameSave)bf.Deserialize(st);
            save.Name = name;
            save.PostCreate();
            return save;
        }

        /// <summary>
        /// Create a new game
        /// </summary>
        /// <param name="name"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        public static GameSave Create(string name, int seed)
        {
            GameSave save = new GameSave(name, seed);
            save.PostCreate();
            return save;
        }

        /// <summary>
        /// Get all saved games' name
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> SavesName()
        {
            return Directory.Exists(SAVES_DIRECTORY) ?
                Directory.EnumerateDirectories(SAVES_DIRECTORY).Select(path => new DirectoryInfo(path).Name)
                : Enumerable.Empty<string>();
        }

        /// <summary>
        /// Rename a game
        /// </summary>
        /// <param name="save"></param>
        /// <param name="newName"></param>
        public static void Rename(GameSave save, string newName)
        {
            Directory.Move(GetGameSaveDirectory(save.Name), GetGameSaveDirectory(newName));
            save.Name = newName;
        }
        
        /// <summary>
        /// Remove a game
        /// </summary>
        /// <param name="name"></param>
        public static void Delete(string name)
        {
            Directory.Delete(GetGameSaveDirectory(name), true);
        }

        public void Dispose()
        {
            IsDisposed = true;
            MachineManager.Dispose();
            MachineManager = null;

            Hud.hide();
            World?.Dispose();
            CameraZoom?.Dispose();
            ActorLight?.Dispose();
            ActorFollower?.Dispose();
            Character?.Dispose();

            World = null;
            CameraZoom = null;
            ActorLight = null;
            ActorFollower = null;
            Character = null;
        }
    }
}
