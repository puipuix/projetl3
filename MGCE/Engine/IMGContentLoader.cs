﻿namespace MGCE.Actor
{
    public interface IMGContentLoader
    {
        void LoadContent();
        void UnloadContent();
    }
}
