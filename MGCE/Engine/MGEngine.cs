﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MGCE.Actor;
using MGCE.Events;
using MGCE.Input;
using MGCE.Shapes;
using MGCE.Util;

namespace MGCE.Engine
{

    public static class MGEngine
    {
        public static readonly Point MAXIMISED_WINDOW_SIZE = new Point(-1);
        public static readonly Point FULL_HD_RESOLUTION = new Point(1920, 1080);

        public static SpriteFont Arial { get; private set; }

        /// <summary>
        /// All actor to draw. Small value first (->background)
        /// </summary>
        public static MGPrioritarizedList<IMGDrawable> ToDraws { get; private set; }
        public static Dictionary<int, MGSpriteBatchParameter> SpriteBatchParameters { get; } = new Dictionary<int, MGSpriteBatchParameter>();
        public static MGSpriteBatchParameter DefaultSpriteBatchParameters { get; set; } = new MGSpriteBatchParameter(SpriteSortMode.Deferred);
        /// <summary>
        /// All object that have to load content
        /// </summary>
        public static HashSet<IMGContentLoader> Loaders { get; private set; }

        /// <summary>
        /// All object that have to unload content
        /// </summary>
        public static HashSet<IMGContentLoader> UnLoaders { get; private set; }

        /// <summary>
        /// The game class who create the engine
        /// </summary>
        public static Game Game { get; private set; }

        /// <summary>
        /// SpriteBatch to draw to the screen
        /// </summary>
        public static SpriteBatch SpriteBatch { get; set; }

        /// <summary>
        /// GraphicsDeviceManager
        /// </summary>
        public static GraphicsDeviceManager Graphics { get; set; }

        /// <summary>
        /// target time between 2 tick
        /// </summary>
        public static TimeSpan TargetElapsedTime => Game.TargetElapsedTime;

        /// <summary>
        /// Real Current time between last update
        /// </summary>
        public static TimeSpan GameTimeSpanUpdate { get; private set; }

        /// <summary>
        /// Real Current time between last draw
        /// </summary>
        public static TimeSpan GameTimeSpanDraw { get; private set; }

        /// <summary>
        /// Update duration
        /// </summary>
        public static TimeSpan UpdateDuration { get; private set; }

        /// <summary>
        /// Draw duration
        /// </summary>
        public static TimeSpan DrawDuration { get; private set; }

        /// <summary>
        /// Monogame gameTime
        /// </summary>
        public static GameTime UpdateGameTime { get; private set; } = new GameTime();

        /// <summary>
        /// Monogame gameTime
        /// </summary>
        public static GameTime DrawGameTime { get; private set; } = new GameTime();
        /// <summary>
        /// Color of the screen background
        /// </summary>

        public static ulong TickCount { get; private set; }
        public static ulong DrawCount { get; private set; }

        internal static MGPrioritarizedList<MGEventBase> UpdateEvents;
        internal static MGPrioritarizedList<MGEventBase> DrawEvents;
        private static MGEvent<IMGEventPreUpdateListener> _preUpdateEvent; // pre update
        private static MGEvent<IMGEventUpdateListener> _updateEvent; // update
        private static MGEvent<IMGEventPostUpdateListener> _postUpdateEvent; // post update

        // REAL DELTATIME
        private static DateTime _lastDrawDate = DateTime.Now, _lastUpdateDate = DateTime.Now;

        // const
        /// <summary>
        /// The screen size for the game. It will be the same for all screen size.
        /// </summary>
        public static Point BufferSize => new Point(Graphics.PreferredBackBufferWidth, Graphics.PreferredBackBufferHeight);

        /// <summary>
        /// The center of screen for the game. It will be the same for all screen size.
        /// </summary>
        public static Vector2 BufferCenter => BufferSize.ToVector2() / 2;

        public static event EventHandler OnBufferSizeChange;

        private static int GcCount;
        public static event EventHandler OnGC;
        public static int GCGenerationToWatch { get; set; } = 2;

        public static Form WindowForm => (Form)Form.FromHandle(Game.Window.Handle);

        /// <summary>
        /// Create a new Engine that will use <see cref="PPGSettings"/> to set the game's parameters.
        /// </summary>
        /// <param name="game"></param>
        public static void Create(Game game, Point windowSize, bool borderless, bool fullScreen, bool vSync, bool allowResize, bool allowAltF4)
        {
            UpdateEvents = new MGPrioritarizedList<MGEventBase>();
            DrawEvents = new MGPrioritarizedList<MGEventBase>();
            ToDraws = new MGPrioritarizedList<IMGDrawable>();

            Loaders = new HashSet<IMGContentLoader>();
            UnLoaders = new HashSet<IMGContentLoader>();
            Game = game;
            Point ScreenSize = new Point(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height);
            try
            {
                if (!fullScreen)
                {
                    var desktopSize = Screen.GetWorkingArea(System.Drawing.Point.Empty);
                    int barSize = ScreenSize.Y - SystemInformation.PrimaryMonitorMaximizedWindowSize.Height;
                    ScreenSize = new Point(desktopSize.Width, desktopSize.Height - barSize);
                }
            } catch (Exception e)
            {
                MGDebug.PrintError(e);
            }
            try
            {
                game.Window.IsBorderless = borderless;
            } catch (Exception e)
            {
                MGDebug.PrintError(e);
            }
            MGDebug.PrintLine(ScreenSize);

            Graphics = new GraphicsDeviceManager(Game) {
                SynchronizeWithVerticalRetrace = vSync,
                PreferredBackBufferWidth = windowSize.X != -1 ? windowSize.X : ScreenSize.X,
                PreferredBackBufferHeight = windowSize.Y != -1 ? windowSize.Y : ScreenSize.Y,
            };

            // update game scale and buffer if the window's size change
            game.Window.ClientSizeChanged += (o, a) => {
                if (WindowForm.Width != Graphics.PreferredBackBufferWidth ||
                    WindowForm.Height != Graphics.PreferredBackBufferHeight)
                {

                    if (game.Window.ClientBounds.Width == 0)
                    {
                        return;
                    }

                    Graphics.PreferredBackBufferWidth = game.Window.ClientBounds.Width;
                    Graphics.PreferredBackBufferHeight = game.Window.ClientBounds.Height;
                    Graphics.ApplyChanges();
                    MGTools.SafeInvoke(OnBufferSizeChange, o, a);
                }
            };
            Graphics.ApplyChanges();
            game.Window.AllowUserResizing = allowResize;
            game.Window.AllowAltF4 = allowAltF4;
            _preUpdateEvent = MGEvent<IMGEventPreUpdateListener>.Create<GameTime>();
            _preUpdateEvent.CallOrder = MGConst.ORDER_1_BEFORE;
            _updateEvent = MGEvent<IMGEventUpdateListener>.Create<GameTime>();
            _postUpdateEvent = MGEvent<IMGEventPostUpdateListener>.Create<GameTime>();
            _postUpdateEvent.CallOrder = MGConst.ORDER_5_AFTER;
            MGEvent<IMGEventGameStartListener> start = MGEvent<IMGEventGameStartListener>.Create();
            start.CallOrder = MGConst.ORDER_0_ALWAYS_FIRST;
            start.AskEvent(); // start tick
        }

        /// <summary>
        /// Initialize Engine.
        /// </summary>
        public static void Initialize(string currentLanguage, string languageFolder)
        {
            MGDebug.Initialize(Game);
            string dir = Game.Content.RootDirectory;
            Game.Content.RootDirectory = "";
            Arial = Game.Content.Load<SpriteFont>("font_arial");
            Game.Content.RootDirectory = dir;

            MGLanguage.Initialize(Game.Content, currentLanguage, languageFolder);
            MGInputs.SaveStates();
        }

        [Obsolete("Use MGDebug.PrintLine")]
        public static void Echo(object data, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            MGDebug.PrintLine(data, memberName, sourceLineNumber);
        }

        [Obsolete("Use MGDebug")]
        public static void PrintError(Exception e, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            MGDebug.PrintError(e, memberName, sourceLineNumber);
        }

        [Obsolete("Use MGDebug")]
        public static void PrintError(string str, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            MGDebug.PrintError(str, memberName, sourceLineNumber);
        }

        /// <summary>
        /// Set the game target fps
        /// </summary>
        /// <param name="fps"></param>
        public static void SetTargetedFps(double fps)
        {
            Game.TargetElapsedTime = TimeSpan.FromMilliseconds(1000.0 / fps);
        }

        /// <summary>
        /// Return fps from this GameTime
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        public static double GetFps(GameTime gameTime)
        {
            return 1.0 / gameTime.ElapsedGameTime.TotalSeconds;
        }

        /// <summary>
        /// Return fps from this TimeSpan
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        public static double GetFps(TimeSpan timeSpan)
        {
            return 1.0 / timeSpan.TotalSeconds;
        }

        /// <summary>
        /// Load all IPPGContentLoader in <see cref="MGEngine.Loaders"/>
        /// </summary>
        public static void LoadContent()
        {
            SpriteBatch = new SpriteBatch(Game.GraphicsDevice);
            foreach (IMGContentLoader loader in Loaders)
            {
                try
                {
                    loader.LoadContent();
                } catch (Exception e)
                {
                    MGDebug.PrintError(e);
                }
            }
        }
        /// <summary>
        /// Load all IPPGContentLoader in  <see cref="MGEngine.UnLoaders"/>
        /// </summary>
        public static void UnloadContent()
        {
            foreach (IMGContentLoader loader in UnLoaders)
            {
                try
                {
                    loader.UnloadContent();
                } catch (Exception e)
                {
                    MGDebug.PrintError(e);
                }
            }
            SpriteBatch?.Dispose();
        }

        private static void CallEvents(MGPrioritarizedList<MGEventBase> events, GameTime gameTime)
        {
            try
            {
                IList<int> keys = events.Keys;

                for (int i = 0; i < keys.Count; i++)
                {
                    foreach (MGEventBase ev in events[keys[i]])
                    {
                        if (ev.NeedUpdate && (!gameTime.IsRunningSlowly || !ev.IsSkipable))
                        {
                            ev.Update();
                        }
                    }
                }

            } catch (Exception e)
            {
                MGDebug.PrintError(e);
            }
        }

        /// <summary>
        /// Update all events
        /// </summary>
        /// <param name="gameTime"></param>
        public static void Update(GameTime gameTime)
        {
            UpdateGameTime = gameTime;
            // a revoir -> check le draw
            TickCount++;
            DateTime start = DateTime.Now;

            MGMouse.Uptate(gameTime);
            _preUpdateEvent.AskEvent(gameTime);
            _updateEvent.AskEvent(gameTime);
            _postUpdateEvent.AskEvent(gameTime);

            CallEvents(UpdateEvents, gameTime);

            if (GcCount != GC.CollectionCount(GCGenerationToWatch))
            {
                GcCount = GC.CollectionCount(GCGenerationToWatch);
                MGTools.SafeInvoke(OnGC, typeof(MGEngine), new EventArgs());
            }

            GameTimeSpanUpdate = start - _lastUpdateDate;
            _lastUpdateDate = DateTime.Now;
            UpdateDuration = _lastUpdateDate - start;

            MGInputs.SaveStates();
        }

        /// <summary>
        /// Draw all IPPGDrawable in <see cref="MGEngine.ToDraws"/>.
        /// <br>Automatically clear the screen and setup the SpriteBatch</br>
        /// </summary>
        /// <param name="gameTime"></param>
        public static void DrawAll(GameTime gameTime)
        {
            DrawGameTime = gameTime;
            // a revoir -> check le update
            DrawCount++;
            DateTime start = DateTime.Now;

            CallEvents(DrawEvents, gameTime);

            bool begin = false;
            IList<int> keys = ToDraws.Keys;
            MGSpriteBatchParameter current = DefaultSpriteBatchParameters;
            for (int i = 0; i < keys.Count; i++)
            {
                if (SpriteBatchParameters.TryGetValue(keys[i], out MGSpriteBatchParameter param))
                {
                    if (!begin || current != param)
                    {
                        if (begin)
                        {
                            SpriteBatch.End();
                        }
                        begin = true;
                        current = param;
                        MGTools.SafeInvoke(current.GetBeforeBegin, SpriteBatch, EventArgs.Empty);
                        SpriteBatch.Begin(param.SortMode, param.Blend, param.Sampler, param.DepthStencil, param.Rasterizer, param.Effect, param.Matrix);
                        MGTools.SafeInvoke(current.GetAfterBegin, SpriteBatch, EventArgs.Empty);
                    }
                }
                if (!begin)
                {
                    begin = true;
                    current = DefaultSpriteBatchParameters;
                    SpriteBatch.Begin(DefaultSpriteBatchParameters.SortMode, DefaultSpriteBatchParameters.Blend, DefaultSpriteBatchParameters.Sampler, DefaultSpriteBatchParameters.DepthStencil, DefaultSpriteBatchParameters.Rasterizer, DefaultSpriteBatchParameters.Effect, DefaultSpriteBatchParameters.Matrix);
                }
                foreach (IMGDrawable dr in ToDraws[keys[i]])
                {
                    try
                    {
                        dr.Draw(gameTime, SpriteBatch);
                    } catch (Exception e)
                    {
                        MGDebug.PrintError(e);
                    }
                }
            }
            if (begin)
            {
                SpriteBatch.End();
            }
            SpriteBatch.Begin();
            MGDebug.DrawPolygons(SpriteBatch);
            SpriteBatch.End();
            GameTimeSpanDraw = start - _lastDrawDate;
            _lastDrawDate = DateTime.Now;
            DrawDuration = _lastDrawDate - start;
        }
    }
}
