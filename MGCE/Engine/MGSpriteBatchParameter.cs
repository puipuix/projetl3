﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Engine
{
    public struct MGSpriteBatchParameter
    {
        public SpriteSortMode SortMode { get; }
        public BlendState Blend { get; }
        public SamplerState Sampler { get; }
        public DepthStencilState DepthStencil { get; }
        public RasterizerState Rasterizer { get; }
        public Effect Effect { get; }
        public Matrix? Matrix { get; }
        public event EventHandler BeforeBegin;
        public EventHandler GetBeforeBegin => BeforeBegin;
        public event EventHandler AfterBegin;
        public EventHandler GetAfterBegin => AfterBegin;
        public MGSpriteBatchParameter(SpriteSortMode sortMode = SpriteSortMode.Deferred, BlendState blend = null, SamplerState sampler = null, DepthStencilState depthStencil = null, RasterizerState rasterizer = null, Effect effect = null, Matrix? matrix = null, EventHandler beforeBegin = null, EventHandler afterBegin = null)
        {
            SortMode = sortMode;
            Blend = blend;
            Sampler = sampler;
            DepthStencil = depthStencil;
            Rasterizer = rasterizer;
            Effect = effect;
            Matrix = matrix;
            BeforeBegin = beforeBegin;
            AfterBegin = afterBegin;
        }

        private bool EqualsOrNull(object a, object b)
        {
            return a is null ? b is null : a.Equals(b);
        }

        public override bool Equals(object obj)
        {
            return obj is MGSpriteBatchParameter parameter &&
                   SortMode == parameter.SortMode &&
                    EqualsOrNull(Blend, parameter.Blend) &&
                    EqualsOrNull(Sampler, parameter.Sampler) &&
                    EqualsOrNull(DepthStencil, parameter.DepthStencil) &&
                    EqualsOrNull(Rasterizer, parameter.Rasterizer) &&
                    EqualsOrNull(Effect, parameter.Effect) &&
                    EqualsOrNull(Matrix, parameter.Matrix);
        }

        public override int GetHashCode()
        {
            var hashCode = 1543920515;
            hashCode = hashCode * -1521134295 + SortMode.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<BlendState>.Default.GetHashCode(Blend);
            hashCode = hashCode * -1521134295 + EqualityComparer<SamplerState>.Default.GetHashCode(Sampler);
            hashCode = hashCode * -1521134295 + EqualityComparer<DepthStencilState>.Default.GetHashCode(DepthStencil);
            hashCode = hashCode * -1521134295 + EqualityComparer<RasterizerState>.Default.GetHashCode(Rasterizer);
            hashCode = hashCode * -1521134295 + EqualityComparer<Effect>.Default.GetHashCode(Effect);
            hashCode = hashCode * -1521134295 + Matrix.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(MGSpriteBatchParameter left, MGSpriteBatchParameter right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(MGSpriteBatchParameter left, MGSpriteBatchParameter right)
        {
            return !(left == right);
        }
    }
}
