﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;

namespace MGCE.Engine
{
    public interface IMGDrawable
    {
		/// <summary>
		/// <para>Draw the actor's <see cref="Texture"/> if not null.</para>
		/// See <see cref="Texture"/>, <see cref="DestinationRectangle"/>, <see cref="SourceRectangle"/>, <see cref="Rotation"/>,
		///  <see cref="Origin"/>, and <see cref="Effects"/>
		/// </summary>
		/// <param name="gameTime"></param>
		/// <param name="spriteBatch"></param>
		void Draw(GameTime gameTime, SpriteBatch spriteBatch);
    }
}
