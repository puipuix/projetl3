﻿using MGCE.Input;
using MGCE.Shapes;
using MGCE.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MGCE.Engine
{
    public static class MGDebug
    {
        public const int MaxExceptionStackTrace = 20;

        /// <summary>
        /// 3x3 White dot.
        /// </summary>
        public static Texture2D Dot { get; private set; }

        public static Stack<(MGPositionedPolygon, Color?, int)> PolygonsToDraw { get; } = new Stack<(MGPositionedPolygon, Color?, int)>();

        /// <summary>
        /// Initialize Engine.
        /// </summary>
        public static void Initialize(Game game)
        {
            Dot = new Texture2D(game.GraphicsDevice, 1, 1);
            Dot.SetData(new Color[] { Color.White });
        }

        private static string GetFileName(string filePath)
        {
            string[] cut = filePath.Split('\\', '.');
            return cut[^2];
        }

        private static string GetInfo(string memberName, int sourceLineNumber)
        {
            return string.Concat(DateTime.Now, " - Thread: ", Thread.CurrentThread.ManagedThreadId, "\t", " (", GetFileName(memberName), ":", sourceLineNumber, ") -> ");
        }

        private static string GetExceptionMessage(Exception e)
        {
            return string.Concat("Caused by:\n'", e.GetType().FullName, "': ", e.Message, "\n", e.StackTrace, "\n");
        }

        /// <summary>
        /// Write text / object in the debug buffer with the line of code who call this function and the time.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="memberName"></param>
        /// <param name="sourceLineNumber"></param>
        public static void PrintLine(object data, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            Debug.Write(GetInfo(memberName, sourceLineNumber));
            Debug.WriteLine(data);
        }

        /// <summary>
        /// Write error in the debug buffer with the line of code who call this function and the time.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="memberName"></param>
        /// <param name="sourceLineNumber"></param>
        public static void PrintError(string str, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            Debug.WriteLine("");
            Debug.Write(GetInfo(memberName, sourceLineNumber));
            Debug.Write("/!\\ ERROR /!\\\n");
            Debug.WriteLine(str);
        }

        /// <summary>
        /// Write exception in the debug buffer with the line of code who call this function and the time.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="memberName"></param>
        /// <param name="sourceLineNumber"></param>
        public static void PrintError(Exception e, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            StringBuilder builder = new StringBuilder();
            Exception inner = e;
            string tab = "";
            int count = 0;
            while (!(inner is null) && count < MaxExceptionStackTrace)
            {
                builder.Append(GetExceptionMessage(inner).Replace("\n", '\n' + tab));
                tab += '\t';
                inner = inner.InnerException;
                if (++count >= MaxExceptionStackTrace)
                {
                    builder.Append("...");
                }
            }
            PrintError(builder.ToString(), memberName, sourceLineNumber);
        }

        public static void StartDebugger(Func<bool> startCondition, MGInputNames ifPressed = MGInputNames.AlwaysTrue)
        {
            if (MGInputs.GetState(ifPressed) > 0.5f && startCondition.Invoke())
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
            }
        }

        public static void StartDebugger(MGInputNames ifPressed = MGInputNames.AlwaysTrue)
        {
            StartDebugger(() => true, ifPressed);
        }

        public static void DrawPolygon(MGPositionedPolygon polygon, Color? color = null, int thickness = 1)
        {
            PolygonsToDraw.Push((polygon, color, thickness));
        }

        internal static void DrawPolygons(SpriteBatch spriteBatch)
        {
            while (!PolygonsToDraw.IsEmpty())
            {
                (MGPositionedPolygon pol, Color? color, int thickness) = PolygonsToDraw.Pop();
                pol.Draw(spriteBatch, color, thickness);
            }
        }
    }
}
