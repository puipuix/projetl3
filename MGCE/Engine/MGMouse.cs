﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MGCE.Engine;
using MGCE.Util;

namespace MGCE.Engine
{
    public static class MGMouse
    {
        /// <summary>
        /// Base cursor
        /// </summary>
        public static MouseCursor Base { get; set; } = MouseCursor.Arrow;

        /// <summary>
        /// Cursor when clicking
        /// </summary>
        public static MouseCursor Click { get; set; } = MouseCursor.Arrow;

        /// <summary>
        /// Cursor when SetLoading == true and SetOnClickable == false and not clicking
        /// </summary>
        public static MouseCursor Loading { get; set; } = MouseCursor.WaitArrow;

        /// <summary>
        /// Cursor when SetOnClickable == true
        /// </summary>
        public static MouseCursor OnClickable { get; set; } = MouseCursor.Hand;

        public static bool SetLoading { get; set; } = false;
        public static bool SetOnClickable { get; set; } = false;
        public static bool IsMouseVisible { get => MGEngine.Game.IsMouseVisible; set => MGEngine.Game.IsMouseVisible = value; }

        public static void Uptate(GameTime gameTime)
        {
            if (SetLoading)
            {
                Mouse.SetCursor(Loading);
            }
            else if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                Mouse.SetCursor(Click);
            }
            else if (SetOnClickable)
            {
                Mouse.SetCursor(OnClickable);
            }
            else
            {
                Mouse.SetCursor(Base);
            }
        }

        /// <summary>
        /// Transform real mouse position like if it was on a 1920x1080 screen. See <see cref="MGEngine.Scale"/>
        /// </summary>
        public static Point Position => Mouse.GetState().Position;

        /// <summary>
        /// Set the cursor position like if it was on a 1920x1080 screen. See <see cref="MGEngine.Scale"/>
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public static void SetPosition(int x, int y)
        {
            Mouse.SetPosition(x, y);
        }

        /// <summary>
        /// Return <see cref="Mouse.GetState()"/>
        /// </summary>
        /// <returns></returns>
        public static MouseState GetState()
        {
            return Mouse.GetState();
        }

        /// <summary>
        /// Return <see cref="Mouse.GetState(GameWindow)"/>
        /// </summary>
        /// <returns></returns>
        public static MouseState GetState(GameWindow gameWindow)
        {
            return Mouse.GetState(gameWindow);
        }
    }
}
