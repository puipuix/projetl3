﻿using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using MGCE.Engine;
using MGCE.Events;
using System.Data;

namespace MGCE.Util
{
    public static class MGLanguage
    {
        /// <summary>
        /// All loaded text translations.
        /// </summary>
        public static Dictionary<string, string> Texts { get; private set; }

        /// <summary>
        /// Game content manager
        /// </summary>
        public static ContentManager Content { get; set; }

        public static string CurrentLanguage { get; private set; }
        public static string LanguageFolder { get; private set; }

        internal static void Initialize(ContentManager content, string currentLanguage, string languageFolder)
        {
            CurrentLanguage = currentLanguage;
            LanguageFolder = languageFolder;
            Content = content;
            MGEvent<IMGEventLanguageChangeListener>.Create<string>();
            MGEvent<IMGEventLanguageChangedListener>.Create<string>();
            try
            {
                MGDebug.PrintLine(CurrentLanguage + " File loading...");
                LoadLanguageDictionary(CurrentLanguage);
            } catch
            {
                MGDebug.PrintLine("LanguageFile dont exist:" + CurrentLanguage);
                Texts = new Dictionary<string, string> {
                    { "key_example1", "value_example1" },
                    { "key_example2", "value_example2" }
                };
            }
        }

        private static string GetFile(string language)
        {
            return Content.RootDirectory + @"\" + LanguageFolder + @"\" + language + ".json";
        }

        /// <summary>
        /// Return the translation from the given key or "ERR: [textName] UNKNOW" if don't exist
        /// </summary>
        /// <param name="textName"></param>
        /// <returns></returns>
        public static string GetText(string textName)
        {
            GetText(textName, out string translation);
            return translation;
        }

        /// <summary>
        /// Return the translation or the textName if doesn't exist.
        /// </summary>
        /// <param name="textName"></param>
        /// <returns></returns>
        public static string GetOrName(string textName)
        {
            return GetText(textName, out string translation) ? translation : textName;
        }

        public static bool GetText(string textName, out string translation)
        {

            if (Texts.ContainsKey(textName))
            {
                translation = Texts[textName];
                return true;
            } else
            {
                translation = "ERR: [" + textName + "] UNKNOW";
                return false;
            }
        }

        /// <summary>
        /// Load a language 
        /// <see cref="MGLanguage.Content"/> + @"\" + <see cref="PPGSettings.LanguageFolder"/> + @"\" + name + ".json";
        /// </summary>
        /// <param name="name"></param>
        public static void LoadLanguageDictionary(string name = "English")
        {
            MGDebug.PrintLine("Loading language: " + name);
            string current = GetFile(name);
            using (StreamReader reader = new StreamReader(current))
            {
                Texts = JsonConvert.DeserializeObject<Dictionary<string, string>>(reader.ReadToEnd());
            }
            CurrentLanguage = name;
            MGEvent<IMGEventLanguageChangeListener>.Get().AskEvent(name);
            MGEvent<IMGEventLanguageChangedListener>.Get().AskEvent(name);
        }

        /// <summary>
        /// Return all (json) languages' name inside Content.RootDirectory + @"\" + PPGSettings.Default.LanguageFolder
        /// </summary>
        /// <returns></returns>
        public static string[] FindLanguages()
        {
            string[] files = Directory.GetFiles(Content.RootDirectory + @"\" + LanguageFolder, "*.json");
            for (int i = 0; i < files.Length; i++)
            {
                string[] str = files[i].Split('\\');
                files[i] = str[str.Length - 1].Replace(".json", "");
            }
            return files;
        }
    }
}