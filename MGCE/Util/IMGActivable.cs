﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCE.Util
{
    public interface IMGDisposable : IDisposable
    {
        bool IsDisposed { get; }
    }
    public interface IMGActivable : IMGDisposable
    {
        
        void Activate();
        void Disactivate();
    }
}
