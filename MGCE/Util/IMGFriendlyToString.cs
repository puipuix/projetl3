﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Util
{
    /// <summary>
    /// Interface that add a user friendly to string.
    /// </summary>
    public interface IMGFriendlyToString
    {
        string FriendlyToString();
    }

    /// <summary>
    /// Delegate that return a user friendly to string.
    /// </summary>
    public delegate string MGGetFriendlyToString<T>(T item);

    /// <summary>
    /// Use delegate to return a friendly to string of any object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MGFriendlyToStringObject<T> : IMGFriendlyToString
    {
        public T Item { get; set; }
        public MGGetFriendlyToString<T> DelegateFriendlyToString { get; set; }

        public MGFriendlyToStringObject(T item, MGGetFriendlyToString<T> delegateFriendlyToString)
        {
            Item = item;
            DelegateFriendlyToString = delegateFriendlyToString;
        }

        public string FriendlyToString()
        {
            return DelegateFriendlyToString(Item);
        }

        public override string ToString()
        {
            return FriendlyToString();
        }

        /// <summary>
        /// Return a list of PPGFriendlyToStringObject based on object.toString().
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static MGFriendlyToStringObject<T>[] FromToString(params T[] items)
        {
            MGFriendlyToStringObject<T>[] tab = new MGFriendlyToStringObject<T>[items.Length];
            for(int i =0; i < items.Length; i++)
            {
                tab[i] = FromToString(items[i]);
            }
            return tab;
        }

        /// <summary>
        /// Return a PPGFriendlyToStringObject based on object.toString().
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static MGFriendlyToStringObject<T> FromToString(T item)
        {
            return new MGFriendlyToStringObject<T>(item, delegate (T obj)
            {
                return obj.ToString();
            });
        }
    }
}
