﻿using System;

namespace MGCE.Util
{
    /// <summary>
    /// Class to create javalike enum
    /// </summary>
    public abstract class MGEnum : IComparable<MGEnum>, IMGFriendlyToString
    {
        /// <summary>
        /// Index of the enum
        /// </summary>
        public readonly int value;

        /// <summary>
        /// Name of the enum
        /// </summary>
        public readonly string name;

        protected MGEnum(int v, string n)
        {
            value = v;
            name = n;
        }

        public static bool operator ==(MGEnum first, MGEnum second)
        {
            bool t1 = first is null, t2 = second is null;
            if (t1 && t2)
            {
                return true;
            }

            return t1 || t2 ? false : first.value == second.value;
        }

        public static bool operator !=(MGEnum first, MGEnum second)
        {
            
            bool t1 = first is null, t2 = second is null;
            if (t1 && t2)
            {
                return false;
            }

            return t1 || t2 ? true : first.value != second.value;
        }

        public override bool Equals(Object other)
        {
            if (other is null)
            {
                return false;
            }
            return other is MGEnum ? value == ((MGEnum)other).value : false;
        }

        public override int GetHashCode()
        {
            return value;
        }

        public int CompareTo(MGEnum other)
        {
            return value.CompareTo(other.value);
        }

        public override string ToString()
        {
            return name;
        }

        public string FriendlyToString()
        {
            return ToString();
        }
    }
}
