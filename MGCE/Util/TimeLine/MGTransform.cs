﻿using Microsoft.Xna.Framework;
using System;

namespace MGCE.Util.TimeLine
{
    public abstract class MGTransform : IDisposable
    {
        public TimeSpan StartTime { get; }
        public TimeSpan EndTime { get; }
        public TimeSpan? LastTime { get; private set; }
        protected MGTransform(TimeSpan startTime, TimeSpan endTime)
        {
            StartTime = startTime;
            EndTime = endTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <returns>True when finished</returns>
        public bool Update(TimeSpan current)
        {
            if (StartTime <= current)
            {
                if (LastTime is null)
                {
                    LastTime = current;
                }
                UpdateValue(Math.Min((current - LastTime.Value).TotalSeconds / (EndTime - LastTime.Value).TotalSeconds, 1.0));
            }
            LastTime = current;
            return EndTime <= current;
        }
        public void Reset()
        {
            LastTime = null;
        }
        public abstract void ForceResult();
        protected abstract void UpdateValue(double timeSpanCoef);

        public virtual void Dispose() { }
    }

    public abstract class MGTransform<T> : MGTransform
    {
        public Func<T> Getter { get; private set; }
        public Action<T> Setter { get; private set; }
        public T Target { get; private set; }
        protected MGTransform(TimeSpan startTime, TimeSpan endTime, T target, Func<T> getter, Action<T> setter) : base(startTime, endTime)
        {
            Getter = getter;
            Setter = setter;
            Target = target;
        }

        public T Value
        {
            get => Getter();
            set => Setter(value);
        }

        public override void ForceResult()
        {
            Value = Target;
        }

        public override void Dispose()
        {
            base.Dispose();
            Getter = null;
            Setter = null;
            Target = default;
        }
    }

    public class MGIntTransform : MGTransform<int>
    {
        public MGIntTransform(TimeSpan endTime, int target, Func<int> getter, Action<int> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Value += (int)((Target - Value) * timeSpanCoef);
        }
    }

    public class MGDoubleTransform : MGTransform<double>
    {
        public MGDoubleTransform(TimeSpan endTime, double target, Func<double> getter, Action<double> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Value += (Target - Value) * timeSpanCoef;
        }
    }

    public class MGLongTransform : MGTransform<long>
    {
        public MGLongTransform(TimeSpan endTime, long target, Func<long> getter, Action<long> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Value += (long)((Target - Value) * timeSpanCoef);
        }
    }

    public class MGFloatTransform : MGTransform<float>
    {
        public MGFloatTransform(TimeSpan endTime, float target, Func<float> getter, Action<float> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Value += (float)((Target - Value) * timeSpanCoef);
        }
    }

    public class MGVector3Transform : MGTransform<Vector3>
    {
        public MGVector3Transform(TimeSpan endTime, Vector3 target, Func<Vector3> getter, Action<Vector3> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Value += (Target - Value) * (float)timeSpanCoef;
        }
    }

    public class MGVector2Transform : MGTransform<Vector2>
    {
        public MGVector2Transform(TimeSpan endTime, Vector2 target, Func<Vector2> getter, Action<Vector2> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Value += (Target - Value) * (float)timeSpanCoef;
        }
    }

    public class MGColorTransform : MGTransform<Color>
    {
        public MGColorTransform(TimeSpan endTime, Color target, Func<Color> getter, Action<Color> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Vector4 color = Value.ToVector4();
            Value = new Color(color + (Target.ToVector4() - color) * (float)timeSpanCoef);
        }
    }

    public class MGVector4Transform : MGTransform<Vector4>
    {
        public MGVector4Transform(TimeSpan endTime, Vector4 target, Func<Vector4> getter, Action<Vector4> setter, TimeSpan? startTime = null) : base(startTime ?? TimeSpan.Zero, endTime, target, getter, setter)
        {
        }

        protected override void UpdateValue(double timeSpanCoef)
        {
            Value += (Target - Value) * (float)timeSpanCoef;
        }
    }
}
