﻿using System;

namespace MGCE.Util.TimeLine
{
    public class TimelineEventArgs : EventArgs
    {
        public TimeSpan CurrentTime { get; }

        public MGTimeline Timeline { get; }

        public TimelineEventArgs(TimeSpan currentTime, MGTimeline timeline)
        {
            CurrentTime = currentTime;
            Timeline = timeline;
        }
    }
}
