﻿using System;

namespace MGCE.Util.TimeLine
{
    public class MGTimeTransform : MGTimeFrame
    {
        public MGTransform Property { get; private set; }
        public MGTimeFrame NextNeedUpdate { get; private set; }
        public MGTimeTransform(MGTransform property) : base(TimeSpan.Zero, (o, args) => property.ForceResult())
        {
            Property = property;
        }

        public override void Dispose()
        {
            base.Dispose();
            NextNeedUpdate = null;
            Property = null;
        }

        public override MGTimeFrame Reset(TimeSpan time)
        {
            Property.Reset();
            NextNeedUpdate = Next?.Reset(time);
            return this;
        }

        public override MGTimeFrame Update(TimeSpan current, MGTimeline timeline)
        {
            if (Property.Update(current))
            {
                MGTools.SafeInvoke(GetOnAction, this, new TimelineEventArgs(current, timeline));
                return NextNeedUpdate?.Update(current, timeline);
            } else
            {
                NextNeedUpdate = NextNeedUpdate?.Update(current, timeline);
                return this;
            }
        }
    }
}
