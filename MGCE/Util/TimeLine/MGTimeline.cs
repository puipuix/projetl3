﻿using Microsoft.Xna.Framework;
using MGCE.Engine;
using MGCE.Events;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Util.TimeLine
{

    public class MGTimeline : IMGEventUpdateListener, IMGDisposable
    {
        /// <summary>
        /// The frames of this timeline
        /// </summary>
        public MGTimeFrame Frames { get; set; }
        public MGTimeFrame NextFrame { get; set; }

        /// <summary>
        /// The number of cycle this timeline will do, -1 for infinite
        /// </summary>
        public long CycleCount { get; set; } = 1;
        /// <summary>
        /// The number of cycles done
        /// </summary>
        public long CycleDone { get; private set; }
        /// <summary>
        /// Set CycleCount to this value will perform an infinite number of cycle
        /// </summary>
        public const long INFINITE_CYCLE = -1;

        /// <summary>
        /// The starting TimeSpan, all frame befor it will be ignore
        /// </summary>
        public TimeSpan StartTime { get; set; } = TimeSpan.Zero;
        /// <summary>
        /// The current TimeSpan
        /// </summary>
        public TimeSpan CurrentTime { get; private set; }

        /// <summary>
        /// if the timeline is started
        /// </summary>
        public bool IsStarted { get; protected set; }

        /// <summary>
        /// if the timeline is getting update
        /// </summary>
        public bool IsRunning { get; protected set; }

        public bool IsDisposed { get; private set; }

        public MGTimeline()
        {
            Reset(true);
        }

        public MGTimeline(MGTimeFrame first, params MGTimeFrame[] others)
        {
            Frames = first.Add(others);
            Reset(true);
        }

        /// <summary>
        /// Update the current time of the timeline and update frames
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Uptate(TimeSpan elapsedGameTime)
        {
            CurrentTime += elapsedGameTime;
            NextFrame = NextFrame?.Update(CurrentTime, this);
            if (NextFrame is null)
            {
                CycleDone++;
                if (CycleCount == -1 || CycleDone < CycleCount)
                {
                    Reset(false);
                } else
                {
                    Stop();
                }
            }
        }
        public virtual void Uptate(GameTime gameTime)
        {
            IsRunning = true;
            Uptate(gameTime.ElapsedGameTime);
        }
        public virtual void Start()
        {
            Reset(true);
            Resume();
        }

        public virtual void Reset(bool resetCycleDone = true)
        {
            CurrentTime = StartTime;
            if (resetCycleDone)
            {
                CycleDone = 0;
            }
            NextFrame = Frames?.Reset(CurrentTime);
        }

        public virtual void Resume()
        {
            if (!IsStarted)
            {
                IsStarted = true;
                MGEvent<IMGEventUpdateListener>.Get().AddListener(this);
                Uptate(TimeSpan.Zero);
            }
        }

        public virtual void Stop()
        {
            if (IsStarted)
            {
                IsStarted = false;
                IsRunning = false;
                MGEvent<IMGEventUpdateListener>.Get().RemoveListener(this);
            }
        }

        public virtual void Dispose()
        {
            Stop();
            Frames?.Dispose();
            Frames = null;
            NextFrame = null;
            IsDisposed = true;
        }
    }
}
