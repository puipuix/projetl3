﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MGCE.Util.TimeLine
{
    public class MGTimeFrame : IEnumerable<MGTimeFrame>, IMGDisposable
    {
        private class MGTimeFrameEnumerator : IEnumerator<MGTimeFrame>, IDisposable
        {
            public MGTimeFrame Head { get; private set; }
            public MGTimeFrame Next { get; private set; }
            public MGTimeFrame Current { get; private set; }

            public MGTimeFrameEnumerator(MGTimeFrame head)
            {
                Head = Next = head;
            }

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                Head = Next = Current = null;
            }

            public bool MoveNext()
            {
                if (Next is null)
                {
                    return false;
                }
                else
                {
                    Current = Next;
                    Next = Next.Next;
                    return true;
                }
            }

            public void Reset()
            {
                Next = Head;
            }
        }

        public MGTimeFrame Next { get; private set; }
        public TimeSpan ActivationTime { get; }

        public event EventHandler<TimelineEventArgs> OnAction;
        protected EventHandler<TimelineEventArgs> GetOnAction => OnAction;

        public bool IsDisposed { get; private set; }

        public MGTimeFrame(TimeSpan activationTime, EventHandler<TimelineEventArgs> onAction)
        {
            ActivationTime = activationTime;
            OnAction += onAction;
        }

        public virtual void Dispose()
        {
            OnAction = null;
            Next?.Dispose();
            Next = null;
            IsDisposed = true;
        }

        /// <summary>
        /// Add the new frame and return the first frame who need to be use.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public virtual MGTimeFrame Add(MGTimeFrame frame)
        {
            // frame is after or with me
            if (frame.ActivationTime.CompareTo(ActivationTime) >= 0)
            {
                Next = Next is null ? frame : Next.Add(frame);
                return this;
            }
            else
            {
                return frame.Add(this);
            }
        }

        /// <summary>
        /// Add the new frame and return the first frame who need to be use.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public virtual MGTimeFrame Add(params MGTimeFrame[] frames)
        {
            MGTimeFrame head = this;
            foreach (MGTimeFrame frame in frames)
            {
                head = head.Add(frame);
            }
            return head;
        }

        /// <summary>
        /// Remove a frame from this chain of frames.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public virtual MGTimeFrame Remove(MGTimeFrame frame)
        {
            if (frame == this)
            {
                MGTimeFrame next = Next;
                Next = null;
                return next;
            }
            else
            {
                if (!(Next is null))
                {
                    Next = Next.Remove(frame);
                }
                return this;
            }
        }

        /// <summary>
        /// Call OnAction and call the next frame if current >= Duration
        /// </summary>
        /// <param name="current"></param>
        /// <param name="timeline"></param>
        /// <returns></returns>
        public virtual MGTimeFrame Update(TimeSpan current, MGTimeline timeline)
        {
            // if the current time is after me
            if (WillInvoke(current))
            {
                MGTools.SafeInvoke(OnAction, this, new TimelineEventArgs(current, timeline));
                return Next?.Update(current, timeline);
            }
            else
            {
                return this;
            }
        }

        public virtual bool WillInvoke(TimeSpan current)
        {
            return current.CompareTo(ActivationTime) >= 0;
        }

        /// <summary>
        /// Return wich frame should be called with this time as a current time
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public virtual MGTimeFrame Reset(TimeSpan time)
        {
            // <= 0 because if time == duration this frame will be called
            if (time.CompareTo(ActivationTime) <= 0)
            {
                return this;
            }
            else
            {
                return Next?.Reset(time);
            }
        }

        public IEnumerator<MGTimeFrame> GetEnumerator()
        {
            return new MGTimeFrameEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        

        public static MGTimeFrame operator +(MGTimeFrame a, MGTimeFrame b)
        {
            return a is null ? b : a.Add(b);
        }

        public static MGTimeFrame operator -(MGTimeFrame a, MGTimeFrame b)
        {
            return a is null ? a : a.Remove(b);
        }
    }
}
