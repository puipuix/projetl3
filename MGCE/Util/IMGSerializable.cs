﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MGCE.Util
{
	public interface IMGSerializable : ISerializable, IDeserializationCallback
	{
		/// <summary>
		/// Populates a System.Runtime.Serialization.SerializationInfo with the data needed to serialize the target object.
		/// </summary>
		/// <param name="info">The System.Runtime.Serialization.SerializationInfo to populate with data.</param>
		/// <param name="context">The destination (see System.Runtime.Serialization.StreamingContext) for this serialization.</param>
		void Serialize(SerializationInfo info, StreamingContext context);

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			Serialize(info, context);
		}
	}
}
