﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Engine;

namespace MGCE.Util
{
    public class MGPrioritarizedList<T>
    {
        /// <summary>
        /// Levels of priority.
        /// </summary>
        public SortedList<int, List<T>> Levels { get; private set; }

        /// <summary>
        /// Return the number of Levels
        /// </summary>
        public int Count { get => Levels.Count; }

        /// <summary>
        /// Return all level indexs
        /// </summary>
        public IList<int> Keys { get => Levels.Keys; }

        /// <summary>
        /// Return all Values inside all levels sorted by the level index.
        /// </summary>
        public List<T> Values
        {
            get {
                List<T> l = new List<T>();
                IList<int> keys = Levels.Keys;
                for (int i = 0; i < keys.Count; i++)
                {
                    l.AddRange(Levels[keys[i]]);
                }
                return l;
            }
        }

        /// <summary>
        /// Return a level of priority.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public List<T> this[int index]
        {
            get {
                return Levels[index];
            }
        }

        public MGPrioritarizedList()
        {
            Levels = new SortedList<int, List<T>>(Comparer<int>.Create((a, b) => a < b ? -1 : a == b ? 0 : 1));
        }

        /// <summary>
        /// Add an object to the corresponding index.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="index"></param>
        public MGPrioritarizedList<T> Add(T obj, int index)
        {
            if (!Levels.ContainsKey(index))
            {
                Levels.Add(index, new List<T>());
            }
            Levels[index].Add(obj);
            return this;
        }

        /// <summary>
        /// Remove an object to the corresponding index.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="index"></param>
        public bool Remove(T obj, int index)
        {
            if (!Levels.ContainsKey(index))
            {
                return false;
            }
            else
            {
                return Levels[index].Remove(obj);
            }
        }

        /// <summary>
        /// Search and remove an object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Remove(T obj)
        {
            bool removed = false;
            IList<int> keys = Levels.Keys;

            for (int i = 0; i < keys.Count && !removed; i++)
            {
                removed = Levels[keys[i]].Remove(obj);
            }

            return removed;
        }
    }
}
