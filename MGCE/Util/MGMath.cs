﻿using MGCE.Util.Caster;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Util
{
    public static class MGMath
    {
        public static float Dot(this Vector2 a, Vector2 b)
        {
            return Vector2.Dot(a, b);
        }
        public static float Cross(this Vector2 a, Vector2 b)
        {
            return a.X * b.Y - a.Y * b.X;
        }

        public static float Min(this Vector2 vector)
        {
            return Math.Min(vector.X, vector.Y);
        }
        public static float Max(this Vector2 vector)
        {
            return Math.Max(vector.X, vector.Y);
        }
        public static float Sum(this Vector2 vec)
        {
            return vec.X + vec.Y;
        }
        public static float Avg(this Vector2 vec)
        {
            return vec.Sum() / 2.0f;
        }
        public static Vector2 Abs(this Vector2 vector)
        {
            return new Vector2(Math.Abs(vector.X), Math.Abs(vector.Y));
        }
        public static Vector2 Max(Vector2 a, Vector2 b)
        {
            return new Vector2(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y));
        }
        public static Vector2 Min(Vector2 a, Vector2 b)
        {
            return new Vector2(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y));
        }
        public static Vector2 Normalized(this Vector2 a)
        {
            return Vector2.Normalize(a);
        }
        public static float DistanceSquare(this Vector2 a, Vector2 b)
        {
            return Vector2.DistanceSquared(a, b);
        }
        public static float Distance(this Vector2 a, Vector2 b)
        {
            return Vector2.Distance(a, b);
        }

        public static float Min(this Vector3 vector)
        {
            return Math.Min(Math.Min(vector.X, vector.Y), vector.Z);
        }
        public static float Max(this Vector3 vector)
        {
            return Math.Max(Math.Max(vector.X, vector.Y), vector.Z);
        }
        public static float Dot(this Vector3 a, Vector3 b)
        {
            return Vector3.Dot(a, b);
        }
        public static float Sum(this Vector3 vec)
        {
            return vec.X + vec.Y + vec.Z;
        }
        public static float Avg(this Vector3 vec)
        {
            return vec.Sum() / 3.0f;
        }
        public static Vector3 Abs(this Vector3 vector)
        {
            return new Vector3(Math.Abs(vector.X), Math.Abs(vector.Y), Math.Abs(vector.Z));
        }
        public static Vector3 Max(Vector3 a, Vector3 b)
        {
            return new Vector3(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y), Math.Max(a.Z, b.Z));
        }
        public static Vector3 Min(Vector3 a, Vector3 b)
        {
            return new Vector3(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y), Math.Min(a.Z, b.Z));
        }
        public static Vector3 Normalized(this Vector3 a)
        {
            return Vector3.Normalize(a);
        }
        public static float DistanceSquare(this Vector3 a, Vector3 b)
        {
            return Vector3.DistanceSquared(a, b);
        }
        public static float Distance(this Vector3 a, Vector3 b)
        {
            return Vector3.Distance(a, b);
        }

        public static Point Minus(this Point p)
        {
            return new Point(-p.X, -p.Y);
        }
        public static Point Mul(this Point p, int mul)
        {
            return new Point(p.X * mul, p.Y * mul);
        }
        public static Point Div(this Point p, int div)
        {
            return new Point(p.X / div, p.Y / div);
        }
        public static Point Mod(this Point p, int div)
        {
            return new Point(p.X % div, p.Y % div);
        }
        public static Point Mod(this Point p, Point div)
        {
            return new Point(p.X % div.X, p.Y % div.Y);
        }
        public static MGPoint ToMGPoint(this Point p)
        {
            return p;
        }
        public static MGPoint ToMGPoint(this System.Drawing.Point p)
        {
            return p;
        }
        public static MGPoint ToMGPoint(this System.Drawing.Size p)
        {
            return p;
        }

        public static Point Max(Point a, Point b)
        {
            return new Point(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y));
        }
        public static Point Min(Point a, Point b)
        {
            return new Point(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y));
        }

        public static float Clamp(float min, float value, float max)
        {
            return Math.Max(min, Math.Min(value, max));
        }
        public static int Clamp(int min, int value, int max)
        {
            return Math.Max(min, Math.Min(value, max));
        }
        public static double Clamp(double min, double value, double max)
        {
            return Math.Max(min, Math.Min(value, max));
        }
        public static long Clamp(long min, long value, long max)
        {
            return Math.Max(min, Math.Min(value, max));
        }


        /// <summary>
        /// Faster than <see cref="Math.Floor(double)"/>
        /// </summary>
        /// <param name="my"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static long FastFloor(double v)
        {
            return (v >= 0) ? ((long)v) : (((long)v) - 1);
        }
        /// <summary>
        /// Faster than <see cref="Math.Floor(double)"/>
        /// </summary>
        /// <param name="my"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static int FastFloor(float v)
        {
            return (v >= 0) ? ((int)v) : (((int)v) - 1);
        }

        public static double Lerp(double a, double b, double coef)
        {
            return a + (b - a) * coef;
        }
        public static float Lerp(float a, float b, float coef)
        {
            return a + (b - a) * coef;
        }
        public static int Lerp(int a, int b, float coef)
        {
            return a + (int)((b - a) * coef);
        }
        public static long Lerp(long a, long b, float coef)
        {
            return a + (long)((b - a) * coef);
        }

        public static Vector2 Average<T>(this IEnumerable<T> enumerable, Func<T, Vector2> selector)
        {
            Vector2 sum = Vector2.Zero;
            int count = 0;
            foreach (Vector2 v in enumerable.Select(selector))
            {
                sum += v;
                count++;
            }
            return sum / count;
        }
        public static Vector3 Average<T>(this IEnumerable<T> enumerable, Func<T, Vector3> selector)
        {
            Vector3 sum = Vector3.Zero;
            int count = 0;
            foreach (Vector3 v in enumerable.Select(selector))
            {
                sum += v;
                count++;
            }
            return sum / count;
        }

        public static Vector2 Average<T>(this IEnumerable<T> enumerable, Func<T, (Vector2, float)> selector)
        {
            Vector2 sum = Vector2.Zero;
            float count = 0;
            foreach ((Vector2 v, float w) in enumerable.Select(selector))
            {
                sum += v * w;
                count += w;
            }
            return sum / count;
        }
        public static Vector3 Average<T>(this IEnumerable<T> enumerable, Func<T, (Vector3, float)> selector)
        {
            Vector3 sum = Vector3.Zero;
            float count = 0;
            foreach ((Vector3 v, float w) in enumerable.Select(selector))
            {
                sum += v * w;
                count += w;
            }
            return sum / count;
        }


        public static int ToSigleDim(Point p, int xLength)
        {
            return p.X + p.Y * xLength;
        }

        public static Point ToDoubleDim(int xy, int xLength)
        {
            return new Point(xy % xLength, xy / xLength);
        }


        public static float DivF(this int a, int b)
        {
            return (float)a / b;
        }
    }
}
