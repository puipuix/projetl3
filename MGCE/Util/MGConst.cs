﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MGCE.Util
{
    public static class MGConst
    {
        public const int ORDER_0_ALWAYS_FIRST = int.MinValue;
        public const int ORDER_1_BEFORE = -1000000;
        public const int ORDER_2_JUST_BEFORE = -1000;
        public const int ORDER_3_DEFAULT = 0;
        public const int ORDER_4_JUST_AFTER = 1000;
        public const int ORDER_5_AFTER = 1000000;
        public const int ORDER_6_ALWAYS_LAST = int.MaxValue;

        public const int DRAW_0_MAX_BACKGROUND = int.MinValue;
        public const int DRAW_1_BACKGROUND_3 = -3000000;
        public const int DRAW_2_BACKGROUND_2 = -2000000;
        public const int DRAW_3_BACKGROUND_1 = -1000000;
        public const int DRAW_4_DEFAULT = 0;
        public const int DRAW_5_FOREGROUND = 1000000;
        public const int DRAW_6_FOREGROUND_2 = 2000000;
        public const int DRAW_7_FOREGROUND_3 = 3000000;
        public const int DRAW_8_POPUP = 1000000000;
        public const int DRAW_9_MAX_FOREGROUND = int.MaxValue;
    }
}
