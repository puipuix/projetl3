﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Util
{
    public struct MGVector2Double
    {
        public double X { get; set; }
        public double Y { get; set; }

        public MGVector2Double(double x, double y)
        {
            X = x;
            Y = y;
        }

        public MGVector2Double(double v) : this(v, v)
        {
        }

        public static MGVector2Double FastFloor(MGVector2Double value)
        {
            return new MGVector2Double(MGMath.FastFloor(value.X), MGMath.FastFloor(value.Y));
        }

        public static MGVector2Double FastRound(MGVector2Double value)
        {
            return new MGVector2Double(MGMath.FastFloor(value.X + 0.5), MGMath.FastFloor(value.Y + 0.5));
        }

        public static double DistanceSquare(MGVector2Double value1, MGVector2Double value2)
        {
            MGVector2Double tmp = value1 - value2;
            tmp *= tmp;
            return tmp.X + tmp.Y;
        }

        public static double Distance(MGVector2Double value1, MGVector2Double value2)
        {
            return Math.Sqrt(DistanceSquare(value1, value2));
        }

        public static MGVector2Double operator +(MGVector2Double value1, MGVector2Double value2)
        {
            return new MGVector2Double(value1.X + value2.X, value1.Y + value2.Y);
        }
        public static MGVector2Double operator -(MGVector2Double value)
        {
            return new MGVector2Double(-value.X, -value.Y);
        }
        public static MGVector2Double operator -(MGVector2Double value1, MGVector2Double value2)
        {
            return new MGVector2Double(value1.X - value2.X, value1.Y - value2.Y);
        }
        public static MGVector2Double operator *(MGVector2Double value1, MGVector2Double value2)
        {
            return new MGVector2Double(value1.X * value2.X, value1.Y * value2.Y);
        }
        public static MGVector2Double operator *(double scaleFactor, MGVector2Double value)
        {
            return value * scaleFactor;
        }
        public static MGVector2Double operator *(MGVector2Double value, double scaleFactor)
        {
            return new MGVector2Double(value.X * scaleFactor, value.Y * scaleFactor);
        }
        public static MGVector2Double operator /(MGVector2Double value1, MGVector2Double value2)
        {
            return new MGVector2Double(value1.X / value2.X, value1.Y / value2.Y);
        }
        public static MGVector2Double operator /(MGVector2Double value, double divider)
        {
            return new MGVector2Double(value.X / divider, value.Y / divider);
        }
        public static bool operator ==(MGVector2Double value1, MGVector2Double value2)
        {
            return value1.X == value2.X && value2.X == value2.Y;
        }
        public static bool operator !=(MGVector2Double value1, MGVector2Double value2)
        {
            return !(value1 == value2);
        }

        public Vector2 ToVector2()
        {
            return new Vector2((float)X, (float)Y);
        }

        public override bool Equals(object obj)
        {
            return obj is MGVector2Double vec && this == vec;
        }
        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode() * 46341;
        }

        public override string ToString()
        {
            return new StringBuilder("{ X=").Append(X).Append("; Y=").Append(Y).Append(" }").ToString();
        }
    }
}
