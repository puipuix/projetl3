﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MGCE.Actor;
using MGCE.Engine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MGCE.Util.Caster;
using System.Runtime.Serialization;

namespace MGCE.Util
{
	public static class MGTools
	{
		/// <summary>
		/// Random Singleton
		/// </summary>
		public static Random RNG { get; } = new Random();


		/// <summary>
		/// Find the type in all assembly using the name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static Type FindType(string name)
		{
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (Type type in assembly.GetTypes())
				{
					if (type.Name == name)
					{
						return type;
					}
				}
			}
			return null;
		}


		/// <summary>
		/// Try to invoke and EventHandler, return false if it was null.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="handler"></param>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static bool SafeInvoke<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs
		{
			if (handler != null)
			{
				handler.Invoke(sender, args);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Try to invoke and EventHandler if the expression is true, return false if the EventHandler was null or if expression was false.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="expression"></param>
		/// <param name="handler"></param>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static bool SafeInvokeIf<T>(this EventHandler<T> handler, bool expression, object sender, T args) where T : EventArgs
		{
			if (expression)
			{
				return SafeInvoke(handler, sender, args);
			} else
			{
				return false;
			}
		}

		/// <summary>
		/// Try to invoke and EventHandler, return false if it was null.
		/// </summary>
		/// <param name="handler"></param>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static bool SafeInvoke(this EventHandler handler, object sender, EventArgs args = null)
		{
			if (handler != null)
			{
				handler.Invoke(sender, args ?? EventArgs.Empty);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Try to invoke and EventHandler if the expression is true, return false if the EventHandler was null or if expression was false.
		/// </summary>
		/// <param name="expression"></param>
		/// <param name="handler"></param>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static bool SafeInvokeIf(this EventHandler handler, bool expression, object sender, EventArgs args = null)
		{
			if (expression)
			{
				return SafeInvoke(handler, sender, args ?? EventArgs.Empty);
			} else
			{
				return false;
			}
		}


		public static void Draw(this SpriteBatch batch, MGTexture2D texture, Rectangle destinationRectangle, Color color, float opacity, float rotation, Vector2 centerOfTexture, SpriteEffects effects, float layerDepth)
		{
			batch.Draw(texture, destinationRectangle, texture, color * opacity, rotation, centerOfTexture, effects, layerDepth);
		}
		public static void Draw(this SpriteBatch batch, MGSpriteSheet spriteSheet, Rectangle destinationRectangle, Color color, float opacity, float rotation, SpriteEffects effects, float layerDepth)
		{
			batch.Draw(spriteSheet.Texture, destinationRectangle, spriteSheet.Texture, color * opacity, rotation, spriteSheet.CenterOfTexture, effects, layerDepth);
		}
		public static void Draw(this SpriteBatch batch, MGSpriteSheet spriteSheet, IMGDrawableActor actor, float layerDepth)
		{
			batch.Draw(spriteSheet.Texture, actor.GetDestinationRectangle(), spriteSheet.Texture, actor.Color * actor.Opacity, actor.Rotation, actor.Origin, actor.Effects, layerDepth);
		}
		public static void Draw(this SpriteBatch batch, MGTexture2D texture, IMGDrawableActor actor, float layerDepth)
		{
			batch.Draw(texture, actor.GetDestinationRectangle(), texture, actor.Color * actor.Opacity, actor.Rotation, actor.Origin, actor.Effects, layerDepth);
		}

		public static void AddValues(this SerializationInfo info, params (string name, object val)[] values)
		{
			foreach (var (name, val) in values)
			{
				info.AddValue(name, val);
			}
		}
		public static void AddAllValues(this SerializationInfo info, object obj, BindingFlags flags, params string[] names)
		{
			obj.GetType().GetProperties(flags).Where(p => names.IsEmpty() || names.Contains(p.Name)).ForEach(p => {
				info.AddValue(p.Name, p.GetValue(obj), p.PropertyType);
			});
			obj.GetType().GetFields(flags).Where(f => names.IsEmpty() || names.Contains(f.Name)).ForEach(f => {
				info.AddValue(f.Name, f.GetValue(obj), f.FieldType);
			});
		}
		public static void AddAllValues(this SerializationInfo info, object obj)
		{
			AddAllValues(info, obj, BindingFlags.Default, new string[0]);
		}
		public static void AddAllValues(this SerializationInfo info, object obj, params string[] names)
		{
			AddAllValues(info, obj, BindingFlags.Default, names);
		}

		public static T GetValue<T>(this SerializationInfo info, string name)
		{
			return (T)(info.GetValue(name, typeof(T)) ?? default(T));
		}
		public static void GetValue<T>(this SerializationInfo info, string name, out T value)
		{
			value = GetValue<T>(info, name);
		}
		public static Vector2 GetVector2(this SerializationInfo info, string name)
		{
			return GetValue<Vector2>(info, name);
		}
		public static Vector3 GetVector3(this SerializationInfo info, string name)
		{
			return GetValue<Vector3>(info, name);
		}
		public static Vector4 GetVector4(this SerializationInfo info, string name)
		{
			return GetValue<Vector4>(info, name);
		}
		public static Color GetColor(this SerializationInfo info, string name)
		{
			return GetValue<Color>(info, name);
		}
		public static Point GetPoint(this SerializationInfo info, string name)
		{
			return GetValue<Point>(info, name);
		}
		public static void GetAndSetAllValues(this SerializationInfo info, object obj, params string[] names)
		{
			object[] tab = new object[1];
			names.ForEach(n => {
				if (obj.GetType().GetProperty(n) is PropertyInfo property && property.SetMethod is MethodInfo setter)
				{
					tab[0] = info.GetValue(n, property.PropertyType);
					setter.Invoke(obj, tab);
				} else if (obj.GetType().GetField(n) is FieldInfo field)
				{
					field.SetValue(obj, info.GetValue(n, field.FieldType));
				}
			});
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="info"></param>
		/// <param name="name"></param>
		/// <param name="_0">Can be null, used to compile Type of T</param>
		/// <returns></returns>
		public static T GetValue<T>(this SerializationInfo info, string name, T _0)
		{
			return GetValue<T>(info, name);
		}

		public static SerializationInfo GetValue<T>(this SerializationInfo info, string name, Action<T> setter)
		{
			setter(GetValue<T>(info, name));
			return info;
		}

		/// <summary>
		/// Return of the collection is empty
		/// </summary>
		/// <param name="enumerable"></param>
		/// <returns></returns>
		public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
		{
			bool empty = true;
			foreach (var item in enumerable)
			{
				empty = false;
				break;
			}
			return empty;
		}
		public static T FindMax<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
		{
			bool start = true;
			T max = default;
			foreach (var item in enumerable)
			{
				if (start)
				{
					max = item;
				} else
				{
					if (comparison(item, max) > 0)
					{
						max = item;
					}
				}
			}

			return max;
		}
		public static T FindMin<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
		{
			bool start = true;
			T max = default;
			foreach (var item in enumerable)
			{
				if (start)
				{
					max = item;
				} else
				{
					if (comparison(item, max) < 0)
					{
						max = item;
					}
				}
			}

			return max;
		}
		public static T[] Sorted<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
		{
			T[] array = enumerable.ToArray();
			Array.Sort(array, comparison);
			return array;
		}
		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			foreach (var item in enumerable)
			{
				action.Invoke(item);
			}
		}
		public static void For<T>(this IEnumerable<T> enumerable, Action<T, int> action)
		{
			int i = 0;
			foreach (var item in enumerable)
			{
				action.Invoke(item, i++);
			}
		}
		public static string ToString<T>(this IEnumerable<T> enumerable, Func<T, string> toStringFunc)
		{
			StringBuilder builder = new StringBuilder(enumerable.GetType().ToString()).Append("{ ");
			foreach (var item in enumerable)
			{
				builder.Append(toStringFunc.Invoke(item)).Append(", ");
			}
			builder.Append(" }");
			return builder.ToString();
		}
		/// <summary>
		/// Return false if one element return true
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumerable"></param>
		/// <param name="predicate"></param>
		/// <returns></returns>
		public static bool None<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
		{
			bool none = true;
			foreach (var item in enumerable)
			{
				if (predicate(item))
				{
					none = false;
					break;
				}
			}
			return none;
		}

		public static T[] MakeArray<T>(params T[] elements)
		{
			return elements;
		}


		public static MGColor ToMGPoint(this Color c)
		{
			return c;
		}
		public static MGColor ToMGPoint(this System.Drawing.Color c)
		{
			return c;
		}


		public static float NextFloat(this Random r)
		{
			return (float)r.NextDouble();
		}
		public static bool NextBool(this Random r, double trueChance = 0.5)
		{
			return r.NextDouble() < trueChance;
		}
		public static Vector2 NextVector2(this Random r)
		{
			return new Vector2(r.NextFloat(), r.NextFloat());
		}
		public static Vector3 NextVector3(this Random r)
		{
			return new Vector3(r.NextFloat(), r.NextFloat(), r.NextFloat());
		}
		public static Vector4 NextVector4(this Random r)
		{
			return new Vector4(r.NextFloat(), r.NextFloat(), r.NextFloat(), r.NextFloat());
		}
		public static Color NextColor(this Random r)
		{
			return new Color(r.NextVector3());
		}
		
		public static void AddIfAbscent<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
		{
			if (!dictionary.ContainsKey(key))
			{
				dictionary.Add(key, value);
			}
		}
		public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
		{
			if (dictionary.TryGetValue(key, out TValue val))
			{
				return val;
			} else
			{
				dictionary.Add(key, defaultValue);
				return defaultValue;
			}
		}
		public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, Func<TValue> generator)
		{
			if (dictionary.TryGetValue(key, out TValue val))
			{
				return val;
			} else
			{
				val = generator();
				dictionary.Add(key, val);
				return val;
			}
		}
		public static void TryAddRange<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, IEnumerable<KeyValuePair<TKey, TValue>> enumerable)
		{
			foreach (var kp in enumerable)
			{
				dictionary.TryAdd(kp.Key, kp.Value);
			}
		}

		public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> enumerable)
		{
			foreach (var v in enumerable)
			{
				collection.Add(v);
			}
		}
		public static void RemoveIf<T>(this ICollection<T> collection, Func<T, bool> selector)
		{
			var array = collection.Where(selector).ToArray();
			foreach (var v in array)
			{
				collection.Remove(v);
			}
		}

		public static TResult CustomCast<T, TResult>(this T v, Func<T, TResult> caster)
		{
			return caster(v);
		}
	}
}
