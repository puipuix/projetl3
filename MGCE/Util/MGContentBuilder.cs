﻿using MGCE.Actor;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Util;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MGCE.Util
{
    public class MGDataReceivedEventArgs : EventArgs
    {
        public string Text { get; }

        public MGDataReceivedEventArgs(string text)
        {
            Text = text;
        }
    }
    public class MGContentBuilder : IMGEventUpdateListener, IMGEventsListenerActor
    {
        public event EventHandler<MGDataReceivedEventArgs> OnOutputDataReceived;
        public event EventHandler OnEnd;
        public List<string> ContentFound { get; }
        public bool IsFinish { get; private set; }

        HashSet<Type> IMGEventsListenerActor.InternalListenedEvents { get; } = new HashSet<Type>();

        private readonly string _srcFile;

        private readonly string _mgcb;
        private readonly string _cd;
        private readonly Stack<Action> _outputData = new Stack<Action>();
        public MGContentBuilder(string srcFile, string destDir, bool addSrcFolderToDest = false)
        {
            int last = srcFile.LastIndexOf('/');
            if (last == -1)
            {
                _cd = "";
                _srcFile = srcFile;
            }
            else
            {
                _srcFile = srcFile.Substring(last + 1);
                _cd = "cd " + srcFile.Substring(0, last) + " && ";
            }
            int folder = srcFile.Split('/').Length - 1;
            _mgcb = "";
            for (int i = 0; i < folder; i++)
            {
                _mgcb += @"../";
            }
            _mgcb += "MGCB.exe ";

            string content;
            using (StreamReader reader = new StreamReader(srcFile.Replace('/', Path.DirectorySeparatorChar)))
            {
                content = reader.ReadToEnd();
            }
            ContentFound = content.Split("#begin ") // split and remove #begin to split by files
                .Skip(1) // remove first string that contains properties
                .Select(s => s.Substring(s.IndexOf('\n'))) // remove things from other lines and keep file name
                .ToList();
        }


        public void AsyncBuildContent(bool block = false)
        {
            IsFinish = false;
            Process cmd = new Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.RedirectStandardError = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            //cmd.StartInfo.Arguments = @"/C cd Content && ..\MGCB.exe /@:Content.mgcb";
            cmd.StartInfo.Arguments = @"/C " + _cd.Replace('/', Path.DirectorySeparatorChar) + _mgcb.Replace('/', Path.DirectorySeparatorChar) + " /rebuild /@:" + _srcFile;

            cmd.OutputDataReceived += (o, args) =>
            {
                lock (_outputData)
                {
                    _outputData?.Push(() => OnOutputDataReceived.SafeInvoke(this, new MGDataReceivedEventArgs(args.Data)));
                }
            };
            if (block)
            {
                cmd.Start();
                cmd.BeginOutputReadLine();
                cmd.WaitForExit();
                cmd.CancelOutputRead();
                cmd.Close();
                IsFinish = true;
                OnEnd.SafeInvoke(this);
            }
            else
            {
                cmd.EnableRaisingEvents = true;
                cmd.Exited += (o, args) =>
                {
                    cmd.CancelOutputRead();
                    cmd.Close();
                    IsFinish = true;
                    lock (_outputData)
                    {
                        _outputData.Push(() =>
                        {
                            (this as IMGEventsListenerActor).StopListeningAll();
                            OnEnd.SafeInvoke(this);
                        });
                    }
                };
                (this as IMGEventsListenerActor).StartListeningEvent<IMGEventUpdateListener>();
                cmd.Start();
                cmd.BeginOutputReadLine();
            }
        }
        public void Uptate(GameTime gameTime)
        {
            lock (_outputData)
            {
                while (!_outputData.IsEmpty())
                {
                    _outputData.Pop().Invoke();
                }
            }
        }
    }

    public class MGMultipleContentBuilder
    {
        public event EventHandler<MGDataReceivedEventArgs> OnAnyOutputDataReceived;
        public event EventHandler OnAllEnd;
        public bool IsFinish { get; private set; }
        public MGContentBuilder[] ContentBuilders { get; }
        public int Running { get; private set; }

        public MGMultipleContentBuilder(params MGContentBuilder[] contentBuilders)
        {
            ContentBuilders = contentBuilders;
        }

        public void AsyncBuildContent(bool block = false)
        {
            IsFinish = false;
            Running = ContentBuilders.Length + 1;
            ContentBuilders.ForEach(c =>
            {
                c.OnEnd += NotifyEnd;
                c.OnOutputDataReceived += OnAnyOutputDataReceived;
                c.AsyncBuildContent(block);
            });
            NotifyEnd(this, EventArgs.Empty);
        }

        private void NotifyEnd(object o, EventArgs args)
        {
            Running--;
            if (Running == 0)
            {
                IsFinish = true;
                OnAllEnd.SafeInvoke(this);
            }
        }
    }
}
