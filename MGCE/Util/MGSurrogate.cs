﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Linq;

namespace MGCE.Util
{
	public class MGContractSurrogate : ISerializationSurrogate
	{
		public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
		{
			if (Attribute.IsDefined(obj.GetType(), typeof(DataContractAttribute)))
			{
				obj.GetType().GetFields().Where(f => Attribute.IsDefined(f, typeof(DataMemberAttribute))).ForEach(f => {
					info.AddValue(f.Name, f.GetValue(obj));
				});
				obj.GetType().GetProperties().Where(p => Attribute.IsDefined(p, typeof(DataMemberAttribute))).ForEach(p => {
					info.AddValue(p.Name, p.GetValue(obj));
				});
			} else
			{
				throw new SerializationException(obj.GetType() + " has no DataContractAttribute defined");
			}
		}

		public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
		{
			if (Attribute.IsDefined(obj.GetType(), typeof(DataContractAttribute)))
			{
				obj.GetType().GetFields().Where(f => Attribute.IsDefined(f, typeof(DataMemberAttribute))).ForEach(f => {
					object val = info.GetValue(f.Name, f.FieldType);
					f.SetValue(obj, val);
				});
				obj.GetType().GetProperties().Where(p => Attribute.IsDefined(p, typeof(DataMemberAttribute))).ForEach(p => {
					p.SetValue(obj, info.GetValue(p.Name, p.PropertyType));
				});
				return obj;
			} else
			{
				throw new SerializationException(obj.GetType() + " has no DataContractAttribute defined");
			}
		}
	}
}
