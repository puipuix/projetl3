﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MGCE.Util.Caster
{
    public struct MGColor
    {
        public Color Color { get; set; }

        public MGColor(Color color)
        {
            Color = color;
        }

        public MGColor(uint packedValue) { Color = new Color(packedValue); }
        public MGColor(Vector4 color) { Color = new Color(color); }
        public MGColor(Vector3 color) { Color = new Color(color); }
        public MGColor(MGColor color, int alpha) { Color = new Color(color.Color, alpha); }
        public MGColor(MGColor color, float alpha) { Color = new Color(color.Color, alpha); }
        public MGColor(float r, float g, float b) { Color = new Color(r,g,b); }
        public MGColor(int r, int g, int b) { Color = new Color(r, g, b); }
        public MGColor(byte r, byte g, byte b, byte alpha) { Color = new Color(r, g, b, alpha); }
        public MGColor(int r, int g, int b, int alpha) { Color = new Color(r, g, b, alpha); }
        public MGColor(float r, float g, float b, float alpha) { Color = new Color(r, g, b, alpha); }

        public static implicit operator Color(MGColor color)
        {
            return color.Color;
        }

        public static implicit operator MGColor(Color color)
        {
            return new MGColor(color);
        }

        public static implicit operator System.Drawing.Color(MGColor color)
        {
            return System.Drawing.Color.FromArgb(color.Color.A, color.Color.R, color.Color.G, color.Color.B);
        }

        public static implicit operator MGColor(System.Drawing.Color color)
        {
            return new MGColor(color.R, color.G, color.B, color.A);
        }

        public override string ToString()
        {
            return Color.ToString();
        }
    }
}
