﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace MGCE.Util.Caster
{
	public struct MGButtonState
	{
		//
		// Résumé :
		//     The button is released.
		public static readonly MGButtonState Released = ButtonState.Released;
		//
		// Résumé :
		//     The button is pressed.
		public static readonly MGButtonState Pressed = ButtonState.Pressed;

		public ButtonState ButtonState { get; set; }
		public MGButtonState(ButtonState buttonState)
		{
			ButtonState = buttonState;
		}

		public static implicit operator ButtonState(MGButtonState buttonState)
		{
			return buttonState.ButtonState;
		}

		public static implicit operator MGButtonState(ButtonState buttonState)
		{
			return new MGButtonState(buttonState);
		}
	}
}
