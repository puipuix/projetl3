﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace MGCE.Util.Caster
{
    public struct MGPoint : IEquatable<Point>, IEquatable<MGPoint>, IEquatable<System.Drawing.Point>
    {
        public static MGPoint Zero { get; } = Point.Zero;

        public Point Point { get; set; }

        public MGPoint(Point point)
        {
            Point = point;
        }

        public MGPoint(int x, int y) : this(new Point(x, y)) { }
        public MGPoint(int value) : this(value, value) { }
        public MGPoint(System.Drawing.Size size) : this(size.Width, size.Height) { }
        public MGPoint(System.Drawing.Point point) : this(point.X, point.Y) { }

        public void Deconstruct(out int x, out int y)
        {
            Point.Deconstruct(out x, out y);
        }

        public (int, int) Deconstruct()
        {
            return (Point.X, Point.Y);
        }

        public override int GetHashCode()
        {
            return Point.GetHashCode();
        }

        public override string ToString()
        {
            return Point.ToString();
        }

        public Vector2 ToVector2()
        {
            return Point.ToVector2();
        }

        public static MGPoint operator +(MGPoint a, MGPoint b) => a.Point + b.Point;
        public static MGPoint operator -(MGPoint a, MGPoint b) => a.Point - b.Point;
        public static MGPoint operator -(MGPoint a) => new MGPoint(-a.Point.X, -a.Point.Y);
        public static MGPoint operator *(MGPoint a, MGPoint b) => a.Point * b.Point;
        public static MGPoint operator /(MGPoint a, MGPoint b) => a.Point / b.Point;
        public static MGPoint operator *(MGPoint a, int b) => a.Point * new Point(b);
        public static MGPoint operator /(MGPoint a, int b) => a.Point / new Point(b);
        public static MGPoint operator +(MGPoint a, int b) => a.Point + new Point(b);
        public static MGPoint operator -(MGPoint a, int b) => a.Point - new Point(b);
        public static bool operator ==(MGPoint a, MGPoint b) => a.Point == b.Point;
        public static bool operator !=(MGPoint a, MGPoint b) => a.Point != b.Point;

        public override bool Equals(object other)
        {
            return Point.Equals(other);
        }
        public bool Equals([AllowNull] Point other)
        {
            return Point.Equals(other);
        }
        public bool Equals([AllowNull] MGPoint other)
        {
            return Point.Equals(other);
        }
        public bool Equals([AllowNull] System.Drawing.Point other)
        {
            return Point.Equals((MGPoint)other);
        }

        public static implicit operator Point(MGPoint point)
        {
            return point.Point;
        }

        public static implicit operator MGPoint(Point point)
        {
            return new MGPoint() { Point = point };
        }

        public static implicit operator System.Drawing.Point(MGPoint point)
        {
            return new System.Drawing.Point(point.Point.X, point.Point.Y);
        }

        public static implicit operator MGPoint(System.Drawing.Point point)
        {
            return new MGPoint() { Point = new Point(point.X, point.Y) };
        }

        public static implicit operator System.Drawing.Size(MGPoint size)
        {
            return new System.Drawing.Size(size.Point.X, size.Point.Y);
        }

        public static implicit operator MGPoint(System.Drawing.Size size)
        {
            return new MGPoint() { Point = new Point(size.Width, size.Height) };
        }
    }
}
