﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Events;

namespace MGCE.Util
{
    public class MGText : IMGEventLanguageChangeListener
    {
        public string TextId { get; set; }

        public string Text { get; private set; }


        public MGText(string textId, bool startUpdating = true)
        {
            TextId = textId;
            OnLanguageChange("");
            if (startUpdating)
            {
                StartUpdating();
            }
        }

        public void StartUpdating()
        {
            MGEvent<IMGEventLanguageChangeListener>.Get().AddListener(this);
        }

        public void StopUpdating()
        {
            MGEvent<IMGEventLanguageChangeListener>.Get().RemoveListener(this);
        }

        public override bool Equals(object obj)
        {
            if (obj is string asstring)
            {
                return asstring == TextId;
            }
            else if (obj is MGText astext)
            {
                return astext.TextId == TextId;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return TextId.GetHashCode();
        }

        public override string ToString()
        {
            return Text;
        }

        public void OnLanguageChange(string language)
        {
            Text = MGLanguage.GetText(TextId, out string translation) ? translation : TextId;
        }

        public static bool operator ==(MGText A, MGText B)
        {
            return A.TextId == B.TextId;
        }

        public static bool operator ==(MGText A, string B)
        {
            return A.TextId == B;
        }

        public static bool operator !=(MGText A, MGText B)
        {
            return !(A == B);
        }

        public static bool operator !=(MGText A, string B)
        {
            return !(A == B);
        }

        public static bool operator ==(string A, MGText B)
        {
            return B == A;
        }

        public static bool operator !=(string A, MGText B)
        {
            return !(B == A);
        }

        /// <summary>
        /// Return Text
        /// </summary>
        /// <param name="d"></param>
        public static implicit operator string(MGText d) => d.Text;

        /// <summary>
        /// Return a new PPGText witn the string as TextId
        /// </summary>
        /// <param name="b"></param>
        public static implicit operator MGText(string b) => new MGText(b);
    }
}
