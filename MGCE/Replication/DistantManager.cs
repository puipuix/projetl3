﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Replication
{
    public class DistantManager : IDisposable
    {
        public int ID { get; }
        public Socket Socket { get; }
        public NetworkStream Stream { get; }

        public BinaryFormatter Formatter { get; set; } = new BinaryFormatter();

        public void Send(VariableUpdateData data)
        {
            Formatter.Serialize(Stream, data);
            Stream.Flush();
        }

        public void LookForUpdate()
        {
            while (Stream.DataAvailable)
            {
                VariableUpdateData data = (VariableUpdateData)Formatter.Deserialize(Stream);
                ReplicationManager.ToUpdate.Add(data);
            }
        }

        public void Dispose()
        {
            Socket.Shutdown(SocketShutdown.Both);
            Stream.Close();
            Socket.Close();
        }
    }
}
