﻿using MGCE.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Replication
{
    public abstract class RVB
    {
        /// <summary>
        /// Id of the replicated variable
        /// </summary>
        public long ID { get; private set; }

        /// <summary>
        /// List of all manager's id who need to be updated
        /// </summary>
        public List<int> ReplicateWith { get; } = new List<int>();

        //  receive value from an other manager
        internal abstract void ReceiveValue(object value);

    }

    public class RV<T> : RVB
    {
        /// <summary>
        /// The variable value
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Set the variable but don't send to other managers
        /// </summary>
        /// <param name="value"></param>
        public void SetValueLocal(T value)
        {
            Value = value;
        }

        /// <summary>
        /// Set the variable and send to other managers
        /// </summary>
        /// <param name="value"></param>
        /// <param name="updateAndRedirect">If the target managers should redirect the value to their local <see cref="RVB.ReplicateWith"/> managers</param>
        public void SetValueReplicated(T value, bool updateAndRedirect = true)
        {
            SetValueLocal(value);
            ReplicationManager.ToSend.Add(new VariableUpdateData(ID, value, updateAndRedirect, ReplicateWith.ToArray()));
        }

        internal override void ReceiveValue(object value)
        {
            SetValueLocal((T)value);
        }

        public static implicit operator T(RV<T> rv)
        {
            return rv.Value;
        }

        /// <summary>
        /// <see cref="RV{T}.SetValueReplicated(T, bool)"/> with bool = true shortcut<br></br>
        /// Set the variable and send to other managers who will redirect the update to their local <see cref="RVB.ReplicateWith"/> managers.
        /// </summary>
        /// <param name="rv">Will be returned with Value = value</param>
        /// <param name="value"></param>
        /// <returns>The rv param</returns>
        public static RV<T> operator *(RV<T> rv, T value)
        {
            rv.SetValueReplicated(value, true);
            return rv;
        }

        /// <summary>
        /// <see cref="RV{T}.SetValueReplicated(T, bool)"/> with bool = false shortcut<br></br>
        /// Set the variable and send to other managers.
        /// </summary>
        /// <param name="rv">Will be returned with Value = value</param>
        /// <param name="value"></param>
        /// <returns>The rv param</returns>
        public static RV<T> operator +(RV<T> rv, T value)
        {
            rv.SetValueReplicated(value, true);
            return rv;
        }


        /// <summary>
        /// <see cref="RV{T}.SetValueLocal(T)"/> shortcut<br></br>
        /// Set the variable but don't send to other managers.
        /// </summary>
        /// <param name="rv">Will be returned with Value = value</param>
        /// <param name="value"></param>
        /// <returns>The rv param</returns>
        public static RV<T> operator -(RV<T> rv, T value)
        {
            rv.SetValueReplicated(value, true);
            return rv;
        }
    }
}
