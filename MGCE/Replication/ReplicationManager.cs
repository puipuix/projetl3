﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Replication
{
    [Serializable]
    public struct VariableUpdateData
    {
        public int ManagerId { get; set; }
        public long RVID { get; set; }
        public object Value { get; set; }
        public bool Reditect { get; set; }
        public int[] Targets { get; set; }

        public VariableUpdateData(int managerId, long rVID, object value, bool redirect, params int[] targets)
        {
            ManagerId = managerId;
            RVID = rVID;
            Value = value;
            Reditect = redirect;
            Targets = targets;
        }

        public VariableUpdateData(long rVID, object value, bool redirect, params int[] targets) : this(ReplicationManager.ID, rVID, value, redirect, targets) { }

    }

    public static class ReplicationManager
    {
        /// <summary>
        /// ID du moteur local
        /// </summary>
        public static int ID { get; private set; }

        /// <summary>
        /// Variables répliqués et IDs
        /// </summary>
        internal static Dictionary<long, RVB> ReplicatedVariables { get; } = new Dictionary<long, RVB>();

        internal static Dictionary<int, DistantManager> Managers { get; } = new Dictionary<int, DistantManager>();

        internal static List<VariableUpdateData> ToSend { get; } = new List<VariableUpdateData>();

        internal static List<VariableUpdateData> ToUpdate { get; } = new List<VariableUpdateData>();

        public static void Update()
        {
            UpdateAll();
            SendAll();
        }

        private static void SendAll()
        {
            foreach (VariableUpdateData data in ToSend)
            {
                foreach (int manager in data.Targets)
                {
                    // Send if the sender is not the target (possible when redirect)
                    if (manager != data.ManagerId && Managers.TryGetValue(manager, out DistantManager distant))
                    {
                        distant.Send(data);
                    }
                }
            }
        }

        private static void UpdateAll()
        {
            foreach (DistantManager manager in Managers.Values)
            {
                manager.LookForUpdate();
            }

            foreach (VariableUpdateData data in ToUpdate)
            {
                if (ReplicatedVariables.TryGetValue(data.RVID, out RVB rvb))
                {
                    // if it's not me who send the update (should always be true)
                    if (data.ManagerId != ID)
                    {
                        rvb.ReceiveValue(data.Value);
                        if (data.Reditect)
                        {
                            ToSend.Add(new VariableUpdateData(data.ManagerId, data.RVID, data.Value, false));
                        }
                    }
                }
            }

            ToUpdate.Clear();
        }
    }
}
