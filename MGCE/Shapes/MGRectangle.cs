﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MGCE.Shapes
{
    public class MGRectangle : MGPolygon
    {
        public MGRectangle(Vector2 center,float width, float height) : base(center, new Vector2[] { Vector2.Zero, new Vector2(width, 0), new Vector2(width, height), new Vector2(0, height) }) { }
        public MGRectangle(Vector2 center, Vector2 size) : this(center, size.X, size.Y) { }
        public MGRectangle(float width, float height) : this(Vector2.Zero, width, height) { }
        public MGRectangle(Vector2 size) : this(Vector2.Zero, size.X, size.Y) { } 

        public override bool IntersectsNoPositioning(Vector2 point)
        {
            // only check box because box == rectangle
            return MinX <= point.X && point.X <= MaxX && MinY <= point.Y && point.Y <= MaxY;
        }

        public override bool Intersects(MGPolygon polygon, Vector2 position, Vector2 otherPosition)
        {
            // only check box because box == rectangle
            return PositionedMaxX(position) >= polygon.PositionedMinX(otherPosition) && PositionedMaxY(position) >= polygon.PositionedMinY(otherPosition) && polygon.PositionedMaxX(otherPosition) >= PositionedMinX(position) && polygon.PositionedMaxY(otherPosition) >= PositionedMinY(position);
        }
    }
}
