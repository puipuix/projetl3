﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE.Util;

namespace MGCE.Shapes
{
    public class MGPolygon
    {
        /// <summary>
        /// Polygon without point (always return false for intersections)
        /// </summary>
        public static readonly MGEmptyPolygon EMPTY = new MGEmptyPolygon();

        /// <summary>
        /// Center of the shape
        /// </summary>
        public virtual Vector2 Center { get; set; }

        /// <summary>
        /// Center of the texture that could be generated with <see cref="MGPolygon.Createtexture2D(GraphicsDevice, Color, Color, int)"/>
        /// </summary>
        public virtual Vector2 CenterOfTexture => Center - new Vector2(MinX, MinY);

        protected Vector2[] _polygonPoints;

        /// <summary>
        /// All points
        /// </summary>
        public virtual Vector2[] PolygonPoints
        {
            get => _polygonPoints;
            set {
                _polygonPoints = value;
                ComputeBoundingBox();
            }
        }

        /// <summary>
        /// Min X coordinate of the shape (local point, not on screen)
        /// </summary>
        public virtual float MinX { get; protected set; }

        /// <summary>
        /// Min Y coordinate of the shape (local point, not on screen)
        /// </summary>
        public virtual float MinY { get; protected set; }

        /// <summary>
        /// Max X coordinate of the shape (local point, not on screen)
        /// </summary>
        public virtual float MaxX { get; protected set; }

        /// <summary>
        /// Max Y coordinate of the shape (local point, not on screen)
        /// </summary>
        public virtual float MaxY { get; protected set; }

        /// <summary>
        /// The size of the bouding box of the shape
        /// </summary>
        public virtual Vector2 BoundingBoxSize => Max - Min;

        /// <summary>
        /// The min X Y
        /// </summary>
        public virtual Vector2 Min => new Vector2(MinX, MinY);

        /// <summary>
        /// The max X Y
        /// </summary>
        public virtual Vector2 Max => new Vector2(MaxX, MaxY);

        public MGPolygon(Vector2 center, params Vector2[] points)
        {
            Center = center;
            PolygonPoints = points;
        }
        public MGPolygon(Vector2[] points) : this(Vector2.Zero, points) { }

        /// <summary>
        /// Find Local Min/Max X/Y values
        /// Compute positioned points
        /// </summary>
        public virtual void ComputeBoundingBox()
        {
            MinX = PolygonPoints[0].X;
            MaxX = PolygonPoints[0].X;
            MinY = PolygonPoints[0].Y;
            MaxY = PolygonPoints[0].Y;
            for (int i = 1; i < PolygonPoints.Length; i++)
            {
                Vector2 q = PolygonPoints[i];
                MinX = Math.Min(q.X, MinX);
                MaxX = Math.Max(q.X, MaxX);
                MinY = Math.Min(q.Y, MinY);
                MaxY = Math.Max(q.Y, MaxY);
            }
        }

        /// <summary>
        /// All points' location on screen
        /// </summary>
        public virtual Vector2[] PositionedPoints(Vector2 position)
        {
            Vector2[] positionedPoints = new Vector2[PolygonPoints.Length];
            for (int i = 0; i < positionedPoints.Length; i++)
            {
                positionedPoints[i] = PolygonPoints[i] + position - Center;
            }
            return positionedPoints;
        }

        /// <summary>
        /// Min X coordinate of the shape on screen
        /// </summary>
        public virtual float PositionedMinX(Vector2 position)
        {
            return MinX + position.X - Center.X;
        }

        /// <summary>
        /// Min Y coordinate of the shape on screen
        /// </summary>
        public virtual float PositionedMinY(Vector2 position)
        {
            return MinY + position.Y - Center.Y;
        }

        /// <summary>
        /// Max X coordinate of the shape on screen
        /// </summary>
        public virtual float PositionedMaxX(Vector2 position)
        {
            return MaxX + position.X - Center.X;
        }

        /// <summary>
        /// Max Y coordinate of the shape on screen
        /// </summary>
        public virtual float PositionedMaxY(Vector2 position)
        {
            return MaxY + position.Y - Center.Y;
        }

        /// <summary>
        /// Min XY coordinate of the shape on screen
        /// </summary>
        public virtual Vector2 PositionedMin(Vector2 position)
        {
            return Min + position - Center;
        }

        /// <summary>
        /// Max XY coordinate of the shape on screen
        /// </summary>
        public virtual Vector2 PositionedMax(Vector2 position)
        {
            return Max + position - Center;
        }

        /// <summary>
        /// Move all points to set the minimum point at local position of (0,0)
        /// </summary>
        public virtual void MoveForMinZeroZero()
        {
            for (int i = 0; i < PolygonPoints.Length; i++)
            {
                PolygonPoints[i].X -= MinX != 0 ? MinX : 0;
                PolygonPoints[i].Y -= MinY != 0 ? MinY : 0;
            }
            Vector2 center = Center;
            center.X -= MinX != 0 ? MinX : 0;
            center.Y -= MinY != 0 ? MinY : 0;
            Center = center;
            ComputeBoundingBox();
        }

        /// <summary>
        /// Copy and apply transform to a polygon.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="polygon"></param>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public virtual MGPolygon Transform(Matrix matrix)
        {
            MGPolygon newOne = new MGPolygon(Center, PolygonPoints.ToArray()); ;

            for (int i = 0; i < newOne._polygonPoints.Length; i++)
            {
                newOne._polygonPoints[i] = Vector2.Transform(newOne._polygonPoints[i] - newOne.Center, matrix) + newOne.Center;
            }
            newOne.ComputeBoundingBox();

            return newOne;
        }

        /// <summary>
        /// Return true if the given point is inside the shape (local coordinate)
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public virtual bool IntersectsNoPositioning(Vector2 point)
        {
            if (point.X < MinX || point.X > MaxX || point.Y < MinY || point.Y > MaxY)
            {
                return false;
            }

            bool inside = false;
            for (int i = 0, j = PolygonPoints.Length - 1; i < PolygonPoints.Length; j = i++)
            {
                if ((PolygonPoints[i].Y > point.Y) != (PolygonPoints[j].Y > point.Y) &&
                     point.X < (PolygonPoints[j].X - PolygonPoints[i].X) * (point.Y - PolygonPoints[i].Y) / (PolygonPoints[j].Y - PolygonPoints[i].Y) + PolygonPoints[i].X)
                {
                    inside = !inside;
                }
            }
            return inside;
        }

        /// <summary>
        /// Return true if the given point is inside the shape (screen coordinate)
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public virtual bool Intersects(Vector2 point, Vector2 position)
        {
            return IntersectsNoPositioning(point - position + Center);
        }

        /// <summary>
        /// Return true if the given polygon intersects the shape (using screen coordinate)
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        public virtual bool Intersects(MGPolygon polygon, Vector2 position, Vector2 otherPosition)
        {
            // out of classic box
            if (PositionedMaxX(position) < polygon.PositionedMinX(otherPosition) || PositionedMaxY(position) < polygon.PositionedMinY(otherPosition) || polygon.PositionedMaxX(otherPosition) < PositionedMinX(position) || polygon.PositionedMaxY(otherPosition) < PositionedMinY(position))
            {
                return false;
            }

            Vector2[] otherPositionedPoints = polygon.PositionedPoints(otherPosition);
            if (otherPositionedPoints.IsEmpty())
            {
                return false;
            }
            else
            {
                bool intersect = false;
                Vector2[] positionedPoints = PositionedPoints(position);

                for (int i = 0, j = positionedPoints.Length - 1; i < positionedPoints.Length && !intersect; j = i++)
                {
                    for (int i2 = 0, j2 = otherPositionedPoints.Length - 1; i2 < otherPositionedPoints.Length && !intersect; j2 = i2++)
                    {
                        intersect = SegmentsIntersect(positionedPoints[i], positionedPoints[j], otherPositionedPoints[i2], otherPositionedPoints[j2], out var _);
                    }
                }

                return intersect || Intersects(otherPositionedPoints[0], position) || polygon.Intersects(positionedPoints[0], otherPosition);
            }
        }

        /// <summary>
        /// Return true if segments [p,pp] and [q,qp] intersect
        /// </summary>
        /// <param name="p">first segment start point</param>
        /// <param name="pp">first segment end point</param>
        /// <param name="q">second segment start point</param>
        /// <param name="qp">second segment end point</param>
        /// <param name="intersection"> set the intersection point or NaN Vector</param>
        /// <returns></returns>
        public static bool SegmentsIntersect(Vector2 p, Vector2 pp, Vector2 q, Vector2 qp, out Vector2 intersection)
        {
            Vector2 r = pp - p;
            Vector2 s = qp - q;

            float rcs = r.Cross(s);
            Vector2 qmp = (q - p);
            if (rcs == 0)
            {
                if (qmp.Cross(r) == 0) // collinear
                {
                    float t0 = qmp.Dot(r) / r.Dot(r);
                    float t1 = t0 + s.Dot(r) / r.Dot(r);
                    float min = Math.Min(t0, t1);
                    float max = Math.Max(t0, t1);

                    if (min <= 1 && max >= 0) // if lines intersect
                    {
                        float tavg = (Math.Min(max, 1) + Math.Max(min, 0)) / 2f; // middle
                        intersection = p + (tavg * r);
                        return true;
                    }
                    else
                    {
                        intersection = new Vector2(float.NaN);
                        return false;
                    }


                }
                else
                {
                    intersection = new Vector2(float.NaN);
                    return false; // parallel
                }

            }
            else
            {
                float t = qmp.Cross(s) / rcs;
                float u = qmp.Cross(r) / rcs;

                if (0 <= t && t <= 1 && 0 <= u && u <= 1)
                {
                    intersection = p + (t * r);
                    return true;
                }
                else
                {
                    intersection = new Vector2(float.NaN);
                    return false;
                }
            }
        }
        
        /// <summary>
        /// Create a texture that represent the shape
        /// </summary>
        /// <param name="device"></param>
        /// <param name="border"></param>
        /// <param name="background"></param>
        /// <param name="borderWidth"></param>
        /// <returns></returns>
        public virtual Texture2D Createtexture2D(GraphicsDevice device, Color border, Color background, int borderWidth)
        {
            // 2D matrix
            int width = (int)Math.Ceiling(MaxX - MinX), height = (int)Math.Ceiling(MaxY - MinY);
            Color[][] colors = new Color[height][];
            for (int i = 0; i < height; i++)
            {
                colors[i] = new Color[width];
            }

            // min value int
            int minX = (int)MinX, minY = (int)MinY;
            // for all line

            for (int i = 0, j = PolygonPoints.Length - 1; i < PolygonPoints.Length; j = i++)
            {
                // get line points
                Point[] line = GetLine(PolygonPoints[i].ToPoint(), PolygonPoints[j].ToPoint());

                // set the border color for each point of the line
                foreach (Point p in line)
                {
                    // direction to take for the borderwidth, aim of the center of the texture
                    int dirX = p.X < MinX + (MaxX - MinX) / 2 ? 1 : -1;
                    int dirY = p.Y < MinY + (MaxY - MinY) / 2 ? 1 : -1;
                    // print a borderWidth rectangle for the line
                    for (int x = 0; x < borderWidth; x++)
                    {
                        for (int y = 0; y < borderWidth; y++)
                        {
                            Point dest = new Point(p.X - minX + x * dirX, p.Y - minY + y * dirY);
                            if (dest.X >= 0 && dest.X < width && dest.Y >= 0 && dest.Y < height)
                            {
                                colors[dest.Y][dest.X] = border;
                            }
                        }
                    }
                }
            }
            // print interior color if not transparent
            if (background != Color.Transparent)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        if (colors[y][i] != border && IntersectsNoPositioning(new Vector2(i + MinX, y + MinY)))
                        {
                            colors[y][i] = background;
                        }
                    }
                }
            }

            // 2D to 1D
            Color[] final = new Color[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int i = 0; i < width; i++)
                {
                    final[y * width + i] = colors[y][i];
                }
            }

            // set colors of the texture
            Texture2D text = new Texture2D(device, width, height);
            text.SetData(final);
            return text;
        }

        /// <summary>
        /// Return all points between 2 points
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static Point[] GetLine(Point p1, Point p2)
        {
            List<Point> points = new List<Point>();

            // no slope (vertical line)
            if (p1.X == p2.X)
            {
                // swap p1 and p2 if p2.Y < p1.Y
                if (p2.Y < p1.Y)
                {
                    Point temp = p1;
                    p1 = p2;
                    p2 = temp;
                }
                for (int y = p1.Y; y <= p2.Y; y++)
                {
                    Point p = new Point(p1.X, y);
                    points.Add(p);
                }
            }
            else
            {
                // swap p1 and p2 if p2.X < p1.X
                if (p2.X < p1.X)
                {
                    Point temp = p1;
                    p1 = p2;
                    p2 = temp;
                }

                // how many Y should increase each X.
                double deltaX = p2.X - p1.X;
                double deltaY = p2.Y - p1.Y;
                double error = -1.0f;
                double deltaErr = Math.Abs(deltaY / deltaX);

                int y = p1.Y;
                // look if y should go up or down.
                int incy = p1.Y < p2.Y ? 1 : -1;
                for (int x = p1.X; x <= p2.X; x++)
                {
                    Point p = new Point(x, y);
                    points.Add(p);
                    // sum how many Y should have increase.
                    error += deltaErr;

                    // if error > 0 then Y must be increase to get back on the line.
                    // stop if y is bigger or lower that p2.Y depending on if y should go up or down.
                    while (error >= 0.0f && (p2.Y - y) * incy > 0)
                    {
                        y += incy;
                        points.Add(new Point(x, y));
                        error -= 1.0f;
                    }
                }
            }

            return points.ToArray();
        }
    }
}
