﻿using MGCE.Engine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Shapes
{
    public struct MGPositionedPolygon : IMGDrawable
    {
        public MGPolygon Polygon { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Center { get => Polygon.Center; set => Polygon.Center = value; }
        public Vector2[] PolygonPoints { get => Polygon.PolygonPoints; set => Polygon.PolygonPoints = value; }
        public MGPositionedPolygon(MGPolygon polygon, Vector2 position)
        {
            Polygon = polygon;
            Position = position;
        }

        public void MoveForMinZeroZero()
        {
            Polygon.MoveForMinZeroZero();
        }

        public bool Intersects(Vector2 point)
        {
            return Polygon.Intersects(point, Position);
        }

        public bool Intersects(MGPolygon polygon, Vector2 position)
        {
            return Polygon.Intersects(polygon, Position, position);
        }

        public bool Intersects(MGPositionedPolygon polygon)
        {
            return Intersects(polygon.Polygon, polygon.Position);
        }

        public static implicit operator MGPositionedPolygon(MGPolygon polygon)
        {
            return new MGPositionedPolygon(polygon, Vector2.Zero);
        }

        public MGPositionedPolygon Transform(Matrix matrix, bool transformPosition = false)
        {
            return new MGPositionedPolygon(Polygon.Transform(matrix), transformPosition ? Vector2.Transform(Position, matrix) : Position);
        }

        public void Draw(SpriteBatch spriteBatch, Color? color = null, int thickness = 1)
        {
            Vector2[] pts = Polygon.PositionedPoints(Position);
            if (pts.Length > 1)
            {
                for (int i = 0, y = pts.Length - 1; i < pts.Length; y = i++)
                {
                    Vector2 from, to;
                    if (pts[y].X < pts[i].X)
                    {
                        from = pts[y];
                        to = pts[i];
                    } else
                    {
                        from = pts[i];
                        to = pts[y];
                    }

                    float r = MathF.Atan((to.Y - from.Y) / (to.X - from.X));

                    spriteBatch.Draw(MGDebug.Dot, new Rectangle((int)from.X, (int)from.Y, (int)Vector2.Distance(from, to), thickness), null, color ?? Color.Red, r, Vector2.Zero, SpriteEffects.None, 1);

                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Draw(spriteBatch);
        }
    }
}
