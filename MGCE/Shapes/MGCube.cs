﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MGCE.Shapes
{
    public class MGCube : MGRectangle
    {
        public MGCube(Vector2 center, float width) : base(center, width, width) { }
        public MGCube(float width) : this(Vector2.Zero, width) { }
    }
}
