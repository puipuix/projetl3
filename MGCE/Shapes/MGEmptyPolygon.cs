﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MGCE.Shapes
{
    public class MGEmptyPolygon : MGPolygon
    {
        public MGEmptyPolygon() : base(new Vector2(float.NaN), new Vector2[0]) { }

        public override bool Intersects(MGPolygon polygon, Vector2 position, Vector2 otherPosition)
        {
            return false;
        }

        public override bool IntersectsNoPositioning(Vector2 point)
        {
            return false;
        }

        public override MGPolygon Transform(Matrix matrix)
        {
            return this;
        }

        public override Texture2D Createtexture2D(GraphicsDevice device, Color border, Color background, int borderWidth)
        {
            return new Texture2D(device, 1, 1);
        }

        public override void ComputeBoundingBox()
        {
            MinX = MaxX = MinY = MaxY = float.NaN;
        }
    }
}
