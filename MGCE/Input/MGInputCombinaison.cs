﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MGCE.Input
{
    /// <summary>
    /// Class who can handle expressions with combinaison of input.
    /// (Like "([M_Right] And ([M_Middle] Or [M_Left]))")
    /// </summary>
    public abstract class MGInputsCombinaison
    {
        /// <summary>
        /// Return if the expression is true.
        /// <br> See <see cref="MGICValue.GetState(int, GameWindow)"/>, <see cref="MGICAnd.GetState(int, GameWindow)"/>, <see cref="MGICOr.GetState(int, GameWindow)"/></br>
        /// </summary>
        /// <param name="playerIndex"></param>
        /// <param name="window"></param>
        /// <returns></returns>
        public abstract bool GetState(int playerIndex = 1, GameWindow window = null);

        /// <summary>
        /// Build a combinaison with string.
        /// <br>Rules: Expression exp can be "[INPUT]", "(exp OR exp OR exp OR...)", "(exp AND exp AND exp AND ...)". It CAN'T be (exp AND exp OR exp...).</br>
        /// <br>Examples:</br>
        /// <br>([A] And ([B] Or [C]))</br>
        /// <br>([A] And [B] And [C])</br>
        /// <br>([A] And [B] And ([C] Or ([D] And [E])))</br>
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static MGInputsCombinaison FromString(string str)
        {
            int trash = 0;
            try
            {
                return FromString(str.Replace(" ", "").ToCharArray(), ref trash);
            }
            catch
            {
                throw new ArgumentException("Incorrect expression : '" + str + "' At or Near:[" + trash + "] input string index");
            }
        }

        protected static MGInputsCombinaison FromString(char[] array, ref int start)
        {
            if (array[start] == '[') // [ = "value"
            {
                return MGICValue.FromString(array, ref start);
            }
            else if (array[start] == '(') // ( = "or" or "and"
            {
                start++;
                // get left operand
                MGInputsCombinaison left = FromString(array, ref start);

                start++;
                // now look wich was the operator
                if (array[start] == 'O' || array[start] == 'o') // Or
                {
                    return MGICOr.FromString(left, array, ref start);
                }
                else if (array[start] == 'A' || array[start] == 'a')
                {
                    return MGICAnd.FromString(left, array, ref start);
                }
                else
                {
                    throw new Exception();
                }
            }
            else
            {
                throw new Exception();
            }
        }

        protected static int CharIndex(char[] array, char c, int start = 0)
        {
            int index = -1;
            for (int i = start; i < array.Length && index == -1; i++)
            {
                index = array[i] == c ? i : -1;
            }
            if (index == -1)
            {
                throw new ArgumentException("Incorrect expression : '" + new string(array) + "'");
            }
            return index;
        }

        /// <summary>
        /// Return the representation of the input.
        /// </summary>
        /// <returns></returns>
        public override abstract string ToString();
    }

    public class MGICValue : MGInputsCombinaison
    {
        public MGInputNames Input { get; }

        public MGICValue(MGInputNames input)
        {
            Input = input;
        }

        protected new static MGInputsCombinaison FromString(char[] input, ref int start)
        {
            int index = CharIndex(input, ']', start + 1);
            MGInputNames ip = (MGInputNames)Enum.Parse(typeof(MGInputNames), new string(input, start + 1, index - 1));
            start = index;
            return new MGICValue(ip);
        }

        public override bool GetState(int playerIndex = 1, GameWindow window = null)
        {
            return MGInputs.GetState(Input, playerIndex, window) >= MGInputs.MinValueToReturnTrue;
        }

        public override string ToString()
        {
            return '[' + Input.ToString() + ']';
        }
    }

    public class MGICAnd : MGInputsCombinaison
    {
        public MGInputsCombinaison[] Inputs { get; }

        public MGICAnd(params MGInputsCombinaison[] inputs)
        {
            Inputs = inputs;
        }

        internal static MGInputsCombinaison FromString(MGInputsCombinaison left, char[] input, ref int start)
        {
            List<MGInputsCombinaison> inputs = new List<MGInputsCombinaison>();
            inputs.Add(left);
            while (input[start] == 'A' || input[start] == 'a') // should be true at start, will stop when reach )
            {
                start += 3;
                MGInputsCombinaison comb = MGInputsCombinaison.FromString(input, ref start);
                inputs.Add(comb);
                start++;
            }
            return new MGICAnd(inputs.ToArray());
        }

        public override bool GetState(int playerIndex = 1, GameWindow window = null)
        {
            bool isTrue = true;
            for (int i = 0; i < Inputs.Length && isTrue; i++)
            {
                isTrue = Inputs[i].GetState(playerIndex, window);
            }
            return isTrue && Inputs.Length > 0;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder("(");
            for (int i = 0; i < Inputs.Length; i++)
            {
                str.Append(Inputs[i].ToString());
                if (i < Inputs.Length - 1)
                {
                    str.Append(" AND ");
                }
            }
            return str.Append(")").ToString();
        }
    }

    public class MGICOr : MGInputsCombinaison
    {
        public MGInputsCombinaison[] Inputs { get; }

        public MGICOr(params MGInputsCombinaison[] inputs)
        {
            Inputs = inputs;
        }

        internal static MGInputsCombinaison FromString(MGInputsCombinaison left, char[] input, ref int start)
        {
            List<MGInputsCombinaison> inputs = new List<MGInputsCombinaison>();
            inputs.Add(left);
            while (input[start] == 'O' || input[start] == 'o') // should be true at start, will stop when reach )
            {
                start += 2;
                MGInputsCombinaison comb = MGInputsCombinaison.FromString(input, ref start);
                inputs.Add(comb);
                start++;
            }
            return new MGICOr(inputs.ToArray());
        }

        public override bool GetState(int playerIndex = 1, GameWindow window = null)
        {
            bool isFalse = true;
            for (int i = 0; i < Inputs.Length && isFalse; i++)
            {
                isFalse = !Inputs[i].GetState(playerIndex, window);
            }
            return !isFalse;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder("(");
            for (int i = 0; i < Inputs.Length; i++)
            {
                str.Append(Inputs[i].ToString());
                if (i < Inputs.Length - 1)
                {
                    str.Append(" OR ");
                }
            }
            return str.Append(")").ToString();
        }
    }
}
