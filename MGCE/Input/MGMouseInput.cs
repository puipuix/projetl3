﻿using Microsoft.Xna.Framework;
using System;
using Microsoft.Xna.Framework.Input;

namespace MGCE.Input
{
    internal class MGMouseInput : MGInput
    {
        public static MGMouseInput I { get; } = new MGMouseInput();

        public override MGInputNames[] Listeners => new MGInputNames[]
        {
            MGInputNames.M_XButton1, MGInputNames.M_XButton2, MGInputNames.M_Right, MGInputNames.M_Left, MGInputNames.M_Middle, MGInputNames.M_Middle, MGInputNames.M_ScrollWheel
        };

        private GameWindow oldWindow = null;
        private MouseState old, oldWithWindow;

        private float Get(MouseState mouse, MGInputNames input)
        {
            switch (input)
            {
                case MGInputNames.M_XButton1:
                    return (int)mouse.XButton1;
                case MGInputNames.M_XButton2:
                    return (int)mouse.XButton2;
                case MGInputNames.M_Right:
                    return (int)mouse.RightButton;
                case MGInputNames.M_Left:
                    return (int)mouse.LeftButton;
                case MGInputNames.M_Middle:
                    return (int)mouse.MiddleButton;
                case MGInputNames.M_HorizontalScrollWheel:
                    return mouse.HorizontalScrollWheelValue;
                case MGInputNames.M_ScrollWheel:
                    return mouse.ScrollWheelValue;
                default:
                    throw new MissingMemberException(input + "isn't a mouse button.");
            }
        }
        public override float GetState(MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            if (window is null)
            {
                return Get(Mouse.GetState(), input);
            }
            else
            {
                return Get(Mouse.GetState(window), input);
            }
        }
        public override float GetOldState(MGInputNames input, int playerIndex = 1)
        {
            if (oldWindow is null)
            {
                return Get(old, input);
            }
            else
            {
                return Get(oldWithWindow, input);
            }
        }

        public void SetOldWindow(GameWindow window)
        {
            oldWindow = window;
            if (oldWindow is GameWindow)
            {
                oldWithWindow = Mouse.GetState(oldWindow);
            }
        }

        public override void SaveState()
        {
            old = Mouse.GetState();
            if (oldWindow is GameWindow)
            {
                oldWithWindow = Mouse.GetState(oldWindow);
            }
        }
    }
}
