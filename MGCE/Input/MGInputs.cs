﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGCE;
using Microsoft.Xna.Framework.Input;

namespace MGCE.Input
{
    public static class MGInputs
    {
        public static float MinValueToReturnTrue { get; set; } = 0.1f;

        internal static Dictionary<MGInputNames, MGInput> Inputs { get; } = new Dictionary<MGInputNames, MGInput>();

        /// <summary>
        /// Return the state of an input. 0 = unpress; 1 = press; ]0;1[ = progressive button (gamepad)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="playerIndex">Gamepad index</param>
        /// <param name="window">Mouse input</param>
        /// <returns></returns>
        public static float GetState(MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            try
            {
                return Inputs[input].GetState(input, playerIndex, window);
            }
            catch (Exception e)
            {
                throw new NotSupportedException("The input: '" + input + "' isn't supported", e);
            }
        }

        /// <summary>
        /// Return the old state (last tick) of an input. 0 = unpress; 1 = press; ]0;1[ = progressive button (gamepad)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="playerIndex">Gamepad index</param>
        /// <param name="window">Mouse input</param>
        /// <returns></returns>
        public static float GetOldState(MGInputNames input, int playerIndex = 1)
        {
            try
            {
                return Inputs[input].GetOldState(input, playerIndex);
            }
            catch (Exception e)
            {
                throw new NotSupportedException("The input: '" + input + "' isn't supported", e);
            }
        }

        public static bool IsPressed(this MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            return GetState(input, playerIndex, window) >= MinValueToReturnTrue;
        }

        public static bool IsPressStart(this MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            return GetState(input, playerIndex, window) >= MinValueToReturnTrue && GetOldState(input, playerIndex) < MinValueToReturnTrue;
        }

        public static bool IsPressEnd(this MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            return GetState(input, playerIndex, window) < MinValueToReturnTrue && GetOldState(input, playerIndex) >= MinValueToReturnTrue;
        }

        internal static void SaveStates()
        {
            MGKeyBoardInput.I.SaveState();
            MGGamePadInput.I.SaveState();
            MGMouseInput.I.SaveState();
            MGSpecialInput.I.SaveState();
        }

        public static T Cast<T>(MGInputNames input) where T : Enum
        {
            return (T)Enum.Parse(typeof(T), input.ToString());
        }

        public static MGInputNames Cast<T>(T input) where T : Enum
        {
            return (MGInputNames)Enum.Parse(typeof(MGInputNames), input.ToString());
        }

        public static Keys CastK(this MGInputNames inputName)
        {
            return Cast<Keys>(inputName);
        }
        public static Buttons CastB(this MGInputNames inputName)
        {
            return Cast<Buttons>(inputName);
        }

        static MGInputs()
        {
            // KEYBOARD
            foreach (MGInputNames input in MGKeyBoardInput.I.Listeners)
            {
                Inputs[input] = MGKeyBoardInput.I;
            }
            // GAMEPAD
            foreach (MGInputNames input in MGGamePadInput.I.Listeners)
            {
                Inputs[input] = MGGamePadInput.I;
            }
            // MOUSE
            foreach (MGInputNames input in MGMouseInput.I.Listeners)
            {
                Inputs[input] = MGMouseInput.I;
            }

            // SPECIAL
            foreach (MGInputNames input in MGSpecialInput.I.Listeners)
            {
                Inputs[input] = MGSpecialInput.I;
            }
        }
    }
}
