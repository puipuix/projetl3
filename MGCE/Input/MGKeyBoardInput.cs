﻿using Microsoft.Xna.Framework;
using System;
using Microsoft.Xna.Framework.Input;

namespace MGCE.Input
{
    internal class MGKeyBoardInput : MGInput
    {

        public static MGKeyBoardInput I { get; } = new MGKeyBoardInput();

        public override MGInputNames[] Listeners { get; }

        private KeyboardState old;

        public MGKeyBoardInput()
        {
            Array names = Enum.GetValues(typeof(Keys));
            Listeners = new MGInputNames[names.Length];
            for (int i = 0; i < names.Length; i++)
            {
                Listeners[i] = Cast((Keys)names.GetValue(i));
            }
        }
        
        public override float GetState(MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            return Keyboard.GetState().IsKeyDown(Cast<Keys>(input)) ? 1 : 0;
        }

        public override float GetOldState(MGInputNames input, int playerIndex = 1)
        {
            return old.IsKeyDown(Cast<Keys>(input)) ? 1 : 0;
        }

        public override void SaveState()
        {
            old = Keyboard.GetState();
        }
    }
}
