﻿using Microsoft.Xna.Framework;
using System;

namespace MGCE.Input
{
    internal abstract class MGInput
    {
        protected MGInput() { }

        public abstract float GetState(MGInputNames input, int playerIndex = 1, GameWindow window = null);

        public abstract float GetOldState(MGInputNames input, int playerIndex = 1);

        public abstract void SaveState();

        public abstract MGInputNames[] Listeners { get; }

        protected virtual T Cast<T>(MGInputNames input) where T : Enum
        {
            return (T)Enum.Parse(typeof(T), input.ToString());
        }

        protected virtual MGInputNames Cast<T>(T input) where T : Enum
        {
            return (MGInputNames)Enum.Parse(typeof(MGInputNames), input.ToString());
        }
    }
}
