﻿using Microsoft.Xna.Framework;
using System;
using Microsoft.Xna.Framework.Input;

namespace MGCE.Input
{
    internal class MGGamePadInput : MGInput
    {
        public static MGGamePadInput I { get; } = new MGGamePadInput();

        public override MGInputNames[] Listeners { get; }

        private GamePadState[] oldStates = new GamePadState[GamePad.MaximumGamePadCount];

        public MGGamePadInput()
        {
            Array names = Enum.GetValues(typeof(Buttons));
            Listeners = new MGInputNames[names.Length];
            for (int i = 0; i < names.Length; i++)
            {
                Listeners[i] = Cast((Buttons)names.GetValue(i));
            }
        }

        private MGInputNames Cast(Buttons input)
        {
            return (MGInputNames)Enum.Parse(typeof(MGInputNames), "GP_" + input.ToString());
        }

        private Buttons Cast(MGInputNames input)
        {
            return (Buttons)Enum.Parse(typeof(Buttons), input.ToString().Replace("GP_", ""));
        }

        private float Get(GamePadState st, MGInputNames input)
        {
            Buttons button = Cast(input);
            switch (button)
            {
                case Buttons.LeftThumbstickDown:
                    return st.ThumbSticks.Left.Y;
                case Buttons.LeftThumbstickUp:
                    return -st.ThumbSticks.Left.Y;
                case Buttons.LeftThumbstickRight:
                    return st.ThumbSticks.Left.X;
                case Buttons.LeftThumbstickLeft:
                    return -st.ThumbSticks.Left.X;
                case Buttons.RightThumbstickDown:
                    return st.ThumbSticks.Right.Y;
                case Buttons.RightThumbstickUp:
                    return -st.ThumbSticks.Right.Y;
                case Buttons.RightThumbstickRight:
                    return st.ThumbSticks.Right.X;
                case Buttons.RightThumbstickLeft:
                    return -st.ThumbSticks.Right.X;
                case Buttons.LeftTrigger:
                    return st.Triggers.Left;
                case Buttons.RightTrigger:
                    return st.Triggers.Right;
                default:
                    return st.IsButtonDown(button) ? 1 : 0;
            }
        }

        public override float GetState(MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            return Get(GamePad.GetState(playerIndex), input);

        }

        public override float GetOldState(MGInputNames input, int playerIndex = 1)
        {
            if (0 < playerIndex && playerIndex <= oldStates.Length)
            {
                return Get(oldStates[playerIndex - 1], input);
            }
            else
            {
                throw new InvalidOperationException("playerIndex: " + playerIndex + "> GamePad.MaximumGamePadCount: " + oldStates.Length);
            }
        }

        public override void SaveState()
        {
            oldStates = new GamePadState[GamePad.MaximumGamePadCount];
            for (int i = 0; i < oldStates.Length; i++)
            {
                oldStates[i] = GamePad.GetState(i + 1);
            }
        }
    }
}
