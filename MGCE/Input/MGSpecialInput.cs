﻿using Microsoft.Xna.Framework;
using System;

namespace MGCE.Input
{
    internal class MGSpecialInput : MGInput
    {
        public static MGSpecialInput I { get; } = new MGSpecialInput();

        public override MGInputNames[] Listeners => new MGInputNames[] { MGInputNames.AlwaysTrue, MGInputNames.AlwaysFalse };

        public override float GetState(MGInputNames input, int playerIndex = 1, GameWindow window = null)
        {
            switch (input)
            {
                case MGInputNames.AlwaysTrue:
                    return 1.0f;
                case MGInputNames.AlwaysFalse:
                    return 0.0f;
                default: throw new MissingMemberException(input + "isn't a special button.");
            }
        }

        public override float GetOldState(MGInputNames input, int playerIndex = 1)
        {
            return GetState(input);
        }

        public override void SaveState() { }
    }
}
