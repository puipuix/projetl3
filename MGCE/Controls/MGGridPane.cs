﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Control
{
    public class MGGridPane
    {
        public Boolean toNotify = false;
        private MGControl[,] C_Grid = new MGControl[0, 0];
        private List<MGControl> liste;
        private int X, Y, Columns = 0, Rows = 0, HSeparator, VSeparator;
        public int MinHigh { get; set; }
        public int MinWide { get; set; }
        public int High { get; set; }
        public int Wide { get; set; }
        public int MaxHigh { get; set; }
        public int MaxWide { get; set; }


        public MGGridPane(int X, int Y, int H = 0, int V = 0)
        {
            this.X = X;
            this.Y = Y;
            HSeparator = H;
            VSeparator = V;
            MinHigh = MinWide = High = Wide = MaxHigh = MaxWide = -1;
            liste = new List<MGControl>();
        }



        public void SetPosition(int i, int j)
        {
            X = i; Y = j;
        }

        public Vector2 GetPosition()
        {
            return new Vector2(X, Y);
        }

        public void Notify()
        {
            Place();
            toNotify = false;
        }

        public void add(MGControl _Texture, int _I, int _J)
        {
            liste.Add(_Texture);
          //  _Texture.Uptate(TimeSpan.Zero);
            toNotify = true;
            if (_I >= Columns || _J >= Rows)
            {
                int _OldColumns = Columns;
                int _OldRows = Rows;
                Columns = _I >= Columns ? _I + 1 : Columns;
                Rows = _J >= Rows ? _J + 1 : Rows;
                MGControl[,] _Tmp = new MGControl[Columns, Rows];

                for (int i = 0; i < _OldColumns; i++)
                {
                    for (int j = 0; j < _OldRows; j++)
                    {
                        _Tmp[i, j] = C_Grid[i, j];
                    }
                }
                C_Grid = _Tmp;
            }

            Remove(_I, _J);
            C_Grid[_I, _J] = _Texture;

        }

        public void Remove(int X, int Y)
        {
            if (C_Grid[X, Y] != null)
            {
                C_Grid[X, Y].Hide();
                C_Grid[X, Y] = null;
            }

        }

        public int Higher(int _I)
        {
            int _Res = 0;

            for (int j = 0; j < Columns; j++)
            {
                MGDebug.PrintLine("I " + _I + " J " + j + " " + C_Grid.Length + " ");
                if (C_Grid[_I, j] != null && C_Grid[_I, j].Size.Y > _Res)
                {
                    _Res = C_Grid[_I, j].Size.Y;
                }

            }
            if (_I > 0 && _Res == 0)
            {
                _Res = Higher(_I - 1);
            }


            if (High >= 0)
            {
                return High + HSeparator;
            }
            else if (MinHigh >= 0 && _Res < MinHigh)
            {
                return MinHigh + HSeparator;
            }
            else if (MaxHigh >= 0 && _Res > MaxHigh)
            {
                return MaxHigh + HSeparator;
            }
            else
            {
                return _Res + HSeparator;
            }
        }

        public MGControl Get(int i,int j)
        {
            return i<Rows && j<Columns && i>=0 && j>=0?  C_Grid[i,j]: null;
        }

        public int Wider(int _J)
        {
            int _Res = 0;

            for (int i = 0; i < Columns; i++)
            {

                if (C_Grid[i, _J] != null && C_Grid[i, _J].Size.X > _Res)
                {
                    _Res = C_Grid[i, _J].Size.X;
                }

            }

            if (_J > 0 && _Res == 0)
            {
                _Res = Wider(_J - 1);
            }


            if (Wide >= 0)
            {
                return Wide + VSeparator;
            }
            else if (MinWide >= 0 && _Res < MinWide)
            {
                return MinWide + VSeparator;
            }
            else if (MaxWide >= 0 && _Res > MaxWide)
            {
                return MaxWide + VSeparator;
            }
            else
            {
                return _Res + VSeparator;
            }
        }

        private void Place()
        {
            int _x = X, _y = Y;
            for (int i = 0; i < Columns; i++)
            {
                if (i > 0)
                    _y += Higher(i - 1);

                for (int j = 0; j < Rows; j++)
                {
                    if (C_Grid[i, j] != null)
                    {
                        if (j > 0)
                            _x += Wider(j - 1);
                        C_Grid[i, j].Position = new Vector2(_x, _y);
                        C_Grid[i, j].Show();
                    }

                }
                _x = X;

            }

        }

        public void Hide()
        {
            foreach(MGControl tohide in liste)
            {
                tohide.Hide();
            }
        }
    }
}
