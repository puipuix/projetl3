﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Util;

namespace MGCE.Control
{
    public class MGImage : MGControl
    {
        public MGSpriteSheet SpriteSheet { get; set; }

        public override Point Size { get => SpriteSheet?.Texture?.Size ?? Point.Zero; protected set { } }

        public override Vector2 Origin {
            get => SpriteSheet.CenterOfTexture;
            set {
                if (SpriteSheet?.CurrentAnimation?.CurrentSpriteFrame is MGSpriteFrame frame)
                {
                    frame.CenterOfTexture = value;
                }
            }
        }

        public MGImage(Game game, MGSpriteSheet spriteSheet) : base(game)
        {
            SpriteSheet = spriteSheet;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (SpriteSheet != null)
            {
                spriteBatch.Draw(SpriteSheet, this, 1);
            }
        }
    }
}
