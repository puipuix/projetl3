﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Engine;
using MGCE.Shapes;
using MGCE.Util;

namespace MGCE.Control
{
    public class MGButton : MGImage
    {
        public MGText Text { get => Label.Text; set => Label.Text = value; }
        public Color TextColor { get => Label.Color; set => Label.Color = value; }
        public override bool AutoOrder { get => base.AutoOrder; set { base.AutoOrder = value; Label.AutoOrder = value; } }
        public override int DrawOrder { get => base.DrawOrder; set { base.DrawOrder = value; Label.DrawOrder = value + 1; } }
        public override SpriteEffects Effects { get => base.Effects; set { base.Effects = value; Label.Effects = value; } }
        
        public MGLabel Label { get; protected set; }

        public MGButton(SpriteFont font, MGSpriteSheet spriteSheet, Game game) : base(game, spriteSheet)
        {
            Label = new MGLabel(font, game);
            SetProtectedChild(this, Label);
            ClickManager.OnMouseEnter += new EventHandler(delegate (object o, EventArgs a)
            {
                MGMouse.SetOnClickable = true;
            });

            ClickManager.OnMouseExit += new EventHandler(delegate (object o, EventArgs a)
            {
                MGMouse.SetOnClickable = false;
            });
        }

        public void SetTexture(MGSpriteSheet spriteSheet)
        {
            SpriteSheet = spriteSheet;
            AutoCenterOrigin();
            UpdateControls();
        }

        public override void AutoCenterOrigin()
        {
            SpriteSheet.CurrentAnimation?.CurrentSpriteFrame?.AutoCenterOfTexture();
            Label.AutoCenterOrigin();
        }
        
        public virtual void DrawMouseIsPress(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Color = Color.DarkGray;
            spriteBatch.Draw(SpriteSheet, this, 1);
        }

        public virtual void DrawMouseIsOn(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Color = Color.LightGray;
            spriteBatch.Draw(SpriteSheet, this, 1);
        }

        public virtual void DrawMouseIsNotOn(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Color = Color.White;
            spriteBatch.Draw(SpriteSheet, this, 1);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ClickManager.IsMouseLeftIsPressed || ClickManager.IsMouseRightIsPressed)
            {
                DrawMouseIsPress(gameTime, spriteBatch);
            }
            else if (ClickManager.IsMouseOn)
            {
                DrawMouseIsOn(gameTime, spriteBatch);
            }
            else
            {
                DrawMouseIsNotOn(gameTime, spriteBatch);
            }
            Label.Draw(gameTime, spriteBatch);
        }
        public virtual void SetText(MGText text)
        {
            Label.SetText(text);
        }

        public virtual void SetFont(SpriteFont font)
        {
            Label.SetFont(font);
        }
    }
}
