﻿using MGCE.Control.EventArguments;
using MGCE.Util;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MGCE.Control
{
    public partial class MGControl
    {
        public class MGControlCollection : IList<MGControl>, ICollection<MGControl>, IReadOnlyList<MGControl>, IReadOnlyCollection<MGControl>, IEnumerable<MGControl>, IEnumerable
        {

            private readonly MGControl owner;

            private List<MGControl> Childrens { get; } = new List<MGControl>();
            public MGControl this[int index]
            {
                get => Childrens[index];
                set {
                    MGControl child = Childrens[index];
                    Childrens[index] = value;

                    if (!(child is null))
                    {
                        child.SetFather(null);
                        MGTools.SafeInvoke(OnChildRemoved, this, new MGControlCollectionChangeEventArgs(owner, child));
                    }
                    
                    Childrens[index]?.RemoveFromFather();
                    Childrens[index]?.SetFather(owner);
                    MGTools.SafeInvoke(OnChildAdded, this, new MGControlCollectionChangeEventArgs(owner, value));
                }
            }
            public MGControl this[string key]
            {
                get {
                    MGControl c = null;
                    for (int i = 0; i < Childrens.Count && c is null; i++)
                    {
                        if (Childrens[i].Id == key)
                        {
                            c = Childrens[i];
                        }
                    }
                    return c;
                }
            }
            public int Count => Childrens.Count;
            public bool IsReadOnly => false;

            public event EventHandler<MGControlCollectionChangeEventArgs> OnChildAdded;
            public event EventHandler<MGControlCollectionChangeEventArgs> OnChildRemoved;

            public EventHandler<MGControlCollectionChangeEventArgs> GetOnChildAdded() { return OnChildAdded; }
            public EventHandler<MGControlCollectionChangeEventArgs> GetOnChildRemoved() { return OnChildRemoved; }

            public MGControlCollection(MGControl owner)
            {
                this.owner = owner;
            }

            public int IndexOf(MGControl item)
            {
                return Childrens.IndexOf(item);
            }
            public void Insert(int index, MGControl item)
            {
                item.RemoveFromFather();
                item.SetFather(owner);
                Childrens.Insert(index, item);
                MGTools.SafeInvoke(OnChildAdded, this, new MGControlCollectionChangeEventArgs(owner, item));
            }
            public void RemoveAt(int index)
            {
                MGControl child = Childrens[index];
                child?.SetFather(null);
                Childrens.RemoveAt(index);
                MGTools.SafeInvoke(OnChildRemoved, this, new MGControlCollectionChangeEventArgs(owner, child));
            }
            public void Add(MGControl item)
            {
                item.RemoveFromFather();
                item.SetFather(owner);
                Childrens.Add(item);
                MGTools.SafeInvoke(OnChildAdded, this, new MGControlCollectionChangeEventArgs(owner, item));
            }
            public void Clear()
            {
                MGControl[] childs = Childrens.ToArray();
                Childrens.Clear();
                foreach (MGControl child in childs)
                {
                    child.SetFather(null);
                    MGTools.SafeInvoke(OnChildRemoved, this, new MGControlCollectionChangeEventArgs(owner, child));
                }
            }
            public bool Contains(MGControl item)
            {
                return Childrens.Contains(item);
            }
            public void CopyTo(MGControl[] array, int arrayIndex)
            {
                Childrens.CopyTo(array, arrayIndex);
            }
            public bool Remove(MGControl item)
            {
                int index = IndexOf(item);
                if (index != -1)
                {
                    RemoveAt(index);
                    return true;
                } else
                {
                    return false;
                }
            }
            public IEnumerator<MGControl> GetEnumerator()
            {
                return Childrens.GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return Childrens.GetEnumerator();
            }
        }
    }

}
