﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Engine;
using MGCE.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Control
{
    public class MGFPSDisplay : MGLabel
    {
        public const int SPACE = 10;
        private GameTime update = new GameTime();
        private ulong lastTickCount = MGEngine.TickCount;
        public MGFPSDisplay(SpriteFont font, Game game) : base(font, game)
        {
            Color = Color.White;
            DrawOrder = MGConst.DRAW_9_MAX_FOREGROUND;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("FPS (U/D): ")
                .Append(MGEngine.GetFps(gameTime).ToString("N")).Append("/")
                .AppendLine(MGEngine.GetFps(update).ToString("N"));
            builder.Append("Tick Count: ").AppendLine(MGEngine.TickCount.ToString());
            builder.Append("Update Duration:").Append(MGEngine.UpdateDuration.Milliseconds).Append("ms/t(~")
                .Append((MGEngine.TickCount - lastTickCount) * (ulong)MGEngine.UpdateDuration.Milliseconds).AppendLine("ms)");
            builder.Append("Draw Duration:").Append(MGEngine.DrawDuration.Milliseconds.ToString()).AppendLine("ms");
            builder.Append("GC:");
            for (int i = 0; i <= GC.MaxGeneration; i++)
            {
                builder.Append(" G").Append(i).Append(": ").Append(GC.CollectionCount(i));
                if (i != GC.MaxGeneration)
                    builder.Append(",");
            }
            Text = builder.ToString();
            base.Draw(gameTime, spriteBatch);
            lastTickCount = MGEngine.TickCount;
        }

        public override void Uptate(GameTime gameTime)
        {
            base.Uptate(gameTime);
            update = gameTime;
        }
    }
}
