﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MGCE.Events;
using MGCE.Input;
using MGCE.Engine;
using MGCE.Shapes;
using MGCE.Util;
using MGCE.Control.EventArguments;

namespace MGCE.Control
{
    public class MGClickableManager
    {
        /// <summary>
        /// Hitbox that can be clicked
        /// </summary>
        public MGPositionedPolygon HitBox { get; set; }

        /// <summary>
        /// Called when mouse is pressed on the hitbox (only first tick)
        /// </summary>
        public event EventHandler<MGButtonStateEventArgs> OnMouseClickStart;

        /// <summary>
        /// Called when mouse is released on the hitbox
        /// </summary>
        public event EventHandler<MGButtonStateEventArgs> OnMouseClickEnd;

        /// <summary>
        /// Called each tick where the mouse is pressed on the hitbox
        /// </summary>
        public event EventHandler<MGButtonStateEventArgs> OnMousePressed;

        /// <summary>
        /// Called when mouse enter on the hitbox
        /// </summary>
        public event EventHandler OnMouseEnter;

        /// <summary>
        /// Called when mouse leave on the hitbox
        /// </summary>
        public event EventHandler OnMouseExit;

        public EventHandler<MGButtonStateEventArgs> GetOnMouseClickStart() { return OnMouseClickStart; }
        public EventHandler<MGButtonStateEventArgs> GetOnMouseClickEnd() { return OnMouseClickEnd; }
        public EventHandler<MGButtonStateEventArgs> GetOnMousePressed() { return OnMousePressed; }
        public EventHandler GetOnMouseEnter() { return OnMouseEnter; }
        public EventHandler GetOnMouseExit() { return OnMouseExit; }

        /// <summary>
        /// ButtonState of the last tick
        /// </summary>
        public ButtonState OldMouseLeftState { get; private set; }

        /// <summary>
        /// ButtonState of the last tick
        /// </summary>
        public ButtonState OldMouseRightState { get; private set; }

        /// <summary>
        /// ButtonState of the last tick
        /// </summary>
        public ButtonState OldMouseMiddleState { get; private set; }

        /// <summary>
        /// If the mouse was on the hitbox the last tick
        /// </summary>
        public bool OldIsMouseOn { get; private set; }

        /// <summary>
        /// If the mouse is on the hitbox
        /// </summary>
        public bool IsMouseOn => HitBox.Intersects(MGMouse.Position.ToVector2());

        /// <summary>
        /// If the mouse is on the hitbox and left click pressed
        /// </summary>
        public bool IsMouseLeftIsPressed => IsMouseOn && MGMouse.GetState().LeftButton == ButtonState.Pressed;

        /// <summary>
        /// If the mouse is on the hitbox and right click pressed
        /// </summary>
        public bool IsMouseRightIsPressed => IsMouseOn && MGMouse.GetState().RightButton == ButtonState.Pressed;

        /// <summary>
        /// If the mouse is on the hitbox and middle click pressed
        /// </summary>
        public bool IsMouseMiddleIsPressed => IsMouseOn && MGMouse.GetState().MiddleButton == ButtonState.Pressed;

        /// <summary>
        /// If the mouse is on the hitbox and left click pressed but not the last tick
        /// </summary>
        public bool IsLeftClickStart => IsMouseLeftIsPressed && OldMouseLeftState == ButtonState.Released;

        /// <summary>
        /// If the mouse is on the hitbox and right click pressed but not the last tick
        /// </summary>
        public bool IsRightClickStart => IsMouseRightIsPressed && OldMouseLeftState == ButtonState.Released;

        /// <summary>
        /// If the mouse is on the hitbox and middle click pressed but not the last tick
        /// </summary>
        public bool IsMiddleClickStart => IsMouseMiddleIsPressed && OldMouseLeftState == ButtonState.Released;

        /// <summary>
        /// If the mouse is on the hitbox and left click unpressed but not the last tick
        /// </summary>
        public bool IsLeftClickEnd => IsMouseOn && !(MGMouse.GetState().LeftButton == ButtonState.Pressed) && OldMouseLeftState == ButtonState.Pressed;

        /// <summary>
        /// If the mouse is on the hitbox and right click unpressed but not the last tick
        /// </summary>
        public bool IsRightClickEnd => IsMouseOn && !(MGMouse.GetState().RightButton == ButtonState.Pressed) && OldMouseRightState == ButtonState.Pressed;

        /// <summary>
        /// If the mouse is on the hitbox and middle click unpressed but not the last tick
        /// </summary>
        public bool IsMiddleClickEnd => IsMouseOn && !(MGMouse.GetState().MiddleButton == ButtonState.Pressed) && OldMouseMiddleState == ButtonState.Pressed;

        public MGClickableManager()
        {
            OldMouseLeftState = ButtonState.Released;
            OldMouseRightState = ButtonState.Released;
            OldMouseMiddleState = ButtonState.Released;
            OldIsMouseOn = false;
            HitBox = MGPolygon.EMPTY;
        }

        /// <summary>
        /// Update the hitbox and event.
        /// </summary>
        public void Update()
        {
            Point position = MGMouse.Position;

            OnMouseEnter.SafeInvokeIf(!OldIsMouseOn && IsMouseOn, this, null);
            OnMouseExit.SafeInvokeIf(OldIsMouseOn && !IsMouseOn, this, null);

            OnMouseClickStart.SafeInvokeIf(IsLeftClickStart, this, new MGButtonStateEventArgs(MGInputNames.M_Left, position));
            OnMouseClickStart.SafeInvokeIf(IsRightClickStart, this, new MGButtonStateEventArgs(MGInputNames.M_Right, position));
            OnMouseClickStart.SafeInvokeIf(IsMiddleClickStart, this, new MGButtonStateEventArgs(MGInputNames.M_Middle, position));

            OnMouseClickEnd.SafeInvokeIf(IsLeftClickEnd, this, new MGButtonStateEventArgs(MGInputNames.M_Left, position));
            OnMouseClickEnd.SafeInvokeIf(IsRightClickEnd, this, new MGButtonStateEventArgs(MGInputNames.M_Right, position));
            OnMouseClickEnd.SafeInvokeIf(IsMiddleClickEnd, this, new MGButtonStateEventArgs(MGInputNames.M_Middle, position));

            OnMousePressed.SafeInvokeIf(IsMouseLeftIsPressed, this, new MGButtonStateEventArgs(MGInputNames.M_Left, position));
            OnMousePressed.SafeInvokeIf(IsMouseRightIsPressed, this, new MGButtonStateEventArgs(MGInputNames.M_Right, position));
            OnMousePressed.SafeInvokeIf(IsMouseMiddleIsPressed, this, new MGButtonStateEventArgs(MGInputNames.M_Middle, position));

            OldIsMouseOn = IsMouseOn;
            OldMouseLeftState = MGMouse.GetState().LeftButton;
            OldMouseRightState = MGMouse.GetState().RightButton;
            OldMouseMiddleState = MGMouse.GetState().MiddleButton;
        }

        /// <summary>
        /// <see cref="MGClickableManager.Update"/> using this new hitbox.
        /// </summary>
        /// <param name="hitBox"></param>
        public void Update(MGPolygon hitBox)
        {
            HitBox = hitBox;
            Update();
        }

        /// <summary>
        /// Add all EventHandler from the given manager.
        /// </summary>
        /// <param name="manager"></param>
        public void CopyEvents(MGClickableManager manager)
        {
            OnMouseClickStart += manager.OnMouseClickStart;
            OnMouseClickEnd += manager.OnMouseClickEnd;
            OnMousePressed += manager.OnMousePressed;
            OnMouseEnter += manager.OnMouseEnter;
            OnMouseExit += manager.OnMouseExit;
        }
    }
}
