﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Actor;
using MGCE.Control.EventArguments;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Shapes;
using MGCE.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Control
{
	public partial class MGControl : MGObject, IMGEventUpdateListener, IMGEventsListenerActor, IMGDrawableActor
	{

		/// <summary>
		/// Facultative id for this control.
		/// </summary>
		public string Id { get; set; }

		/// <summary>
		/// The ClickManager for this control.
		/// </summary>
		public MGClickableManager ClickManager { get; set; }

		#region NODE MANAGMENT

		/// <summary>
		/// The parent control.
		/// </summary>
		public virtual MGControl Parent
		{
			get => _parent;
			set {
				if (value != null)
				{
					value.Childrens.Add(this);
				} else
				{
					SetFather(null);
				}
			}
		}
		protected MGControl _parent;

		/// <summary>
		/// Add a control to the parent's protected childs.
		/// </summary>
		/// <param name="father"></param>
		/// <param name="child"></param>
		protected static void SetProtectedChild(MGControl father, MGControl child)
		{
			father.ProtectedChildrens.Add(child);
		}

		private void RemoveFromFather()
		{
			if (_parent != null)
			{
				_parent.Childrens.Remove(this);
				_parent.ProtectedChildrens.Remove(this);
			}
			_parent = null;
		}

		private void SetFather(MGControl father)
		{
			_parent = father;
			UpdateControls();
		}

		/// <summary>
		/// List of children control.
		/// </summary>
		public MGControlCollection Childrens { get; }

		/// <summary>
		/// Protected list of children, whey can't be remove by the user.
		/// </summary>
		protected MGControlCollection ProtectedChildrens { get; }

		/// <summary>
		/// If the drawing order will be set automatically to be in draw after his parent.
		/// </summary>
		public virtual bool AutoOrder { get; set; } = true;

		/// <summary>
		/// If the position whould be relative to the parent or absolute to the screen.
		/// </summary>
		public virtual MGPositionTypes PositionType { get; set; } = MGPositionTypes.Relative;

		#endregion

		#region USER INPUTS
		/// <summary>
		/// The local position of the control.
		/// </summary>
		public virtual Vector2 Position
		{
			get => _position;
			set {
				if (_position != value)
				{
					_position = value;
					UpdateControls();
				}
			}
		}
		protected Vector2 _position;

		/// <summary>
		/// The target size of the control if exist.
		/// </summary>
		public virtual Vector2? TargetSize
		{
			get => _targetSize;
			set {
				if (_targetSize != value)
				{
					_targetSize = value;
					UpdateControls();
				}
			}
		}
		protected Vector2? _targetSize;

		/// <summary>
		/// The local rotation of the control.
		/// </summary>
		public virtual float Rotation
		{
			get => _rotation;
			set {
				if (value != _rotation)
				{
					_rotation = value;
					UpdateControls();
				}
			}
		}
		protected float _rotation;

		// TODO
		public virtual SpriteEffects Effects { get; set; }

		/// <summary>
		/// The origin coordinate of the control
		/// </summary>
		public virtual Vector2 Origin
		{
			get => _origin;
			set {
				if (_origin != value)
				{
					_origin = value;
					UpdateControls();
				}
			}
		}
		protected Vector2 _origin;

		/// <summary>
		/// Color effect.
		/// </summary>
		public virtual Color Color { get; set; } = Color.White;

		/// <summary>
		/// Opacity, draw method will be called even if opacity == 0
		/// </summary>
		public virtual float Opacity { get; set; } = 1.0f;

		#endregion

		#region COMPUTED DATA

		/// <summary>
		/// Real position on screen.
		/// </summary>
		public virtual Vector2 ScreenPosition { get => PositionType == MGPositionTypes.Absolute || Parent is null ? Position : Parent.ScreenPosition + Vector2.Transform(Position, Matrix.CreateRotationZ(Parent.ScreenRotation)); }

		/// <summary>
		/// Real rotation on screen.
		/// </summary>
		public virtual float ScreenRotation { get => PositionType == MGPositionTypes.Absolute || Parent is null ? Rotation : Parent.ScreenRotation + Rotation; }

		/// <summary>
		/// The max Y local coordinate.
		/// </summary>
		public virtual float Bottom => this.GetDestinationSize().Y - Origin.Y;

		/// <summary>
		/// The min Y local coordinate.
		/// </summary>
		public virtual float Top => Origin.Y;

		/// <summary>
		/// The max X local coordinate.
		/// </summary>
		public virtual float Right => this.GetDestinationSize().X - Origin.X;

		/// <summary>
		/// The min X local coordinate.
		/// </summary>
		public virtual float Left => Origin.X;

		#endregion

		#region Hidden Internal Implementations

		HashSet<Type> IMGEventsListenerActor.InternalListenedEvents { get; } = new HashSet<Type>();
		bool IMGDrawableActor.InternalVisible { get; set; }
		int IMGDrawableActor.InternalDrawOrder { get; set; }

		#endregion


		///////////// NEED OVERRIDE //////////////

		/// <summary>
		/// The size of the control
		/// </summary>
		public virtual Point Size { get; protected set; }

		#region EVENTS

		/// <summary>
		/// Called each tick when visible.
		/// </summary>
		public event EventHandler<MGGameTimeEventArgs> OnUpdate;

		/// <summary>
		/// When the control is being shown
		/// </summary>
		public event EventHandler OnShow;

		/// <summary>
		/// When the control is being hidden
		/// </summary>
		public event EventHandler OnHide;

		/// <summary>
		/// Call OnUpdate event.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="gameTime"></param>
		protected void CallOnUpdate(object sender, GameTime gameTime)
		{
			MGTools.SafeInvoke(OnUpdate, sender, new MGGameTimeEventArgs(gameTime));
		}

		/// <summary>
		/// Call onShow event.
		/// </summary>
		/// <param name="sender"></param>
		protected void CallOnShow(object sender)
		{
			MGTools.SafeInvoke(OnShow, sender, EventArgs.Empty);
		}

		/// <summary>
		/// Call onHide event.
		/// </summary>
		/// <param name="sender"></param>
		protected void CallOnHide(object sender)
		{
			MGTools.SafeInvoke(OnHide, sender, EventArgs.Empty);
		}
		#endregion

		public MGControl(Game game) : base(game)
		{
			ClickManager = new MGClickableManager();
			Childrens = new MGControlCollection(this);
			ProtectedChildrens = new MGControlCollection(this);
		}

		/// <summary>
		/// Update this control and each of his child.
		/// </summary>
		public virtual void UpdateControls()
		{
			Rectangle dim = this.GetDestinationRectangle();
			ClickManager.HitBox = new MGPositionedPolygon(new MGRectangle(this.GetDestinationOrigin(), dim.Size.ToVector2()), dim.Location.ToVector2());
			if (ScreenRotation != 0)
			{
				ClickManager.HitBox = ClickManager.HitBox.Transform(Matrix.CreateRotationZ(ScreenRotation));
			}
			foreach (MGControl control in Childrens)
			{
				control.UpdateControls();
			}
			foreach (MGControl control in ProtectedChildrens)
			{
				control.UpdateControls();
			}
		}

		/// <summary>
		/// Show the control and his child.
		/// </summary>
		public virtual void Show(int? order = null)
		{
			CallOnShow(this);
			if (!Visible)
			{
				IMGDrawableActor.DefaultShow(this, order);
				this.StartListeningEvent<IMGEventUpdateListener>();
				foreach (MGControl child in Childrens)
				{
					child.Show(child.AutoOrder ? DrawOrder + 1 : child.DrawOrder);
				}
				foreach (MGControl child in ProtectedChildrens)
				{
					child.Show(child.AutoOrder ? DrawOrder + 1 : child.DrawOrder);
				}
			}
		}

		/// <summary>
		/// Hide the control and his child.
		/// </summary>
		public virtual void Hide()
		{
			CallOnHide(this);
			if (Visible)
			{
				IMGDrawableActor.DefaultHide(this);
				this.StopListeningEvent<IMGEventUpdateListener>();
				foreach (MGControl child in Childrens)
				{
					child.Hide();
				}
				foreach (MGControl child in ProtectedChildrens)
				{
					child.Hide();
				}
			}
		}

		/// <summary>
		/// Update the ClickManager and call onUpdate.
		/// </summary>
		/// <param name="gameTime"></param>
		public virtual void Uptate(GameTime gameTime)
		{
			ClickManager.Update();
			CallOnUpdate(this, gameTime);
		}

		/// <summary>
		/// Distance to Stick controls.
		/// </summary>
		/// <param name="leftTop"></param>
		/// <param name="rightBottom"></param>
		/// <returns></returns>
		public static Vector2 DistanceToStickBottomRight(MGControl leftTop, MGControl rightBottom)
		{
			return new Vector2(leftTop.Right, leftTop.Bottom) + new Vector2(rightBottom.Left, rightBottom.Top);
		}

		/// <summary>
		/// Draw point on screen, (position, top left and right bottom)
		/// </summary>
		/// <param name="spriteBatch"></param>
		public virtual void DrawDimentions(SpriteBatch spriteBatch)
		{
			uint color = (uint)GetHashCode();
			spriteBatch.Draw(MGDebug.Dot, ScreenPosition + new Vector2(-Left, -Top) - Vector2.One, new Color(color - 1) { A = 255 });
			spriteBatch.Draw(MGDebug.Dot, ScreenPosition - Vector2.One, new Color(color) { A = 255 });
			spriteBatch.Draw(MGDebug.Dot, ScreenPosition + new Vector2(Right, Bottom) - Vector2.One, new Color(color + 1) { A = 255 });
		}

		public virtual void AutoCenterOrigin()
		{
			Origin = Size.ToVector2() / 2;
		}

		public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) { }

		#region Implementation to show Override possibility

		public virtual void StartListeningEvent(Type eventType)
		{
			IMGEventsListenerActor.DefaultStartListeningEvent(eventType, this);
		}
		public virtual void StopListeningAll()
		{
			IMGEventsListenerActor.DefaultStopListeningAll(this);
		}
		public virtual void StopListeningEvent(Type eventType)
		{
			IMGEventsListenerActor.DefaultStopListeningEvent(eventType, this);
		}
		public virtual void StartListeningEvent<T>() where T : IMGEventListener
		{
			IMGEventsListenerActor.DefaultStartListeningEvent<T>(this);
		}
		public virtual void StopListeningEvent<T>() where T : IMGEventListener
		{
			IMGEventsListenerActor.DefaultStopListeningEvent<T>(this);
		}

		public virtual bool Visible { get => IMGDrawableActor.DefaultVisibleGet(this); set => IMGDrawableActor.DefaultVisibleSet(value, this); }
		public virtual int DrawOrder { get => IMGDrawableActor.DefaultDrawOrderGet(this); set => IMGDrawableActor.DefaultDrawOrderSet(value, this); }
		public virtual Vector2 GetDestinationOrigin()
		{
			return IMGDrawableActor.DefaultGetDestinationOrigin(this);
		}
		public virtual Point GetDestinationSize()
		{
			return IMGDrawableActor.DefaultGetDestinationSize(this);
		}
		public virtual Rectangle GetDestinationRectangle()
		{
			return IMGDrawableActor.DefaultGetDestinationRectangle(this);
		}

		#endregion

	}
}
