﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MGCE.Input;

namespace MGCE.Control.EventArguments
{
    public class MGButtonStateEventArgs : EventArgs
    {
        public MGInputNames Button { get; }

        public Point MousePosition { get; }
        public bool Ctrl => Keyboard.GetState().IsKeyDown(Keys.LeftControl) || Keyboard.GetState().IsKeyDown(Keys.RightControl);
        public bool Alt => Keyboard.GetState().IsKeyDown(Keys.LeftAlt);
        public bool Shift => Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift);
        public bool AltGr => Keyboard.GetState().IsKeyDown(Keys.RightAlt);

        public MGButtonStateEventArgs(MGInputNames button, Point mousePosition)
        {
            Button = button;
            MousePosition = mousePosition;
        }
    }
}
