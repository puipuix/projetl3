﻿using Microsoft.Xna.Framework;
using System;

namespace MGCE.Control.EventArguments
{
    public class MGGameTimeEventArgs : EventArgs
    {
        public GameTime GameTime { get; set; }
        public MGGameTimeEventArgs(GameTime gameTime)
        {
            GameTime = gameTime;
        }
    }

}
