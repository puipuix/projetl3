﻿using MGCE.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Control.EventArguments
{
    public class MGControlCollectionChangeEventArgs : EventArgs
    {
        public MGControl Father { get; }
        public MGControl Child { get; }
        public MGControlCollectionChangeEventArgs(MGControl father, MGControl child)
        {
            Father = father;
            Child = child;
        }
    }
}
