﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Control.EventArguments
{
    public class MGSelectedItemEventArgs<C,T>: EventArgs
    {
        public C Control { get; }
        public T Item { get; }
        public MGSelectedItemEventArgs(C control, T item)
        {
            Control = control;
            Item = item;
        }
    }
}
