﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Util;
using MGCE.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Control
{
    public class MGLabel : MGControl, IMGEventLanguageChangedListener
    {
        public SpriteFont Font { get; set; }
        public MGText Text { get; set; } = "";
        public override Point Size { get => Font.MeasureString(Text).ToPoint(); protected set { } }

        public MGLabel(SpriteFont font, Game game) : base(game)
        {
            Font = font;
            AutoCenterOrigin();
        }

        public virtual void SetText(MGText text)
        {
            Text = text;
            AutoCenterOrigin();
        }

        public virtual void SetFont(SpriteFont font)
        {
            Font = font;
            AutoCenterOrigin();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, Text, ScreenPosition, Color, ScreenRotation, Origin, 1.0F, Effects, 1);
        }

        public void StartUpdatingTextAutoCenter()
        {
            MGEvent<IMGEventLanguageChangedListener>.Get().AddListener(this);
        }

        public void StopUpdatingTextAutoCenter()
        {
            MGEvent<IMGEventLanguageChangedListener>.Get().RemoveListener(this);
        }

        /// <summary>
        /// This will not be called
        /// </summary>
        /// <param name="language"></param>
        public void OnLanguageChanged(string language)
        {
            AutoCenterOrigin();
        }
    }
}
