﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MGCE.Control.EventArguments;

namespace MGCE.Control
{
    public abstract class MGBarBase : MGControl
    {
        protected float _margin = 10;
        public float Margin
        {
            get => _margin;
            set { _margin = value; Replace(); }
        }

        public MGBarBase(Game game) : base(game)
        {
            EventHandler<MGControlCollectionChangeEventArgs> ev = new EventHandler<MGControlCollectionChangeEventArgs>(delegate (object o, MGControlCollectionChangeEventArgs a)
            {
                Replace();
            });
            Childrens.OnChildAdded += ev;
            Childrens.OnChildRemoved += ev;
        }
        public abstract void Replace();

    }
    public class MGVBar : MGBarBase
    {
        public MGVBar(Game game) : base(game) { }

        public override void Replace()
        {
            float y = 0;
            for (int i = 1; i < Childrens.Count && Childrens.Count > 1; i++)
            {
                MGControl last = Childrens[i - 1];
                MGControl child = Childrens[i];
                y += DistanceToStickBottomRight(last, child).Y + Margin;
                child.Position = new Vector2(0, y);
            }
        }
    }

    public class MGHBar : MGBarBase
    {
        public MGHBar(Game game) : base(game) { }

        public override void Replace()
        {
            float x = 0;
            for (int i = 1; i < Childrens.Count && Childrens.Count > 1; i++)
            {
                MGControl last = Childrens[i-1];
                MGControl child = Childrens[i];
                x += DistanceToStickBottomRight(last, child).X + Margin;
                child.Position = new Vector2(x, 0);
            }
        }
    }
}
