﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

namespace MGCE.Events
{
    /// <summary>
    /// Base interface for all events. Extend from it to create custom events.
    /// </summary>
    public interface IMGEventListener { }

    /// <summary>
    /// Pre Update event
    /// </summary>
    public interface IMGEventPreUpdateListener : IMGEventListener
    {
        void PreUptate(GameTime gameTime);
    }

    /// <summary>
    /// Update event
    /// </summary>
    public interface IMGEventUpdateListener : IMGEventListener
    {
        void Uptate(GameTime gameTime);
    }

    /// <summary>
    /// Post Update event
    /// </summary>
    public interface IMGEventPostUpdateListener : IMGEventListener
    {
        void PostUptate(GameTime gameTime);
    }

    /// <summary>
    /// Called during the first update
    /// </summary>
    public interface IMGEventGameStartListener : IMGEventListener
    {
        void OnGameStart();
    }

    public interface IMGEventLanguageChangeListener : IMGEventListener
    {
        void OnLanguageChange(string language);
    }
    public interface IMGEventLanguageChangedListener : IMGEventListener
    {
        void OnLanguageChanged(string language);
    }
}
