﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using MGCE.Engine;
using MGCE.Util;
using System.Threading;
using System.Runtime.InteropServices.WindowsRuntime;

namespace MGCE.Events
{
    public abstract class MGEventBase
    {
        protected static readonly Dictionary<Type, MGEventBase> events = new Dictionary<Type, MGEventBase>(); // all loaded event

        protected static Stopwatch _stopwatch = new Stopwatch();

        /// <summary>
        /// The method that will be called when the event is fired
        /// </summary>
        public MethodInfo MethodToCall { get; private set; }

        /// <summary>
        /// Queue of arguments that will be passed the nexts ticks.
        /// </summary>
        public Queue<object[]> ArgsQueue { get; private set; }

        // Will be used if LstArgs is empty. If null, will use the last arguments used.
        public Func<object[]> ArgsProvider { get; set; }
        private object[] lastArgs;

        /// <summary>
        /// Get the next/current arguments or the last one
        /// <br>Add arguments to the queue</br>
        /// </summary>
        public object[] Args
        {
            get {
                if (ArgsQueue.Count > 0)
                {
                    return ArgsQueue.Peek();
                } else if (ArgsProvider != null)
                {
                    return ArgsProvider();
                } else
                {
                    return lastArgs;
                }
            }
            private set {
                object[] _args = value ?? new object[MethodToCall.GetParameters().Length];
                if (_args.Length != MethodToCall.GetParameters().Length)
                {
                    throw new Exception("Invalid array size : " + _args.Length + " != " + MethodToCall.GetParameters().Length + " (expected)");
                };
                ArgsQueue.Enqueue(_args);
            }
        }

        public bool IsSkipable { get; set; }

        public int TimeOutMs { get; set; } = int.MaxValue;

        /// <summary>
        /// Delay before fire in frame.
        /// </summary>
        public long Delay { get; private set; }

        /// <summary>
        /// Tell if the event is always asking (will be call each tick)
        /// </summary>
        public bool IsAlwaysAsk { get; private set; }

        /// <summary>
        /// Set or tell if the event should stop
        /// </summary>
        public bool AskToStop { get; set; }

        /// <summary>
        /// Tell if the event is asked
        /// </summary>
        public bool IsAsked => ArgsQueue.Count > 0;

        /// <summary>
        /// Tell if the event have to be called NOW
        /// </summary>
        public bool HasToBeCalled => (IsAsked || IsAlwaysAsk) && Delay <= 0;

        /// <summary>
        /// Tell if the event need to be called now or later
        /// </summary>
        public bool NeedUpdate => IsAsked || IsAlwaysAsk;

        public int CallOrder
        {
            get => _callOrder;
            set {
                (_drawEvent ? MGEngine.DrawEvents : MGEngine.UpdateEvents).Remove(this, _callOrder);
                _callOrder = value;
                (_drawEvent ? MGEngine.DrawEvents : MGEngine.UpdateEvents).Add(this, _callOrder);
            }
        }
        private int _callOrder = 0;

        public bool DrawEvent
        {
            get => _drawEvent;
            set {
                (_drawEvent ? MGEngine.DrawEvents : MGEngine.UpdateEvents).Remove(this, _callOrder);
                _drawEvent = value;
                (_drawEvent ? MGEngine.DrawEvents : MGEngine.UpdateEvents).Add(this, _callOrder);
            }
        }
        private bool _drawEvent = false;

        protected MGEventBase(Type t, MethodInfo mf)
        {
            MethodToCall = mf;
            ArgsQueue = new Queue<object[]>();
            IsAlwaysAsk = false;
            AskToStop = false;
            Delay = 0;

            events.Add(t, this);
            MGEngine.UpdateEvents.Add(this, _callOrder);
        }

        /// <summary>
        /// Set new delay if it's bigger that currrent one.
        /// </summary>
        /// <param name="frames"></param>
        public void SetDelayFrame(long frames)
        {
            Delay = (Delay > frames) ? Delay : frames;
        }

        /// <summary>
        /// Set new delay if it's bigger that currrent one but here in milliseconds.
        /// </summary>
        /// <param name="frames"></param>
        public void SetDelayMiliSecond(long milis)
        {
            SetDelayFrame(milis / (long)MGEngine.TargetElapsedTime.TotalMilliseconds);
        }

        /// <summary>
        /// Ask the event for the next tick. The event can be called only once a tick
        /// </summary>
        /// <param name="args"></param>
        public void AskEvent(params object[] args)
        {
            Args = args;
        }

        /// <summary>
        /// Make the event asked each tick
        /// </summary>
        /// <param name="args"></param>
        public void MakeEventAlwaysAsked(Func<object[]> argsProvider = null)
        {
            ArgsProvider = argsProvider;
            IsAlwaysAsk = true;
        }

        public void MakeEventAlwaysAsked<P1>(Func<P1> argsProvider = null)
        {
            if (argsProvider is null)
            {
                MakeEventAlwaysAsked(null);
            } else
            {
                MakeEventAlwaysAsked(() => new object[] { argsProvider() });
            }
        }

        public void MakeEventAlwaysAsked<P1, P2>(Func<(P1, P2)> argsProvider = null)
        {
            if (argsProvider is null)
            {
                MakeEventAlwaysAsked(null);
            } else
            {

                MakeEventAlwaysAsked(() => {
                    (P1 a, P2 b) = argsProvider();
                    return new object[] { a, b };
                });
            }
        }

        public void MakeEventAlwaysAsked<P1, P2, P3>(Func<(P1, P2, P3)> argsProvider = null)
        {
            if (argsProvider is null)
            {
                MakeEventAlwaysAsked(null);
            } else
            {

                MakeEventAlwaysAsked(() => {
                    (P1 a, P2 b, P3 c) = argsProvider();
                    return new object[] { a, b, c };
                });
            }
        }

        public void MakeEventAlwaysAsked<P1, P2, P3, P4>(Func<(P1, P2, P3, P4)> argsProvider = null)
        {
            if (argsProvider is null)
            {
                MakeEventAlwaysAsked(null);
            } else
            {

                MakeEventAlwaysAsked(() => {
                    (P1 a, P2 b, P3 c, P4 d) = argsProvider();
                    return new object[] { a, b, c, d };
                });
            }
        }

        /// <summary>
        /// Make the event asked each tick
        /// </summary>
        /// <param name="args"></param>
        public void StopMakingEventAlwaysAsked(bool removeArgsProvider = true)
        {
            if (removeArgsProvider)
            {
                ArgsProvider = null;
            }
            IsAlwaysAsk = false;
        }

        /// <summary>
        /// Set if the event should stop
        /// </summary>
        public void StopEvent()
        {
            AskToStop = true;
        }

        public static MGEventBase Get(Type t)
        {
            if (!events.TryGetValue(t, out MGEventBase ev))
            {
                ev = (MGEventBase)Activator.CreateInstance(typeof(MGEvent<>).MakeGenericType(t));
            }
            return ev;
        }

        internal void Update()
        {
            if (HasToBeCalled)
            {
                CallEvent();

                if (ArgsQueue.Count > 0)
                {
                    lastArgs = ArgsQueue.Dequeue();
                }
            } else if (Delay > 0)
            {
                Delay--;
            }
        }

        internal abstract void CallEvent();
        public abstract void AddObjectAsListener(object obj);
        public abstract void RemoveObjectAsListener(object obj);
    }

    public class MGEvent<T> : MGEventBase where T : IMGEventListener
    {
        /// <summary>
        /// The listeners type
        /// </summary>
        public Type ListenersInterface { get => typeof(T); }

        public Action<T, object[]> Invokator { get; }

        private readonly HashSet<T> _listeners = new HashSet<T>();
        private readonly List<T> _toRemove = new List<T>();
        private readonly List<T> _toAdd = new List<T>();
        private bool _lockListener = false;

        private MGEvent(Action<T, object[]> actionOnCall) : base(typeof(T), typeof(T).GetMethods()[0])
        {
            Invokator = actionOnCall;
        }

        private static Delegate CreateDelegate()
        {
            MethodInfo mf = typeof(T).GetMethods()[0];
            ParameterInfo[] methodParam = mf.GetParameters();
            Type[] delegateTypeParam = new Type[methodParam.Length + 1];
            delegateTypeParam[0] = typeof(T);
            for (int i = 0; i < methodParam.Length; i++)
            {
                delegateTypeParam[i + 1] = methodParam[i].ParameterType;
            }

            return mf.CreateDelegate(MGTools.FindType("Action`" + delegateTypeParam.Length).MakeGenericType(delegateTypeParam));
        }

        /// <summary>
        /// Create a new event without parameter
        /// </summary>
        /// <returns></returns>
        public static MGEvent<T> Create()
        {
            Action<T> action = (Action<T>)CreateDelegate();
            return new MGEvent<T>((o, args) => {
                action.Invoke(o);
            });
        }

        /// <summary>
        /// Create a new event with 1 parameter
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <returns></returns>
        public static MGEvent<T> Create<P1>()
        {
            Action<T, P1> action = (Action<T, P1>)CreateDelegate();
            return new MGEvent<T>((o, args) => {
                action.Invoke(o, (P1)args[0]);
            });
        }

        /// <summary>
        /// Create a new event with 2 parameters
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <typeparam name="P2"></typeparam>
        /// <returns></returns>
        public static MGEvent<T> Create<P1, P2>()
        {
            Action<T, P1, P2> action = (Action<T, P1, P2>)CreateDelegate();
            return new MGEvent<T>((o, args) => {
                action.Invoke(o, (P1)args[0], (P2)args[1]);
            });
        }

        /// <summary>
        /// Create a new event with 3 parameters
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <typeparam name="P2"></typeparam>
        /// <typeparam name="P3"></typeparam>
        /// <returns></returns>
        public static MGEvent<T> Create<P1, P2, P3>()
        {
            Action<T, P1, P2, P3> action = (Action<T, P1, P2, P3>)CreateDelegate();
            return new MGEvent<T>((o, args) => {
                action.Invoke(o, (P1)args[0], (P2)args[1], (P3)args[2]);
            });
        }

        /// <summary>
        /// Create a new event with 4 parameters
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <typeparam name="P2"></typeparam>
        /// <typeparam name="P3"></typeparam>
        /// <typeparam name="P4"></typeparam>
        /// <returns></returns>
        public static MGEvent<T> Create<P1, P2, P3, P4>()
        {
            Action<T, P1, P2, P3, P4> action = (Action<T, P1, P2, P3, P4>)CreateDelegate();
            return new MGEvent<T>((o, args) => {
                action.Invoke(o, (P1)args[0], (P2)args[1], (P3)args[2], (P4)args[3]);
            });
        }

        /// <summary>
        /// Return <see cref="MGEngine.GetEvent()"/>
        /// </summary>
        /// <typeparam name="I"></typeparam>
        /// <returns></returns>
        public static MGEvent<T> Get()
        {
            return (MGEvent<T>)events[typeof(T)];
        }

        /// <summary>
        /// Return a description of the event.
        /// </summary>
        /// <returns></returns>
        public string GetDescription()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Listener Interface : ").Append(ListenersInterface).Append('\n');
            builder.Append("\tMethod : ").Append(MethodToCall.ToString()).Append('\n');
            builder.Append("\t").Append(MethodToCall.GetParameters().Length).Append(" Method's arguments\n");
            builder.Append("\t").Append(ArgsQueue.Count).Append(" Call remaining\n");
            builder.Append("\tHas to be called : ").Append(NeedUpdate).Append('\n');
            builder.Append("\tHas to be active : ").Append(HasToBeCalled).Append('\n');
            builder.Append("\tDelay (frame) : ").Append(Delay).Append('\n');
            builder.Append("\tAsked to stop : ").Append(AskToStop).Append('\n');
            builder.Append("\t").Append(_listeners.Count).Append(" priority level\n");
            return builder.ToString();
        }

        public void AddListener(T listener)
        {
            if (_lockListener)
            {
                _toAdd.Add(listener);
            } else
            {
                _listeners.Add(listener);
            }

        }

        public void RemoveListener(T listener)
        {
            if (_lockListener)
            {
                _toRemove.Add(listener);
            } else
            {
                _listeners.Remove(listener);
            }
        }

        internal override void CallEvent()
        {
            AskToStop = false;
            _lockListener = true;
            _stopwatch.Restart();
            foreach (T listener in _listeners)
            {
                try
                {
                    Invokator(listener, Args);
                } catch (Exception e)
                {
                    MGDebug.PrintError(e);
                }
                if (AskToStop)
                {
                    break;
                } else if (_stopwatch.ElapsedMilliseconds > TimeOutMs)
                {
                    //MGDebug.PrintLine("Warning: Event " + ListenersInterface.Name + " Time out (" + _stopwatch.ElapsedMilliseconds + "ms) ");
                    break;
                }
            }
            _stopwatch.Stop();
            _lockListener = false;
            if (_toRemove.Count > 0)
            {
                foreach (T listener in _toRemove)
                {
                    _listeners.Remove(listener);
                }
                _toRemove.Clear();
            }
            if (_toAdd.Count > 0)
            {
                foreach (T listener in _toAdd)
                {
                    _listeners.Add(listener);
                }
                _toAdd.Clear();
            }
        }

        public override void AddObjectAsListener(object obj)
        {
            AddListener((T)obj);
        }

        public override void RemoveObjectAsListener(object obj)
        {
            RemoveListener((T)obj);
        }
    }
}
