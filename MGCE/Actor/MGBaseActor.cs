﻿using MGCE.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace MGCE.Actor
{
    public abstract class MGBaseActor : MGDrawableActor, IMGEventsListenerActor
    {
        HashSet<Type> IMGEventsListenerActor.InternalListenedEvents => ListenedEvents;
        protected HashSet<Type> ListenedEvents { get; } = new HashSet<Type>();

        #region Use Static Default Implementations

        /// <summary>
        /// Start listening the event and add the event to the actor's listened event. The actor will be automaticaly removed from the event when <see cref="Destroy"/> will be called.
        /// </summary>
        /// <param name="eventType">The event, should be and IPPGEventListener sub-class or sub-interface </param>
        /// <param name="order">The order of calling. Lower first. You can cancel the event for strict higher order </param>
        public void StartListeningEvent(Type eventType)
        {
            IMGEventsListenerActor.DefaultStartListeningEvent(eventType, this);
        }

        /// <summary>
        /// Stop listening all events and remove the events to the actor's listened event.
        /// </summary>
        /// <param name="listener"></param>
        public void StopListeningAll()
        {
            IMGEventsListenerActor.DefaultStopListeningAll(this);
        }

        /// <summary>
        /// Stop listening the event and remove the event to the actor's listened event.
        /// </summary>
        /// <param name="eventType">The event, should be and IPPGEventListener sub-class or sub-interface </param>
        /// <param name="order">The order of calling used when the actor has been added the the listeners.
        /// You can use <c>PPGEvent.Get{I}().Listeners.Remove(T)</c> if you don't remember it. </param>
        public void StopListeningEvent(Type eventType)
        {
            IMGEventsListenerActor.DefaultStopListeningEvent(eventType, this);
        }

        /// <summary>
        /// Start listening the event and add the event to the actor's listened event. The actor will be automaticaly removed from the event when <see cref="Destroy"/> will be called.
        /// </summary>
        /// <typeparam name="T">The event, should be and IPPGEventListener sub-class or sub-interface</typeparam>
        /// <param name="order">The order of calling. Lower first. You can cancel the event for strict higher order</param>
        public void StartListeningEvent<T>() where T : IMGEventListener
        {
            IMGEventsListenerActor.DefaultStartListeningEvent<T>(this);
        }

        /// <summary>
        /// Stop listening the event and remove the event to the actor's listened event.
        /// </summary>
        /// <typeparam name="T">The event, should be and IPPGEventListener sub-class or sub-interface</typeparam>
        /// <param name="order">The order of calling used when the actor has been added the the listeners.
        /// You can use <code>PPGEvent.Get{I}().Listeners.Remove(T)</code> if you don't remember it. </param>
        public void StopListeningEvent<T>() where T : IMGEventListener
        {
            IMGEventsListenerActor.DefaultStopListeningEvent<T>(this);
        }
        #endregion
    }
}
