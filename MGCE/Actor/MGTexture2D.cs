﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MGCE.Actor
{
    public class MGTexture2D
    {
        public Texture2D Texture { get; set; }
        public Rectangle? SourceRectangle { get; set; }
        public Point Size => SourceRectangle is null ? Texture.Bounds.Size : SourceRectangle.Value.Size;
        public Point Location => SourceRectangle is null ? Point.Zero : SourceRectangle.Value.Location;
        public MGTexture2D(Texture2D texture, Rectangle? sourceRectangle = null)
        {
            Texture = texture;
            SourceRectangle = sourceRectangle;
        }

        /// <summary>
        /// <para>Return the center of the displayed texture after resizing.</para> 
        /// <para>If size is null, return center</para>
        /// </summary>
        /// <param name="texture">The texture.</param>
        /// <param name="center">The center of the Texture.</param>
        /// <param name="resizedSize">The size of the displayed texture. See <see cref="Size"/></param>
        /// <returns></returns>
        public Vector2 ResizedCenterOfTexture(Vector2 center, Vector2? resizedSize)
        {
            return resizedSize is null ? center : center * resizedSize.Value / Size.ToVector2();
        }

        public static implicit operator Texture2D(MGTexture2D texture)
        {
            return texture?.Texture;
        }

        public static explicit operator MGTexture2D(Texture2D texture)
        {
            return new MGTexture2D(texture);
        }

        public static implicit operator Rectangle?(MGTexture2D texture)
        {
            return texture?.SourceRectangle;
        }
    }
}
