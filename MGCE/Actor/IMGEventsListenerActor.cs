﻿using MGCE.Engine;
using MGCE.Events;
using MGCE.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Actor
{
    public interface IMGEventsListenerActor : IMGEventListener
    {
        #region Need Implementations

        /// <summary>
        /// All event listened by this actor. (If listened with the method StartListeningEvent)
        /// </summary>
        protected HashSet<Type> InternalListenedEvents { get; }
        #endregion

        #region Static Default Implementations

        public static void DefaultStopListeningEvent(Type eventType, IMGEventsListenerActor actor)
        {
            if (actor.InternalListenedEvents.Contains(eventType))
            {
                MGEventBase.Get(eventType).RemoveObjectAsListener(actor);
                actor.InternalListenedEvents.Remove(eventType);
            }
        }

        public static void DefaultStartListeningEvent(Type eventType, IMGEventsListenerActor actor)
        {
            // Do not add if already listen
            if (!actor.InternalListenedEvents.Contains(eventType))
            {
                MGEventBase.Get(eventType).AddObjectAsListener(actor);
                actor.InternalListenedEvents.Add(eventType);
            }
        }

        public static void DefaultStopListeningAll(IMGEventsListenerActor actor)
        {
            foreach (Type t in actor.InternalListenedEvents.ToArray())
            {
                actor.StopListeningEvent(t);
            }
            actor.InternalListenedEvents.Clear();
        }

        public static void DefaultStartListeningEvent<T>(IMGEventsListenerActor actor) where T : IMGEventListener
        {
            actor.StartListeningEvent(typeof(T));
        }

        public static void DefaultStopListeningEvent<T>(IMGEventsListenerActor actor) where T : IMGEventListener
        {
            actor.StopListeningEvent(typeof(T));
        }

        #endregion

        #region Use Static Default Implementations

        /// <summary>
        /// Start listening the event and add the event to the actor's listened event. The actor will be automaticaly removed from the event when <see cref="Destroy"/> will be called.
        /// </summary>
        /// <param name="eventType">The event, should be and IPPGEventListener sub-class or sub-interface </param>
        /// <param name="order">The order of calling. Lower first. You can cancel the event for strict higher order </param>
        public void StartListeningEvent(Type eventType)
        {
            DefaultStartListeningEvent(eventType, this);
        }

        /// <summary>
        /// Stop listening all events and remove the events to the actor's listened event.
        /// </summary>
        /// <param name="listener"></param>
        public void StopListeningAll()
        {
            DefaultStopListeningAll(this);
        }

        /// <summary>
        /// Stop listening the event and remove the event to the actor's listened event.
        /// </summary>
        /// <param name="eventType">The event, should be and IPPGEventListener sub-class or sub-interface </param>
        /// <param name="order">The order of calling used when the actor has been added the the listeners.
        /// You can use <c>PPGEvent.Get{I}().Listeners.Remove(T)</c> if you don't remember it. </param>
        public void StopListeningEvent(Type eventType)
        {
            DefaultStopListeningEvent(eventType, this);
        }

        /// <summary>
        /// Start listening the event and add the event to the actor's listened event. The actor will be automaticaly removed from the event when <see cref="Destroy"/> will be called.
        /// </summary>
        /// <typeparam name="T">The event, should be and IPPGEventListener sub-class or sub-interface</typeparam>
        /// <param name="order">The order of calling. Lower first. You can cancel the event for strict higher order</param>
        public void StartListeningEvent<T>() where T : IMGEventListener
        {
            DefaultStartListeningEvent<T>(this);
        }

        /// <summary>
        /// Stop listening the event and remove the event to the actor's listened event.
        /// </summary>
        /// <typeparam name="T">The event, should be and IPPGEventListener sub-class or sub-interface</typeparam>
        /// <param name="order">The order of calling used when the actor has been added the the listeners.
        /// You can use <code>PPGEvent.Get{I}().Listeners.Remove(T)</code> if you don't remember it. </param>
        public void StopListeningEvent<T>() where T : IMGEventListener
        {
            DefaultStopListeningEvent<T>(this);
        }
        #endregion
    }

    public abstract class MGEventsListenerActor : IMGEventsListenerActor
    {
        HashSet<Type> IMGEventsListenerActor.InternalListenedEvents => ListenedEvents;
        protected HashSet<Type> ListenedEvents { get; } = new HashSet<Type>();

        #region Use Static Default Implementations

        /// <summary>
        /// Start listening the event and add the event to the actor's listened event. The actor will be automaticaly removed from the event when <see cref="Destroy"/> will be called.
        /// </summary>
        /// <param name="eventType">The event, should be and IPPGEventListener sub-class or sub-interface </param>
        /// <param name="order">The order of calling. Lower first. You can cancel the event for strict higher order </param>
        public virtual void StartListeningEvent(Type eventType)
        {
            IMGEventsListenerActor.DefaultStartListeningEvent(eventType, this);
        }

        /// <summary>
        /// Stop listening all events and remove the events to the actor's listened event.
        /// </summary>
        /// <param name="listener"></param>
        public virtual void StopListeningAll()
        {
            IMGEventsListenerActor.DefaultStopListeningAll(this);
        }

        /// <summary>
        /// Stop listening the event and remove the event to the actor's listened event.
        /// </summary>
        /// <param name="eventType">The event, should be and IPPGEventListener sub-class or sub-interface </param>
        /// <param name="order">The order of calling used when the actor has been added the the listeners.
        /// You can use <c>PPGEvent.Get{I}().Listeners.Remove(T)</c> if you don't remember it. </param>
        public virtual void StopListeningEvent(Type eventType)
        {
            IMGEventsListenerActor.DefaultStopListeningEvent(eventType, this);
        }

        /// <summary>
        /// Start listening the event and add the event to the actor's listened event. The actor will be automaticaly removed from the event when <see cref="Destroy"/> will be called.
        /// </summary>
        /// <typeparam name="T">The event, should be and IPPGEventListener sub-class or sub-interface</typeparam>
        /// <param name="order">The order of calling. Lower first. You can cancel the event for strict higher order</param>
        public virtual void StartListeningEvent<T>() where T : IMGEventListener
        {
            IMGEventsListenerActor.DefaultStartListeningEvent<T>(this);
        }

        /// <summary>
        /// Stop listening the event and remove the event to the actor's listened event.
        /// </summary>
        /// <typeparam name="T">The event, should be and IPPGEventListener sub-class or sub-interface</typeparam>
        /// <param name="order">The order of calling used when the actor has been added the the listeners.
        /// You can use <code>PPGEvent.Get{I}().Listeners.Remove(T)</code> if you don't remember it. </param>
        public virtual void StopListeningEvent<T>() where T : IMGEventListener
        {
            IMGEventsListenerActor.DefaultStopListeningEvent<T>(this);
        }
        #endregion
    }
}
