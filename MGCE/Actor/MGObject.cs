﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Actor
{
    public abstract class MGObject
    {
        /// <summary>
        /// Game's Content Manager
        /// </summary>
        public virtual ContentManager Content { get; protected set; }

        /// <summary>
        /// The associated Game
        /// </summary>
        public virtual Game Game { get; protected set; }

        public MGObject(Game game, ContentManager manager)
        {
            Content = manager;
            Game = game;
        }

        public MGObject(Game game) : this(game, game.Content) { }
    }
}
