﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MGCE.Actor
{
	public interface IMGDrawableActor : IMGDrawable
	{
		#region Need Implementations
		
		/// <summary>
		/// The center of the texture
		/// </summary>
		Vector2 Origin { get; }

		/// <summary>
		/// Position of the Actor on screen.
		/// </summary>
		Vector2 ScreenPosition { get; }

		/// <summary>
		/// The size on screen.
		/// </summary>
		Vector2? TargetSize { get; }

		/// <summary>
		/// The size of the texture.
		/// </summary>
		Point Size { get; }

		/// <summary>
		/// The rotation of the actor.
		/// </summary>
		float Rotation { get; }

		/// <summary>
		/// The effect to apply to the texture of the actor.
		/// </summary>
		SpriteEffects Effects { get; }

		/// <summary>
		/// The drawing color
		/// </summary>
		Color Color { get; }

		/// <summary>
		/// Opacity of the actor, draw method will be called even if opacity == 0
		/// </summary>
		float Opacity { get; }

		/// <summary>
		/// Should only be an indicative value
		/// </summary>
		public bool InternalVisible { get; set; }

		/// <summary>
		/// Should only be an indicative value
		/// </summary>
		protected int InternalDrawOrder { get; set; }

		#endregion

		#region Static Default Implementations

		public static bool DefaultVisibleGet(IMGDrawableActor actor)
		{
			return actor.InternalVisible;
		}
		public static void DefaultVisibleSet(bool value, IMGDrawableActor actor)
		{
			if (value)
			{
				if (!actor.InternalVisible)
				{
					actor.InternalVisible = true;
					MGEngine.ToDraws.Add(actor, actor.DrawOrder);
				}
			} else
			{
				if (actor.InternalVisible)
				{
					actor.InternalVisible = false;
					MGEngine.ToDraws.Remove(actor, actor.DrawOrder);
				}
			}
		}
		public static int DefaultDrawOrderGet(IMGDrawableActor actor)
		{
			return actor.InternalDrawOrder;
		}
		public static void DefaultDrawOrderSet(int value, IMGDrawableActor actor)
		{
			if (actor.Visible)
			{
				actor.Visible = false;
				actor.InternalDrawOrder = value;
				actor.Visible = true;
			} else
			{
				actor.InternalDrawOrder = value;
			}
		}
		public static Vector2 DefaultGetDestinationOrigin(IMGDrawableActor actor)
		{
			return actor.TargetSize is null ? actor.Origin : actor.Origin * actor.TargetSize.Value / actor.Size.ToVector2();
		}
		public static Point DefaultGetDestinationSize(IMGDrawableActor actor)
		{
			return actor.TargetSize?.ToPoint() ?? actor.Size;
		}
		public static Rectangle DefaultGetDestinationRectangle(IMGDrawableActor actor)
		{
			return new Rectangle(actor.ScreenPosition.ToPoint(), actor.GetDestinationSize());
		}
		public static void DefaultShow(IMGDrawableActor actor, int? order = null)
		{
			if (order.HasValue)
			{
				actor.DrawOrder = order.Value;
			}
			actor.Visible = true;
		}
		public static void DefaultHide(IMGDrawableActor actor)
		{
			actor.Visible = false;
		}
		#endregion

		#region Use Static Default Implementations

		/// <summary>
		/// Get or Set the visibility 
		/// </summary>
		public bool Visible { 
			get => DefaultVisibleGet(this); 
			set => DefaultVisibleSet(value, this);
		}
		/// <summary>
		/// Get or Set draw order
		/// </summary>
		public int DrawOrder { get => DefaultDrawOrderGet(this); set => DefaultDrawOrderSet(value, this); }

		/// <summary>
		/// Return the center of the destination resized texture
		/// </summary>
		/// <returns></returns>
		public Vector2 GetDestinationOrigin()
		{
			return DefaultGetDestinationOrigin(this);
		}

		/// <summary>
		/// Get the Screen size of the actor
		/// </summary>
		/// <param name="actor"></param>
		/// <returns></returns>
		public Point GetDestinationSize()
		{
			return DefaultGetDestinationSize(this);
		}

		/// <summary>
		/// The dimention of the texture for the SpriteBatch.Draw() method.
		/// </summary>
		public Rectangle GetDestinationRectangle()
		{
			return DefaultGetDestinationRectangle(this);
		}

		/// <summary>
		/// Start displaying the actor.
		/// </summary>
		/// <param name="order">The <see cref="DrawOrder"/> value to set.</param>
		public void Show(int? order)
		{
			DefaultShow(this, order);
		}

		/// <summary>
		/// Stop displaying the actor.
		/// </summary>
		public void Hide()
		{
			DefaultHide(this);
		}

		[Obsolete("Use 'DrawOrder = value' instead")]
		/// <summary>
		/// Set a new value for DrawOrder and change on screen if visible
		/// </summary>
		/// <param name="actor"></param>
		/// <param name="value"></param>
		public void ChangeDrawOrder(int value)
		{
			DrawOrder = value;
		}

		#endregion
	}

	public abstract class MGDrawableActor : IMGDrawableActor
	{

		public abstract Vector2 Origin { get; protected set; }
		public abstract Vector2 ScreenPosition { get; set; }
		public abstract Vector2? TargetSize { get; set; }
		public abstract Point Size { get; set; }
		public abstract float Rotation { get; set; }
		public abstract SpriteEffects Effects { get; set; }
		public abstract Color Color { get; set; }
		public abstract float Opacity { get; set; }

		bool IMGDrawableActor.InternalVisible { get; set; }
		int IMGDrawableActor.InternalDrawOrder { get; set; }


		public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);

		#region Use Static Default Implementations

		/// <summary>
		/// Get or Set the visibility 
		/// </summary>
		public virtual bool Visible
		{
			get => IMGDrawableActor.DefaultVisibleGet(this);
			set => IMGDrawableActor.DefaultVisibleSet(value, this);
		}
		/// <summary>
		/// Get or Set draw order
		/// </summary>
		public virtual int DrawOrder { 
			get => IMGDrawableActor.DefaultDrawOrderGet(this); 
			set => IMGDrawableActor.DefaultDrawOrderSet(value, this); 
		}

		/// <summary>
		/// Return the center of the destination resized texture
		/// </summary>
		/// <returns></returns>
		public virtual Vector2 GetDestinationOrigin()
		{
			return IMGDrawableActor.DefaultGetDestinationOrigin(this);
		}

		/// <summary>
		/// Get the Screen size of the actor
		/// </summary>
		/// <param name="actor"></param>
		/// <returns></returns>
		public virtual Point GetDestinationSize()
		{
			return IMGDrawableActor.DefaultGetDestinationSize(this);
		}

		/// <summary>
		/// The dimention of the texture for the SpriteBatch.Draw() method.
		/// </summary>
		public virtual Rectangle GetDestinationRectangle()
		{
			return IMGDrawableActor.DefaultGetDestinationRectangle(this);
		}

		/// <summary>
		/// Start displaying the actor.
		/// </summary>
		/// <param name="order">The <see cref="DrawOrder"/> value to set.</param>
		public virtual void Show(int? order = null)
		{
			IMGDrawableActor.DefaultShow(this, order);
		}

		/// <summary>
		/// Stop displaying the actor.
		/// </summary>
		public virtual void Hide()
		{
			IMGDrawableActor.DefaultHide(this);
		}

		#endregion
	}

}
