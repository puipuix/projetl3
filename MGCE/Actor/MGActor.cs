﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Engine;
using MGCE.Events;
using MGCE.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Actor
{
	public class MGActor : MGBaseActor, IMGEventUpdateListener, IMGDisposable
	{

		/// <summary>
		/// If the Destroy method as been called
		/// </summary>
		public virtual bool IsDisposed { get; protected set; }
		public virtual MGSpriteSheet SpriteSheet { get; set; }


		#region Implementations

		public override Vector2 ScreenPosition { get; set; } = new Vector2(0, 0);
				
		public override Vector2? TargetSize { get; set; } = null;

		/// <summary>
		/// Read Only <see cref="IMGDrawableActor.Size"/>
		/// </summary>
		public override Point Size { get => SpriteSheet?.Texture?.Size ?? Point.Zero; set { } }

		/// <summary>
		/// Read Only <see cref="IMGDrawableActor.Origin"/>
		/// </summary>
		public override Vector2 Origin { get => SpriteSheet is null ? Vector2.Zero : SpriteSheet.CenterOfTexture; protected set { } }
		
		public override float Rotation { get; set; } = 0;

		public override SpriteEffects Effects { get; set; } = SpriteEffects.None;

		public override Color Color { get; set; } = Color.White;

		public override float Opacity { get; set; } = 1.0f;

		#endregion

		protected MGActor() : base() { }

		[Obsolete()]
		protected MGActor(Game game) : base() { }

		[Obsolete()]
		protected MGActor(Game game, ContentManager manager) : base() { }

		/// <summary>
		/// Hide and stop listening all listened events.
		/// </summary>
		public virtual void Dispose()
		{
			Hide();
			StopListeningAll();
			SpriteSheet?.Dispose();
			IsDisposed = true;
		}

		public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
		{
			if (MGSpriteSheet.IsReady(SpriteSheet))
			{
				spriteBatch.Draw(SpriteSheet, this, 1);
			}
		}

		/// <summary>
		/// Update your logic here.
		/// </summary>
		/// <param name="gameTime"></param>
		public virtual void Uptate(GameTime gameTime) { }

	}
}
