﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MGCE.Engine;
using MGCE.Util;
using MGCE.Util.TimeLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCE.Actor
{
    public class MGSpriteFrame : MGTimeFrame
    {
        public MGTexture2D Texture { get; set; }
        public Vector2 CenterOfTexture { get; set; }

        public MGSpriteFrame(TimeSpan duration, MGTexture2D texture, Vector2 centerOfTexture) : base(duration, null)
        {
            OnAction += (o, args) => {
                if (args.Timeline is MGSpriteAnimation animation)
                {
                    animation.CurrentSpriteFrame = this;
                    if (animation.PlayingSpriteSheet is MGSpriteSheet spriteSheet)
                    {
                        MGTools.SafeInvoke(spriteSheet.GetOnTextureChange, this, EventArgs.Empty);
                    }
                }
            };
            Texture = texture;
            CenterOfTexture = centerOfTexture;
        }

        public MGSpriteFrame(TimeSpan duration, MGTexture2D texture) : this(duration, texture, Vector2.Zero) { }

        public override void Dispose()
        {
            base.Dispose();
            Texture = null;
        }

        /// <summary>
        /// Calculate the value of <see cref="CenterOfTexture"/> from the the size of <see cref="SourceRectangle"/> or <see cref="Texture"/> if null.
        /// </summary>
        public virtual void AutoCenterOfTexture()
        {
            CenterOfTexture = Texture is null ? Vector2.Zero : Texture.Size.ToVector2() / 2.0f;
        }

        /// <summary>
        /// <para>Return the center of the displayed texture after resizing.</para> 
        /// <para>If <see cref="Size"/> is null, return <see cref="CenterOfTexture"/></para>
        /// </summary>
        /// <returns></returns>
        public virtual Vector2 GetResizedCenterOfTexture(Vector2? size)
        {
            return Texture.ResizedCenterOfTexture(CenterOfTexture, size);
        }
    }

    public class MGSpriteAnimation : MGTimeline
    {
        public const string DEFAULT_ANIMATION = "Default";

        public string Name { get; }
        public MGSpriteFrame CurrentSpriteFrame { get; set; }
        public MGTexture2D Texture { get => CurrentSpriteFrame?.Texture; }
        public Vector2 CenterOfTexture { get => CurrentSpriteFrame is null ? Vector2.Zero : CurrentSpriteFrame.CenterOfTexture; }
        public MGSpriteSheet PlayingSpriteSheet { get; set; }
        public MGSpriteAnimation(string name) : base()
        {
            Name = name;
        }

        public MGSpriteAnimation(string name, MGTimeFrame frames) : base(frames)
        {
            Name = name;
        }

        public MGSpriteAnimation(string name, MGSpriteFrame frames) : this(name, (MGTimeFrame)frames) { }

        public override void Dispose()
        {
            base.Dispose();
            CurrentSpriteFrame = null;
            PlayingSpriteSheet = null;
        }

        public void Add(MGTimeFrame frames)
        {
            Frames += frames;
        }

        public void Start(MGSpriteSheet playingSpriteSheet)
        {
            PlayingSpriteSheet = playingSpriteSheet;
            base.Start();
        }

        public void Resume(MGSpriteSheet playingSpriteSheet)
        {
            PlayingSpriteSheet = playingSpriteSheet;
            base.Resume();
        }

        public override void Stop()
        {
            PlayingSpriteSheet = null;
            base.Stop();
        }

        public static List<MGTexture2D> TileTexture(Texture2D texture, Point tileSize)
        {
            var tiles = new List<MGTexture2D>();
            for (int y = 0; y < texture.Height; y += tileSize.Y)
            {
                for (int x = 0; x < texture.Width; x += tileSize.X)
                {
                    tiles.Add(new MGTexture2D(texture, new Rectangle(new Point(x, y), tileSize)));
                }
            }

            return tiles;
        }

        public static MGSpriteAnimation NewFromTileTexture(string name, Texture2D texture, Point tileSize, double fps, Vector2 centerOfTexture, long cycleCount = INFINITE_CYCLE)
        {
            TimeSpan eachFrameDuration = TimeSpan.FromSeconds(1.0 / fps);
            TimeSpan current = TimeSpan.Zero;

            MGSpriteAnimation ani = new MGSpriteAnimation(name);
            ani.CycleCount = cycleCount;

            var tiles = TileTexture(texture, tileSize);
            for (int i = 0; i < tiles.Count; i++)
            {
                ani.Add(new MGSpriteFrame(current, tiles[i], centerOfTexture));
                current += eachFrameDuration;
            }
            ani.Add(new MGTimeFrame(current, null));
            return ani;
        }

        public static MGSpriteAnimation NewFromTileTexture(string name, Texture2D texture, Point tileSize, double fps, long cycleCount = INFINITE_CYCLE)
        {
            return NewFromTileTexture(name, texture, tileSize, fps, Vector2.Zero, cycleCount);
        }

        public static MGSpriteAnimation NewSingleFrameFromTexture(string name, MGTexture2D texture, Vector2 centerOfTexture = default)
        {
            return new MGSpriteAnimation(name, new MGSpriteFrame(TimeSpan.Zero, texture, centerOfTexture));
        }

        public static MGSpriteAnimation NewSingleFrameFromTexture(string name, Texture2D texture, Vector2 centerOfTexture = default)
        {
            return NewSingleFrameFromTexture(name, (MGTexture2D)texture, centerOfTexture);
        }
    }

    public class MGSpriteSheet : IMGDisposable
    {
        public MGSpriteAnimation CurrentAnimation { get; private set; }
        public Dictionary<string, MGSpriteAnimation> Animations { get; set; } = new Dictionary<string, MGSpriteAnimation>();

        public MGTexture2D Texture { get => CurrentAnimation?.Texture; }
        public Vector2 CenterOfTexture { get => CurrentAnimation is null ? Vector2.Zero : CurrentAnimation.CenterOfTexture; }
        public event EventHandler OnTextureChange;
        public EventHandler GetOnTextureChange => OnTextureChange;

        public bool IsDisposed { get; private set; }

        public MGSpriteSheet(params MGSpriteAnimation[] animations)
        {
            foreach (MGSpriteAnimation animation in animations)
            {
                AddAnimation(animation);
            }
        }

        public void Dispose()
        {
            OnTextureChange = null;
            Animations.ForEach(k => k.Value.Dispose());
            Animations.Clear();
            CurrentAnimation = null;
            IsDisposed = true;
        }

        public void AddAnimation(MGSpriteAnimation animation, bool autoPlay = false, bool cancelOld = true)
        {
            Animations.Add(animation.Name, animation);
            if (autoPlay)
            {
                PlayAnimation(animation.Name, cancelOld);
            }
        }

        public void StopAnimation()
        {
            CurrentAnimation?.Stop();
        }

        public bool SetAnimation(string name, bool cancelOld = true)
        {
            if (name is null)
            {
                return false;
            } else if (name != CurrentAnimation?.Name)
            {
                if (cancelOld)
                {
                    StopAnimation();
                }
                if (Animations.TryGetValue(name, out MGSpriteAnimation animation))
                {
                    CurrentAnimation = animation;
                    return true;
                } else
                {
                    return false;
                }
            } else
            {
                return true;
            }
        }

        public bool PlayAnimation(string name, bool cancelOld = true, bool fromStart = true)
        {
            bool r = SetAnimation(name, cancelOld);
            if (fromStart)
            {
                CurrentAnimation?.Start(this);
            } else
            {
                CurrentAnimation?.Resume(this);
            }
            return r;
        }

        public static MGSpriteSheet NewSingleAnimationFromTileTexture(Texture2D texture, Point tileSize, int fps, Vector2 centerOfTexture = default, string name = MGSpriteAnimation.DEFAULT_ANIMATION)
        {
            return new MGSpriteSheet(MGSpriteAnimation.NewFromTileTexture(name, texture, tileSize, fps, centerOfTexture));
        }

        public static MGSpriteSheet NewSingleAnimationFromTexture(MGTexture2D texture, Vector2 centerOfTexture = default, string name = MGSpriteAnimation.DEFAULT_ANIMATION)
        {
            return new MGSpriteSheet(MGSpriteAnimation.NewSingleFrameFromTexture(name, texture, centerOfTexture));
        }

        public static MGSpriteSheet NewSingleAnimationFromTexture(Texture2D texture, Vector2 centerOfTexture = default, string name = MGSpriteAnimation.DEFAULT_ANIMATION)
        {
            return NewSingleAnimationFromTexture((MGTexture2D)texture, centerOfTexture, name);
        }

        /// <summary>
        /// Tel if the spriteSheet is ready to draw
        /// </summary>
        /// <param name="spriteSheet"></param>
        /// <returns></returns>
        public static bool IsReady(MGSpriteSheet spriteSheet)
        {
            return spriteSheet is null ? false : !(spriteSheet.Texture is null);
        }

        
    }
}
